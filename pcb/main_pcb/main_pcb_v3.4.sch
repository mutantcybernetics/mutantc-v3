<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.7.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="58" name="bCAD" color="11" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="56" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="56" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="57" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="58" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="59" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="60" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="61" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="62" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="62" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="63" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="63" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="no" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="no" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="no" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="117" name="mPads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="mUnrouted" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="mDimension" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="mbStop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="133" name="mtFinish" color="7" fill="1" visible="yes" active="yes"/>
<layer number="134" name="mbFinish" color="7" fill="1" visible="yes" active="yes"/>
<layer number="135" name="mtGlue" color="7" fill="1" visible="yes" active="yes"/>
<layer number="136" name="mbGlue" color="7" fill="1" visible="yes" active="yes"/>
<layer number="137" name="mtTest" color="7" fill="1" visible="yes" active="yes"/>
<layer number="138" name="mbTest" color="7" fill="1" visible="yes" active="yes"/>
<layer number="139" name="mtKeepout" color="7" fill="1" visible="yes" active="yes"/>
<layer number="140" name="mbKeepout" color="7" fill="1" visible="yes" active="yes"/>
<layer number="141" name="mtRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="142" name="mbRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="143" name="mvRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="145" name="mHoles" color="7" fill="1" visible="yes" active="yes"/>
<layer number="146" name="mMilling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="147" name="mMeasures" color="7" fill="1" visible="yes" active="yes"/>
<layer number="148" name="mDocument" color="7" fill="1" visible="yes" active="yes"/>
<layer number="149" name="mReference" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="166" name="AntennaArea" color="7" fill="1" visible="no" active="yes"/>
<layer number="168" name="4mmHeightArea" color="7" fill="1" visible="no" active="yes"/>
<layer number="191" name="mNets" color="7" fill="1" visible="yes" active="yes"/>
<layer number="192" name="mBusses" color="7" fill="1" visible="yes" active="yes"/>
<layer number="193" name="mPins" color="7" fill="1" visible="yes" active="yes"/>
<layer number="194" name="mSymbols" color="7" fill="1" visible="yes" active="yes"/>
<layer number="195" name="mNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="196" name="mValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="pinhead" urn="urn:adsk.eagle:library:325">
<description>&lt;b&gt;Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="2X20" urn="urn:adsk.eagle:footprint:22315/1" library_version="3">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-25.4" y1="-1.905" x2="-24.765" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-24.765" y1="-2.54" x2="-23.495" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-23.495" y1="-2.54" x2="-22.86" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="-1.905" x2="-22.225" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-22.225" y1="-2.54" x2="-20.955" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-20.955" y1="-2.54" x2="-20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="-1.905" x2="-19.685" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-19.685" y1="-2.54" x2="-18.415" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-18.415" y1="-2.54" x2="-17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="-1.905" x2="-17.145" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-17.145" y1="-2.54" x2="-15.875" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-15.875" y1="-2.54" x2="-15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="-1.905" x2="-14.605" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-14.605" y1="-2.54" x2="-13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-2.54" x2="-12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-1.905" x2="-12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="-2.54" x2="-10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-2.54" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-25.4" y1="-1.905" x2="-25.4" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-25.4" y1="1.905" x2="-24.765" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-24.765" y1="2.54" x2="-23.495" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-23.495" y1="2.54" x2="-22.86" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="1.905" x2="-22.225" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-22.225" y1="2.54" x2="-20.955" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-20.955" y1="2.54" x2="-20.32" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="1.905" x2="-19.685" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-19.685" y1="2.54" x2="-18.415" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-18.415" y1="2.54" x2="-17.78" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="1.905" x2="-17.145" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-17.145" y1="2.54" x2="-15.875" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-15.875" y1="2.54" x2="-15.24" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="1.905" x2="-14.605" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-14.605" y1="2.54" x2="-13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="2.54" x2="-12.7" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.065" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="2.54" x2="-10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="2.54" x2="-10.16" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.905" x2="-9.525" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="2.54" x2="-8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="2.54" x2="-7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="2.54" x2="-5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="2.54" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="2.54" x2="7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="2.54" x2="8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="2.54" x2="10.16" y2="1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.905" x2="10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="12.065" y1="2.54" x2="10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="12.065" y1="2.54" x2="12.7" y2="1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="1.905" x2="13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="14.605" y1="2.54" x2="13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="14.605" y1="2.54" x2="15.24" y2="1.905" width="0.1524" layer="21"/>
<wire x1="15.24" y1="1.905" x2="15.875" y2="2.54" width="0.1524" layer="21"/>
<wire x1="17.145" y1="2.54" x2="15.875" y2="2.54" width="0.1524" layer="21"/>
<wire x1="17.145" y1="2.54" x2="17.78" y2="1.905" width="0.1524" layer="21"/>
<wire x1="17.78" y1="1.905" x2="18.415" y2="2.54" width="0.1524" layer="21"/>
<wire x1="19.685" y1="2.54" x2="18.415" y2="2.54" width="0.1524" layer="21"/>
<wire x1="19.685" y1="2.54" x2="20.32" y2="1.905" width="0.1524" layer="21"/>
<wire x1="20.32" y1="1.905" x2="20.955" y2="2.54" width="0.1524" layer="21"/>
<wire x1="22.225" y1="2.54" x2="20.955" y2="2.54" width="0.1524" layer="21"/>
<wire x1="22.225" y1="2.54" x2="22.86" y2="1.905" width="0.1524" layer="21"/>
<wire x1="22.86" y1="-1.905" x2="22.225" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="22.225" y1="-2.54" x2="20.955" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-1.905" x2="20.955" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-1.905" x2="19.685" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="19.685" y1="-2.54" x2="18.415" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="17.78" y1="-1.905" x2="18.415" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="17.78" y1="-1.905" x2="17.145" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="17.145" y1="-2.54" x2="15.875" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-1.905" x2="15.875" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-1.905" x2="14.605" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="13.335" y1="-2.54" x2="14.605" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="13.335" y1="-2.54" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-2.54" x2="12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-2.54" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-2.54" x2="8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-2.54" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.54" x2="-6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.54" x2="-9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="1.905" x2="-22.86" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="1.905" x2="-20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="1.905" x2="-17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="1.905" x2="-15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.905" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.905" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="1.905" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="15.24" y1="1.905" x2="15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="17.78" y1="1.905" x2="17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="20.32" y1="1.905" x2="20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="22.86" y1="1.905" x2="22.86" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="22.86" y1="1.905" x2="23.495" y2="2.54" width="0.1524" layer="21"/>
<wire x1="24.765" y1="2.54" x2="23.495" y2="2.54" width="0.1524" layer="21"/>
<wire x1="24.765" y1="2.54" x2="25.4" y2="1.905" width="0.1524" layer="21"/>
<wire x1="25.4" y1="-1.905" x2="24.765" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="24.765" y1="-2.54" x2="23.495" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="22.86" y1="-1.905" x2="23.495" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="25.4" y1="1.905" x2="25.4" y2="-1.905" width="0.1524" layer="21"/>
<pad name="1" x="-24.13" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-24.13" y="1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="-21.59" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="-21.59" y="1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="-19.05" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="-19.05" y="1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="-16.51" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="-16.51" y="1.27" drill="1.016" shape="octagon"/>
<pad name="9" x="-13.97" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="10" x="-13.97" y="1.27" drill="1.016" shape="octagon"/>
<pad name="11" x="-11.43" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="12" x="-11.43" y="1.27" drill="1.016" shape="octagon"/>
<pad name="13" x="-8.89" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="14" x="-8.89" y="1.27" drill="1.016" shape="octagon"/>
<pad name="15" x="-6.35" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="16" x="-6.35" y="1.27" drill="1.016" shape="octagon"/>
<pad name="17" x="-3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="18" x="-3.81" y="1.27" drill="1.016" shape="octagon"/>
<pad name="19" x="-1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="20" x="-1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="21" x="1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="22" x="1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="23" x="3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="24" x="3.81" y="1.27" drill="1.016" shape="octagon"/>
<pad name="25" x="6.35" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="26" x="6.35" y="1.27" drill="1.016" shape="octagon"/>
<pad name="27" x="8.89" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="28" x="8.89" y="1.27" drill="1.016" shape="octagon"/>
<pad name="29" x="11.43" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="30" x="11.43" y="1.27" drill="1.016" shape="octagon"/>
<pad name="31" x="13.97" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="32" x="13.97" y="1.27" drill="1.016" shape="octagon"/>
<pad name="33" x="16.51" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="34" x="16.51" y="1.27" drill="1.016" shape="octagon"/>
<pad name="35" x="19.05" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="36" x="19.05" y="1.27" drill="1.016" shape="octagon"/>
<pad name="37" x="21.59" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="38" x="21.59" y="1.27" drill="1.016" shape="octagon"/>
<pad name="39" x="24.13" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="40" x="24.13" y="1.27" drill="1.016" shape="octagon"/>
<text x="-25.4" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-25.4" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-24.384" y1="-1.524" x2="-23.876" y2="-1.016" layer="51"/>
<rectangle x1="-24.384" y1="1.016" x2="-23.876" y2="1.524" layer="51"/>
<rectangle x1="-21.844" y1="1.016" x2="-21.336" y2="1.524" layer="51"/>
<rectangle x1="-21.844" y1="-1.524" x2="-21.336" y2="-1.016" layer="51"/>
<rectangle x1="-19.304" y1="1.016" x2="-18.796" y2="1.524" layer="51"/>
<rectangle x1="-19.304" y1="-1.524" x2="-18.796" y2="-1.016" layer="51"/>
<rectangle x1="-16.764" y1="1.016" x2="-16.256" y2="1.524" layer="51"/>
<rectangle x1="-14.224" y1="1.016" x2="-13.716" y2="1.524" layer="51"/>
<rectangle x1="-11.684" y1="1.016" x2="-11.176" y2="1.524" layer="51"/>
<rectangle x1="-16.764" y1="-1.524" x2="-16.256" y2="-1.016" layer="51"/>
<rectangle x1="-14.224" y1="-1.524" x2="-13.716" y2="-1.016" layer="51"/>
<rectangle x1="-11.684" y1="-1.524" x2="-11.176" y2="-1.016" layer="51"/>
<rectangle x1="-9.144" y1="1.016" x2="-8.636" y2="1.524" layer="51"/>
<rectangle x1="-9.144" y1="-1.524" x2="-8.636" y2="-1.016" layer="51"/>
<rectangle x1="-6.604" y1="1.016" x2="-6.096" y2="1.524" layer="51"/>
<rectangle x1="-6.604" y1="-1.524" x2="-6.096" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
<rectangle x1="6.096" y1="1.016" x2="6.604" y2="1.524" layer="51"/>
<rectangle x1="6.096" y1="-1.524" x2="6.604" y2="-1.016" layer="51"/>
<rectangle x1="8.636" y1="1.016" x2="9.144" y2="1.524" layer="51"/>
<rectangle x1="8.636" y1="-1.524" x2="9.144" y2="-1.016" layer="51"/>
<rectangle x1="11.176" y1="1.016" x2="11.684" y2="1.524" layer="51"/>
<rectangle x1="11.176" y1="-1.524" x2="11.684" y2="-1.016" layer="51"/>
<rectangle x1="13.716" y1="1.016" x2="14.224" y2="1.524" layer="51"/>
<rectangle x1="13.716" y1="-1.524" x2="14.224" y2="-1.016" layer="51"/>
<rectangle x1="16.256" y1="1.016" x2="16.764" y2="1.524" layer="51"/>
<rectangle x1="16.256" y1="-1.524" x2="16.764" y2="-1.016" layer="51"/>
<rectangle x1="18.796" y1="1.016" x2="19.304" y2="1.524" layer="51"/>
<rectangle x1="18.796" y1="-1.524" x2="19.304" y2="-1.016" layer="51"/>
<rectangle x1="21.336" y1="1.016" x2="21.844" y2="1.524" layer="51"/>
<rectangle x1="21.336" y1="-1.524" x2="21.844" y2="-1.016" layer="51"/>
<rectangle x1="23.876" y1="1.016" x2="24.384" y2="1.524" layer="51"/>
<rectangle x1="23.876" y1="-1.524" x2="24.384" y2="-1.016" layer="51"/>
</package>
<package name="2X20/90" urn="urn:adsk.eagle:footprint:22316/1" library_version="3">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-25.4" y1="-1.905" x2="-22.86" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="-1.905" x2="-22.86" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="0.635" x2="-25.4" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-25.4" y1="0.635" x2="-25.4" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-24.13" y1="6.985" x2="-24.13" y2="1.27" width="0.762" layer="21"/>
<wire x1="-22.86" y1="-1.905" x2="-20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="-1.905" x2="-20.32" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="0.635" x2="-22.86" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-21.59" y1="6.985" x2="-21.59" y2="1.27" width="0.762" layer="21"/>
<wire x1="-20.32" y1="-1.905" x2="-17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="-1.905" x2="-17.78" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="0.635" x2="-20.32" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-19.05" y1="6.985" x2="-19.05" y2="1.27" width="0.762" layer="21"/>
<wire x1="-17.78" y1="-1.905" x2="-15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="-1.905" x2="-15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="0.635" x2="-17.78" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-16.51" y1="6.985" x2="-16.51" y2="1.27" width="0.762" layer="21"/>
<wire x1="-15.24" y1="-1.905" x2="-12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-1.905" x2="-12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="0.635" x2="-15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-13.97" y1="6.985" x2="-13.97" y2="1.27" width="0.762" layer="21"/>
<wire x1="-12.7" y1="-1.905" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="0.635" x2="-12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="6.985" x2="-11.43" y2="1.27" width="0.762" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="6.985" x2="-8.89" y2="1.27" width="0.762" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="6.985" x2="-6.35" y2="1.27" width="0.762" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="6.985" x2="-3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="6.985" x2="3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="6.985" x2="6.35" y2="1.27" width="0.762" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="0.635" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="8.89" y1="6.985" x2="8.89" y2="1.27" width="0.762" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="12.7" y1="0.635" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="11.43" y1="6.985" x2="11.43" y2="1.27" width="0.762" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-1.905" x2="15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="15.24" y1="0.635" x2="12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="13.97" y1="6.985" x2="13.97" y2="1.27" width="0.762" layer="21"/>
<wire x1="15.24" y1="-1.905" x2="17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="17.78" y1="-1.905" x2="17.78" y2="0.635" width="0.1524" layer="21"/>
<wire x1="17.78" y1="0.635" x2="15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="16.51" y1="6.985" x2="16.51" y2="1.27" width="0.762" layer="21"/>
<wire x1="17.78" y1="-1.905" x2="20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-1.905" x2="20.32" y2="0.635" width="0.1524" layer="21"/>
<wire x1="20.32" y1="0.635" x2="17.78" y2="0.635" width="0.1524" layer="21"/>
<wire x1="19.05" y1="6.985" x2="19.05" y2="1.27" width="0.762" layer="21"/>
<wire x1="20.32" y1="-1.905" x2="22.86" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="22.86" y1="-1.905" x2="22.86" y2="0.635" width="0.1524" layer="21"/>
<wire x1="22.86" y1="0.635" x2="20.32" y2="0.635" width="0.1524" layer="21"/>
<wire x1="21.59" y1="6.985" x2="21.59" y2="1.27" width="0.762" layer="21"/>
<wire x1="22.86" y1="-1.905" x2="25.4" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="25.4" y1="-1.905" x2="25.4" y2="0.635" width="0.1524" layer="21"/>
<wire x1="25.4" y1="0.635" x2="22.86" y2="0.635" width="0.1524" layer="21"/>
<wire x1="24.13" y1="6.985" x2="24.13" y2="1.27" width="0.762" layer="21"/>
<pad name="2" x="-24.13" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="4" x="-21.59" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="6" x="-19.05" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="8" x="-16.51" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="10" x="-13.97" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="12" x="-11.43" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="14" x="-8.89" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="16" x="-6.35" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="18" x="-3.81" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="20" x="-1.27" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="22" x="1.27" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="24" x="3.81" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="26" x="6.35" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="28" x="8.89" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="30" x="11.43" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="32" x="13.97" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="34" x="16.51" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="36" x="19.05" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="38" x="21.59" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="1" x="-24.13" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="3" x="-21.59" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="5" x="-19.05" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="7" x="-16.51" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="9" x="-13.97" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="11" x="-11.43" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="13" x="-8.89" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="15" x="-6.35" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="17" x="-3.81" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="19" x="-1.27" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="21" x="1.27" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="23" x="3.81" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="25" x="6.35" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="27" x="8.89" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="29" x="11.43" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="31" x="13.97" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="33" x="16.51" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="35" x="19.05" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="37" x="21.59" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="40" x="24.13" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="39" x="24.13" y="-6.35" drill="1.016" shape="octagon"/>
<text x="-26.035" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="27.305" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-24.511" y1="0.635" x2="-23.749" y2="1.143" layer="21"/>
<rectangle x1="-21.971" y1="0.635" x2="-21.209" y2="1.143" layer="21"/>
<rectangle x1="-19.431" y1="0.635" x2="-18.669" y2="1.143" layer="21"/>
<rectangle x1="-16.891" y1="0.635" x2="-16.129" y2="1.143" layer="21"/>
<rectangle x1="-14.351" y1="0.635" x2="-13.589" y2="1.143" layer="21"/>
<rectangle x1="-11.811" y1="0.635" x2="-11.049" y2="1.143" layer="21"/>
<rectangle x1="-9.271" y1="0.635" x2="-8.509" y2="1.143" layer="21"/>
<rectangle x1="-6.731" y1="0.635" x2="-5.969" y2="1.143" layer="21"/>
<rectangle x1="-4.191" y1="0.635" x2="-3.429" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="3.429" y1="0.635" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="5.969" y1="0.635" x2="6.731" y2="1.143" layer="21"/>
<rectangle x1="8.509" y1="0.635" x2="9.271" y2="1.143" layer="21"/>
<rectangle x1="11.049" y1="0.635" x2="11.811" y2="1.143" layer="21"/>
<rectangle x1="13.589" y1="0.635" x2="14.351" y2="1.143" layer="21"/>
<rectangle x1="16.129" y1="0.635" x2="16.891" y2="1.143" layer="21"/>
<rectangle x1="18.669" y1="0.635" x2="19.431" y2="1.143" layer="21"/>
<rectangle x1="21.209" y1="0.635" x2="21.971" y2="1.143" layer="21"/>
<rectangle x1="23.749" y1="0.635" x2="24.511" y2="1.143" layer="21"/>
<rectangle x1="-24.511" y1="-2.921" x2="-23.749" y2="-1.905" layer="21"/>
<rectangle x1="-21.971" y1="-2.921" x2="-21.209" y2="-1.905" layer="21"/>
<rectangle x1="-24.511" y1="-5.461" x2="-23.749" y2="-4.699" layer="21"/>
<rectangle x1="-24.511" y1="-4.699" x2="-23.749" y2="-2.921" layer="51"/>
<rectangle x1="-21.971" y1="-4.699" x2="-21.209" y2="-2.921" layer="51"/>
<rectangle x1="-21.971" y1="-5.461" x2="-21.209" y2="-4.699" layer="21"/>
<rectangle x1="-19.431" y1="-2.921" x2="-18.669" y2="-1.905" layer="21"/>
<rectangle x1="-16.891" y1="-2.921" x2="-16.129" y2="-1.905" layer="21"/>
<rectangle x1="-19.431" y1="-5.461" x2="-18.669" y2="-4.699" layer="21"/>
<rectangle x1="-19.431" y1="-4.699" x2="-18.669" y2="-2.921" layer="51"/>
<rectangle x1="-16.891" y1="-4.699" x2="-16.129" y2="-2.921" layer="51"/>
<rectangle x1="-16.891" y1="-5.461" x2="-16.129" y2="-4.699" layer="21"/>
<rectangle x1="-14.351" y1="-2.921" x2="-13.589" y2="-1.905" layer="21"/>
<rectangle x1="-14.351" y1="-5.461" x2="-13.589" y2="-4.699" layer="21"/>
<rectangle x1="-14.351" y1="-4.699" x2="-13.589" y2="-2.921" layer="51"/>
<rectangle x1="-11.811" y1="-2.921" x2="-11.049" y2="-1.905" layer="21"/>
<rectangle x1="-9.271" y1="-2.921" x2="-8.509" y2="-1.905" layer="21"/>
<rectangle x1="-11.811" y1="-5.461" x2="-11.049" y2="-4.699" layer="21"/>
<rectangle x1="-11.811" y1="-4.699" x2="-11.049" y2="-2.921" layer="51"/>
<rectangle x1="-9.271" y1="-4.699" x2="-8.509" y2="-2.921" layer="51"/>
<rectangle x1="-9.271" y1="-5.461" x2="-8.509" y2="-4.699" layer="21"/>
<rectangle x1="-6.731" y1="-2.921" x2="-5.969" y2="-1.905" layer="21"/>
<rectangle x1="-4.191" y1="-2.921" x2="-3.429" y2="-1.905" layer="21"/>
<rectangle x1="-6.731" y1="-5.461" x2="-5.969" y2="-4.699" layer="21"/>
<rectangle x1="-6.731" y1="-4.699" x2="-5.969" y2="-2.921" layer="51"/>
<rectangle x1="-4.191" y1="-4.699" x2="-3.429" y2="-2.921" layer="51"/>
<rectangle x1="-4.191" y1="-5.461" x2="-3.429" y2="-4.699" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="-1.651" y1="-5.461" x2="-0.889" y2="-4.699" layer="21"/>
<rectangle x1="-1.651" y1="-4.699" x2="-0.889" y2="-2.921" layer="51"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
<rectangle x1="3.429" y1="-2.921" x2="4.191" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-5.461" x2="1.651" y2="-4.699" layer="21"/>
<rectangle x1="0.889" y1="-4.699" x2="1.651" y2="-2.921" layer="51"/>
<rectangle x1="3.429" y1="-4.699" x2="4.191" y2="-2.921" layer="51"/>
<rectangle x1="3.429" y1="-5.461" x2="4.191" y2="-4.699" layer="21"/>
<rectangle x1="5.969" y1="-2.921" x2="6.731" y2="-1.905" layer="21"/>
<rectangle x1="8.509" y1="-2.921" x2="9.271" y2="-1.905" layer="21"/>
<rectangle x1="5.969" y1="-5.461" x2="6.731" y2="-4.699" layer="21"/>
<rectangle x1="5.969" y1="-4.699" x2="6.731" y2="-2.921" layer="51"/>
<rectangle x1="8.509" y1="-4.699" x2="9.271" y2="-2.921" layer="51"/>
<rectangle x1="8.509" y1="-5.461" x2="9.271" y2="-4.699" layer="21"/>
<rectangle x1="11.049" y1="-2.921" x2="11.811" y2="-1.905" layer="21"/>
<rectangle x1="11.049" y1="-5.461" x2="11.811" y2="-4.699" layer="21"/>
<rectangle x1="11.049" y1="-4.699" x2="11.811" y2="-2.921" layer="51"/>
<rectangle x1="13.589" y1="-2.921" x2="14.351" y2="-1.905" layer="21"/>
<rectangle x1="16.129" y1="-2.921" x2="16.891" y2="-1.905" layer="21"/>
<rectangle x1="13.589" y1="-5.461" x2="14.351" y2="-4.699" layer="21"/>
<rectangle x1="13.589" y1="-4.699" x2="14.351" y2="-2.921" layer="51"/>
<rectangle x1="16.129" y1="-4.699" x2="16.891" y2="-2.921" layer="51"/>
<rectangle x1="16.129" y1="-5.461" x2="16.891" y2="-4.699" layer="21"/>
<rectangle x1="18.669" y1="-2.921" x2="19.431" y2="-1.905" layer="21"/>
<rectangle x1="21.209" y1="-2.921" x2="21.971" y2="-1.905" layer="21"/>
<rectangle x1="18.669" y1="-5.461" x2="19.431" y2="-4.699" layer="21"/>
<rectangle x1="18.669" y1="-4.699" x2="19.431" y2="-2.921" layer="51"/>
<rectangle x1="21.209" y1="-4.699" x2="21.971" y2="-2.921" layer="51"/>
<rectangle x1="21.209" y1="-5.461" x2="21.971" y2="-4.699" layer="21"/>
<rectangle x1="23.749" y1="-2.921" x2="24.511" y2="-1.905" layer="21"/>
<rectangle x1="23.749" y1="-5.461" x2="24.511" y2="-4.699" layer="21"/>
<rectangle x1="23.749" y1="-4.699" x2="24.511" y2="-2.921" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="2X20" urn="urn:adsk.eagle:package:22443/2" type="model" library_version="3">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="2X20"/>
</packageinstances>
</package3d>
<package3d name="2X20/90" urn="urn:adsk.eagle:package:22440/2" type="model" library_version="3">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="2X20/90"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PINH2X20" urn="urn:adsk.eagle:symbol:22314/1" library_version="3">
<wire x1="-6.35" y1="-27.94" x2="8.89" y2="-27.94" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-27.94" x2="8.89" y2="25.4" width="0.4064" layer="94"/>
<wire x1="8.89" y1="25.4" x2="-6.35" y2="25.4" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="25.4" x2="-6.35" y2="-27.94" width="0.4064" layer="94"/>
<text x="-6.35" y="26.035" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-30.48" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="22.86" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="22.86" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="20.32" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="20.32" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="17.78" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="17.78" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="7" x="-2.54" y="15.24" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="5.08" y="15.24" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="9" x="-2.54" y="12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="5.08" y="12.7" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="11" x="-2.54" y="10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="12" x="5.08" y="10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="13" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="14" x="5.08" y="7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="15" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="16" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="17" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="18" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="19" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="20" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="21" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="22" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="23" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="24" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="25" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="26" x="5.08" y="-7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="27" x="-2.54" y="-10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="28" x="5.08" y="-10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="29" x="-2.54" y="-12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="30" x="5.08" y="-12.7" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="31" x="-2.54" y="-15.24" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="32" x="5.08" y="-15.24" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="33" x="-2.54" y="-17.78" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="34" x="5.08" y="-17.78" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="35" x="-2.54" y="-20.32" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="36" x="5.08" y="-20.32" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="37" x="-2.54" y="-22.86" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="38" x="5.08" y="-22.86" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="39" x="-2.54" y="-25.4" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="40" x="5.08" y="-25.4" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-2X20" urn="urn:adsk.eagle:component:22518/4" prefix="JP" uservalue="yes" library_version="4">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINH2X20" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X20">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="17" pad="17"/>
<connect gate="A" pin="18" pad="18"/>
<connect gate="A" pin="19" pad="19"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="20" pad="20"/>
<connect gate="A" pin="21" pad="21"/>
<connect gate="A" pin="22" pad="22"/>
<connect gate="A" pin="23" pad="23"/>
<connect gate="A" pin="24" pad="24"/>
<connect gate="A" pin="25" pad="25"/>
<connect gate="A" pin="26" pad="26"/>
<connect gate="A" pin="27" pad="27"/>
<connect gate="A" pin="28" pad="28"/>
<connect gate="A" pin="29" pad="29"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="30" pad="30"/>
<connect gate="A" pin="31" pad="31"/>
<connect gate="A" pin="32" pad="32"/>
<connect gate="A" pin="33" pad="33"/>
<connect gate="A" pin="34" pad="34"/>
<connect gate="A" pin="35" pad="35"/>
<connect gate="A" pin="36" pad="36"/>
<connect gate="A" pin="37" pad="37"/>
<connect gate="A" pin="38" pad="38"/>
<connect gate="A" pin="39" pad="39"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="40" pad="40"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22443/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="12" constant="no"/>
</technology>
</technologies>
</device>
<device name="/90" package="2X20/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="17" pad="17"/>
<connect gate="A" pin="18" pad="18"/>
<connect gate="A" pin="19" pad="19"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="20" pad="20"/>
<connect gate="A" pin="21" pad="21"/>
<connect gate="A" pin="22" pad="22"/>
<connect gate="A" pin="23" pad="23"/>
<connect gate="A" pin="24" pad="24"/>
<connect gate="A" pin="25" pad="25"/>
<connect gate="A" pin="26" pad="26"/>
<connect gate="A" pin="27" pad="27"/>
<connect gate="A" pin="28" pad="28"/>
<connect gate="A" pin="29" pad="29"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="30" pad="30"/>
<connect gate="A" pin="31" pad="31"/>
<connect gate="A" pin="32" pad="32"/>
<connect gate="A" pin="33" pad="33"/>
<connect gate="A" pin="34" pad="34"/>
<connect gate="A" pin="35" pad="35"/>
<connect gate="A" pin="36" pad="36"/>
<connect gate="A" pin="37" pad="37"/>
<connect gate="A" pin="38" pad="38"/>
<connect gate="A" pin="39" pad="39"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="40" pad="40"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22440/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="oshw">
<description>&lt;p&gt;&lt;strong&gt;Open Source Hardware PCB Logos&lt;/strong&gt;
&lt;p&gt;A simple polygon based version of the OSHWA logos for use on Eagle PCB's
&lt;p&gt;For use please refer to the OSHWA definition at &lt;a href="http://www.oshwa.org/definition/"&gt;http://www.oshwa.org/definition/&lt;/a&gt;
&lt;p&gt;Based on the NBitWonder version (&lt;a href="http://nbitwonder.com"&gt;http://nbitwonder.com&lt;/a&gt;)
&lt;p&gt;&lt;strong&gt;Andrew Cooper&lt;/strong&gt;&lt;br&gt;28Nov2016&lt;br&gt;&lt;a href="http://www.darkerview.com"&gt;www.DarkerView.com&lt;/a&gt;</description>
<packages>
<package name="OSHW_16MM">
<polygon width="0.127" layer="25" pour="solid">
<vertex x="-5.00380625" y="-5.89279375"/>
<vertex x="-4.673596875" y="-6.1468"/>
<vertex x="-3.03129375" y="-4.977275"/>
<vertex x="-3.022340625" y="-4.970340625"/>
<vertex x="-3.02106875" y="-4.96999375"/>
<vertex x="-3.01999375" y="-4.969228125"/>
<vertex x="-3.00889375" y="-4.966675"/>
<vertex x="-2.99796875" y="-4.963696875"/>
<vertex x="-2.996665625" y="-4.9638625"/>
<vertex x="-2.995375" y="-4.963565625"/>
<vertex x="-2.9841375" y="-4.965453125"/>
<vertex x="-2.972909375" y="-4.96688125"/>
<vertex x="-2.97176875" y="-4.967534375"/>
<vertex x="-2.9704625" y="-4.967753125"/>
<vertex x="-2.960834375" y="-4.97378125"/>
<vertex x="-2.64159375" y="-5.15620625"/>
<vertex x="-2.285971875" y="-5.3340125"/>
<vertex x="-1.9812" y="-5.461003125"/>
<vertex x="-0.7874" y="-2.032"/>
<vertex x="-1.0807625" y="-1.893765625"/>
<vertex x="-1.085696875" y="-1.892753125"/>
<vertex x="-1.092078125" y="-1.888434375"/>
<vertex x="-1.099053125" y="-1.885146875"/>
<vertex x="-1.10244375" y="-1.88141875"/>
<vertex x="-1.360640625" y="-1.7066375"/>
<vertex x="-1.3653625" y="-1.704875"/>
<vertex x="-1.37100625" y="-1.69961875"/>
<vertex x="-1.3773875" y="-1.6953"/>
<vertex x="-1.380159375" y="-1.69109375"/>
<vertex x="-1.60834375" y="-1.478609375"/>
<vertex x="-1.61274375" y="-1.4761375"/>
<vertex x="-1.617515625" y="-1.47006875"/>
<vertex x="-1.62315" y="-1.464821875"/>
<vertex x="-1.625240625" y="-1.46024375"/>
<vertex x="-1.81795" y="-1.215140625"/>
<vertex x="-1.8219125" y="-1.212025"/>
<vertex x="-1.8256875" y="-1.2053"/>
<vertex x="-1.83045" y="-1.19924375"/>
<vertex x="-1.831809375" y="-1.194396875"/>
<vertex x="-1.984446875" y="-0.9225375"/>
<vertex x="-1.98788125" y="-0.918846875"/>
<vertex x="-1.990575" y="-0.91161875"/>
<vertex x="-1.994346875" y="-0.904903125"/>
<vertex x="-1.99494375" y="-0.899903125"/>
<vertex x="-2.103871875" y="-0.6077375"/>
<vertex x="-2.1067" y="-0.603559375"/>
<vertex x="-2.10825" y="-0.596"/>
<vertex x="-2.110940625" y="-0.58878125"/>
<vertex x="-2.110759375" y="-0.58375"/>
<vertex x="-2.1733625" y="-0.27830625"/>
<vertex x="-2.17550625" y="-0.27375625"/>
<vertex x="-2.175871875" y="-0.26606875"/>
<vertex x="-2.177421875" y="-0.258503125"/>
<vertex x="-2.176465625" y="-0.253546875"/>
<vertex x="-2.19125625" y="0.0578875"/>
<vertex x="-2.192675" y="0.062721875"/>
<vertex x="-2.19185" y="0.070384375"/>
<vertex x="-2.192215625" y="0.0780875"/>
<vertex x="-2.190509375" y="0.082828125"/>
<vertex x="-2.15713125" y="0.392821875"/>
<vertex x="-2.1577875" y="0.39780625"/>
<vertex x="-2.155796875" y="0.405234375"/>
<vertex x="-2.15496875" y="0.412915625"/>
<vertex x="-2.152546875" y="0.417346875"/>
<vertex x="-2.07179375" y="0.718525"/>
<vertex x="-2.071671875" y="0.72355625"/>
<vertex x="-2.06855625" y="0.730596875"/>
<vertex x="-2.06655625" y="0.738053125"/>
<vertex x="-2.063484375" y="0.74205625"/>
<vertex x="-1.937303125" y="1.027134375"/>
<vertex x="-1.93640625" y="1.032103125"/>
<vertex x="-1.932228125" y="1.0386"/>
<vertex x="-1.929115625" y="1.045634375"/>
<vertex x="-1.925475" y="1.04910625"/>
<vertex x="-1.756853125" y="1.3113625"/>
<vertex x="-1.755203125" y="1.316121875"/>
<vertex x="-1.7500875" y="1.321884375"/>
<vertex x="-1.745915625" y="1.328371875"/>
<vertex x="-1.741775" y="1.331246875"/>
<vertex x="-1.53474375" y="1.5644125"/>
<vertex x="-1.53238125" y="1.568859375"/>
<vertex x="-1.52644375" y="1.573759375"/>
<vertex x="-1.52131875" y="1.57953125"/>
<vertex x="-1.516778125" y="1.581734375"/>
<vertex x="-1.2763125" y="1.7801875"/>
<vertex x="-1.27329375" y="1.78421875"/>
<vertex x="-1.266665625" y="1.78815"/>
<vertex x="-1.260715625" y="1.793059375"/>
<vertex x="-1.25589375" y="1.7945375"/>
<vertex x="-0.9877125" y="1.953571875"/>
<vertex x="-0.984109375" y="1.9570875"/>
<vertex x="-0.9769625" y="1.959946875"/>
<vertex x="-0.970321875" y="1.963884375"/>
<vertex x="-0.965328125" y="1.9646"/>
<vertex x="-0.67585625" y="2.0804"/>
<vertex x="-0.671753125" y="2.08331875"/>
<vertex x="-0.66424375" y="2.08504375"/>
<vertex x="-0.65708125" y="2.087909375"/>
<vertex x="-0.652040625" y="2.087846875"/>
<vertex x="-0.34816875" y="2.1576625"/>
<vertex x="-0.343665625" y="2.159915625"/>
<vertex x="-0.33598125" y="2.1604625"/>
<vertex x="-0.3284625" y="2.162190625"/>
<vertex x="-0.323490625" y="2.161353125"/>
<vertex x="-0.01250625" y="2.183509375"/>
<vertex x="-0.009640625" y="2.18461875"/>
<vertex x="-0.000009375" y="2.184396875"/>
<vertex x="0.009621875" y="2.185084375"/>
<vertex x="0.01254375" y="2.1841125"/>
<vertex x="0.329975" y="2.176846875"/>
<vertex x="0.334971875" y="2.177925"/>
<vertex x="0.342484375" y="2.176559375"/>
<vertex x="0.35011875" y="2.176384375"/>
<vertex x="0.354796875" y="2.17431875"/>
<vertex x="0.667184375" y="2.11751875"/>
<vertex x="0.672290625" y="2.117803125"/>
<vertex x="0.6795" y="2.115278125"/>
<vertex x="0.6870125" y="2.1139125"/>
<vertex x="0.69130625" y="2.11114375"/>
<vertex x="0.991003125" y="2.006190625"/>
<vertex x="0.996090625" y="2.005675"/>
<vertex x="1.002815625" y="2.00205625"/>
<vertex x="1.010021875" y="1.99953125"/>
<vertex x="1.01383125" y="1.996125"/>
<vertex x="1.293421875" y="1.845615625"/>
<vertex x="1.29836875" y="1.844309375"/>
<vertex x="1.304446875" y="1.83968125"/>
<vertex x="1.31116875" y="1.8360625"/>
<vertex x="1.3144" y="1.8321"/>
<vertex x="1.56701875" y="1.639728125"/>
<vertex x="1.571696875" y="1.637665625"/>
<vertex x="1.576978125" y="1.63214375"/>
<vertex x="1.58305" y="1.62751875"/>
<vertex x="1.585621875" y="1.623103125"/>
<vertex x="1.8050875" y="1.39358125"/>
<vertex x="1.80938125" y="1.390815625"/>
<vertex x="1.813721875" y="1.38455"/>
<vertex x="1.819009375" y="1.379021875"/>
<vertex x="1.8208625" y="1.37425"/>
<vertex x="2.001709375" y="1.11329375"/>
<vertex x="2.00551875" y="1.109890625"/>
<vertex x="2.00883125" y="1.103015625"/>
<vertex x="2.013184375" y="1.096734375"/>
<vertex x="2.014265625" y="1.091734375"/>
<vertex x="2.1521125" y="0.80566875"/>
<vertex x="2.155346875" y="0.801709375"/>
<vertex x="2.157546875" y="0.79439375"/>
<vertex x="2.160859375" y="0.78751875"/>
<vertex x="2.161146875" y="0.78241875"/>
<vertex x="2.252575" y="0.47831875"/>
<vertex x="2.255153125" y="0.473896875"/>
<vertex x="2.25618125" y="0.466321875"/>
<vertex x="2.258378125" y="0.45901875"/>
<vertex x="2.257865625" y="0.4539375"/>
<vertex x="2.30061875" y="0.139303125"/>
<vertex x="2.302475" y="0.13453125"/>
<vertex x="2.30230625" y="0.126884375"/>
<vertex x="2.303334375" y="0.119328125"/>
<vertex x="2.302034375" y="0.114390625"/>
<vertex x="2.295075" y="-0.2030625"/>
<vertex x="2.296159375" y="-0.208053125"/>
<vertex x="2.294803125" y="-0.215559375"/>
<vertex x="2.294634375" y="-0.223203125"/>
<vertex x="2.292571875" y="-0.2278875"/>
<vertex x="2.23606875" y="-0.54035"/>
<vertex x="2.236359375" y="-0.5454625"/>
<vertex x="2.2338375" y="-0.552684375"/>
<vertex x="2.23248125" y="-0.5601875"/>
<vertex x="2.22971875" y="-0.56448125"/>
<vertex x="2.1250625" y="-0.864271875"/>
<vertex x="2.12455" y="-0.869365625"/>
<vertex x="2.12093125" y="-0.876103125"/>
<vertex x="2.11841875" y="-0.8833"/>
<vertex x="2.115021875" y="-0.887109375"/>
<vertex x="1.964771875" y="-1.166859375"/>
<vertex x="1.963471875" y="-1.171809375"/>
<vertex x="1.958846875" y="-1.17789375"/>
<vertex x="1.9552375" y="-1.184615625"/>
<vertex x="1.951284375" y="-1.187846875"/>
<vertex x="1.75915625" y="-1.440659375"/>
<vertex x="1.7571" y="-1.4453375"/>
<vertex x="1.7515875" y="-1.45061875"/>
<vertex x="1.746965625" y="-1.4567"/>
<vertex x="1.74255" y="-1.459275"/>
<vertex x="1.5132625" y="-1.67894375"/>
<vertex x="1.5105" y="-1.683240625"/>
<vertex x="1.50423125" y="-1.68759375"/>
<vertex x="1.498715625" y="-1.692878125"/>
<vertex x="1.49395" y="-1.694734375"/>
<vertex x="1.233128125" y="-1.8758625"/>
<vertex x="1.22973125" y="-1.879675"/>
<vertex x="1.2228625" y="-1.88299375"/>
<vertex x="1.216584375" y="-1.887353125"/>
<vertex x="1.211584375" y="-1.888440625"/>
<vertex x="0.914396875" y="-2.032003125"/>
<vertex x="2.2352" y="-5.3848"/>
<vertex x="2.565403125" y="-5.232396875"/>
<vertex x="2.97179375" y="-5.00380625"/>
<vertex x="3.23046875" y="-4.83135625"/>
<vertex x="3.231015625" y="-4.8305875"/>
<vertex x="3.240975" y="-4.82435"/>
<vertex x="3.2507" y="-4.81786875"/>
<vertex x="3.251621875" y="-4.8176875"/>
<vertex x="3.252428125" y="-4.81718125"/>
<vertex x="3.2640375" y="-4.81523125"/>
<vertex x="3.27548125" y="-4.812965625"/>
<vertex x="3.27640625" y="-4.81315"/>
<vertex x="3.277340625" y="-4.81299375"/>
<vertex x="3.288784375" y="-4.815625"/>
<vertex x="3.30025" y="-4.81791875"/>
<vertex x="3.301034375" y="-4.81844375"/>
<vertex x="3.301959375" y="-4.81865625"/>
<vertex x="3.31154375" y="-4.82548125"/>
<vertex x="3.321240625" y="-4.831975"/>
<vertex x="3.3217625" y="-4.832759375"/>
<vertex x="4.953" y="-5.994403125"/>
<vertex x="5.384803125" y="-5.613396875"/>
<vertex x="5.791203125" y="-5.181596875"/>
<vertex x="6.070590625" y="-4.8514125"/>
<vertex x="6.248396875" y="-4.622803125"/>
<vertex x="5.077796875" y="-2.92915625"/>
<vertex x="5.072153125" y="-2.92295625"/>
<vertex x="5.070678125" y="-2.918859375"/>
<vertex x="5.068203125" y="-2.915278125"/>
<vertex x="5.0664375" y="-2.907078125"/>
<vertex x="5.063596875" y="-2.8991875"/>
<vertex x="5.063803125" y="-2.894840625"/>
<vertex x="5.062884375" y="-2.89058125"/>
<vertex x="5.06439375" y="-2.882328125"/>
<vertex x="5.0647875" y="-2.873953125"/>
<vertex x="5.06664375" y="-2.870009375"/>
<vertex x="5.067425" y="-2.86573125"/>
<vertex x="5.071971875" y="-2.8586875"/>
<vertex x="5.25780625" y="-2.4637875"/>
<vertex x="5.435603125" y="-1.9811875"/>
<vertex x="5.588" y="-1.47320625"/>
<vertex x="5.65403125" y="-1.209071875"/>
<vertex x="5.656421875" y="-1.1978375"/>
<vertex x="5.65708125" y="-1.19688125"/>
<vertex x="5.6573625" y="-1.19575"/>
<vertex x="5.6642125" y="-1.1865"/>
<vertex x="5.67073125" y="-1.17701875"/>
<vertex x="5.67170625" y="-1.176384375"/>
<vertex x="5.6724" y="-1.17545"/>
<vertex x="5.682303125" y="-1.169509375"/>
<vertex x="5.691921875" y="-1.163265625"/>
<vertex x="5.693059375" y="-1.16305625"/>
<vertex x="5.6940625" y="-1.162453125"/>
<vertex x="5.70550625" y="-1.16075"/>
<vertex x="7.7216" y="-0.787403125"/>
<vertex x="7.7216" y="0.0507875"/>
<vertex x="7.696203125" y="0.558784375"/>
<vertex x="7.6454" y="1.0922"/>
<vertex x="5.57788125" y="1.49075625"/>
<vertex x="5.5677375" y="1.491921875"/>
<vertex x="5.565559375" y="1.49313125"/>
<vertex x="5.5631125" y="1.493603125"/>
<vertex x="5.554590625" y="1.499225"/>
<vertex x="5.545653125" y="1.504190625"/>
<vertex x="5.5441" y="1.506146875"/>
<vertex x="5.542025" y="1.507515625"/>
<vertex x="5.53630625" y="1.515965625"/>
<vertex x="5.529946875" y="1.523975"/>
<vertex x="5.5292625" y="1.526371875"/>
<vertex x="5.527865625" y="1.528434375"/>
<vertex x="5.5258125" y="1.53844375"/>
<vertex x="5.384796875" y="2.032"/>
<vertex x="5.1562" y="2.5146"/>
<vertex x="5.003828125" y="2.79395"/>
<vertex x="4.873053125" y="3.011909375"/>
<vertex x="4.866709375" y="3.022040625"/>
<vertex x="4.86660625" y="3.022653125"/>
<vertex x="4.8662875" y="3.023184375"/>
<vertex x="4.8645125" y="3.035103125"/>
<vertex x="4.862521875" y="3.046953125"/>
<vertex x="4.862659375" y="3.04755625"/>
<vertex x="4.86256875" y="3.048171875"/>
<vertex x="4.865478125" y="3.0598125"/>
<vertex x="4.868184375" y="3.071571875"/>
<vertex x="4.868546875" y="3.07208125"/>
<vertex x="4.868696875" y="3.072678125"/>
<vertex x="4.875815625" y="3.082290625"/>
<vertex x="6.0452" y="4.724396875"/>
<vertex x="5.74874375" y="5.070265625"/>
<vertex x="5.748559375" y="5.070359375"/>
<vertex x="5.74065" y="5.079709375"/>
<vertex x="5.732440625" y="5.089284375"/>
<vertex x="5.732375" y="5.0894875"/>
<vertex x="5.461" y="5.410196875"/>
<vertex x="5.089521875" y="5.73215"/>
<vertex x="5.089246875" y="5.73224375"/>
<vertex x="5.08001875" y="5.7403875"/>
<vertex x="5.070653125" y="5.748503125"/>
<vertex x="5.070521875" y="5.748765625"/>
<vertex x="4.6482" y="6.1214"/>
<vertex x="2.904653125" y="4.925825"/>
<vertex x="2.895834375" y="4.919121875"/>
<vertex x="2.894278125" y="4.9187125"/>
<vertex x="2.892953125" y="4.917803125"/>
<vertex x="2.882125" y="4.9155125"/>
<vertex x="2.87140625" y="4.912690625"/>
<vertex x="2.8698125" y="4.912909375"/>
<vertex x="2.8682375" y="4.912575"/>
<vertex x="2.857353125" y="4.914603125"/>
<vertex x="2.846375" y="4.9161"/>
<vertex x="2.844984375" y="4.9169125"/>
<vertex x="2.843403125" y="4.91720625"/>
<vertex x="2.834121875" y="4.923246875"/>
<vertex x="2.565396875" y="5.080003125"/>
<vertex x="2.1336125" y="5.2578"/>
<vertex x="1.727178125" y="5.410209375"/>
<vertex x="1.3853625" y="5.52414375"/>
<vertex x="1.37585" y="5.5263875"/>
<vertex x="1.373453125" y="5.528115625"/>
<vertex x="1.37065625" y="5.529046875"/>
<vertex x="1.363271875" y="5.53545"/>
<vertex x="1.355353125" y="5.54115625"/>
<vertex x="1.353803125" y="5.5436625"/>
<vertex x="1.351571875" y="5.545596875"/>
<vertex x="1.34720625" y="5.554328125"/>
<vertex x="1.342065625" y="5.562640625"/>
<vertex x="1.341590625" y="5.5655625"/>
<vertex x="1.340275" y="5.56819375"/>
<vertex x="1.339584375" y="5.577909375"/>
<vertex x="1.016" y="7.5692"/>
<vertex x="0.533409375" y="7.62"/>
<vertex x="-0.507990625" y="7.62"/>
<vertex x="-0.9398" y="7.5692"/>
<vertex x="-1.03926875" y="6.997275"/>
<vertex x="-1.039165625" y="6.9967875"/>
<vertex x="-1.0414125" y="6.98493125"/>
<vertex x="-1.043509375" y="6.972884375"/>
<vertex x="-1.04378125" y="6.97245625"/>
<vertex x="-1.31313125" y="5.5522375"/>
<vertex x="-1.31395" y="5.542834375"/>
<vertex x="-1.31546875" y="5.5399125"/>
<vertex x="-1.31608125" y="5.536684375"/>
<vertex x="-1.32124375" y="5.52880625"/>
<vertex x="-1.325603125" y="5.520421875"/>
<vertex x="-1.328125" y="5.518303125"/>
<vertex x="-1.329928125" y="5.515553125"/>
<vertex x="-1.337728125" y="5.510240625"/>
<vertex x="-1.34495" y="5.504175"/>
<vertex x="-1.348084375" y="5.503184375"/>
<vertex x="-1.35080625" y="5.50133125"/>
<vertex x="-1.360040625" y="5.499409375"/>
<vertex x="-1.803415625" y="5.35939375"/>
<vertex x="-2.209784375" y="5.20700625"/>
<vertex x="-2.8841125" y="4.869846875"/>
<vertex x="-2.8923375" y="4.86473125"/>
<vertex x="-2.895334375" y="4.8642375"/>
<vertex x="-2.89805" y="4.862878125"/>
<vertex x="-2.907703125" y="4.862190625"/>
<vertex x="-2.9172625" y="4.8606125"/>
<vertex x="-2.920221875" y="4.861303125"/>
<vertex x="-2.923246875" y="4.8610875"/>
<vertex x="-2.93243125" y="4.864146875"/>
<vertex x="-2.9418625" y="4.866346875"/>
<vertex x="-2.944328125" y="4.8681125"/>
<vertex x="-2.9472125" y="4.869075"/>
<vertex x="-2.9545375" y="4.875428125"/>
<vertex x="-4.6228" y="6.0706"/>
<vertex x="-5.0037875" y="5.740409375"/>
<vertex x="-5.334003125" y="5.43559375"/>
<vertex x="-5.67279375" y="5.07074375"/>
<vertex x="-5.9944" y="4.724403125"/>
<vertex x="-4.832015625" y="3.042653125"/>
<vertex x="-4.8316125" y="3.042384375"/>
<vertex x="-4.824865625" y="3.032309375"/>
<vertex x="-4.817928125" y="3.022271875"/>
<vertex x="-4.817825" y="3.021796875"/>
<vertex x="-4.81755625" y="3.02139375"/>
<vertex x="-4.81518125" y="3.009515625"/>
<vertex x="-4.812609375" y="2.997575"/>
<vertex x="-4.812696875" y="2.99709375"/>
<vertex x="-4.812603125" y="2.996625"/>
<vertex x="-4.814953125" y="2.98474375"/>
<vertex x="-4.81715" y="2.972725"/>
<vertex x="-4.8174125" y="2.97231875"/>
<vertex x="-4.81750625" y="2.97184375"/>
<vertex x="-4.824190625" y="2.961815625"/>
<vertex x="-4.83085" y="2.9515"/>
<vertex x="-4.83125625" y="2.95121875"/>
<vertex x="-5.0546" y="2.6162"/>
<vertex x="-5.25780625" y="2.184384375"/>
<vertex x="-5.410190625" y="1.778021875"/>
<vertex x="-5.551075" y="1.284928125"/>
<vertex x="-5.553884375" y="1.273721875"/>
<vertex x="-5.554521875" y="1.2728625"/>
<vertex x="-5.55481875" y="1.271825"/>
<vertex x="-5.56203125" y="1.2627375"/>
<vertex x="-5.568934375" y="1.253434375"/>
<vertex x="-5.56985625" y="1.25288125"/>
<vertex x="-5.570525" y="1.252040625"/>
<vertex x="-5.580675" y="1.246403125"/>
<vertex x="-5.59060625" y="1.240453125"/>
<vertex x="-5.591665625" y="1.240296875"/>
<vertex x="-5.592609375" y="1.239771875"/>
<vertex x="-5.604184375" y="1.238440625"/>
<vertex x="-7.620003125" y="0.9398"/>
<vertex x="-7.6454" y="0.60960625"/>
<vertex x="-7.6454" y="-0.533409375"/>
<vertex x="-7.619996875" y="-0.9398"/>
<vertex x="-5.6043" y="-1.313078125"/>
<vertex x="-5.592040625" y="-1.315234375"/>
<vertex x="-5.5917875" y="-1.315396875"/>
<vertex x="-5.59149375" y="-1.31545"/>
<vertex x="-5.581034375" y="-1.322240625"/>
<vertex x="-5.570728125" y="-1.328796875"/>
<vertex x="-5.570559375" y="-1.3290375"/>
<vertex x="-5.570303125" y="-1.329203125"/>
<vertex x="-5.563259375" y="-1.339453125"/>
<vertex x="-5.556228125" y="-1.34948125"/>
<vertex x="-5.5561625" y="-1.349775"/>
<vertex x="-5.55599375" y="-1.350021875"/>
<vertex x="-5.553434375" y="-1.362046875"/>
<vertex x="-5.461" y="-1.77800625"/>
<vertex x="-5.308584375" y="-2.159034375"/>
<vertex x="-5.1054125" y="-2.616175"/>
<vertex x="-4.845175" y="-3.112990625"/>
<vertex x="-4.839834375" y="-3.1213875"/>
<vertex x="-4.839359375" y="-3.1240875"/>
<vertex x="-4.838084375" y="-3.126525"/>
<vertex x="-4.837190625" y="-3.136459375"/>
<vertex x="-4.83546875" y="-3.14626875"/>
<vertex x="-4.836065625" y="-3.148946875"/>
<vertex x="-4.83581875" y="-3.151684375"/>
<vertex x="-4.83879375" y="-3.16120625"/>
<vertex x="-4.840959375" y="-3.170925"/>
<vertex x="-4.842534375" y="-3.173171875"/>
<vertex x="-4.84335625" y="-3.175796875"/>
<vertex x="-4.849753125" y="-3.183459375"/>
<vertex x="-6.0198" y="-4.8514"/>
<vertex x="-5.715015625" y="-5.20698125"/>
<vertex x="-5.410190625" y="-5.53720625"/>
</polygon>
<text x="9.779" y="-7.0612" size="2.54" layer="25" rot="R180">open source
  hardware</text>
</package>
<package name="OSHW_8MM">
<polygon width="0.0508" layer="25" pour="solid">
<vertex x="1.20990625" y="2.59544375"/>
<vertex x="1.016003125" y="2.6924"/>
<vertex x="0.848315625" y="2.740309375"/>
<vertex x="0.681434375" y="2.78799375"/>
<vertex x="0.676425" y="2.787259375"/>
<vertex x="0.671709375" y="2.790771875"/>
<vertex x="0.6660625" y="2.792384375"/>
<vertex x="0.66360625" y="2.79680625"/>
<vertex x="0.659546875" y="2.799828125"/>
<vertex x="0.658696875" y="2.80564375"/>
<vertex x="0.65584375" y="2.810778125"/>
<vertex x="0.657234375" y="2.815640625"/>
<vertex x="0.508" y="3.8354"/>
<vertex x="-0.4572" y="3.8354"/>
<vertex x="-0.65624375" y="2.815309375"/>
<vertex x="-0.6550375" y="2.8108875"/>
<vertex x="-0.658178125" y="2.8053875"/>
<vertex x="-0.65939375" y="2.7991625"/>
<vertex x="-0.6632" y="2.7966"/>
<vertex x="-0.665475" y="2.792615625"/>
<vertex x="-0.67159375" y="2.790946875"/>
<vertex x="-0.676846875" y="2.787409375"/>
<vertex x="-0.681346875" y="2.7882875"/>
<vertex x="-0.9398" y="2.7178"/>
<vertex x="-1.4285" y="2.473446875"/>
<vertex x="-1.430884375" y="2.47006875"/>
<vertex x="-1.43755625" y="2.46891875"/>
<vertex x="-1.44361875" y="2.4658875"/>
<vertex x="-1.44754375" y="2.467196875"/>
<vertex x="-1.451621875" y="2.46649375"/>
<vertex x="-1.457159375" y="2.470403125"/>
<vertex x="-1.46358125" y="2.47254375"/>
<vertex x="-1.46543125" y="2.476240625"/>
<vertex x="-2.3114" y="3.0734"/>
<vertex x="-2.48918125" y="2.92101875"/>
<vertex x="-2.6744375" y="2.735759375"/>
<vertex x="-2.84481875" y="2.56538125"/>
<vertex x="-2.9972" y="2.387603125"/>
<vertex x="-2.4251375" y="1.541946875"/>
<vertex x="-2.42125" y="1.5397875"/>
<vertex x="-2.419475" y="1.533575"/>
<vertex x="-2.415846875" y="1.5282125"/>
<vertex x="-2.416690625" y="1.523834375"/>
<vertex x="-2.41546875" y="1.51955625"/>
<vertex x="-2.41860625" y="1.51390625"/>
<vertex x="-2.419834375" y="1.50755"/>
<vertex x="-2.423528125" y="1.505053125"/>
<vertex x="-2.539990625" y="1.29541875"/>
<vertex x="-2.6416" y="1.066803125"/>
<vertex x="-2.71410625" y="0.873453125"/>
<vertex x="-2.714103125" y="0.87345"/>
<vertex x="-2.78644375" y="0.680553125"/>
<vertex x="-2.785859375" y="0.676665625"/>
<vertex x="-2.790003125" y="0.671059375"/>
<vertex x="-2.79245" y="0.664534375"/>
<vertex x="-2.796028125" y="0.662909375"/>
<vertex x="-2.798365625" y="0.65974375"/>
<vertex x="-2.8052625" y="0.658709375"/>
<vertex x="-2.81160625" y="0.655825"/>
<vertex x="-2.8152875" y="0.65720625"/>
<vertex x="-3.81" y="0.508003125"/>
<vertex x="-3.8354" y="0.2032125"/>
<vertex x="-3.8354" y="-0.4572"/>
<vertex x="-2.79048125" y="-0.631353125"/>
<vertex x="-2.784734375" y="-0.630203125"/>
<vertex x="-2.78051875" y="-0.6330125"/>
<vertex x="-2.775515625" y="-0.633846875"/>
<vertex x="-2.772103125" y="-0.638621875"/>
<vertex x="-2.767225" y="-0.641875"/>
<vertex x="-2.76623125" y="-0.64684375"/>
<vertex x="-2.763284375" y="-0.65096875"/>
<vertex x="-2.76425" y="-0.65675625"/>
<vertex x="-2.717803125" y="-0.888996875"/>
<vertex x="-2.6162" y="-1.142996875"/>
<vertex x="-2.51458125" y="-1.3716375"/>
<vertex x="-2.422696875" y="-1.5554"/>
<vertex x="-2.41908125" y="-1.5581125"/>
<vertex x="-2.418178125" y="-1.5644375"/>
<vertex x="-2.4153125" y="-1.57016875"/>
<vertex x="-2.416746875" y="-1.574465625"/>
<vertex x="-2.41610625" y="-1.578940625"/>
<vertex x="-2.41994375" y="-1.58405625"/>
<vertex x="-2.42196875" y="-1.59013125"/>
<vertex x="-2.42601875" y="-1.59215625"/>
<vertex x="-3.0226" y="-2.3876"/>
<vertex x="-2.87021875" y="-2.56538125"/>
<vertex x="-2.684959375" y="-2.7506375"/>
<vertex x="-2.5145875" y="-2.9210125"/>
<vertex x="-2.3622" y="-3.048"/>
<vertex x="-1.51659375" y="-2.47596875"/>
<vertex x="-1.51430625" y="-2.4719625"/>
<vertex x="-1.50821875" y="-2.470303125"/>
<vertex x="-1.502990625" y="-2.466765625"/>
<vertex x="-1.49845625" y="-2.467640625"/>
<vertex x="-1.49400625" y="-2.466428125"/>
<vertex x="-1.48853125" y="-2.46955625"/>
<vertex x="-1.482328125" y="-2.470753125"/>
<vertex x="-1.4797375" y="-2.47458125"/>
<vertex x="-1.320790625" y="-2.56540625"/>
<vertex x="-1.15255625" y="-2.63750625"/>
<vertex x="-1.151734375" y="-2.63723125"/>
<vertex x="-1.14298125" y="-2.641609375"/>
<vertex x="-1.134009375" y="-2.645453125"/>
<vertex x="-1.1336875" y="-2.646253125"/>
<vertex x="-0.9906" y="-2.717803125"/>
<vertex x="-0.381" y="-0.9906"/>
<vertex x="-0.550484375" y="-0.90961875"/>
<vertex x="-0.553140625" y="-0.910028125"/>
<vertex x="-0.559715625" y="-0.90520625"/>
<vertex x="-0.56705" y="-0.901703125"/>
<vertex x="-0.56794375" y="-0.899175"/>
<vertex x="-0.71119375" y="-0.79415"/>
<vertex x="-0.71388125" y="-0.794059375"/>
<vertex x="-0.719446875" y="-0.7881"/>
<vertex x="-0.726003125" y="-0.78329375"/>
<vertex x="-0.7264125" y="-0.78064375"/>
<vertex x="-0.847659375" y="-0.65083125"/>
<vertex x="-0.850275" y="-0.65024375"/>
<vertex x="-0.854621875" y="-0.643378125"/>
<vertex x="-0.8601875" y="-0.63741875"/>
<vertex x="-0.860096875" y="-0.63473125"/>
<vertex x="-0.9551" y="-0.484671875"/>
<vertex x="-0.95756875" y="-0.48360625"/>
<vertex x="-0.960575" y="-0.476025"/>
<vertex x="-0.964921875" y="-0.469159375"/>
<vertex x="-0.964334375" y="-0.46654375"/>
<vertex x="-1.02981875" y="-0.301415625"/>
<vertex x="-1.032040625" y="-0.2999125"/>
<vertex x="-1.033584375" y="-0.291921875"/>
<vertex x="-1.036584375" y="-0.284353125"/>
<vertex x="-1.03551875" y="-0.2818875"/>
<vertex x="-1.06918125" y="-0.10748125"/>
<vertex x="-1.071084375" y="-0.105590625"/>
<vertex x="-1.071115625" y="-0.097459375"/>
<vertex x="-1.072659375" y="-0.089459375"/>
<vertex x="-1.071153125" y="-0.087234375"/>
<vertex x="-1.071825" y="0.09039375"/>
<vertex x="-1.07334375" y="0.092603125"/>
<vertex x="-1.0718625" y="0.1006"/>
<vertex x="-1.07189375" y="0.108746875"/>
<vertex x="-1.07" y="0.110653125"/>
<vertex x="-1.037659375" y="0.285303125"/>
<vertex x="-1.038740625" y="0.287759375"/>
<vertex x="-1.0358" y="0.29534375"/>
<vertex x="-1.034315625" y="0.303353125"/>
<vertex x="-1.032103125" y="0.304875"/>
<vertex x="-0.967875" y="0.470478125"/>
<vertex x="-0.96848125" y="0.473090625"/>
<vertex x="-0.964184375" y="0.479990625"/>
<vertex x="-0.9612375" y="0.487590625"/>
<vertex x="-0.958778125" y="0.488675"/>
<vertex x="-0.864896875" y="0.639465625"/>
<vertex x="-0.865009375" y="0.64214375"/>
<vertex x="-0.85950625" y="0.648125"/>
<vertex x="-0.855196875" y="0.655046875"/>
<vertex x="-0.852578125" y="0.65565625"/>
<vertex x="-0.73231875" y="0.786375"/>
<vertex x="-0.73193125" y="0.78903125"/>
<vertex x="-0.7254" y="0.79389375"/>
<vertex x="-0.719890625" y="0.799884375"/>
<vertex x="-0.717209375" y="0.799996875"/>
<vertex x="-0.57474375" y="0.9061"/>
<vertex x="-0.57386875" y="0.9086375"/>
<vertex x="-0.566553125" y="0.9122"/>
<vertex x="-0.560021875" y="0.917065625"/>
<vertex x="-0.557365625" y="0.916678125"/>
<vertex x="-0.397690625" y="0.99445625"/>
<vertex x="-0.396359375" y="0.996784375"/>
<vertex x="-0.3885125" y="0.998925"/>
<vertex x="-0.3811875" y="1.00249375"/>
<vertex x="-0.378646875" y="1.00161875"/>
<vertex x="-0.2072875" y="1.04838125"/>
<vertex x="-0.205546875" y="1.050421875"/>
<vertex x="-0.197434375" y="1.05106875"/>
<vertex x="-0.189578125" y="1.0532125"/>
<vertex x="-0.187246875" y="1.05188125"/>
<vertex x="-0.010290625" y="1.06598125"/>
<vertex x="-0.009109375" y="1.067090625"/>
<vertex x="-0.000028125" y="1.0668"/>
<vertex x="0.0090625" y="1.067525"/>
<vertex x="0.0103" y="1.06646875"/>
<vertex x="0.190590625" y="1.06070625"/>
<vertex x="0.19288125" y="1.062159375"/>
<vertex x="0.200815625" y="1.06038125"/>
<vertex x="0.20891875" y="1.060121875"/>
<vertex x="0.21076875" y="1.05815"/>
<vertex x="0.38686875" y="1.018671875"/>
<vertex x="0.389384375" y="1.019665625"/>
<vertex x="0.396821875" y="1.016440625"/>
<vertex x="0.404753125" y="1.0146625"/>
<vertex x="0.406203125" y="1.012371875"/>
<vertex x="0.57176875" y="0.94058125"/>
<vertex x="0.574428125" y="0.9410875"/>
<vertex x="0.58113125" y="0.936521875"/>
<vertex x="0.5885875" y="0.933290625"/>
<vertex x="0.589584375" y="0.93076875"/>
<vertex x="0.7387625" y="0.82920625"/>
<vertex x="0.741471875" y="0.829203125"/>
<vertex x="0.747209375" y="0.82345625"/>
<vertex x="0.75391875" y="0.8188875"/>
<vertex x="0.754425" y="0.816228125"/>
<vertex x="0.88191875" y="0.688490625"/>
<vertex x="0.88458125" y="0.68798125"/>
<vertex x="0.889140625" y="0.681259375"/>
<vertex x="0.894871875" y="0.675515625"/>
<vertex x="0.89486875" y="0.672809375"/>
<vertex x="0.996153125" y="0.523440625"/>
<vertex x="0.998671875" y="0.522440625"/>
<vertex x="1.001890625" y="0.51498125"/>
<vertex x="1.00644375" y="0.508265625"/>
<vertex x="1.005934375" y="0.50560625"/>
<vertex x="1.077415625" y="0.339903125"/>
<vertex x="1.0797" y="0.33845"/>
<vertex x="1.081459375" y="0.330528125"/>
<vertex x="1.084678125" y="0.32306875"/>
<vertex x="1.083678125" y="0.32055"/>
<vertex x="1.122825" y="0.144384375"/>
<vertex x="1.124796875" y="0.142528125"/>
<vertex x="1.125040625" y="0.13441875"/>
<vertex x="1.126803125" y="0.1264875"/>
<vertex x="1.125346875" y="0.1242"/>
<vertex x="1.130778125" y="-0.056184375"/>
<vertex x="1.132365625" y="-0.058378125"/>
<vertex x="1.131084375" y="-0.06639375"/>
<vertex x="1.131328125" y="-0.074509375"/>
<vertex x="1.129471875" y="-0.07648125"/>
<vertex x="1.100978125" y="-0.25469375"/>
<vertex x="1.102128125" y="-0.25714375"/>
<vertex x="1.099365625" y="-0.264778125"/>
<vertex x="1.098084375" y="-0.272796875"/>
<vertex x="1.095890625" y="-0.274384375"/>
<vertex x="1.034496875" y="-0.4440875"/>
<vertex x="1.035165625" y="-0.4467125"/>
<vertex x="1.031021875" y="-0.45369375"/>
<vertex x="1.028259375" y="-0.461328125"/>
<vertex x="1.02580625" y="-0.462478125"/>
<vertex x="0.933684375" y="-0.617665625"/>
<vertex x="0.93385" y="-0.620365625"/>
<vertex x="0.928478125" y="-0.6264375"/>
<vertex x="0.924328125" y="-0.633428125"/>
<vertex x="0.9217" y="-0.634096875"/>
<vertex x="0.802128125" y="-0.76925"/>
<vertex x="0.801784375" y="-0.7719375"/>
<vertex x="0.795359375" y="-0.7769"/>
<vertex x="0.78998125" y="-0.78298125"/>
<vertex x="0.787278125" y="-0.783146875"/>
<vertex x="0.644459375" y="-0.893503125"/>
<vertex x="0.643615625" y="-0.896078125"/>
<vertex x="0.63636875" y="-0.899753125"/>
<vertex x="0.62995" y="-0.9047125"/>
<vertex x="0.627265625" y="-0.90436875"/>
<vertex x="0.4572" y="-0.9906"/>
<vertex x="1.117596875" y="-2.6924"/>
<vertex x="1.286265625" y="-2.596021875"/>
<vertex x="1.286265625" y="-2.59601875"/>
<vertex x="1.63200625" y="-2.39845625"/>
<vertex x="1.634990625" y="-2.39435625"/>
<vertex x="1.6407875" y="-2.393440625"/>
<vertex x="1.645878125" y="-2.39053125"/>
<vertex x="1.650765625" y="-2.3918625"/>
<vertex x="1.655775" y="-2.391071875"/>
<vertex x="1.660521875" y="-2.394521875"/>
<vertex x="1.666178125" y="-2.396065625"/>
<vertex x="1.668690625" y="-2.400465625"/>
<vertex x="2.489196875" y="-2.9972"/>
<vertex x="2.659559375" y="-2.8268375"/>
<vertex x="2.81941875" y="-2.66698125"/>
<vertex x="2.978646875" y="-2.4812125"/>
<vertex x="2.97865" y="-2.4812125"/>
<vertex x="3.1242" y="-2.3114"/>
<vertex x="2.526734375" y="-1.4152"/>
<vertex x="2.522446875" y="-1.41251875"/>
<vertex x="2.521121875" y="-1.40678125"/>
<vertex x="2.5178625" y="-1.401890625"/>
<vertex x="2.518853125" y="-1.396940625"/>
<vertex x="2.517715625" y="-1.392015625"/>
<vertex x="2.520834375" y="-1.387025"/>
<vertex x="2.5219875" y="-1.381259375"/>
<vertex x="2.5261875" y="-1.378459375"/>
<vertex x="2.641596875" y="-1.193803125"/>
<vertex x="2.714471875" y="-0.975184375"/>
<vertex x="2.714471875" y="-0.97518125"/>
<vertex x="2.793996875" y="-0.736615625"/>
<vertex x="2.838825" y="-0.5797125"/>
<vertex x="2.838015625" y="-0.575090625"/>
<vertex x="2.84160625" y="-0.569978125"/>
<vertex x="2.84331875" y="-0.56398125"/>
<vertex x="2.847415625" y="-0.56170625"/>
<vertex x="2.850109375" y="-0.55786875"/>
<vertex x="2.856259375" y="-0.55679375"/>
<vertex x="2.8617125" y="-0.5537625"/>
<vertex x="2.86621875" y="-0.55505"/>
<vertex x="3.8608" y="-0.381"/>
<vertex x="3.8862" y="-0.10159375"/>
<vertex x="3.8862" y="0.126990625"/>
<vertex x="3.86210625" y="0.319734375"/>
<vertex x="3.861821875" y="0.319965625"/>
<vertex x="3.860803125" y="0.3301375"/>
<vertex x="3.859540625" y="0.34025"/>
<vertex x="3.859765625" y="0.3405375"/>
<vertex x="3.8354" y="0.5842"/>
<vertex x="2.8151875" y="0.758384375"/>
<vertex x="2.811621875" y="0.7570875"/>
<vertex x="2.80518125" y="0.76009375"/>
<vertex x="2.79818125" y="0.7612875"/>
<vertex x="2.795990625" y="0.76438125"/>
<vertex x="2.792553125" y="0.765984375"/>
<vertex x="2.790125" y="0.772659375"/>
<vertex x="2.78601875" y="0.77845625"/>
<vertex x="2.78665625" y="0.782196875"/>
<vertex x="2.695959375" y="1.031615625"/>
<vertex x="2.695603125" y="1.03179375"/>
<vertex x="2.692390625" y="1.041428125"/>
<vertex x="2.68890625" y="1.0510125"/>
<vertex x="2.689075" y="1.051375"/>
<vertex x="2.641596875" y="1.1938"/>
<vertex x="2.540021875" y="1.371565625"/>
<vertex x="2.424796875" y="1.555925"/>
<vertex x="2.4200375" y="1.55964375"/>
<vertex x="2.419440625" y="1.56449375"/>
<vertex x="2.41685" y="1.568640625"/>
<vertex x="2.418209375" y="1.574528125"/>
<vertex x="2.417471875" y="1.580528125"/>
<vertex x="2.42048125" y="1.58438125"/>
<vertex x="2.42158125" y="1.58914375"/>
<vertex x="2.42670625" y="1.592346875"/>
<vertex x="3.048" y="2.3876"/>
<vertex x="2.89561875" y="2.56538125"/>
<vertex x="2.717803125" y="2.743196875"/>
<vertex x="2.522515625" y="2.914071875"/>
<vertex x="2.3114" y="3.0988"/>
<vertex x="1.465303125" y="2.50155"/>
<vertex x="1.463871875" y="2.498334375"/>
<vertex x="1.457003125" y="2.49569375"/>
<vertex x="1.4509875" y="2.491446875"/>
<vertex x="1.44751875" y="2.49204375"/>
<vertex x="1.44423125" y="2.49078125"/>
<vertex x="1.437503125" y="2.493771875"/>
<vertex x="1.43025" y="2.495021875"/>
<vertex x="1.42821875" y="2.497896875"/>
<vertex x="1.228765625" y="2.586546875"/>
<vertex x="1.228128125" y="2.586334375"/>
<vertex x="1.21920625" y="2.59079375"/>
<vertex x="1.210146875" y="2.594821875"/>
</polygon>
<text x="4.8006" y="-3.6576" size="1.27" layer="25" rot="R180">open source
  hardware</text>
</package>
<package name="OSHW_5MM">
<polygon width="0.0508" layer="25" pour="solid">
<vertex x="-1.498575" y="-1.752621875"/>
<vertex x="-1.4097" y="-1.8288"/>
<vertex x="-0.906828125" y="-1.485371875"/>
<vertex x="-0.904903125" y="-1.481684375"/>
<vertex x="-0.898465625" y="-1.4796625"/>
<vertex x="-0.8928875" y="-1.475853125"/>
<vertex x="-0.88879375" y="-1.476625"/>
<vertex x="-0.884828125" y="-1.475378125"/>
<vertex x="-0.878846875" y="-1.4785"/>
<vertex x="-0.872209375" y="-1.47975"/>
<vertex x="-0.8698625" y="-1.4831875"/>
<vertex x="-0.5969" y="-1.625596875"/>
<vertex x="-0.2413" y="-0.609596875"/>
<vertex x="-0.358453125" y="-0.55098125"/>
<vertex x="-0.361521875" y="-0.55130625"/>
<vertex x="-0.36756875" y="-0.54641875"/>
<vertex x="-0.374515625" y="-0.54294375"/>
<vertex x="-0.375490625" y="-0.54001875"/>
<vertex x="-0.4694625" y="-0.464075"/>
<vertex x="-0.472528125" y="-0.4637375"/>
<vertex x="-0.477384375" y="-0.457675"/>
<vertex x="-0.483428125" y="-0.452790625"/>
<vertex x="-0.483753125" y="-0.449721875"/>
<vertex x="-0.559275" y="-0.355434375"/>
<vertex x="-0.5622" y="-0.354446875"/>
<vertex x="-0.56565" y="-0.347475"/>
<vertex x="-0.570503125" y="-0.341415625"/>
<vertex x="-0.570165625" y="-0.338353125"/>
<vertex x="-0.623753125" y="-0.2300625"/>
<vertex x="-0.62639375" y="-0.228475"/>
<vertex x="-0.62826875" y="-0.2209375"/>
<vertex x="-0.631715625" y="-0.213971875"/>
<vertex x="-0.630728125" y="-0.21105"/>
<vertex x="-0.65989375" y="-0.09380625"/>
<vertex x="-0.6621375" y="-0.091684375"/>
<vertex x="-0.66235625" y="-0.08390625"/>
<vertex x="-0.664228125" y="-0.076378125"/>
<vertex x="-0.662640625" y="-0.0737375"/>
<vertex x="-0.666021875" y="0.047034375"/>
<vertex x="-0.667759375" y="0.0495875"/>
<vertex x="-0.66630625" y="0.057228125"/>
<vertex x="-0.666525" y="0.0649875"/>
<vertex x="-0.664409375" y="0.067228125"/>
<vertex x="-0.641859375" y="0.185915625"/>
<vertex x="-0.64300625" y="0.188775"/>
<vertex x="-0.639959375" y="0.1959125"/>
<vertex x="-0.63850625" y="0.20355625"/>
<vertex x="-0.635953125" y="0.20529375"/>
<vertex x="-0.5885125" y="0.316409375"/>
<vertex x="-0.589021875" y="0.31945625"/>
<vertex x="-0.58450625" y="0.3257875"/>
<vertex x="-0.581459375" y="0.332928125"/>
<vertex x="-0.5786" y="0.334078125"/>
<vertex x="-0.508475" y="0.43244375"/>
<vertex x="-0.508321875" y="0.435521875"/>
<vertex x="-0.5025625" y="0.440734375"/>
<vertex x="-0.49805" y="0.447065625"/>
<vertex x="-0.49500625" y="0.447575"/>
<vertex x="-0.405428125" y="0.528665625"/>
<vertex x="-0.40461875" y="0.531640625"/>
<vertex x="-0.397875" y="0.5355"/>
<vertex x="-0.392115625" y="0.540715625"/>
<vertex x="-0.389034375" y="0.5405625"/>
<vertex x="-0.284190625" y="0.600590625"/>
<vertex x="-0.2827625" y="0.603325"/>
<vertex x="-0.27535625" y="0.60565"/>
<vertex x="-0.268609375" y="0.6095125"/>
<vertex x="-0.26563125" y="0.608703125"/>
<vertex x="-0.150359375" y="0.644884375"/>
<vertex x="-0.148378125" y="0.64725"/>
<vertex x="-0.1406375" y="0.6479375"/>
<vertex x="-0.133225" y="0.6502625"/>
<vertex x="-0.130490625" y="0.648834375"/>
<vertex x="-0.010153125" y="0.6595"/>
<vertex x="-0.007775" y="0.66135625"/>
<vertex x="0.0000125" y="0.6604"/>
<vertex x="0.007809375" y="0.661090625"/>
<vertex x="0.010115625" y="0.659159375"/>
<vertex x="0.125928125" y="0.6449375"/>
<vertex x="0.128565625" y="0.6462375"/>
<vertex x="0.13605" y="0.64369375"/>
<vertex x="0.14389375" y="0.64273125"/>
<vertex x="0.145703125" y="0.640415625"/>
<vertex x="0.256196875" y="0.602875"/>
<vertex x="0.25904375" y="0.603609375"/>
<vertex x="0.265846875" y="0.599596875"/>
<vertex x="0.273334375" y="0.597053125"/>
<vertex x="0.274634375" y="0.594415625"/>
<vertex x="0.375140625" y="0.53514375"/>
<vertex x="0.378078125" y="0.53528125"/>
<vertex x="0.383928125" y="0.529959375"/>
<vertex x="0.39073125" y="0.525946875"/>
<vertex x="0.391465625" y="0.5231"/>
<vertex x="0.477775" y="0.44456875"/>
<vertex x="0.48068125" y="0.44410625"/>
<vertex x="0.485321875" y="0.437703125"/>
<vertex x="0.491165625" y="0.4323875"/>
<vertex x="0.491303125" y="0.429453125"/>
<vertex x="0.559790625" y="0.334978125"/>
<vertex x="0.562540625" y="0.33393125"/>
<vertex x="0.565778125" y="0.32671875"/>
<vertex x="0.570415625" y="0.320321875"/>
<vertex x="0.569953125" y="0.31741875"/>
<vertex x="0.61773125" y="0.21096875"/>
<vertex x="0.620209375" y="0.209384375"/>
<vertex x="0.62190625" y="0.20166875"/>
<vertex x="0.62514375" y="0.19445625"/>
<vertex x="0.624096875" y="0.19170625"/>
<vertex x="0.64916875" y="0.07774375"/>
<vertex x="0.651271875" y="0.0756875"/>
<vertex x="0.651359375" y="0.067784375"/>
<vertex x="0.653059375" y="0.0600625"/>
<vertex x="0.651475" y="0.057584375"/>
<vertex x="0.65278125" y="-0.05908125"/>
<vertex x="0.654421875" y="-0.061525"/>
<vertex x="0.652896875" y="-0.0692875"/>
<vertex x="0.652984375" y="-0.077184375"/>
<vertex x="0.65093125" y="-0.079284375"/>
<vertex x="0.62841875" y="-0.193784375"/>
<vertex x="0.629525" y="-0.19650625"/>
<vertex x="0.626453125" y="-0.20378125"/>
<vertex x="0.624928125" y="-0.211540625"/>
<vertex x="0.622484375" y="-0.21318125"/>
<vertex x="0.577096875" y="-0.320678125"/>
<vertex x="0.577625" y="-0.32356875"/>
<vertex x="0.57313125" y="-0.33006875"/>
<vertex x="0.57005625" y="-0.337353125"/>
<vertex x="0.56733125" y="-0.338459375"/>
<vertex x="0.500978125" y="-0.434453125"/>
<vertex x="0.50090625" y="-0.437390625"/>
<vertex x="0.49518125" y="-0.4428375"/>
<vertex x="0.4906875" y="-0.449340625"/>
<vertex x="0.48779375" y="-0.44986875"/>
<vertex x="0.403278125" y="-0.5303"/>
<vertex x="0.40260625" y="-0.533165625"/>
<vertex x="0.395884375" y="-0.537334375"/>
<vertex x="0.3901625" y="-0.54278125"/>
<vertex x="0.387225" y="-0.542709375"/>
<vertex x="0.2794" y="-0.6096"/>
<vertex x="0.6731" y="-1.6002"/>
<vertex x="0.82550625" y="-1.536696875"/>
<vertex x="0.95948125" y="-1.44738125"/>
<vertex x="0.962775" y="-1.442675"/>
<vertex x="0.96789375" y="-1.441771875"/>
<vertex x="0.972215625" y="-1.438890625"/>
<vertex x="0.97784375" y="-1.440015625"/>
<vertex x="0.983496875" y="-1.43901875"/>
<vertex x="0.98775" y="-1.441996875"/>
<vertex x="0.992846875" y="-1.443015625"/>
<vertex x="0.99603125" y="-1.44779375"/>
<vertex x="1.4859" y="-1.7907"/>
<vertex x="1.5875" y="-1.7018"/>
<vertex x="1.676390625" y="-1.612909375"/>
<vertex x="1.77798125" y="-1.498625"/>
<vertex x="1.8669" y="-1.3843"/>
<vertex x="1.510990625" y="-0.868840625"/>
<vertex x="1.5067875" y="-0.866390625"/>
<vertex x="1.50524375" y="-0.86051875"/>
<vertex x="1.501796875" y="-0.855528125"/>
<vertex x="1.502671875" y="-0.850746875"/>
<vertex x="1.501434375" y="-0.84604375"/>
<vertex x="1.504490625" y="-0.840803125"/>
<vertex x="1.505584375" y="-0.834828125"/>
<vertex x="1.509590625" y="-0.8320625"/>
<vertex x="1.587496875" y="-0.69850625"/>
<vertex x="1.638296875" y="-0.546115625"/>
<vertex x="1.6838" y="-0.3641"/>
<vertex x="1.68289375" y="-0.35916875"/>
<vertex x="1.686246875" y="-0.354303125"/>
<vertex x="1.687684375" y="-0.348559375"/>
<vertex x="1.691990625" y="-0.345975"/>
<vertex x="1.6948375" y="-0.341846875"/>
<vertex x="1.700653125" y="-0.340778125"/>
<vertex x="1.705728125" y="-0.337734375"/>
<vertex x="1.7106" y="-0.338953125"/>
<vertex x="2.3114" y="-0.2286"/>
<vertex x="2.3114" y="0.06349375"/>
<vertex x="2.298721875" y="0.20295"/>
<vertex x="2.286" y="0.3302"/>
<vertex x="1.684975" y="0.440590625"/>
<vertex x="1.6808625" y="0.439325"/>
<vertex x="1.675015625" y="0.442421875"/>
<vertex x="1.668509375" y="0.443615625"/>
<vertex x="1.666065625" y="0.447159375"/>
<vertex x="1.662265625" y="0.449171875"/>
<vertex x="1.660321875" y="0.455490625"/>
<vertex x="1.656565625" y="0.4609375"/>
<vertex x="1.65734375" y="0.46516875"/>
<vertex x="1.61289375" y="0.60961875"/>
<vertex x="1.549396875" y="0.74930625"/>
<vertex x="1.46574375" y="0.892715625"/>
<vertex x="1.464840625" y="0.893015625"/>
<vertex x="1.460490625" y="0.901721875"/>
<vertex x="1.45659375" y="0.908396875"/>
<vertex x="1.45401875" y="0.910246875"/>
<vertex x="1.4529375" y="0.916825"/>
<vertex x="1.449953125" y="0.922796875"/>
<vertex x="1.451296875" y="0.926825"/>
<vertex x="1.450609375" y="0.931009375"/>
<vertex x="1.454496875" y="0.936425"/>
<vertex x="1.456609375" y="0.942759375"/>
<vertex x="1.460409375" y="0.944659375"/>
<vertex x="1.8034" y="1.4224"/>
<vertex x="1.7018" y="1.5494"/>
<vertex x="1.574803125" y="1.676396875"/>
<vertex x="1.47320625" y="1.76529375"/>
<vertex x="1.3843" y="1.8288"/>
<vertex x="0.8813625" y="1.4730625"/>
<vertex x="0.879065625" y="1.468928125"/>
<vertex x="0.873109375" y="1.467225"/>
<vertex x="0.86805" y="1.463646875"/>
<vertex x="0.863384375" y="1.464446875"/>
<vertex x="0.858834375" y="1.463146875"/>
<vertex x="0.85341875" y="1.46615625"/>
<vertex x="0.847309375" y="1.467203125"/>
<vertex x="0.844575" y="1.47106875"/>
<vertex x="0.74930625" y="1.523996875"/>
<vertex x="0.644615625" y="1.570525"/>
<vertex x="0.6446125" y="1.570525"/>
<vertex x="0.520703125" y="1.6256"/>
<vertex x="0.427859375" y="1.64623125"/>
<vertex x="0.42258125" y="1.6452625"/>
<vertex x="0.417996875" y="1.648421875"/>
<vertex x="0.412559375" y="1.64963125"/>
<vertex x="0.409678125" y="1.654159375"/>
<vertex x="0.405259375" y="1.65720625"/>
<vertex x="0.404253125" y="1.662684375"/>
<vertex x="0.4012625" y="1.667384375"/>
<vertex x="0.402428125" y="1.672625"/>
<vertex x="0.2921" y="2.273303125"/>
<vertex x="0.1397125" y="2.286"/>
<vertex x="-0.1523875" y="2.286"/>
<vertex x="-0.2794" y="2.2733"/>
<vertex x="-0.377446875" y="1.685009375"/>
<vertex x="-0.3761125" y="1.6810125"/>
<vertex x="-0.3791125" y="1.675015625"/>
<vertex x="-0.3802125" y="1.668409375"/>
<vertex x="-0.383640625" y="1.665959375"/>
<vertex x="-0.385525" y="1.66219375"/>
<vertex x="-0.391878125" y="1.660075"/>
<vertex x="-0.397334375" y="1.656178125"/>
<vertex x="-0.401490625" y="1.656871875"/>
<vertex x="-0.57151875" y="1.600190625"/>
<vertex x="-0.698496875" y="1.5494"/>
<vertex x="-0.857309375" y="1.45865625"/>
<vertex x="-0.860278125" y="1.4545625"/>
<vertex x="-0.866096875" y="1.453634375"/>
<vertex x="-0.8712" y="1.45071875"/>
<vertex x="-0.876065625" y="1.452046875"/>
<vertex x="-0.88105625" y="1.45125"/>
<vertex x="-0.885821875" y="1.45470625"/>
<vertex x="-0.8915" y="1.456253125"/>
<vertex x="-0.894003125" y="1.4606375"/>
<vertex x="-1.384296875" y="1.8161"/>
<vertex x="-1.498603125" y="1.727196875"/>
<vertex x="-1.625590625" y="1.60020625"/>
<vertex x="-1.720309375" y="1.493653125"/>
<vertex x="-1.720309375" y="1.4927875"/>
<vertex x="-1.727234375" y="1.4858625"/>
<vertex x="-1.733715625" y="1.478571875"/>
<vertex x="-1.734575" y="1.478521875"/>
<vertex x="-1.7907" y="1.4224"/>
<vertex x="-1.447415625" y="0.919734375"/>
<vertex x="-1.4431625" y="0.917184375"/>
<vertex x="-1.4417125" y="0.9113875"/>
<vertex x="-1.43834375" y="0.906453125"/>
<vertex x="-1.4392625" y="0.90158125"/>
<vertex x="-1.438059375" y="0.89676875"/>
<vertex x="-1.441134375" y="0.89164375"/>
<vertex x="-1.442240625" y="0.885775"/>
<vertex x="-1.44633125" y="0.88298125"/>
<vertex x="-1.51130625" y="0.774690625"/>
<vertex x="-1.587503125" y="0.609596875"/>
<vertex x="-1.622896875" y="0.479809375"/>
<vertex x="-1.622153125" y="0.477946875"/>
<vertex x="-1.6256" y="0.469903125"/>
<vertex x="-1.62790625" y="0.461446875"/>
<vertex x="-1.62965" y="0.46045"/>
<vertex x="-1.65529375" y="0.4006125"/>
<vertex x="-1.654790625" y="0.39753125"/>
<vertex x="-1.6593" y="0.391259375"/>
<vertex x="-1.66235" y="0.384146875"/>
<vertex x="-1.665253125" y="0.382984375"/>
<vertex x="-1.667078125" y="0.38045"/>
<vertex x="-1.674703125" y="0.37920625"/>
<vertex x="-1.6818875" y="0.37633125"/>
<vertex x="-1.6847625" y="0.3775625"/>
<vertex x="-2.286" y="0.279403125"/>
<vertex x="-2.286" y="-0.279403125"/>
<vertex x="-1.685209375" y="-0.377490625"/>
<vertex x="-1.68055625" y="-0.376159375"/>
<vertex x="-1.675228125" y="-0.37911875"/>
<vertex x="-1.66921875" y="-0.3801"/>
<vertex x="-1.66639375" y="-0.384028125"/>
<vertex x="-1.6621625" y="-0.386378125"/>
<vertex x="-1.6604875" y="-0.392234375"/>
<vertex x="-1.65693125" y="-0.39718125"/>
<vertex x="-1.6577125" y="-0.401959375"/>
<vertex x="-1.612915625" y="-0.558753125"/>
<vertex x="-1.574796875" y="-0.685809375"/>
<vertex x="-1.5239875" y="-0.787425"/>
<vertex x="-1.44614375" y="-0.92086875"/>
<vertex x="-1.44195625" y="-0.92390625"/>
<vertex x="-1.441046875" y="-0.92960625"/>
<vertex x="-1.43814375" y="-0.934584375"/>
<vertex x="-1.43945625" y="-0.939578125"/>
<vertex x="-1.43864375" y="-0.944684375"/>
<vertex x="-1.44203125" y="-0.94935625"/>
<vertex x="-1.443496875" y="-0.95493125"/>
<vertex x="-1.447959375" y="-0.957534375"/>
<vertex x="-1.8034" y="-1.4478"/>
<vertex x="-1.701790625" y="-1.56210625"/>
<vertex x="-1.61290625" y="-1.650990625"/>
</polygon>
<text x="2.921" y="-2.032" size="0.762" layer="25" rot="R180">open source
  hardware</text>
</package>
</packages>
<symbols>
<symbol name="OSHWLOGO">
<wire x1="0" y1="0" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="17.78" y2="7.62" width="0.254" layer="94"/>
<wire x1="17.78" y1="7.62" x2="17.78" y2="0" width="0.254" layer="94"/>
<wire x1="17.78" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<text x="2.54" y="2.54" size="1.778" layer="94">OSHW Logo</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="OSHWLOGO" prefix="GOLD_ORB_SM">
<description>&lt;p&gt;The OSHW logo for PCB layout</description>
<gates>
<gate name="G$1" symbol="OSHWLOGO" x="0" y="0"/>
</gates>
<devices>
<device name="LOGO16MM" package="OSHW_16MM">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOGO8MM" package="OSHW_8MM">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOGO5MM" package="OSHW_5MM">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors" urn="urn:adsk.eagle:library:513">
<description>&lt;h3&gt;SparkFun Connectors&lt;/h3&gt;
This library contains electrically-functional connectors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="1X04_1MM_RA" urn="urn:adsk.eagle:footprint:37714/1" library_version="1">
<description>&lt;h3&gt;SMD- 4 Pin Right Angle &lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.5" y1="-4.6" x2="1.5" y2="-4.6" width="0.254" layer="21"/>
<wire x1="-3" y1="-2" x2="-3" y2="-0.35" width="0.254" layer="21"/>
<wire x1="2.25" y1="-0.35" x2="3" y2="-0.35" width="0.254" layer="21"/>
<wire x1="3" y1="-0.35" x2="3" y2="-2" width="0.254" layer="21"/>
<wire x1="-3" y1="-0.35" x2="-2.25" y2="-0.35" width="0.254" layer="21"/>
<circle x="-2.5" y="0.3" radius="0.1414" width="0.4" layer="21"/>
<smd name="NC2" x="-2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="NC1" x="2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="1" x="-1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="2" x="-0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="3" x="0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="4" x="1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<text x="-1.397" y="-2.159" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-3.302" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_1MM_RA_STRESSRELIEF" urn="urn:adsk.eagle:footprint:37987/1" library_version="1">
<description>Qwiic connector with milled cutout. Sliding the cable into this slot prevents the cable from coming unplugged.</description>
<wire x1="-1.5" y1="-4.6" x2="1.5" y2="-4.6" width="0.254" layer="21"/>
<wire x1="-3" y1="-2" x2="-3" y2="-0.35" width="0.254" layer="21"/>
<wire x1="2.25" y1="-0.35" x2="3" y2="-0.35" width="0.254" layer="21"/>
<wire x1="3" y1="-0.35" x2="3" y2="-2" width="0.254" layer="21"/>
<wire x1="-3" y1="-0.35" x2="-2.25" y2="-0.35" width="0.254" layer="21"/>
<wire x1="-2" y1="-10.16" x2="-2" y2="-8" width="0.3048" layer="20"/>
<wire x1="-2" y1="-8" x2="4" y2="-8" width="0.3048" layer="20"/>
<wire x1="4" y1="-8" x2="4" y2="-6" width="0.3048" layer="20"/>
<wire x1="4" y1="-6" x2="-4" y2="-6" width="0.3048" layer="20"/>
<wire x1="-4" y1="-6" x2="-4" y2="-10.16" width="0.3048" layer="20"/>
<circle x="-2.5" y="0.3" radius="0.1414" width="0.4" layer="21"/>
<smd name="NC2" x="-2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="NC1" x="2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="1" x="-1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="2" x="-0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="3" x="0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="4" x="1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<text x="-1.397" y="-2.159" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-3.302" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<rectangle x1="-4" y1="-8" x2="4" y2="-6" layer="46"/>
<rectangle x1="-4" y1="-10" x2="-2" y2="-8" layer="46"/>
</package>
</packages>
<packages3d>
<package3d name="1X04_1MM_RA" urn="urn:adsk.eagle:package:38096/1" type="box" library_version="1">
<description>SMD- 4 Pin Right Angle 
Specifications:
Pin count:4
Pin pitch:0.1"

Example device(s):
CONN_04
</description>
<packageinstances>
<packageinstance name="1X04_1MM_RA"/>
</packageinstances>
</package3d>
<package3d name="1X04_1MM_RA_STRESSRELIEF" urn="urn:adsk.eagle:package:38303/1" type="box" library_version="1">
<description>Qwiic connector with milled cutout. Sliding the cable into this slot prevents the cable from coming unplugged.</description>
<packageinstances>
<packageinstance name="1X04_1MM_RA_STRESSRELIEF"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="I2C_STANDARD-1" urn="urn:adsk.eagle:symbol:37986/1" library_version="1">
<description>&lt;h3&gt;SparkFun I&lt;sup&gt;2&lt;/sup&gt;C Standard Pinout Header&lt;/h3&gt;
&lt;p&gt;SparkFun has standardized on a pinout for all I&lt;sup&gt;2&lt;/sup&gt;C based sensor breakouts.&lt;br&gt;</description>
<wire x1="3.81" y1="-5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<text x="-5.08" y="-5.334" size="1.778" layer="96" font="vector" align="top-left">&gt;VALUE</text>
<text x="-5.08" y="7.874" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-4.572" y="2.54" size="1.778" layer="94" font="vector" align="center-left">SDA</text>
<text x="-4.572" y="0" size="1.778" layer="94" font="vector" align="center-left">VCC</text>
<text x="-4.572" y="-2.54" size="1.778" layer="94" font="vector" align="center-left">GND</text>
<text x="-4.572" y="5.08" size="1.778" layer="94" font="vector" align="center-left">SCL</text>
<pin name="GND" x="7.62" y="-2.54" visible="pad" length="middle" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="VCC" x="7.62" y="0" visible="pad" length="middle" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="SDA" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="SCL" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="QWIIC_CONNECTOR" urn="urn:adsk.eagle:component:38395/1" prefix="J" uservalue="yes" library_version="1">
<description>&lt;h3&gt;SparkFun I&lt;sup&gt;2&lt;/sup&gt;C Standard Qwiic Connector&lt;/h3&gt;
An SMD 1mm pitch JST connector makes it easy and quick (get it? Qwiic?) to connect I&lt;sup&gt;2&lt;/sup&gt;C devices to each other. The &lt;a href=”http://www.sparkfun.com/qwiic”&gt;Qwiic system&lt;/a&gt; enables fast and solderless connection between popular platforms and various sensors and actuators.

&lt;br&gt;&lt;br&gt;

We carry &lt;a href=”https://www.sparkfun.com/products/14204”&gt;200mm&lt;/a&gt;, &lt;a href=”https://www.sparkfun.com/products/14205”&gt;100mm&lt;/a&gt;, &lt;a href=”https://www.sparkfun.com/products/14206”&gt;50mm&lt;/a&gt;, and &lt;a href=”https://www.sparkfun.com/products/14207”&gt;breadboard friendly&lt;/a&gt; Qwiic cables. We also offer &lt;a href=”https://www.sparkfun.com/products/14323”&gt;10 pcs strips&lt;/a&gt; the SMD connectors.</description>
<gates>
<gate name="J1" symbol="I2C_STANDARD-1" x="2.54" y="0"/>
</gates>
<devices>
<device name="JS-1MM" package="1X04_1MM_RA">
<connects>
<connect gate="J1" pin="GND" pad="1"/>
<connect gate="J1" pin="SCL" pad="4"/>
<connect gate="J1" pin="SDA" pad="3"/>
<connect gate="J1" pin="VCC" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38096/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SR" package="1X04_1MM_RA_STRESSRELIEF">
<connects>
<connect gate="J1" pin="GND" pad="1"/>
<connect gate="J1" pin="SCL" pad="4"/>
<connect gate="J1" pin="SDA" pad="3"/>
<connect gate="J1" pin="VCC" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38303/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ch" urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A">
<packages>
<package name="SW_M" urn="urn:adsk.eagle:footprint:3097553/4" locally_modified="yes" library_version="6">
<description>WS-TSW-6x6 mm washable J-Bend SMD Tact Switch,4 pins</description>
<smd name="3" x="-3.35" y="-2.2" dx="1.7" dy="1.4" layer="1"/>
<smd name="4" x="3.35" y="-2.2" dx="1.7" dy="1.4" layer="1"/>
<smd name="1" x="-3.35" y="2.2" dx="1.7" dy="1.4" layer="1"/>
<smd name="2" x="3.35" y="2.2" dx="1.7" dy="1.4" layer="1"/>
<text x="1.408" y="4.1759" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="-4.567" y="-4.9334" size="1.27" layer="27">&gt;VALUE</text>
<text x="-1.84" y="1.455" size="1.27" layer="51">1</text>
<text x="1.305" y="-2.34" size="1.27" layer="51">4</text>
<text x="1.06" y="1.555" size="1.27" layer="51">2</text>
<text x="-1.64" y="-2.295" size="1.27" layer="51">3</text>
<wire x1="-3.1" y1="3.1" x2="-3.1" y2="-3.1" width="0.127" layer="51"/>
<wire x1="-3.1" y1="-3.1" x2="3.1" y2="-3.1" width="0.127" layer="21"/>
<wire x1="3.1" y1="-3.1" x2="3.1" y2="3.1" width="0.127" layer="51"/>
<wire x1="3.1" y1="3.1" x2="-3.1" y2="3.1" width="0.127" layer="21"/>
<wire x1="-3.1" y1="1.01" x2="-3.1" y2="-1.07" width="0.127" layer="21"/>
<wire x1="3.1" y1="1.02" x2="3.1" y2="-1.06" width="0.127" layer="21"/>
<polygon width="0.127" layer="39" pour="solid">
<vertex x="-4.38" y="-3.35"/>
<vertex x="4.38" y="-3.35"/>
<vertex x="4.38" y="3.35"/>
<vertex x="-4.38" y="3.35"/>
</polygon>
<circle x="-3.7" y="3.2" radius="0.05" width="0.2" layer="21"/>
</package>
<package name="PC">
<smd name="OUT+" x="11" y="5" dx="2.5" dy="2.5" layer="1"/>
<smd name="B+" x="11" y="2" dx="2.5" dy="2.5" layer="1"/>
<smd name="B-" x="11" y="-6" dx="2.5" dy="2.5" layer="1"/>
<smd name="OUT-" x="11" y="-9" dx="2.5" dy="2.5" layer="1"/>
<smd name="IN-" x="-12" y="-9" dx="2.5" dy="2.5" layer="1"/>
<smd name="IN+" x="-12" y="5" dx="2.5" dy="2.5" layer="1"/>
<rectangle x1="-13.5" y1="-10.5" x2="12.5" y2="6.5" layer="21"/>
</package>
<package name="BOOST">
<smd name="VIN+" x="15" y="4" dx="3" dy="5" layer="1" rot="R90"/>
<smd name="VIN-" x="15" y="-4" dx="3" dy="5" layer="1" rot="R90"/>
<smd name="VOUT-" x="-16" y="-4" dx="3" dy="5" layer="1" rot="R90"/>
<smd name="VOUT+" x="-16" y="4" dx="3" dy="5" layer="1" rot="R90"/>
<rectangle x1="-19" y1="-8.5" x2="18" y2="8.5" layer="21"/>
</package>
<package name="DOCKING_CONNECTOR" urn="urn:adsk.eagle:footprint:14805061/1" locally_modified="yes" library_version="2">
<description>&lt;B&gt;WR-PHD &lt;/B&gt;&lt;BR&gt;2.54 mm Dual Socket Header, 12 Pins</description>
<pad name="2" x="-6.35" y="1.27" drill="1.02"/>
<pad name="4" x="-3.81" y="1.27" drill="1.02"/>
<pad name="1" x="-6.35" y="-1.27" drill="1.02"/>
<pad name="3" x="-3.81" y="-1.27" drill="1.02"/>
<pad name="5" x="-1.27" y="-1.27" drill="1.02"/>
<pad name="6" x="-1.27" y="1.27" drill="1.02"/>
<pad name="7" x="1.27" y="-1.27" drill="1.02"/>
<pad name="8" x="1.27" y="1.27" drill="1.02"/>
<pad name="9" x="3.81" y="-1.27" drill="1.02"/>
<pad name="10" x="3.81" y="1.27" drill="1.02"/>
<pad name="11" x="6.35" y="-1.27" drill="1.02"/>
<pad name="12" x="6.35" y="1.27" drill="1.02"/>
<wire x1="7.87" y1="2.54" x2="-7.87" y2="2.54" width="0.1" layer="51"/>
<wire x1="-7.87" y1="2.54" x2="-7.87" y2="-2.54" width="0.1" layer="51"/>
<wire x1="-7.87" y1="-2.54" x2="7.87" y2="-2.54" width="0.1" layer="51"/>
<wire x1="7.87" y1="-2.54" x2="7.87" y2="2.54" width="0.1" layer="51"/>
<wire x1="-7.97" y1="2.64" x2="7.97" y2="2.64" width="0.2" layer="21"/>
<wire x1="7.97" y1="2.64" x2="7.97" y2="-2.64" width="0.2" layer="21"/>
<wire x1="7.97" y1="-2.64" x2="-7.97" y2="-2.64" width="0.2" layer="21"/>
<wire x1="-7.97" y1="-2.64" x2="-7.97" y2="2.64" width="0.2" layer="21"/>
<polygon width="0.1" layer="39" pour="solid">
<vertex x="-8.17" y="-2.84"/>
<vertex x="8.17" y="-2.84"/>
<vertex x="8.17" y="2.84"/>
<vertex x="-8.17" y="2.84"/>
</polygon>
<circle x="-7.48" y="-2" radius="0.1" width="0.2" layer="21"/>
<text x="-8.8276" y="0.5061" size="0.8128" layer="25" align="bottom-right">&gt;NAME</text>
<text x="-8.7476" y="-1.1942" size="0.8128" layer="27" align="bottom-right">&gt;VALUE</text>
</package>
<package name="GYRO" library_version="115">
<pad name="SDA" x="-5.08" y="0" drill="0.8" diameter="1.778" shape="long"/>
<pad name="XDA" x="-5.08" y="-2.54" drill="0.8" diameter="1.778" shape="long"/>
<pad name="SCL" x="-5.08" y="2.54" drill="0.8" diameter="1.778" shape="long"/>
<pad name="GND" x="-5.08" y="5.08" drill="0.8" diameter="1.778" shape="long"/>
<pad name="VCC" x="-5.08" y="7.62" drill="0.8" diameter="1.778" shape="long"/>
<pad name="XCL" x="-5.08" y="-5.08" drill="0.8" diameter="1.778" shape="long"/>
<pad name="ADO" x="-5.08" y="-7.62" drill="0.8" diameter="1.778" shape="long"/>
<pad name="INT" x="-5.08" y="-10.16" drill="0.8" diameter="1.778" shape="long"/>
<wire x1="-7.62" y1="9.525" x2="8.255" y2="9.525" width="0.127" layer="21"/>
<wire x1="8.255" y1="9.525" x2="8.255" y2="-11.43" width="0.127" layer="21"/>
<wire x1="8.255" y1="-11.43" x2="-7.62" y2="-11.43" width="0.127" layer="21"/>
<wire x1="-7.62" y1="-11.43" x2="-7.62" y2="9.525" width="0.127" layer="21"/>
<wire x1="1.27" y1="-10.16" x2="5.08" y2="-10.16" width="0.127" layer="21"/>
<wire x1="5.08" y1="-10.16" x2="4.445" y2="-9.525" width="0.127" layer="21"/>
<wire x1="5.08" y1="-10.16" x2="4.445" y2="-10.795" width="0.127" layer="21"/>
<wire x1="1.27" y1="-10.16" x2="1.27" y2="-6.35" width="0.127" layer="21"/>
<wire x1="1.27" y1="-6.35" x2="1.905" y2="-6.985" width="0.127" layer="21"/>
<wire x1="1.27" y1="-6.35" x2="0.635" y2="-6.985" width="0.127" layer="21"/>
<text x="5.715" y="-10.16" size="1.27" layer="21">x</text>
<text x="2.54" y="-6.35" size="1.27" layer="21">y</text>
<text x="7.62" y="-2.54" size="1.27" layer="21" rot="R90">ITG/MPU</text>
<rectangle x1="-7.7" y1="-11.5" x2="8.3" y2="9.6" layer="21"/>
</package>
<package name="GYRO_SMD" library_version="115">
<wire x1="-7.62" y1="9.525" x2="8.255" y2="9.525" width="0.127" layer="21"/>
<wire x1="8.255" y1="9.525" x2="8.255" y2="-11.43" width="0.127" layer="21"/>
<wire x1="8.255" y1="-11.43" x2="-7.62" y2="-11.43" width="0.127" layer="21"/>
<wire x1="-7.62" y1="-11.43" x2="-7.62" y2="9.525" width="0.127" layer="21"/>
<wire x1="1.27" y1="-10.16" x2="5.08" y2="-10.16" width="0.127" layer="21"/>
<wire x1="5.08" y1="-10.16" x2="4.445" y2="-9.525" width="0.127" layer="21"/>
<wire x1="5.08" y1="-10.16" x2="4.445" y2="-10.795" width="0.127" layer="21"/>
<wire x1="1.27" y1="-10.16" x2="1.27" y2="-6.35" width="0.127" layer="21"/>
<wire x1="1.27" y1="-6.35" x2="1.905" y2="-6.985" width="0.127" layer="21"/>
<wire x1="1.27" y1="-6.35" x2="0.635" y2="-6.985" width="0.127" layer="21"/>
<text x="5.715" y="-10.16" size="1.27" layer="21">x</text>
<text x="2.54" y="-6.35" size="1.27" layer="21">y</text>
<text x="7.62" y="-2.54" size="1.27" layer="21" rot="R90">ITG/MPU</text>
<rectangle x1="-7.7" y1="-11.5" x2="8.3" y2="9.6" layer="21"/>
<smd name="VCC" x="-5.08" y="7.62" dx="2.54" dy="1.27" layer="1"/>
<smd name="GND" x="-5.08" y="5.08" dx="2.54" dy="1.27" layer="1"/>
<smd name="SCL" x="-5.08" y="2.54" dx="2.54" dy="1.27" layer="1"/>
<smd name="SDA" x="-5.08" y="0" dx="2.54" dy="1.27" layer="1"/>
<smd name="XDA" x="-5.08" y="-2.54" dx="2.54" dy="1.27" layer="1"/>
<smd name="XCL" x="-5.08" y="-5.08" dx="2.54" dy="1.27" layer="1"/>
<smd name="ADO" x="-5.08" y="-7.62" dx="2.54" dy="1.27" layer="1"/>
<smd name="INT" x="-5.08" y="-10.16" dx="2.54" dy="1.27" layer="1"/>
</package>
<package name="BME280">
<smd name="GND" x="0" y="-2.54" dx="2.54" dy="1.27" layer="1"/>
<smd name="SCL" x="0" y="-5.08" dx="2.54" dy="1.27" layer="1"/>
<smd name="SDA" x="0" y="-7.62" dx="2.54" dy="1.27" layer="1"/>
<smd name="CSB" x="0" y="-10.16" dx="2.54" dy="1.27" layer="1"/>
<smd name="SDD" x="0" y="-12.7" dx="2.54" dy="1.27" layer="1"/>
<smd name="VCC" x="0" y="0" dx="2.54" dy="1.27" layer="1"/>
<rectangle x1="-2" y1="-13.7" x2="9.5" y2="1.3" layer="21"/>
</package>
<package name="MUTANTC_TEXT" library_version="120">
<rectangle x1="4.99" y1="-0.65" x2="5.09" y2="-0.55" layer="25"/>
<rectangle x1="10.49" y1="-0.65" x2="10.59" y2="-0.55" layer="25"/>
<rectangle x1="4.99" y1="-0.55" x2="5.29" y2="-0.45" layer="25"/>
<rectangle x1="10.49" y1="-0.55" x2="10.79" y2="-0.45" layer="25"/>
<rectangle x1="4.99" y1="-0.45" x2="5.39" y2="-0.35" layer="25"/>
<rectangle x1="10.49" y1="-0.45" x2="10.99" y2="-0.35" layer="25"/>
<rectangle x1="4.99" y1="-0.35" x2="5.39" y2="-0.25" layer="25"/>
<rectangle x1="10.49" y1="-0.35" x2="10.99" y2="-0.25" layer="25"/>
<rectangle x1="4.99" y1="-0.25" x2="5.39" y2="-0.15" layer="25"/>
<rectangle x1="10.49" y1="-0.25" x2="10.99" y2="-0.15" layer="25"/>
<rectangle x1="4.99" y1="-0.15" x2="5.39" y2="-0.05" layer="25"/>
<rectangle x1="10.49" y1="-0.15" x2="10.99" y2="-0.05" layer="25"/>
<rectangle x1="4.99" y1="-0.05" x2="5.39" y2="0.05" layer="25"/>
<rectangle x1="10.49" y1="-0.05" x2="10.99" y2="0.05" layer="25"/>
<rectangle x1="4.99" y1="0.05" x2="5.39" y2="0.15" layer="25"/>
<rectangle x1="10.49" y1="0.05" x2="10.99" y2="0.15" layer="25"/>
<rectangle x1="0.09" y1="0.15" x2="0.39" y2="0.25" layer="25"/>
<rectangle x1="3.29" y1="0.05" x2="4.39" y2="0.15" layer="25"/>
<rectangle x1="4.99" y1="0.15" x2="5.39" y2="0.25" layer="25"/>
<rectangle x1="7.19" y1="0.15" x2="7.49" y2="0.25" layer="25"/>
<rectangle x1="9.09" y1="0.15" x2="9.39" y2="0.25" layer="25"/>
<rectangle x1="10.49" y1="0.15" x2="10.99" y2="0.25" layer="25"/>
<rectangle x1="12.59" y1="0.15" x2="13.19" y2="0.25" layer="25"/>
<rectangle x1="0.09" y1="0.25" x2="0.59" y2="0.35" layer="25"/>
<rectangle x1="3.19" y1="0.15" x2="4.49" y2="0.25" layer="25"/>
<rectangle x1="4.99" y1="0.25" x2="5.39" y2="0.35" layer="25"/>
<rectangle x1="6.39" y1="0.25" x2="7.69" y2="0.35" layer="25"/>
<rectangle x1="7.99" y1="0.25" x2="8.39" y2="0.35" layer="25"/>
<rectangle x1="9.09" y1="0.25" x2="9.59" y2="0.35" layer="25"/>
<rectangle x1="10.49" y1="0.25" x2="10.99" y2="0.35" layer="25"/>
<rectangle x1="12.19" y1="0.25" x2="13.59" y2="0.35" layer="25"/>
<rectangle x1="0.09" y1="0.35" x2="0.59" y2="0.45" layer="25"/>
<rectangle x1="3.09" y1="0.25" x2="4.59" y2="0.35" layer="25"/>
<rectangle x1="4.99" y1="0.35" x2="5.39" y2="0.45" layer="25"/>
<rectangle x1="6.29" y1="0.35" x2="7.69" y2="0.45" layer="25"/>
<rectangle x1="7.99" y1="0.35" x2="8.39" y2="0.45" layer="25"/>
<rectangle x1="9.09" y1="0.35" x2="9.59" y2="0.45" layer="25"/>
<rectangle x1="10.49" y1="0.35" x2="10.99" y2="0.45" layer="25"/>
<rectangle x1="12.09" y1="0.35" x2="13.69" y2="0.45" layer="25"/>
<rectangle x1="0.09" y1="0.45" x2="0.59" y2="0.55" layer="25"/>
<rectangle x1="3.09" y1="0.35" x2="4.59" y2="0.45" layer="25"/>
<rectangle x1="4.99" y1="0.45" x2="5.39" y2="0.55" layer="25"/>
<rectangle x1="6.19" y1="0.45" x2="7.69" y2="0.55" layer="25"/>
<rectangle x1="7.99" y1="0.45" x2="8.39" y2="0.55" layer="25"/>
<rectangle x1="9.09" y1="0.45" x2="9.59" y2="0.55" layer="25"/>
<rectangle x1="10.49" y1="0.45" x2="10.99" y2="0.55" layer="25"/>
<rectangle x1="11.99" y1="0.45" x2="13.79" y2="0.55" layer="25"/>
<rectangle x1="0.09" y1="0.55" x2="0.59" y2="0.65" layer="25"/>
<rectangle x1="3.09" y1="0.45" x2="3.49" y2="0.55" layer="25"/>
<rectangle x1="4.19" y1="0.45" x2="4.59" y2="0.55" layer="25"/>
<rectangle x1="4.99" y1="0.55" x2="5.39" y2="0.65" layer="25"/>
<rectangle x1="6.19" y1="0.55" x2="7.69" y2="0.65" layer="25"/>
<rectangle x1="7.99" y1="0.55" x2="8.39" y2="0.65" layer="25"/>
<rectangle x1="9.09" y1="0.55" x2="9.59" y2="0.65" layer="25"/>
<rectangle x1="10.49" y1="0.55" x2="10.99" y2="0.65" layer="25"/>
<rectangle x1="11.99" y1="0.55" x2="13.79" y2="0.65" layer="25"/>
<rectangle x1="0.09" y1="0.65" x2="0.59" y2="0.75" layer="25"/>
<rectangle x1="3.09" y1="0.55" x2="3.49" y2="0.65" layer="25"/>
<rectangle x1="4.19" y1="0.55" x2="4.59" y2="0.65" layer="25"/>
<rectangle x1="4.99" y1="0.65" x2="5.39" y2="0.75" layer="25"/>
<rectangle x1="6.19" y1="0.65" x2="6.59" y2="0.75" layer="25"/>
<rectangle x1="7.19" y1="0.65" x2="7.69" y2="0.75" layer="25"/>
<rectangle x1="7.99" y1="0.65" x2="8.39" y2="0.75" layer="25"/>
<rectangle x1="9.09" y1="0.65" x2="9.59" y2="0.75" layer="25"/>
<rectangle x1="10.49" y1="0.65" x2="10.99" y2="0.75" layer="25"/>
<rectangle x1="11.99" y1="0.65" x2="12.39" y2="0.75" layer="25"/>
<rectangle x1="13.39" y1="0.65" x2="13.79" y2="0.75" layer="25"/>
<rectangle x1="0.09" y1="0.75" x2="0.59" y2="0.85" layer="25"/>
<rectangle x1="3.09" y1="0.65" x2="3.49" y2="0.75" layer="25"/>
<rectangle x1="4.19" y1="0.65" x2="4.59" y2="0.75" layer="25"/>
<rectangle x1="4.99" y1="0.75" x2="5.39" y2="0.85" layer="25"/>
<rectangle x1="6.19" y1="0.75" x2="6.59" y2="0.85" layer="25"/>
<rectangle x1="7.19" y1="0.75" x2="7.69" y2="0.85" layer="25"/>
<rectangle x1="7.99" y1="0.75" x2="8.39" y2="0.85" layer="25"/>
<rectangle x1="9.09" y1="0.75" x2="9.59" y2="0.85" layer="25"/>
<rectangle x1="10.49" y1="0.75" x2="10.99" y2="0.85" layer="25"/>
<rectangle x1="11.99" y1="0.75" x2="12.39" y2="0.85" layer="25"/>
<rectangle x1="0.09" y1="0.85" x2="0.59" y2="0.95" layer="25"/>
<rectangle x1="3.09" y1="0.75" x2="3.49" y2="0.85" layer="25"/>
<rectangle x1="4.19" y1="0.75" x2="4.59" y2="0.85" layer="25"/>
<rectangle x1="4.99" y1="0.85" x2="5.39" y2="0.95" layer="25"/>
<rectangle x1="6.19" y1="0.85" x2="6.59" y2="0.95" layer="25"/>
<rectangle x1="7.19" y1="0.85" x2="7.69" y2="0.95" layer="25"/>
<rectangle x1="7.99" y1="0.85" x2="8.39" y2="0.95" layer="25"/>
<rectangle x1="9.09" y1="0.85" x2="9.59" y2="0.95" layer="25"/>
<rectangle x1="10.49" y1="0.85" x2="10.99" y2="0.95" layer="25"/>
<rectangle x1="11.99" y1="0.85" x2="12.39" y2="0.95" layer="25"/>
<rectangle x1="0.09" y1="0.95" x2="0.59" y2="1.05" layer="25"/>
<rectangle x1="3.09" y1="0.85" x2="3.49" y2="0.95" layer="25"/>
<rectangle x1="4.19" y1="0.85" x2="4.59" y2="0.95" layer="25"/>
<rectangle x1="4.99" y1="0.95" x2="5.39" y2="1.05" layer="25"/>
<rectangle x1="6.19" y1="0.95" x2="6.59" y2="1.05" layer="25"/>
<rectangle x1="7.19" y1="0.95" x2="7.69" y2="1.05" layer="25"/>
<rectangle x1="7.99" y1="0.95" x2="8.39" y2="1.05" layer="25"/>
<rectangle x1="11.99" y1="0.95" x2="12.39" y2="1.05" layer="25"/>
<rectangle x1="0.09" y1="1.05" x2="0.59" y2="1.15" layer="25"/>
<rectangle x1="1.29" y1="1.05" x2="1.69" y2="1.15" layer="25"/>
<rectangle x1="2.39" y1="1.05" x2="2.89" y2="1.15" layer="25"/>
<rectangle x1="3.09" y1="0.95" x2="3.49" y2="1.05" layer="25"/>
<rectangle x1="4.19" y1="0.95" x2="4.59" y2="1.05" layer="25"/>
<rectangle x1="4.99" y1="1.05" x2="5.39" y2="1.15" layer="25"/>
<rectangle x1="6.19" y1="1.05" x2="6.59" y2="1.15" layer="25"/>
<rectangle x1="7.19" y1="1.05" x2="7.69" y2="1.15" layer="25"/>
<rectangle x1="7.99" y1="1.05" x2="8.39" y2="1.15" layer="25"/>
<rectangle x1="11.99" y1="1.05" x2="12.39" y2="1.15" layer="25"/>
<rectangle x1="0.09" y1="1.15" x2="0.59" y2="1.25" layer="25"/>
<rectangle x1="1.29" y1="1.15" x2="1.69" y2="1.25" layer="25"/>
<rectangle x1="2.39" y1="1.15" x2="2.89" y2="1.25" layer="25"/>
<rectangle x1="3.09" y1="1.05" x2="3.49" y2="1.15" layer="25"/>
<rectangle x1="4.19" y1="1.05" x2="4.59" y2="1.15" layer="25"/>
<rectangle x1="4.99" y1="1.15" x2="5.39" y2="1.25" layer="25"/>
<rectangle x1="6.19" y1="1.15" x2="6.69" y2="1.25" layer="25"/>
<rectangle x1="7.09" y1="1.15" x2="7.69" y2="1.25" layer="25"/>
<rectangle x1="7.99" y1="1.15" x2="8.39" y2="1.25" layer="25"/>
<rectangle x1="11.99" y1="1.15" x2="12.39" y2="1.25" layer="25"/>
<rectangle x1="13.49" y1="1.15" x2="13.79" y2="1.25" layer="25"/>
<rectangle x1="0.09" y1="1.25" x2="0.59" y2="1.35" layer="25"/>
<rectangle x1="1.29" y1="1.25" x2="1.69" y2="1.35" layer="25"/>
<rectangle x1="2.39" y1="1.25" x2="2.89" y2="1.35" layer="25"/>
<rectangle x1="3.09" y1="1.15" x2="3.49" y2="1.25" layer="25"/>
<rectangle x1="4.99" y1="1.25" x2="5.39" y2="1.35" layer="25"/>
<rectangle x1="6.19" y1="1.25" x2="7.69" y2="1.35" layer="25"/>
<rectangle x1="7.99" y1="1.25" x2="8.39" y2="1.35" layer="25"/>
<rectangle x1="11.99" y1="1.25" x2="12.39" y2="1.35" layer="25"/>
<rectangle x1="0.09" y1="1.35" x2="0.59" y2="1.45" layer="25"/>
<rectangle x1="1.29" y1="1.35" x2="1.69" y2="1.45" layer="25"/>
<rectangle x1="2.39" y1="1.35" x2="2.89" y2="1.45" layer="25"/>
<rectangle x1="3.09" y1="1.25" x2="3.49" y2="1.35" layer="25"/>
<rectangle x1="4.99" y1="1.35" x2="5.39" y2="1.45" layer="25"/>
<rectangle x1="6.19" y1="1.35" x2="7.69" y2="1.45" layer="25"/>
<rectangle x1="7.99" y1="1.35" x2="8.39" y2="1.45" layer="25"/>
<rectangle x1="9.09" y1="1.35" x2="9.59" y2="1.45" layer="25"/>
<rectangle x1="10.49" y1="1.35" x2="10.99" y2="1.45" layer="25"/>
<rectangle x1="11.99" y1="1.35" x2="12.39" y2="1.45" layer="25"/>
<rectangle x1="0.09" y1="1.45" x2="0.59" y2="1.55" layer="25"/>
<rectangle x1="1.29" y1="1.45" x2="1.69" y2="1.55" layer="25"/>
<rectangle x1="2.39" y1="1.45" x2="2.89" y2="1.55" layer="25"/>
<rectangle x1="3.09" y1="1.35" x2="3.49" y2="1.45" layer="25"/>
<rectangle x1="4.99" y1="1.45" x2="5.39" y2="1.55" layer="25"/>
<rectangle x1="6.29" y1="1.45" x2="7.69" y2="1.55" layer="25"/>
<rectangle x1="7.99" y1="1.45" x2="8.39" y2="1.55" layer="25"/>
<rectangle x1="9.09" y1="1.45" x2="9.59" y2="1.55" layer="25"/>
<rectangle x1="10.49" y1="1.45" x2="10.99" y2="1.55" layer="25"/>
<rectangle x1="11.99" y1="1.45" x2="12.39" y2="1.55" layer="25"/>
<rectangle x1="0.09" y1="1.55" x2="0.59" y2="1.65" layer="25"/>
<rectangle x1="1.29" y1="1.55" x2="1.69" y2="1.65" layer="25"/>
<rectangle x1="2.39" y1="1.55" x2="2.89" y2="1.65" layer="25"/>
<rectangle x1="3.09" y1="1.45" x2="3.49" y2="1.55" layer="25"/>
<rectangle x1="4.99" y1="1.55" x2="5.39" y2="1.65" layer="25"/>
<rectangle x1="6.49" y1="1.55" x2="7.69" y2="1.65" layer="25"/>
<rectangle x1="7.99" y1="1.55" x2="8.39" y2="1.65" layer="25"/>
<rectangle x1="9.09" y1="1.55" x2="9.59" y2="1.65" layer="25"/>
<rectangle x1="10.49" y1="1.55" x2="10.99" y2="1.65" layer="25"/>
<rectangle x1="11.99" y1="1.55" x2="12.39" y2="1.65" layer="25"/>
<rectangle x1="0.09" y1="1.65" x2="0.59" y2="1.75" layer="25"/>
<rectangle x1="1.29" y1="1.65" x2="1.69" y2="1.75" layer="25"/>
<rectangle x1="2.39" y1="1.65" x2="2.89" y2="1.75" layer="25"/>
<rectangle x1="3.09" y1="1.55" x2="3.49" y2="1.65" layer="25"/>
<rectangle x1="4.99" y1="1.65" x2="5.39" y2="1.75" layer="25"/>
<rectangle x1="7.19" y1="1.65" x2="7.69" y2="1.75" layer="25"/>
<rectangle x1="7.99" y1="1.65" x2="8.49" y2="1.75" layer="25"/>
<rectangle x1="9.09" y1="1.65" x2="9.59" y2="1.75" layer="25"/>
<rectangle x1="10.49" y1="1.65" x2="10.99" y2="1.75" layer="25"/>
<rectangle x1="11.99" y1="1.65" x2="12.39" y2="1.75" layer="25"/>
<rectangle x1="0.09" y1="1.75" x2="0.59" y2="1.85" layer="25"/>
<rectangle x1="1.19" y1="1.75" x2="1.79" y2="1.85" layer="25"/>
<rectangle x1="2.39" y1="1.75" x2="2.79" y2="1.85" layer="25"/>
<rectangle x1="3.09" y1="1.65" x2="3.49" y2="1.75" layer="25"/>
<rectangle x1="4.99" y1="1.75" x2="5.39" y2="1.85" layer="25"/>
<rectangle x1="7.19" y1="1.75" x2="7.69" y2="1.85" layer="25"/>
<rectangle x1="7.99" y1="1.75" x2="8.49" y2="1.85" layer="25"/>
<rectangle x1="9.09" y1="1.75" x2="9.59" y2="1.85" layer="25"/>
<rectangle x1="10.49" y1="1.75" x2="10.99" y2="1.85" layer="25"/>
<rectangle x1="11.99" y1="1.75" x2="12.39" y2="1.85" layer="25"/>
<rectangle x1="0.19" y1="1.85" x2="2.79" y2="1.95" layer="25"/>
<rectangle x1="3.09" y1="1.75" x2="3.49" y2="1.85" layer="25"/>
<rectangle x1="4.69" y1="1.85" x2="6.09" y2="1.95" layer="25"/>
<rectangle x1="6.39" y1="1.85" x2="7.69" y2="1.95" layer="25"/>
<rectangle x1="7.99" y1="1.85" x2="9.59" y2="1.95" layer="25"/>
<rectangle x1="9.79" y1="1.85" x2="11.59" y2="1.95" layer="25"/>
<rectangle x1="11.99" y1="1.85" x2="12.39" y2="1.95" layer="25"/>
<rectangle x1="0.19" y1="1.95" x2="2.79" y2="2.05" layer="25"/>
<rectangle x1="3.09" y1="1.85" x2="3.49" y2="1.95" layer="25"/>
<rectangle x1="6.49" y1="1.95" x2="7.59" y2="2.05" layer="25"/>
<rectangle x1="8.09" y1="1.95" x2="9.49" y2="2.05" layer="25"/>
<rectangle x1="9.79" y1="1.95" x2="11.69" y2="2.05" layer="25"/>
<rectangle x1="11.99" y1="1.95" x2="12.39" y2="2.05" layer="25"/>
<rectangle x1="13.39" y1="1.95" x2="13.59" y2="2.05" layer="25"/>
<rectangle x1="0.29" y1="2.05" x2="2.69" y2="2.15" layer="25"/>
<rectangle x1="3.09" y1="1.95" x2="3.49" y2="2.05" layer="25"/>
<rectangle x1="4.69" y1="2.05" x2="6.19" y2="2.15" layer="25"/>
<rectangle x1="6.49" y1="2.05" x2="7.59" y2="2.15" layer="25"/>
<rectangle x1="8.09" y1="2.05" x2="9.49" y2="2.15" layer="25"/>
<rectangle x1="9.79" y1="2.05" x2="11.69" y2="2.15" layer="25"/>
<rectangle x1="11.99" y1="2.05" x2="12.39" y2="2.15" layer="25"/>
<rectangle x1="0.39" y1="2.15" x2="1.39" y2="2.25" layer="25"/>
<rectangle x1="1.59" y1="2.15" x2="2.59" y2="2.25" layer="25"/>
<rectangle x1="4.69" y1="2.15" x2="6.19" y2="2.25" layer="25"/>
<rectangle x1="6.59" y1="2.15" x2="7.49" y2="2.25" layer="25"/>
<rectangle x1="8.19" y1="2.15" x2="9.29" y2="2.25" layer="25"/>
<rectangle x1="9.89" y1="2.15" x2="11.69" y2="2.25" layer="25"/>
<rectangle x1="11.99" y1="2.15" x2="12.39" y2="2.25" layer="25"/>
<rectangle x1="11.99" y1="2.25" x2="12.39" y2="2.35" layer="25"/>
<rectangle x1="11.99" y1="2.35" x2="12.39" y2="2.45" layer="25"/>
<rectangle x1="11.99" y1="2.45" x2="12.39" y2="2.55" layer="25"/>
<rectangle x1="13.39" y1="2.45" x2="13.79" y2="2.55" layer="25"/>
<rectangle x1="11.99" y1="2.55" x2="13.79" y2="2.65" layer="25"/>
<rectangle x1="11.99" y1="2.65" x2="13.79" y2="2.75" layer="25"/>
<rectangle x1="12.09" y1="2.75" x2="13.69" y2="2.85" layer="25"/>
<rectangle x1="12.19" y1="2.85" x2="13.59" y2="2.95" layer="25"/>
<rectangle x1="12.49" y1="2.95" x2="13.29" y2="3.05" layer="25"/>
<rectangle x1="7.19" y1="0.05" x2="7.29" y2="0.15" layer="25"/>
<rectangle x1="7.99" y1="0.05" x2="8.09" y2="0.15" layer="25"/>
<rectangle x1="9.09" y1="0.05" x2="9.19" y2="0.15" layer="25"/>
<rectangle x1="0.09" y1="0.05" x2="0.19" y2="0.15" layer="25"/>
<rectangle x1="3.39" y1="2.15" x2="3.49" y2="2.25" layer="25"/>
<rectangle x1="6.19" y1="2.15" x2="6.29" y2="2.25" layer="25"/>
<rectangle x1="9.79" y1="2.15" x2="9.89" y2="2.25" layer="25"/>
<rectangle x1="11.69" y1="2.15" x2="11.79" y2="2.25" layer="25"/>
<rectangle x1="13.39" y1="1.85" x2="13.49" y2="1.95" layer="25"/>
<rectangle x1="13.69" y1="1.25" x2="13.79" y2="1.35" layer="25"/>
<rectangle x1="13.39" y1="2.35" x2="13.79" y2="2.45" layer="25"/>
<rectangle x1="13.39" y1="2.25" x2="13.79" y2="2.35" layer="25"/>
<rectangle x1="13.39" y1="2.15" x2="13.79" y2="2.25" layer="25"/>
<rectangle x1="13.39" y1="2.05" x2="13.79" y2="2.15" layer="25"/>
<rectangle x1="13.39" y1="0.75" x2="13.79" y2="0.85" layer="25"/>
<rectangle x1="13.39" y1="0.85" x2="13.79" y2="0.95" layer="25"/>
<rectangle x1="13.39" y1="0.95" x2="13.79" y2="1.05" layer="25"/>
<rectangle x1="13.39" y1="1.05" x2="13.79" y2="1.15" layer="25"/>
<rectangle x1="13.29" y1="0.65" x2="13.39" y2="0.75" layer="25"/>
<rectangle x1="12.39" y1="0.65" x2="12.49" y2="0.75" layer="25"/>
<rectangle x1="12.39" y1="0.75" x2="12.49" y2="0.85" layer="25"/>
<rectangle x1="12.49" y1="0.65" x2="12.59" y2="0.75" layer="25"/>
<rectangle x1="12.39" y1="2.45" x2="12.49" y2="2.55" layer="25"/>
<rectangle x1="12.39" y1="2.35" x2="12.49" y2="2.45" layer="25"/>
<rectangle x1="12.49" y1="2.45" x2="12.59" y2="2.55" layer="25"/>
<rectangle x1="13.29" y1="2.45" x2="13.39" y2="2.55" layer="25"/>
<rectangle x1="13.59" y1="1.95" x2="13.69" y2="2.05" layer="25"/>
<rectangle x1="4.69" y1="1.95" x2="6.19" y2="2.05" layer="25"/>
<rectangle x1="3.19" y1="2.05" x2="3.49" y2="2.15" layer="25"/>
<rectangle x1="7.99" y1="0.15" x2="8.29" y2="0.25" layer="25"/>
<rectangle x1="12.39" y1="0.15" x2="12.69" y2="0.25" layer="25"/>
<rectangle x1="12.39" y1="0.85" x2="12.49" y2="0.95" layer="25"/>
<rectangle x1="12.39" y1="2.27" x2="12.49" y2="2.37" layer="25"/>
</package>
<package name="MUTANTC_LOGO" library_version="120">
<rectangle x1="1.87" y1="0.05" x2="1.93" y2="0.07" layer="25"/>
<rectangle x1="1.61" y1="0.07" x2="2.17" y2="0.09" layer="25"/>
<rectangle x1="1.51" y1="0.09" x2="2.27" y2="0.11" layer="25"/>
<rectangle x1="1.43" y1="0.11" x2="2.35" y2="0.13" layer="25"/>
<rectangle x1="1.35" y1="0.13" x2="2.41" y2="0.15" layer="25"/>
<rectangle x1="1.29" y1="0.15" x2="2.47" y2="0.17" layer="25"/>
<rectangle x1="1.25" y1="0.17" x2="2.53" y2="0.19" layer="25"/>
<rectangle x1="1.19" y1="0.19" x2="2.59" y2="0.21" layer="25"/>
<rectangle x1="1.15" y1="0.21" x2="2.63" y2="0.23" layer="25"/>
<rectangle x1="1.11" y1="0.23" x2="2.67" y2="0.25" layer="25"/>
<rectangle x1="1.07" y1="0.25" x2="2.71" y2="0.27" layer="25"/>
<rectangle x1="1.03" y1="0.27" x2="1.31" y2="0.29" layer="25"/>
<rectangle x1="1.53" y1="0.27" x2="2.75" y2="0.29" layer="25"/>
<rectangle x1="0.99" y1="0.29" x2="1.21" y2="0.31" layer="25"/>
<rectangle x1="1.63" y1="0.29" x2="2.79" y2="0.31" layer="25"/>
<rectangle x1="0.95" y1="0.31" x2="1.15" y2="0.33" layer="25"/>
<rectangle x1="1.69" y1="0.31" x2="2.81" y2="0.33" layer="25"/>
<rectangle x1="0.93" y1="0.33" x2="1.11" y2="0.35" layer="25"/>
<rectangle x1="1.73" y1="0.33" x2="2.85" y2="0.35" layer="25"/>
<rectangle x1="0.89" y1="0.35" x2="1.05" y2="0.37" layer="25"/>
<rectangle x1="1.77" y1="0.35" x2="2.87" y2="0.37" layer="25"/>
<rectangle x1="0.87" y1="0.37" x2="1.03" y2="0.39" layer="25"/>
<rectangle x1="1.81" y1="0.37" x2="2.91" y2="0.39" layer="25"/>
<rectangle x1="0.83" y1="0.39" x2="0.99" y2="0.41" layer="25"/>
<rectangle x1="1.85" y1="0.39" x2="2.93" y2="0.41" layer="25"/>
<rectangle x1="0.81" y1="0.41" x2="0.95" y2="0.43" layer="25"/>
<rectangle x1="1.87" y1="0.41" x2="2.97" y2="0.43" layer="25"/>
<rectangle x1="0.79" y1="0.43" x2="0.93" y2="0.45" layer="25"/>
<rectangle x1="1.89" y1="0.43" x2="2.99" y2="0.45" layer="25"/>
<rectangle x1="0.75" y1="0.45" x2="0.91" y2="0.47" layer="25"/>
<rectangle x1="1.93" y1="0.45" x2="3.01" y2="0.47" layer="25"/>
<rectangle x1="0.73" y1="0.47" x2="0.89" y2="0.49" layer="25"/>
<rectangle x1="1.95" y1="0.47" x2="3.03" y2="0.49" layer="25"/>
<rectangle x1="0.71" y1="0.49" x2="0.87" y2="0.51" layer="25"/>
<rectangle x1="1.97" y1="0.49" x2="3.05" y2="0.51" layer="25"/>
<rectangle x1="0.69" y1="0.51" x2="0.85" y2="0.53" layer="25"/>
<rectangle x1="1.99" y1="0.51" x2="3.09" y2="0.53" layer="25"/>
<rectangle x1="0.67" y1="0.53" x2="0.83" y2="0.55" layer="25"/>
<rectangle x1="2.01" y1="0.53" x2="3.11" y2="0.55" layer="25"/>
<rectangle x1="0.65" y1="0.55" x2="0.81" y2="0.57" layer="25"/>
<rectangle x1="2.01" y1="0.55" x2="3.13" y2="0.57" layer="25"/>
<rectangle x1="0.63" y1="0.57" x2="0.79" y2="0.59" layer="25"/>
<rectangle x1="2.03" y1="0.57" x2="3.15" y2="0.59" layer="25"/>
<rectangle x1="0.61" y1="0.59" x2="0.77" y2="0.61" layer="25"/>
<rectangle x1="2.05" y1="0.59" x2="3.17" y2="0.61" layer="25"/>
<rectangle x1="0.59" y1="0.61" x2="0.77" y2="0.63" layer="25"/>
<rectangle x1="2.07" y1="0.61" x2="3.19" y2="0.63" layer="25"/>
<rectangle x1="0.57" y1="0.63" x2="0.75" y2="0.65" layer="25"/>
<rectangle x1="2.07" y1="0.63" x2="3.21" y2="0.65" layer="25"/>
<rectangle x1="0.55" y1="0.65" x2="0.73" y2="0.67" layer="25"/>
<rectangle x1="2.09" y1="0.65" x2="3.21" y2="0.67" layer="25"/>
<rectangle x1="0.53" y1="0.67" x2="0.73" y2="0.69" layer="25"/>
<rectangle x1="2.09" y1="0.67" x2="3.23" y2="0.69" layer="25"/>
<rectangle x1="0.51" y1="0.69" x2="0.71" y2="0.71" layer="25"/>
<rectangle x1="2.11" y1="0.69" x2="3.25" y2="0.71" layer="25"/>
<rectangle x1="0.49" y1="0.71" x2="0.71" y2="0.73" layer="25"/>
<rectangle x1="2.11" y1="0.71" x2="3.27" y2="0.73" layer="25"/>
<rectangle x1="0.47" y1="0.73" x2="0.69" y2="0.75" layer="25"/>
<rectangle x1="2.13" y1="0.73" x2="3.29" y2="0.75" layer="25"/>
<rectangle x1="0.45" y1="0.75" x2="0.69" y2="0.77" layer="25"/>
<rectangle x1="2.13" y1="0.75" x2="3.31" y2="0.77" layer="25"/>
<rectangle x1="0.45" y1="0.77" x2="0.67" y2="0.79" layer="25"/>
<rectangle x1="2.13" y1="0.77" x2="3.31" y2="0.79" layer="25"/>
<rectangle x1="0.43" y1="0.79" x2="0.67" y2="0.81" layer="25"/>
<rectangle x1="2.15" y1="0.79" x2="3.33" y2="0.81" layer="25"/>
<rectangle x1="0.41" y1="0.81" x2="0.65" y2="0.83" layer="25"/>
<rectangle x1="2.15" y1="0.81" x2="2.67" y2="0.83" layer="25"/>
<rectangle x1="2.71" y1="0.81" x2="3.35" y2="0.83" layer="25"/>
<rectangle x1="0.41" y1="0.83" x2="0.65" y2="0.85" layer="25"/>
<rectangle x1="2.15" y1="0.83" x2="2.65" y2="0.85" layer="25"/>
<rectangle x1="2.75" y1="0.83" x2="3.37" y2="0.85" layer="25"/>
<rectangle x1="0.39" y1="0.85" x2="0.65" y2="0.87" layer="25"/>
<rectangle x1="2.17" y1="0.85" x2="2.63" y2="0.87" layer="25"/>
<rectangle x1="2.77" y1="0.85" x2="3.37" y2="0.87" layer="25"/>
<rectangle x1="0.37" y1="0.87" x2="0.65" y2="0.89" layer="25"/>
<rectangle x1="2.17" y1="0.87" x2="2.61" y2="0.89" layer="25"/>
<rectangle x1="2.81" y1="0.87" x2="3.39" y2="0.89" layer="25"/>
<rectangle x1="0.35" y1="0.89" x2="0.63" y2="0.91" layer="25"/>
<rectangle x1="2.17" y1="0.89" x2="2.61" y2="0.91" layer="25"/>
<rectangle x1="2.83" y1="0.89" x2="3.39" y2="0.91" layer="25"/>
<rectangle x1="0.35" y1="0.91" x2="0.63" y2="0.93" layer="25"/>
<rectangle x1="2.17" y1="0.91" x2="2.57" y2="0.93" layer="25"/>
<rectangle x1="2.85" y1="0.91" x2="3.41" y2="0.93" layer="25"/>
<rectangle x1="0.33" y1="0.93" x2="0.63" y2="0.95" layer="25"/>
<rectangle x1="2.17" y1="0.93" x2="2.47" y2="0.95" layer="25"/>
<rectangle x1="2.89" y1="0.93" x2="3.43" y2="0.95" layer="25"/>
<rectangle x1="0.33" y1="0.95" x2="0.63" y2="0.97" layer="25"/>
<rectangle x1="2.17" y1="0.95" x2="2.35" y2="0.97" layer="25"/>
<rectangle x1="2.91" y1="0.95" x2="3.43" y2="0.97" layer="25"/>
<rectangle x1="0.31" y1="0.97" x2="0.63" y2="0.99" layer="25"/>
<rectangle x1="2.19" y1="0.97" x2="2.25" y2="0.99" layer="25"/>
<rectangle x1="2.93" y1="0.97" x2="3.45" y2="0.99" layer="25"/>
<rectangle x1="0.29" y1="0.99" x2="0.61" y2="1.01" layer="25"/>
<rectangle x1="2.91" y1="0.99" x2="3.45" y2="1.01" layer="25"/>
<rectangle x1="0.29" y1="1.01" x2="0.61" y2="1.03" layer="25"/>
<rectangle x1="2.89" y1="1.01" x2="3.47" y2="1.03" layer="25"/>
<rectangle x1="0.27" y1="1.03" x2="0.61" y2="1.05" layer="25"/>
<rectangle x1="2.87" y1="1.03" x2="3.47" y2="1.05" layer="25"/>
<rectangle x1="0.27" y1="1.05" x2="0.61" y2="1.07" layer="25"/>
<rectangle x1="2.87" y1="1.05" x2="3.49" y2="1.07" layer="25"/>
<rectangle x1="0.25" y1="1.07" x2="0.61" y2="1.09" layer="25"/>
<rectangle x1="2.85" y1="1.07" x2="3.49" y2="1.09" layer="25"/>
<rectangle x1="0.25" y1="1.09" x2="0.61" y2="1.11" layer="25"/>
<rectangle x1="2.83" y1="1.09" x2="3.51" y2="1.11" layer="25"/>
<rectangle x1="0.23" y1="1.11" x2="0.61" y2="1.13" layer="25"/>
<rectangle x1="2.81" y1="1.11" x2="3.51" y2="1.13" layer="25"/>
<rectangle x1="0.23" y1="1.13" x2="0.61" y2="1.15" layer="25"/>
<rectangle x1="2.81" y1="1.13" x2="3.53" y2="1.15" layer="25"/>
<rectangle x1="0.21" y1="1.15" x2="0.61" y2="1.17" layer="25"/>
<rectangle x1="2.79" y1="1.15" x2="3.53" y2="1.17" layer="25"/>
<rectangle x1="0.21" y1="1.17" x2="0.63" y2="1.19" layer="25"/>
<rectangle x1="2.77" y1="1.17" x2="3.55" y2="1.19" layer="25"/>
<rectangle x1="0.21" y1="1.19" x2="0.63" y2="1.21" layer="25"/>
<rectangle x1="2.75" y1="1.19" x2="3.55" y2="1.21" layer="25"/>
<rectangle x1="0.19" y1="1.21" x2="0.63" y2="1.23" layer="25"/>
<rectangle x1="2.75" y1="1.21" x2="3.55" y2="1.23" layer="25"/>
<rectangle x1="0.19" y1="1.23" x2="0.63" y2="1.25" layer="25"/>
<rectangle x1="2.31" y1="1.23" x2="2.35" y2="1.25" layer="25"/>
<rectangle x1="2.73" y1="1.23" x2="3.57" y2="1.25" layer="25"/>
<rectangle x1="0.17" y1="1.25" x2="0.63" y2="1.27" layer="25"/>
<rectangle x1="2.21" y1="1.25" x2="2.33" y2="1.27" layer="25"/>
<rectangle x1="2.71" y1="1.25" x2="3.57" y2="1.27" layer="25"/>
<rectangle x1="0.17" y1="1.27" x2="0.63" y2="1.29" layer="25"/>
<rectangle x1="2.15" y1="1.27" x2="2.31" y2="1.29" layer="25"/>
<rectangle x1="2.69" y1="1.27" x2="3.59" y2="1.29" layer="25"/>
<rectangle x1="0.17" y1="1.29" x2="0.65" y2="1.31" layer="25"/>
<rectangle x1="2.15" y1="1.29" x2="2.29" y2="1.31" layer="25"/>
<rectangle x1="2.69" y1="1.29" x2="3.59" y2="1.31" layer="25"/>
<rectangle x1="0.15" y1="1.31" x2="0.65" y2="1.33" layer="25"/>
<rectangle x1="2.13" y1="1.31" x2="2.29" y2="1.33" layer="25"/>
<rectangle x1="2.69" y1="1.31" x2="3.59" y2="1.33" layer="25"/>
<rectangle x1="0.15" y1="1.33" x2="0.65" y2="1.35" layer="25"/>
<rectangle x1="2.13" y1="1.33" x2="2.27" y2="1.35" layer="25"/>
<rectangle x1="2.69" y1="1.33" x2="3.61" y2="1.35" layer="25"/>
<rectangle x1="0.15" y1="1.35" x2="0.67" y2="1.37" layer="25"/>
<rectangle x1="2.13" y1="1.35" x2="2.25" y2="1.37" layer="25"/>
<rectangle x1="2.69" y1="1.35" x2="3.61" y2="1.37" layer="25"/>
<rectangle x1="0.13" y1="1.37" x2="0.67" y2="1.39" layer="25"/>
<rectangle x1="2.11" y1="1.37" x2="2.23" y2="1.39" layer="25"/>
<rectangle x1="2.69" y1="1.37" x2="3.61" y2="1.39" layer="25"/>
<rectangle x1="0.13" y1="1.39" x2="0.67" y2="1.41" layer="25"/>
<rectangle x1="2.11" y1="1.39" x2="2.23" y2="1.41" layer="25"/>
<rectangle x1="2.69" y1="1.39" x2="3.61" y2="1.41" layer="25"/>
<rectangle x1="0.13" y1="1.41" x2="0.69" y2="1.43" layer="25"/>
<rectangle x1="2.09" y1="1.41" x2="2.21" y2="1.43" layer="25"/>
<rectangle x1="2.69" y1="1.41" x2="3.63" y2="1.43" layer="25"/>
<rectangle x1="0.13" y1="1.43" x2="0.69" y2="1.45" layer="25"/>
<rectangle x1="2.09" y1="1.43" x2="2.19" y2="1.45" layer="25"/>
<rectangle x1="2.69" y1="1.43" x2="3.63" y2="1.45" layer="25"/>
<rectangle x1="0.11" y1="1.45" x2="0.71" y2="1.47" layer="25"/>
<rectangle x1="2.07" y1="1.45" x2="2.17" y2="1.47" layer="25"/>
<rectangle x1="2.69" y1="1.45" x2="3.63" y2="1.47" layer="25"/>
<rectangle x1="0.11" y1="1.47" x2="0.71" y2="1.49" layer="25"/>
<rectangle x1="2.07" y1="1.47" x2="2.15" y2="1.49" layer="25"/>
<rectangle x1="2.69" y1="1.47" x2="3.63" y2="1.49" layer="25"/>
<rectangle x1="0.11" y1="1.49" x2="0.73" y2="1.51" layer="25"/>
<rectangle x1="2.05" y1="1.49" x2="2.15" y2="1.51" layer="25"/>
<rectangle x1="2.69" y1="1.49" x2="3.65" y2="1.51" layer="25"/>
<rectangle x1="0.11" y1="1.51" x2="0.75" y2="1.53" layer="25"/>
<rectangle x1="2.03" y1="1.51" x2="2.13" y2="1.53" layer="25"/>
<rectangle x1="2.69" y1="1.51" x2="3.65" y2="1.53" layer="25"/>
<rectangle x1="0.09" y1="1.53" x2="0.75" y2="1.55" layer="25"/>
<rectangle x1="2.01" y1="1.53" x2="2.11" y2="1.55" layer="25"/>
<rectangle x1="2.69" y1="1.53" x2="3.65" y2="1.55" layer="25"/>
<rectangle x1="0.09" y1="1.55" x2="0.77" y2="1.57" layer="25"/>
<rectangle x1="2.01" y1="1.55" x2="2.09" y2="1.57" layer="25"/>
<rectangle x1="2.69" y1="1.55" x2="3.65" y2="1.57" layer="25"/>
<rectangle x1="0.09" y1="1.57" x2="0.79" y2="1.59" layer="25"/>
<rectangle x1="1.99" y1="1.57" x2="2.09" y2="1.59" layer="25"/>
<rectangle x1="2.69" y1="1.57" x2="3.65" y2="1.59" layer="25"/>
<rectangle x1="0.09" y1="1.59" x2="0.81" y2="1.61" layer="25"/>
<rectangle x1="1.97" y1="1.59" x2="2.07" y2="1.61" layer="25"/>
<rectangle x1="2.69" y1="1.59" x2="3.65" y2="1.61" layer="25"/>
<rectangle x1="0.09" y1="1.61" x2="0.83" y2="1.63" layer="25"/>
<rectangle x1="1.95" y1="1.61" x2="2.05" y2="1.63" layer="25"/>
<rectangle x1="2.43" y1="1.61" x2="2.45" y2="1.63" layer="25"/>
<rectangle x1="2.69" y1="1.61" x2="3.67" y2="1.63" layer="25"/>
<rectangle x1="0.07" y1="1.63" x2="0.85" y2="1.65" layer="25"/>
<rectangle x1="1.93" y1="1.63" x2="2.03" y2="1.65" layer="25"/>
<rectangle x1="2.41" y1="1.63" x2="2.45" y2="1.65" layer="25"/>
<rectangle x1="2.69" y1="1.63" x2="3.67" y2="1.65" layer="25"/>
<rectangle x1="0.07" y1="1.65" x2="0.87" y2="1.67" layer="25"/>
<rectangle x1="1.91" y1="1.65" x2="2.03" y2="1.67" layer="25"/>
<rectangle x1="2.41" y1="1.65" x2="2.45" y2="1.67" layer="25"/>
<rectangle x1="2.69" y1="1.65" x2="3.67" y2="1.67" layer="25"/>
<rectangle x1="0.07" y1="1.67" x2="0.89" y2="1.69" layer="25"/>
<rectangle x1="1.89" y1="1.67" x2="2.01" y2="1.69" layer="25"/>
<rectangle x1="2.39" y1="1.67" x2="2.45" y2="1.69" layer="25"/>
<rectangle x1="2.69" y1="1.67" x2="3.67" y2="1.69" layer="25"/>
<rectangle x1="0.07" y1="1.69" x2="0.91" y2="1.71" layer="25"/>
<rectangle x1="1.85" y1="1.69" x2="1.99" y2="1.71" layer="25"/>
<rectangle x1="2.37" y1="1.69" x2="2.45" y2="1.71" layer="25"/>
<rectangle x1="2.69" y1="1.69" x2="3.67" y2="1.71" layer="25"/>
<rectangle x1="0.07" y1="1.71" x2="0.93" y2="1.73" layer="25"/>
<rectangle x1="1.83" y1="1.71" x2="1.97" y2="1.73" layer="25"/>
<rectangle x1="2.35" y1="1.71" x2="2.45" y2="1.73" layer="25"/>
<rectangle x1="2.69" y1="1.71" x2="3.67" y2="1.73" layer="25"/>
<rectangle x1="0.07" y1="1.73" x2="0.97" y2="1.75" layer="25"/>
<rectangle x1="1.81" y1="1.73" x2="1.97" y2="1.75" layer="25"/>
<rectangle x1="2.35" y1="1.73" x2="2.45" y2="1.75" layer="25"/>
<rectangle x1="2.69" y1="1.73" x2="3.67" y2="1.75" layer="25"/>
<rectangle x1="0.07" y1="1.75" x2="0.99" y2="1.77" layer="25"/>
<rectangle x1="1.77" y1="1.75" x2="1.95" y2="1.77" layer="25"/>
<rectangle x1="2.33" y1="1.75" x2="2.45" y2="1.77" layer="25"/>
<rectangle x1="2.69" y1="1.75" x2="3.67" y2="1.77" layer="25"/>
<rectangle x1="0.07" y1="1.77" x2="1.03" y2="1.79" layer="25"/>
<rectangle x1="1.73" y1="1.77" x2="1.93" y2="1.79" layer="25"/>
<rectangle x1="2.31" y1="1.77" x2="2.45" y2="1.79" layer="25"/>
<rectangle x1="2.69" y1="1.77" x2="3.67" y2="1.79" layer="25"/>
<rectangle x1="0.07" y1="1.79" x2="1.09" y2="1.81" layer="25"/>
<rectangle x1="1.69" y1="1.79" x2="1.91" y2="1.81" layer="25"/>
<rectangle x1="2.29" y1="1.79" x2="2.45" y2="1.81" layer="25"/>
<rectangle x1="2.69" y1="1.79" x2="3.67" y2="1.81" layer="25"/>
<rectangle x1="0.07" y1="1.81" x2="1.13" y2="1.83" layer="25"/>
<rectangle x1="1.63" y1="1.81" x2="1.89" y2="1.83" layer="25"/>
<rectangle x1="2.29" y1="1.81" x2="2.45" y2="1.83" layer="25"/>
<rectangle x1="2.69" y1="1.81" x2="3.69" y2="1.83" layer="25"/>
<rectangle x1="0.07" y1="1.83" x2="1.21" y2="1.85" layer="25"/>
<rectangle x1="1.55" y1="1.83" x2="1.89" y2="1.85" layer="25"/>
<rectangle x1="2.27" y1="1.83" x2="2.45" y2="1.85" layer="25"/>
<rectangle x1="2.69" y1="1.83" x2="3.69" y2="1.85" layer="25"/>
<rectangle x1="0.05" y1="1.85" x2="1.35" y2="1.87" layer="25"/>
<rectangle x1="1.39" y1="1.85" x2="1.87" y2="1.87" layer="25"/>
<rectangle x1="2.25" y1="1.85" x2="2.45" y2="1.87" layer="25"/>
<rectangle x1="2.69" y1="1.85" x2="3.69" y2="1.87" layer="25"/>
<rectangle x1="0.05" y1="1.87" x2="1.85" y2="1.89" layer="25"/>
<rectangle x1="2.23" y1="1.87" x2="2.45" y2="1.89" layer="25"/>
<rectangle x1="2.69" y1="1.87" x2="3.69" y2="1.89" layer="25"/>
<rectangle x1="0.05" y1="1.89" x2="1.83" y2="1.91" layer="25"/>
<rectangle x1="2.23" y1="1.89" x2="2.45" y2="1.91" layer="25"/>
<rectangle x1="2.69" y1="1.89" x2="3.69" y2="1.91" layer="25"/>
<rectangle x1="0.05" y1="1.91" x2="1.83" y2="1.93" layer="25"/>
<rectangle x1="2.21" y1="1.91" x2="2.45" y2="1.93" layer="25"/>
<rectangle x1="2.69" y1="1.91" x2="3.69" y2="1.93" layer="25"/>
<rectangle x1="0.05" y1="1.93" x2="1.81" y2="1.95" layer="25"/>
<rectangle x1="2.19" y1="1.93" x2="2.45" y2="1.95" layer="25"/>
<rectangle x1="2.69" y1="1.93" x2="3.69" y2="1.95" layer="25"/>
<rectangle x1="0.05" y1="1.95" x2="1.79" y2="1.97" layer="25"/>
<rectangle x1="2.17" y1="1.95" x2="2.45" y2="1.97" layer="25"/>
<rectangle x1="2.69" y1="1.95" x2="3.67" y2="1.97" layer="25"/>
<rectangle x1="0.05" y1="1.97" x2="1.77" y2="1.99" layer="25"/>
<rectangle x1="2.15" y1="1.97" x2="2.45" y2="1.99" layer="25"/>
<rectangle x1="2.69" y1="1.97" x2="3.67" y2="1.99" layer="25"/>
<rectangle x1="0.07" y1="1.99" x2="1.77" y2="2.01" layer="25"/>
<rectangle x1="2.15" y1="1.99" x2="2.45" y2="2.01" layer="25"/>
<rectangle x1="2.69" y1="1.99" x2="3.67" y2="2.01" layer="25"/>
<rectangle x1="0.07" y1="2.01" x2="1.75" y2="2.03" layer="25"/>
<rectangle x1="2.13" y1="2.01" x2="2.45" y2="2.03" layer="25"/>
<rectangle x1="2.69" y1="2.01" x2="3.67" y2="2.03" layer="25"/>
<rectangle x1="0.07" y1="2.03" x2="1.73" y2="2.05" layer="25"/>
<rectangle x1="2.11" y1="2.03" x2="2.43" y2="2.05" layer="25"/>
<rectangle x1="2.77" y1="2.03" x2="3.67" y2="2.05" layer="25"/>
<rectangle x1="0.07" y1="2.05" x2="1.71" y2="2.07" layer="25"/>
<rectangle x1="2.09" y1="2.05" x2="2.39" y2="2.07" layer="25"/>
<rectangle x1="2.83" y1="2.05" x2="3.67" y2="2.07" layer="25"/>
<rectangle x1="0.07" y1="2.07" x2="1.69" y2="2.09" layer="25"/>
<rectangle x1="2.09" y1="2.07" x2="2.35" y2="2.09" layer="25"/>
<rectangle x1="2.87" y1="2.07" x2="3.67" y2="2.09" layer="25"/>
<rectangle x1="0.07" y1="2.09" x2="1.69" y2="2.11" layer="25"/>
<rectangle x1="2.07" y1="2.09" x2="2.31" y2="2.11" layer="25"/>
<rectangle x1="2.91" y1="2.09" x2="3.67" y2="2.11" layer="25"/>
<rectangle x1="0.07" y1="2.11" x2="1.21" y2="2.13" layer="25"/>
<rectangle x1="1.49" y1="2.11" x2="1.67" y2="2.13" layer="25"/>
<rectangle x1="2.05" y1="2.11" x2="2.27" y2="2.13" layer="25"/>
<rectangle x1="2.95" y1="2.11" x2="3.67" y2="2.13" layer="25"/>
<rectangle x1="0.07" y1="2.13" x2="1.15" y2="2.15" layer="25"/>
<rectangle x1="1.55" y1="2.13" x2="1.65" y2="2.15" layer="25"/>
<rectangle x1="2.03" y1="2.13" x2="2.25" y2="2.15" layer="25"/>
<rectangle x1="2.97" y1="2.13" x2="3.67" y2="2.15" layer="25"/>
<rectangle x1="0.07" y1="2.15" x2="1.11" y2="2.17" layer="25"/>
<rectangle x1="1.59" y1="2.15" x2="1.63" y2="2.17" layer="25"/>
<rectangle x1="2.03" y1="2.15" x2="2.23" y2="2.17" layer="25"/>
<rectangle x1="2.99" y1="2.15" x2="3.67" y2="2.17" layer="25"/>
<rectangle x1="0.07" y1="2.17" x2="1.07" y2="2.19" layer="25"/>
<rectangle x1="2.01" y1="2.17" x2="2.21" y2="2.19" layer="25"/>
<rectangle x1="3.01" y1="2.17" x2="3.65" y2="2.19" layer="25"/>
<rectangle x1="0.09" y1="2.19" x2="1.03" y2="2.21" layer="25"/>
<rectangle x1="1.99" y1="2.19" x2="2.19" y2="2.21" layer="25"/>
<rectangle x1="3.03" y1="2.19" x2="3.65" y2="2.21" layer="25"/>
<rectangle x1="0.09" y1="2.21" x2="1.01" y2="2.23" layer="25"/>
<rectangle x1="1.97" y1="2.21" x2="2.17" y2="2.23" layer="25"/>
<rectangle x1="3.05" y1="2.21" x2="3.65" y2="2.23" layer="25"/>
<rectangle x1="0.09" y1="2.23" x2="0.97" y2="2.25" layer="25"/>
<rectangle x1="1.97" y1="2.23" x2="2.15" y2="2.25" layer="25"/>
<rectangle x1="3.07" y1="2.23" x2="3.65" y2="2.25" layer="25"/>
<rectangle x1="0.09" y1="2.25" x2="0.95" y2="2.27" layer="25"/>
<rectangle x1="1.95" y1="2.25" x2="2.13" y2="2.27" layer="25"/>
<rectangle x1="3.09" y1="2.25" x2="3.65" y2="2.27" layer="25"/>
<rectangle x1="0.09" y1="2.27" x2="0.93" y2="2.29" layer="25"/>
<rectangle x1="1.93" y1="2.27" x2="2.13" y2="2.29" layer="25"/>
<rectangle x1="3.11" y1="2.27" x2="3.63" y2="2.29" layer="25"/>
<rectangle x1="0.09" y1="2.29" x2="0.91" y2="2.31" layer="25"/>
<rectangle x1="1.91" y1="2.29" x2="2.11" y2="2.31" layer="25"/>
<rectangle x1="3.11" y1="2.29" x2="3.63" y2="2.31" layer="25"/>
<rectangle x1="0.11" y1="2.31" x2="0.89" y2="2.33" layer="25"/>
<rectangle x1="1.89" y1="2.31" x2="2.11" y2="2.33" layer="25"/>
<rectangle x1="3.13" y1="2.31" x2="3.63" y2="2.33" layer="25"/>
<rectangle x1="0.11" y1="2.33" x2="0.89" y2="2.35" layer="25"/>
<rectangle x1="1.89" y1="2.33" x2="2.09" y2="2.35" layer="25"/>
<rectangle x1="3.13" y1="2.33" x2="3.63" y2="2.35" layer="25"/>
<rectangle x1="0.11" y1="2.35" x2="0.87" y2="2.37" layer="25"/>
<rectangle x1="1.87" y1="2.35" x2="2.09" y2="2.37" layer="25"/>
<rectangle x1="3.15" y1="2.35" x2="3.61" y2="2.37" layer="25"/>
<rectangle x1="0.11" y1="2.37" x2="0.85" y2="2.39" layer="25"/>
<rectangle x1="1.85" y1="2.37" x2="2.07" y2="2.39" layer="25"/>
<rectangle x1="3.15" y1="2.37" x2="3.61" y2="2.39" layer="25"/>
<rectangle x1="0.13" y1="2.39" x2="0.85" y2="2.41" layer="25"/>
<rectangle x1="1.85" y1="2.39" x2="2.07" y2="2.41" layer="25"/>
<rectangle x1="3.17" y1="2.39" x2="3.61" y2="2.41" layer="25"/>
<rectangle x1="0.13" y1="2.41" x2="0.83" y2="2.43" layer="25"/>
<rectangle x1="1.87" y1="2.41" x2="2.07" y2="2.43" layer="25"/>
<rectangle x1="3.17" y1="2.41" x2="3.61" y2="2.43" layer="25"/>
<rectangle x1="0.13" y1="2.43" x2="0.83" y2="2.45" layer="25"/>
<rectangle x1="1.87" y1="2.43" x2="2.07" y2="2.45" layer="25"/>
<rectangle x1="3.17" y1="2.43" x2="3.59" y2="2.45" layer="25"/>
<rectangle x1="0.13" y1="2.45" x2="0.81" y2="2.47" layer="25"/>
<rectangle x1="1.87" y1="2.45" x2="2.05" y2="2.47" layer="25"/>
<rectangle x1="3.19" y1="2.45" x2="3.59" y2="2.47" layer="25"/>
<rectangle x1="0.15" y1="2.47" x2="0.81" y2="2.49" layer="25"/>
<rectangle x1="1.89" y1="2.47" x2="2.05" y2="2.49" layer="25"/>
<rectangle x1="3.19" y1="2.47" x2="3.59" y2="2.49" layer="25"/>
<rectangle x1="0.15" y1="2.49" x2="0.79" y2="2.51" layer="25"/>
<rectangle x1="1.89" y1="2.49" x2="2.05" y2="2.51" layer="25"/>
<rectangle x1="3.19" y1="2.49" x2="3.57" y2="2.51" layer="25"/>
<rectangle x1="0.15" y1="2.51" x2="0.79" y2="2.53" layer="25"/>
<rectangle x1="1.89" y1="2.51" x2="2.05" y2="2.53" layer="25"/>
<rectangle x1="3.19" y1="2.51" x2="3.57" y2="2.53" layer="25"/>
<rectangle x1="0.17" y1="2.53" x2="0.79" y2="2.55" layer="25"/>
<rectangle x1="1.91" y1="2.53" x2="2.05" y2="2.55" layer="25"/>
<rectangle x1="3.19" y1="2.53" x2="3.57" y2="2.55" layer="25"/>
<rectangle x1="0.17" y1="2.55" x2="0.79" y2="2.57" layer="25"/>
<rectangle x1="1.91" y1="2.55" x2="2.05" y2="2.57" layer="25"/>
<rectangle x1="3.19" y1="2.55" x2="3.55" y2="2.57" layer="25"/>
<rectangle x1="0.19" y1="2.57" x2="0.77" y2="2.59" layer="25"/>
<rectangle x1="1.91" y1="2.57" x2="2.05" y2="2.59" layer="25"/>
<rectangle x1="3.21" y1="2.57" x2="3.55" y2="2.59" layer="25"/>
<rectangle x1="0.19" y1="2.59" x2="0.77" y2="2.61" layer="25"/>
<rectangle x1="1.91" y1="2.59" x2="2.05" y2="2.61" layer="25"/>
<rectangle x1="3.21" y1="2.59" x2="3.53" y2="2.61" layer="25"/>
<rectangle x1="0.19" y1="2.61" x2="0.77" y2="2.63" layer="25"/>
<rectangle x1="1.91" y1="2.61" x2="2.05" y2="2.63" layer="25"/>
<rectangle x1="3.21" y1="2.61" x2="3.53" y2="2.63" layer="25"/>
<rectangle x1="0.21" y1="2.63" x2="0.77" y2="2.65" layer="25"/>
<rectangle x1="1.91" y1="2.63" x2="2.05" y2="2.65" layer="25"/>
<rectangle x1="3.21" y1="2.63" x2="3.53" y2="2.65" layer="25"/>
<rectangle x1="0.21" y1="2.65" x2="0.77" y2="2.67" layer="25"/>
<rectangle x1="1.93" y1="2.65" x2="2.05" y2="2.67" layer="25"/>
<rectangle x1="3.19" y1="2.65" x2="3.51" y2="2.67" layer="25"/>
<rectangle x1="0.23" y1="2.67" x2="0.77" y2="2.69" layer="25"/>
<rectangle x1="1.93" y1="2.67" x2="2.05" y2="2.69" layer="25"/>
<rectangle x1="3.19" y1="2.67" x2="3.51" y2="2.69" layer="25"/>
<rectangle x1="0.23" y1="2.69" x2="0.77" y2="2.71" layer="25"/>
<rectangle x1="1.93" y1="2.69" x2="2.05" y2="2.71" layer="25"/>
<rectangle x1="3.19" y1="2.69" x2="3.49" y2="2.71" layer="25"/>
<rectangle x1="0.25" y1="2.71" x2="0.77" y2="2.73" layer="25"/>
<rectangle x1="1.91" y1="2.71" x2="2.07" y2="2.73" layer="25"/>
<rectangle x1="3.19" y1="2.71" x2="3.49" y2="2.73" layer="25"/>
<rectangle x1="0.25" y1="2.73" x2="0.77" y2="2.75" layer="25"/>
<rectangle x1="1.91" y1="2.73" x2="2.07" y2="2.75" layer="25"/>
<rectangle x1="3.19" y1="2.73" x2="3.47" y2="2.75" layer="25"/>
<rectangle x1="0.27" y1="2.75" x2="0.77" y2="2.77" layer="25"/>
<rectangle x1="1.91" y1="2.75" x2="2.07" y2="2.77" layer="25"/>
<rectangle x1="3.19" y1="2.75" x2="3.47" y2="2.77" layer="25"/>
<rectangle x1="0.27" y1="2.77" x2="0.77" y2="2.79" layer="25"/>
<rectangle x1="1.91" y1="2.77" x2="2.09" y2="2.79" layer="25"/>
<rectangle x1="3.17" y1="2.77" x2="3.45" y2="2.79" layer="25"/>
<rectangle x1="0.29" y1="2.79" x2="0.77" y2="2.81" layer="25"/>
<rectangle x1="1.91" y1="2.79" x2="2.09" y2="2.81" layer="25"/>
<rectangle x1="3.17" y1="2.79" x2="3.45" y2="2.81" layer="25"/>
<rectangle x1="0.29" y1="2.81" x2="0.79" y2="2.83" layer="25"/>
<rectangle x1="1.91" y1="2.81" x2="2.11" y2="2.83" layer="25"/>
<rectangle x1="3.17" y1="2.81" x2="3.43" y2="2.83" layer="25"/>
<rectangle x1="0.31" y1="2.83" x2="0.79" y2="2.85" layer="25"/>
<rectangle x1="1.89" y1="2.83" x2="2.11" y2="2.85" layer="25"/>
<rectangle x1="3.15" y1="2.83" x2="3.41" y2="2.85" layer="25"/>
<rectangle x1="0.31" y1="2.85" x2="0.79" y2="2.87" layer="25"/>
<rectangle x1="1.89" y1="2.85" x2="2.13" y2="2.87" layer="25"/>
<rectangle x1="3.15" y1="2.85" x2="3.41" y2="2.87" layer="25"/>
<rectangle x1="0.33" y1="2.87" x2="0.79" y2="2.89" layer="25"/>
<rectangle x1="1.89" y1="2.87" x2="2.13" y2="2.89" layer="25"/>
<rectangle x1="3.13" y1="2.87" x2="3.39" y2="2.89" layer="25"/>
<rectangle x1="0.35" y1="2.89" x2="0.81" y2="2.91" layer="25"/>
<rectangle x1="1.87" y1="2.89" x2="2.15" y2="2.91" layer="25"/>
<rectangle x1="3.13" y1="2.89" x2="3.39" y2="2.91" layer="25"/>
<rectangle x1="0.35" y1="2.91" x2="0.81" y2="2.93" layer="25"/>
<rectangle x1="1.87" y1="2.91" x2="2.15" y2="2.93" layer="25"/>
<rectangle x1="3.11" y1="2.91" x2="3.37" y2="2.93" layer="25"/>
<rectangle x1="0.37" y1="2.93" x2="0.83" y2="2.95" layer="25"/>
<rectangle x1="1.85" y1="2.93" x2="2.17" y2="2.95" layer="25"/>
<rectangle x1="3.09" y1="2.93" x2="3.35" y2="2.95" layer="25"/>
<rectangle x1="0.37" y1="2.95" x2="0.83" y2="2.97" layer="25"/>
<rectangle x1="1.85" y1="2.95" x2="2.19" y2="2.97" layer="25"/>
<rectangle x1="3.07" y1="2.95" x2="3.35" y2="2.97" layer="25"/>
<rectangle x1="0.39" y1="2.97" x2="0.85" y2="2.99" layer="25"/>
<rectangle x1="1.83" y1="2.97" x2="2.21" y2="2.99" layer="25"/>
<rectangle x1="3.07" y1="2.97" x2="3.33" y2="2.99" layer="25"/>
<rectangle x1="0.41" y1="2.99" x2="0.85" y2="3.01" layer="25"/>
<rectangle x1="1.83" y1="2.99" x2="2.23" y2="3.01" layer="25"/>
<rectangle x1="3.05" y1="2.99" x2="3.31" y2="3.01" layer="25"/>
<rectangle x1="0.43" y1="3.01" x2="0.87" y2="3.03" layer="25"/>
<rectangle x1="1.81" y1="3.01" x2="2.25" y2="3.03" layer="25"/>
<rectangle x1="3.03" y1="3.01" x2="3.29" y2="3.03" layer="25"/>
<rectangle x1="0.43" y1="3.03" x2="0.89" y2="3.05" layer="25"/>
<rectangle x1="1.79" y1="3.03" x2="2.27" y2="3.05" layer="25"/>
<rectangle x1="2.99" y1="3.03" x2="3.29" y2="3.05" layer="25"/>
<rectangle x1="0.45" y1="3.05" x2="0.89" y2="3.07" layer="25"/>
<rectangle x1="1.79" y1="3.05" x2="2.31" y2="3.07" layer="25"/>
<rectangle x1="2.97" y1="3.05" x2="3.27" y2="3.07" layer="25"/>
<rectangle x1="0.47" y1="3.07" x2="0.91" y2="3.09" layer="25"/>
<rectangle x1="1.77" y1="3.07" x2="2.33" y2="3.09" layer="25"/>
<rectangle x1="2.95" y1="3.07" x2="3.25" y2="3.09" layer="25"/>
<rectangle x1="0.49" y1="3.09" x2="0.93" y2="3.11" layer="25"/>
<rectangle x1="1.75" y1="3.09" x2="2.37" y2="3.11" layer="25"/>
<rectangle x1="2.91" y1="3.09" x2="3.23" y2="3.11" layer="25"/>
<rectangle x1="0.51" y1="3.11" x2="0.95" y2="3.13" layer="25"/>
<rectangle x1="1.73" y1="3.11" x2="2.41" y2="3.13" layer="25"/>
<rectangle x1="2.87" y1="3.11" x2="3.21" y2="3.13" layer="25"/>
<rectangle x1="0.53" y1="3.13" x2="0.97" y2="3.15" layer="25"/>
<rectangle x1="1.69" y1="3.13" x2="2.47" y2="3.15" layer="25"/>
<rectangle x1="2.81" y1="3.13" x2="3.19" y2="3.15" layer="25"/>
<rectangle x1="0.53" y1="3.15" x2="1.01" y2="3.17" layer="25"/>
<rectangle x1="1.67" y1="3.15" x2="2.57" y2="3.17" layer="25"/>
<rectangle x1="2.71" y1="3.15" x2="3.17" y2="3.17" layer="25"/>
<rectangle x1="0.55" y1="3.17" x2="1.03" y2="3.19" layer="25"/>
<rectangle x1="1.65" y1="3.17" x2="3.15" y2="3.19" layer="25"/>
<rectangle x1="0.57" y1="3.19" x2="1.07" y2="3.21" layer="25"/>
<rectangle x1="1.61" y1="3.19" x2="3.15" y2="3.21" layer="25"/>
<rectangle x1="0.59" y1="3.21" x2="1.11" y2="3.23" layer="25"/>
<rectangle x1="1.57" y1="3.21" x2="3.13" y2="3.23" layer="25"/>
<rectangle x1="0.61" y1="3.23" x2="1.17" y2="3.25" layer="25"/>
<rectangle x1="1.51" y1="3.23" x2="3.09" y2="3.25" layer="25"/>
<rectangle x1="0.63" y1="3.25" x2="1.25" y2="3.27" layer="25"/>
<rectangle x1="1.41" y1="3.25" x2="3.07" y2="3.27" layer="25"/>
<rectangle x1="0.65" y1="3.27" x2="3.05" y2="3.29" layer="25"/>
<rectangle x1="0.69" y1="3.29" x2="3.03" y2="3.31" layer="25"/>
<rectangle x1="0.71" y1="3.31" x2="3.01" y2="3.33" layer="25"/>
<rectangle x1="0.73" y1="3.33" x2="2.99" y2="3.35" layer="25"/>
<rectangle x1="0.75" y1="3.35" x2="2.95" y2="3.37" layer="25"/>
<rectangle x1="0.77" y1="3.37" x2="2.93" y2="3.39" layer="25"/>
<rectangle x1="0.81" y1="3.39" x2="2.91" y2="3.41" layer="25"/>
<rectangle x1="0.83" y1="3.41" x2="2.87" y2="3.43" layer="25"/>
<rectangle x1="0.87" y1="3.43" x2="2.85" y2="3.45" layer="25"/>
<rectangle x1="0.89" y1="3.45" x2="2.81" y2="3.47" layer="25"/>
<rectangle x1="0.93" y1="3.47" x2="2.79" y2="3.49" layer="25"/>
<rectangle x1="0.95" y1="3.49" x2="2.75" y2="3.51" layer="25"/>
<rectangle x1="0.99" y1="3.51" x2="2.71" y2="3.53" layer="25"/>
<rectangle x1="1.03" y1="3.53" x2="2.67" y2="3.55" layer="25"/>
<rectangle x1="1.07" y1="3.55" x2="2.63" y2="3.57" layer="25"/>
<rectangle x1="1.11" y1="3.57" x2="2.59" y2="3.59" layer="25"/>
<rectangle x1="1.15" y1="3.59" x2="2.55" y2="3.61" layer="25"/>
<rectangle x1="1.21" y1="3.61" x2="2.49" y2="3.63" layer="25"/>
<rectangle x1="1.27" y1="3.63" x2="2.45" y2="3.65" layer="25"/>
<rectangle x1="1.33" y1="3.65" x2="2.39" y2="3.67" layer="25"/>
<rectangle x1="1.39" y1="3.67" x2="2.31" y2="3.69" layer="25"/>
<rectangle x1="1.47" y1="3.69" x2="2.23" y2="3.71" layer="25"/>
<rectangle x1="1.57" y1="3.71" x2="2.13" y2="3.73" layer="25"/>
<rectangle x1="1.79" y1="3.73" x2="1.91" y2="3.75" layer="25"/>
</package>
<package name="MUTANTC_TEXT+LOGO" library_version="120">
<rectangle x1="9.13" y1="0.06" x2="9.23" y2="0.16" layer="25"/>
<rectangle x1="14.63" y1="0.06" x2="14.73" y2="0.16" layer="25"/>
<rectangle x1="9.13" y1="0.16" x2="9.43" y2="0.26" layer="25"/>
<rectangle x1="14.63" y1="0.16" x2="14.93" y2="0.26" layer="25"/>
<rectangle x1="9.13" y1="0.26" x2="9.53" y2="0.36" layer="25"/>
<rectangle x1="14.63" y1="0.26" x2="15.13" y2="0.36" layer="25"/>
<rectangle x1="9.13" y1="0.36" x2="9.53" y2="0.46" layer="25"/>
<rectangle x1="14.63" y1="0.36" x2="15.13" y2="0.46" layer="25"/>
<rectangle x1="9.13" y1="0.46" x2="9.53" y2="0.56" layer="25"/>
<rectangle x1="14.63" y1="0.46" x2="15.13" y2="0.56" layer="25"/>
<rectangle x1="9.13" y1="0.56" x2="9.53" y2="0.66" layer="25"/>
<rectangle x1="14.63" y1="0.56" x2="15.13" y2="0.66" layer="25"/>
<rectangle x1="9.13" y1="0.66" x2="9.53" y2="0.76" layer="25"/>
<rectangle x1="14.63" y1="0.66" x2="15.13" y2="0.76" layer="25"/>
<rectangle x1="9.13" y1="0.76" x2="9.53" y2="0.86" layer="25"/>
<rectangle x1="14.63" y1="0.76" x2="15.13" y2="0.86" layer="25"/>
<rectangle x1="4.23" y1="0.86" x2="4.53" y2="0.96" layer="25"/>
<rectangle x1="7.43" y1="0.76" x2="8.53" y2="0.86" layer="25"/>
<rectangle x1="9.13" y1="0.86" x2="9.53" y2="0.96" layer="25"/>
<rectangle x1="11.33" y1="0.86" x2="11.63" y2="0.96" layer="25"/>
<rectangle x1="13.23" y1="0.86" x2="13.53" y2="0.96" layer="25"/>
<rectangle x1="14.63" y1="0.86" x2="15.13" y2="0.96" layer="25"/>
<rectangle x1="16.73" y1="0.86" x2="17.33" y2="0.96" layer="25"/>
<rectangle x1="4.23" y1="0.96" x2="4.73" y2="1.06" layer="25"/>
<rectangle x1="7.33" y1="0.86" x2="8.63" y2="0.96" layer="25"/>
<rectangle x1="9.13" y1="0.96" x2="9.53" y2="1.06" layer="25"/>
<rectangle x1="10.53" y1="0.96" x2="11.83" y2="1.06" layer="25"/>
<rectangle x1="12.13" y1="0.96" x2="12.53" y2="1.06" layer="25"/>
<rectangle x1="13.23" y1="0.96" x2="13.73" y2="1.06" layer="25"/>
<rectangle x1="14.63" y1="0.96" x2="15.13" y2="1.06" layer="25"/>
<rectangle x1="16.33" y1="0.96" x2="17.73" y2="1.06" layer="25"/>
<rectangle x1="4.23" y1="1.06" x2="4.73" y2="1.16" layer="25"/>
<rectangle x1="7.23" y1="0.96" x2="8.73" y2="1.06" layer="25"/>
<rectangle x1="9.13" y1="1.06" x2="9.53" y2="1.16" layer="25"/>
<rectangle x1="10.43" y1="1.06" x2="11.83" y2="1.16" layer="25"/>
<rectangle x1="12.13" y1="1.06" x2="12.53" y2="1.16" layer="25"/>
<rectangle x1="13.23" y1="1.06" x2="13.73" y2="1.16" layer="25"/>
<rectangle x1="14.63" y1="1.06" x2="15.13" y2="1.16" layer="25"/>
<rectangle x1="16.23" y1="1.06" x2="17.83" y2="1.16" layer="25"/>
<rectangle x1="4.23" y1="1.16" x2="4.73" y2="1.26" layer="25"/>
<rectangle x1="7.23" y1="1.06" x2="8.73" y2="1.16" layer="25"/>
<rectangle x1="9.13" y1="1.16" x2="9.53" y2="1.26" layer="25"/>
<rectangle x1="10.33" y1="1.16" x2="11.83" y2="1.26" layer="25"/>
<rectangle x1="12.13" y1="1.16" x2="12.53" y2="1.26" layer="25"/>
<rectangle x1="13.23" y1="1.16" x2="13.73" y2="1.26" layer="25"/>
<rectangle x1="14.63" y1="1.16" x2="15.13" y2="1.26" layer="25"/>
<rectangle x1="16.13" y1="1.16" x2="17.93" y2="1.26" layer="25"/>
<rectangle x1="4.23" y1="1.26" x2="4.73" y2="1.36" layer="25"/>
<rectangle x1="7.23" y1="1.16" x2="7.63" y2="1.26" layer="25"/>
<rectangle x1="8.33" y1="1.16" x2="8.73" y2="1.26" layer="25"/>
<rectangle x1="9.13" y1="1.26" x2="9.53" y2="1.36" layer="25"/>
<rectangle x1="10.33" y1="1.26" x2="11.83" y2="1.36" layer="25"/>
<rectangle x1="12.13" y1="1.26" x2="12.53" y2="1.36" layer="25"/>
<rectangle x1="13.23" y1="1.26" x2="13.73" y2="1.36" layer="25"/>
<rectangle x1="14.63" y1="1.26" x2="15.13" y2="1.36" layer="25"/>
<rectangle x1="16.13" y1="1.26" x2="17.93" y2="1.36" layer="25"/>
<rectangle x1="4.23" y1="1.36" x2="4.73" y2="1.46" layer="25"/>
<rectangle x1="7.23" y1="1.26" x2="7.63" y2="1.36" layer="25"/>
<rectangle x1="8.33" y1="1.26" x2="8.73" y2="1.36" layer="25"/>
<rectangle x1="9.13" y1="1.36" x2="9.53" y2="1.46" layer="25"/>
<rectangle x1="10.33" y1="1.36" x2="10.73" y2="1.46" layer="25"/>
<rectangle x1="11.33" y1="1.36" x2="11.83" y2="1.46" layer="25"/>
<rectangle x1="12.13" y1="1.36" x2="12.53" y2="1.46" layer="25"/>
<rectangle x1="13.23" y1="1.36" x2="13.73" y2="1.46" layer="25"/>
<rectangle x1="14.63" y1="1.36" x2="15.13" y2="1.46" layer="25"/>
<rectangle x1="16.13" y1="1.36" x2="16.53" y2="1.46" layer="25"/>
<rectangle x1="17.53" y1="1.36" x2="17.93" y2="1.46" layer="25"/>
<rectangle x1="4.23" y1="1.46" x2="4.73" y2="1.56" layer="25"/>
<rectangle x1="7.23" y1="1.36" x2="7.63" y2="1.46" layer="25"/>
<rectangle x1="8.33" y1="1.36" x2="8.73" y2="1.46" layer="25"/>
<rectangle x1="9.13" y1="1.46" x2="9.53" y2="1.56" layer="25"/>
<rectangle x1="10.33" y1="1.46" x2="10.73" y2="1.56" layer="25"/>
<rectangle x1="11.33" y1="1.46" x2="11.83" y2="1.56" layer="25"/>
<rectangle x1="12.13" y1="1.46" x2="12.53" y2="1.56" layer="25"/>
<rectangle x1="13.23" y1="1.46" x2="13.73" y2="1.56" layer="25"/>
<rectangle x1="14.63" y1="1.46" x2="15.13" y2="1.56" layer="25"/>
<rectangle x1="16.13" y1="1.46" x2="16.53" y2="1.56" layer="25"/>
<rectangle x1="4.23" y1="1.56" x2="4.73" y2="1.66" layer="25"/>
<rectangle x1="7.23" y1="1.46" x2="7.63" y2="1.56" layer="25"/>
<rectangle x1="8.33" y1="1.46" x2="8.73" y2="1.56" layer="25"/>
<rectangle x1="9.13" y1="1.56" x2="9.53" y2="1.66" layer="25"/>
<rectangle x1="10.33" y1="1.56" x2="10.73" y2="1.66" layer="25"/>
<rectangle x1="11.33" y1="1.56" x2="11.83" y2="1.66" layer="25"/>
<rectangle x1="12.13" y1="1.56" x2="12.53" y2="1.66" layer="25"/>
<rectangle x1="13.23" y1="1.56" x2="13.73" y2="1.66" layer="25"/>
<rectangle x1="14.63" y1="1.56" x2="15.13" y2="1.66" layer="25"/>
<rectangle x1="16.13" y1="1.56" x2="16.53" y2="1.66" layer="25"/>
<rectangle x1="4.23" y1="1.66" x2="4.73" y2="1.76" layer="25"/>
<rectangle x1="7.23" y1="1.56" x2="7.63" y2="1.66" layer="25"/>
<rectangle x1="8.33" y1="1.56" x2="8.73" y2="1.66" layer="25"/>
<rectangle x1="9.13" y1="1.66" x2="9.53" y2="1.76" layer="25"/>
<rectangle x1="10.33" y1="1.66" x2="10.73" y2="1.76" layer="25"/>
<rectangle x1="11.33" y1="1.66" x2="11.83" y2="1.76" layer="25"/>
<rectangle x1="12.13" y1="1.66" x2="12.53" y2="1.76" layer="25"/>
<rectangle x1="16.13" y1="1.66" x2="16.53" y2="1.76" layer="25"/>
<rectangle x1="4.23" y1="1.76" x2="4.73" y2="1.86" layer="25"/>
<rectangle x1="5.43" y1="1.76" x2="5.83" y2="1.86" layer="25"/>
<rectangle x1="6.53" y1="1.76" x2="7.03" y2="1.86" layer="25"/>
<rectangle x1="7.23" y1="1.66" x2="7.63" y2="1.76" layer="25"/>
<rectangle x1="8.33" y1="1.66" x2="8.73" y2="1.76" layer="25"/>
<rectangle x1="9.13" y1="1.76" x2="9.53" y2="1.86" layer="25"/>
<rectangle x1="10.33" y1="1.76" x2="10.73" y2="1.86" layer="25"/>
<rectangle x1="11.33" y1="1.76" x2="11.83" y2="1.86" layer="25"/>
<rectangle x1="12.13" y1="1.76" x2="12.53" y2="1.86" layer="25"/>
<rectangle x1="16.13" y1="1.76" x2="16.53" y2="1.86" layer="25"/>
<rectangle x1="4.23" y1="1.86" x2="4.73" y2="1.96" layer="25"/>
<rectangle x1="5.43" y1="1.86" x2="5.83" y2="1.96" layer="25"/>
<rectangle x1="6.53" y1="1.86" x2="7.03" y2="1.96" layer="25"/>
<rectangle x1="7.23" y1="1.76" x2="7.63" y2="1.86" layer="25"/>
<rectangle x1="8.33" y1="1.76" x2="8.73" y2="1.86" layer="25"/>
<rectangle x1="9.13" y1="1.86" x2="9.53" y2="1.96" layer="25"/>
<rectangle x1="10.33" y1="1.86" x2="10.83" y2="1.96" layer="25"/>
<rectangle x1="11.23" y1="1.86" x2="11.83" y2="1.96" layer="25"/>
<rectangle x1="12.13" y1="1.86" x2="12.53" y2="1.96" layer="25"/>
<rectangle x1="16.13" y1="1.86" x2="16.53" y2="1.96" layer="25"/>
<rectangle x1="17.63" y1="1.86" x2="17.93" y2="1.96" layer="25"/>
<rectangle x1="4.23" y1="1.96" x2="4.73" y2="2.06" layer="25"/>
<rectangle x1="5.43" y1="1.96" x2="5.83" y2="2.06" layer="25"/>
<rectangle x1="6.53" y1="1.96" x2="7.03" y2="2.06" layer="25"/>
<rectangle x1="7.23" y1="1.86" x2="7.63" y2="1.96" layer="25"/>
<rectangle x1="9.13" y1="1.96" x2="9.53" y2="2.06" layer="25"/>
<rectangle x1="10.33" y1="1.96" x2="11.83" y2="2.06" layer="25"/>
<rectangle x1="12.13" y1="1.96" x2="12.53" y2="2.06" layer="25"/>
<rectangle x1="16.13" y1="1.96" x2="16.53" y2="2.06" layer="25"/>
<rectangle x1="4.23" y1="2.06" x2="4.73" y2="2.16" layer="25"/>
<rectangle x1="5.43" y1="2.06" x2="5.83" y2="2.16" layer="25"/>
<rectangle x1="6.53" y1="2.06" x2="7.03" y2="2.16" layer="25"/>
<rectangle x1="7.23" y1="1.96" x2="7.63" y2="2.06" layer="25"/>
<rectangle x1="9.13" y1="2.06" x2="9.53" y2="2.16" layer="25"/>
<rectangle x1="10.33" y1="2.06" x2="11.83" y2="2.16" layer="25"/>
<rectangle x1="12.13" y1="2.06" x2="12.53" y2="2.16" layer="25"/>
<rectangle x1="13.23" y1="2.06" x2="13.73" y2="2.16" layer="25"/>
<rectangle x1="14.63" y1="2.06" x2="15.13" y2="2.16" layer="25"/>
<rectangle x1="16.13" y1="2.06" x2="16.53" y2="2.16" layer="25"/>
<rectangle x1="4.23" y1="2.16" x2="4.73" y2="2.26" layer="25"/>
<rectangle x1="5.43" y1="2.16" x2="5.83" y2="2.26" layer="25"/>
<rectangle x1="6.53" y1="2.16" x2="7.03" y2="2.26" layer="25"/>
<rectangle x1="7.23" y1="2.06" x2="7.63" y2="2.16" layer="25"/>
<rectangle x1="9.13" y1="2.16" x2="9.53" y2="2.26" layer="25"/>
<rectangle x1="10.43" y1="2.16" x2="11.83" y2="2.26" layer="25"/>
<rectangle x1="12.13" y1="2.16" x2="12.53" y2="2.26" layer="25"/>
<rectangle x1="13.23" y1="2.16" x2="13.73" y2="2.26" layer="25"/>
<rectangle x1="14.63" y1="2.16" x2="15.13" y2="2.26" layer="25"/>
<rectangle x1="16.13" y1="2.16" x2="16.53" y2="2.26" layer="25"/>
<rectangle x1="4.23" y1="2.26" x2="4.73" y2="2.36" layer="25"/>
<rectangle x1="5.43" y1="2.26" x2="5.83" y2="2.36" layer="25"/>
<rectangle x1="6.53" y1="2.26" x2="7.03" y2="2.36" layer="25"/>
<rectangle x1="7.23" y1="2.16" x2="7.63" y2="2.26" layer="25"/>
<rectangle x1="9.13" y1="2.26" x2="9.53" y2="2.36" layer="25"/>
<rectangle x1="10.63" y1="2.26" x2="11.83" y2="2.36" layer="25"/>
<rectangle x1="12.13" y1="2.26" x2="12.53" y2="2.36" layer="25"/>
<rectangle x1="13.23" y1="2.26" x2="13.73" y2="2.36" layer="25"/>
<rectangle x1="14.63" y1="2.26" x2="15.13" y2="2.36" layer="25"/>
<rectangle x1="16.13" y1="2.26" x2="16.53" y2="2.36" layer="25"/>
<rectangle x1="4.23" y1="2.36" x2="4.73" y2="2.46" layer="25"/>
<rectangle x1="5.43" y1="2.36" x2="5.83" y2="2.46" layer="25"/>
<rectangle x1="6.53" y1="2.36" x2="7.03" y2="2.46" layer="25"/>
<rectangle x1="7.23" y1="2.26" x2="7.63" y2="2.36" layer="25"/>
<rectangle x1="9.13" y1="2.36" x2="9.53" y2="2.46" layer="25"/>
<rectangle x1="11.33" y1="2.36" x2="11.83" y2="2.46" layer="25"/>
<rectangle x1="12.13" y1="2.36" x2="12.63" y2="2.46" layer="25"/>
<rectangle x1="13.23" y1="2.36" x2="13.73" y2="2.46" layer="25"/>
<rectangle x1="14.63" y1="2.36" x2="15.13" y2="2.46" layer="25"/>
<rectangle x1="16.13" y1="2.36" x2="16.53" y2="2.46" layer="25"/>
<rectangle x1="4.23" y1="2.46" x2="4.73" y2="2.56" layer="25"/>
<rectangle x1="5.33" y1="2.46" x2="5.93" y2="2.56" layer="25"/>
<rectangle x1="6.53" y1="2.46" x2="6.93" y2="2.56" layer="25"/>
<rectangle x1="7.23" y1="2.36" x2="7.63" y2="2.46" layer="25"/>
<rectangle x1="9.13" y1="2.46" x2="9.53" y2="2.56" layer="25"/>
<rectangle x1="11.33" y1="2.46" x2="11.83" y2="2.56" layer="25"/>
<rectangle x1="12.13" y1="2.46" x2="12.63" y2="2.56" layer="25"/>
<rectangle x1="13.23" y1="2.46" x2="13.73" y2="2.56" layer="25"/>
<rectangle x1="14.63" y1="2.46" x2="15.13" y2="2.56" layer="25"/>
<rectangle x1="16.13" y1="2.46" x2="16.53" y2="2.56" layer="25"/>
<rectangle x1="4.33" y1="2.56" x2="6.93" y2="2.66" layer="25"/>
<rectangle x1="7.23" y1="2.46" x2="7.63" y2="2.56" layer="25"/>
<rectangle x1="8.83" y1="2.56" x2="10.23" y2="2.66" layer="25"/>
<rectangle x1="10.53" y1="2.56" x2="11.83" y2="2.66" layer="25"/>
<rectangle x1="12.13" y1="2.56" x2="13.73" y2="2.66" layer="25"/>
<rectangle x1="13.93" y1="2.56" x2="15.73" y2="2.66" layer="25"/>
<rectangle x1="16.13" y1="2.56" x2="16.53" y2="2.66" layer="25"/>
<rectangle x1="4.33" y1="2.66" x2="6.93" y2="2.76" layer="25"/>
<rectangle x1="7.23" y1="2.56" x2="7.63" y2="2.66" layer="25"/>
<rectangle x1="10.63" y1="2.66" x2="11.73" y2="2.76" layer="25"/>
<rectangle x1="12.23" y1="2.66" x2="13.63" y2="2.76" layer="25"/>
<rectangle x1="13.93" y1="2.66" x2="15.83" y2="2.76" layer="25"/>
<rectangle x1="16.13" y1="2.66" x2="16.53" y2="2.76" layer="25"/>
<rectangle x1="17.53" y1="2.66" x2="17.73" y2="2.76" layer="25"/>
<rectangle x1="4.43" y1="2.76" x2="6.83" y2="2.86" layer="25"/>
<rectangle x1="7.23" y1="2.66" x2="7.63" y2="2.76" layer="25"/>
<rectangle x1="8.83" y1="2.76" x2="10.33" y2="2.86" layer="25"/>
<rectangle x1="10.63" y1="2.76" x2="11.73" y2="2.86" layer="25"/>
<rectangle x1="12.23" y1="2.76" x2="13.63" y2="2.86" layer="25"/>
<rectangle x1="13.93" y1="2.76" x2="15.83" y2="2.86" layer="25"/>
<rectangle x1="16.13" y1="2.76" x2="16.53" y2="2.86" layer="25"/>
<rectangle x1="4.53" y1="2.86" x2="5.53" y2="2.96" layer="25"/>
<rectangle x1="5.73" y1="2.86" x2="6.73" y2="2.96" layer="25"/>
<rectangle x1="8.83" y1="2.86" x2="10.33" y2="2.96" layer="25"/>
<rectangle x1="10.73" y1="2.86" x2="11.63" y2="2.96" layer="25"/>
<rectangle x1="12.33" y1="2.86" x2="13.43" y2="2.96" layer="25"/>
<rectangle x1="14.03" y1="2.86" x2="15.83" y2="2.96" layer="25"/>
<rectangle x1="16.13" y1="2.86" x2="16.53" y2="2.96" layer="25"/>
<rectangle x1="16.13" y1="2.96" x2="16.53" y2="3.06" layer="25"/>
<rectangle x1="16.13" y1="3.06" x2="16.53" y2="3.16" layer="25"/>
<rectangle x1="16.13" y1="3.16" x2="16.53" y2="3.26" layer="25"/>
<rectangle x1="17.53" y1="3.16" x2="17.93" y2="3.26" layer="25"/>
<rectangle x1="16.13" y1="3.26" x2="17.93" y2="3.36" layer="25"/>
<rectangle x1="16.13" y1="3.36" x2="17.93" y2="3.46" layer="25"/>
<rectangle x1="16.23" y1="3.46" x2="17.83" y2="3.56" layer="25"/>
<rectangle x1="16.33" y1="3.56" x2="17.73" y2="3.66" layer="25"/>
<rectangle x1="16.63" y1="3.66" x2="17.43" y2="3.76" layer="25"/>
<rectangle x1="11.33" y1="0.76" x2="11.43" y2="0.86" layer="25"/>
<rectangle x1="12.13" y1="0.76" x2="12.23" y2="0.86" layer="25"/>
<rectangle x1="13.23" y1="0.76" x2="13.33" y2="0.86" layer="25"/>
<rectangle x1="4.23" y1="0.76" x2="4.33" y2="0.86" layer="25"/>
<rectangle x1="7.53" y1="2.86" x2="7.63" y2="2.96" layer="25"/>
<rectangle x1="10.33" y1="2.86" x2="10.43" y2="2.96" layer="25"/>
<rectangle x1="13.93" y1="2.86" x2="14.03" y2="2.96" layer="25"/>
<rectangle x1="15.83" y1="2.86" x2="15.93" y2="2.96" layer="25"/>
<rectangle x1="17.53" y1="2.56" x2="17.63" y2="2.66" layer="25"/>
<rectangle x1="17.83" y1="1.96" x2="17.93" y2="2.06" layer="25"/>
<rectangle x1="17.53" y1="3.06" x2="17.93" y2="3.16" layer="25"/>
<rectangle x1="17.53" y1="2.96" x2="17.93" y2="3.06" layer="25"/>
<rectangle x1="17.53" y1="2.86" x2="17.93" y2="2.96" layer="25"/>
<rectangle x1="17.53" y1="2.76" x2="17.93" y2="2.86" layer="25"/>
<rectangle x1="17.53" y1="1.46" x2="17.93" y2="1.56" layer="25"/>
<rectangle x1="17.53" y1="1.56" x2="17.93" y2="1.66" layer="25"/>
<rectangle x1="17.53" y1="1.66" x2="17.93" y2="1.76" layer="25"/>
<rectangle x1="17.53" y1="1.76" x2="17.93" y2="1.86" layer="25"/>
<rectangle x1="17.43" y1="1.36" x2="17.53" y2="1.46" layer="25"/>
<rectangle x1="16.53" y1="1.36" x2="16.63" y2="1.46" layer="25"/>
<rectangle x1="16.53" y1="1.46" x2="16.63" y2="1.56" layer="25"/>
<rectangle x1="16.63" y1="1.36" x2="16.73" y2="1.46" layer="25"/>
<rectangle x1="16.53" y1="3.16" x2="16.63" y2="3.26" layer="25"/>
<rectangle x1="16.53" y1="3.06" x2="16.63" y2="3.16" layer="25"/>
<rectangle x1="16.63" y1="3.16" x2="16.73" y2="3.26" layer="25"/>
<rectangle x1="17.43" y1="3.16" x2="17.53" y2="3.26" layer="25"/>
<rectangle x1="17.73" y1="2.66" x2="17.83" y2="2.76" layer="25"/>
<rectangle x1="8.83" y1="2.66" x2="10.33" y2="2.76" layer="25"/>
<rectangle x1="7.33" y1="2.76" x2="7.63" y2="2.86" layer="25"/>
<rectangle x1="12.13" y1="0.86" x2="12.43" y2="0.96" layer="25"/>
<rectangle x1="16.53" y1="0.86" x2="16.83" y2="0.96" layer="25"/>
<rectangle x1="2.07" y1="0.05" x2="2.13" y2="0.07" layer="25"/>
<rectangle x1="1.81" y1="0.07" x2="2.37" y2="0.09" layer="25"/>
<rectangle x1="1.71" y1="0.09" x2="2.47" y2="0.11" layer="25"/>
<rectangle x1="1.63" y1="0.11" x2="2.55" y2="0.13" layer="25"/>
<rectangle x1="1.55" y1="0.13" x2="2.61" y2="0.15" layer="25"/>
<rectangle x1="1.49" y1="0.15" x2="2.67" y2="0.17" layer="25"/>
<rectangle x1="1.45" y1="0.17" x2="2.73" y2="0.19" layer="25"/>
<rectangle x1="1.39" y1="0.19" x2="2.79" y2="0.21" layer="25"/>
<rectangle x1="1.35" y1="0.21" x2="2.83" y2="0.23" layer="25"/>
<rectangle x1="1.31" y1="0.23" x2="2.87" y2="0.25" layer="25"/>
<rectangle x1="1.27" y1="0.25" x2="2.91" y2="0.27" layer="25"/>
<rectangle x1="1.23" y1="0.27" x2="1.51" y2="0.29" layer="25"/>
<rectangle x1="1.73" y1="0.27" x2="2.95" y2="0.29" layer="25"/>
<rectangle x1="1.19" y1="0.29" x2="1.41" y2="0.31" layer="25"/>
<rectangle x1="1.83" y1="0.29" x2="2.99" y2="0.31" layer="25"/>
<rectangle x1="1.15" y1="0.31" x2="1.35" y2="0.33" layer="25"/>
<rectangle x1="1.89" y1="0.31" x2="3.01" y2="0.33" layer="25"/>
<rectangle x1="1.13" y1="0.33" x2="1.31" y2="0.35" layer="25"/>
<rectangle x1="1.93" y1="0.33" x2="3.05" y2="0.35" layer="25"/>
<rectangle x1="1.09" y1="0.35" x2="1.25" y2="0.37" layer="25"/>
<rectangle x1="1.97" y1="0.35" x2="3.07" y2="0.37" layer="25"/>
<rectangle x1="1.07" y1="0.37" x2="1.23" y2="0.39" layer="25"/>
<rectangle x1="2.01" y1="0.37" x2="3.11" y2="0.39" layer="25"/>
<rectangle x1="1.03" y1="0.39" x2="1.19" y2="0.41" layer="25"/>
<rectangle x1="2.05" y1="0.39" x2="3.13" y2="0.41" layer="25"/>
<rectangle x1="1.01" y1="0.41" x2="1.15" y2="0.43" layer="25"/>
<rectangle x1="2.07" y1="0.41" x2="3.17" y2="0.43" layer="25"/>
<rectangle x1="0.99" y1="0.43" x2="1.13" y2="0.45" layer="25"/>
<rectangle x1="2.09" y1="0.43" x2="3.19" y2="0.45" layer="25"/>
<rectangle x1="0.95" y1="0.45" x2="1.11" y2="0.47" layer="25"/>
<rectangle x1="2.13" y1="0.45" x2="3.21" y2="0.47" layer="25"/>
<rectangle x1="0.93" y1="0.47" x2="1.09" y2="0.49" layer="25"/>
<rectangle x1="2.15" y1="0.47" x2="3.23" y2="0.49" layer="25"/>
<rectangle x1="0.91" y1="0.49" x2="1.07" y2="0.51" layer="25"/>
<rectangle x1="2.17" y1="0.49" x2="3.25" y2="0.51" layer="25"/>
<rectangle x1="0.89" y1="0.51" x2="1.05" y2="0.53" layer="25"/>
<rectangle x1="2.19" y1="0.51" x2="3.29" y2="0.53" layer="25"/>
<rectangle x1="0.87" y1="0.53" x2="1.03" y2="0.55" layer="25"/>
<rectangle x1="2.21" y1="0.53" x2="3.31" y2="0.55" layer="25"/>
<rectangle x1="0.85" y1="0.55" x2="1.01" y2="0.57" layer="25"/>
<rectangle x1="2.21" y1="0.55" x2="3.33" y2="0.57" layer="25"/>
<rectangle x1="0.83" y1="0.57" x2="0.99" y2="0.59" layer="25"/>
<rectangle x1="2.23" y1="0.57" x2="3.35" y2="0.59" layer="25"/>
<rectangle x1="0.81" y1="0.59" x2="0.97" y2="0.61" layer="25"/>
<rectangle x1="2.25" y1="0.59" x2="3.37" y2="0.61" layer="25"/>
<rectangle x1="0.79" y1="0.61" x2="0.97" y2="0.63" layer="25"/>
<rectangle x1="2.27" y1="0.61" x2="3.39" y2="0.63" layer="25"/>
<rectangle x1="0.77" y1="0.63" x2="0.95" y2="0.65" layer="25"/>
<rectangle x1="2.27" y1="0.63" x2="3.41" y2="0.65" layer="25"/>
<rectangle x1="0.75" y1="0.65" x2="0.93" y2="0.67" layer="25"/>
<rectangle x1="2.29" y1="0.65" x2="3.41" y2="0.67" layer="25"/>
<rectangle x1="0.73" y1="0.67" x2="0.93" y2="0.69" layer="25"/>
<rectangle x1="2.29" y1="0.67" x2="3.43" y2="0.69" layer="25"/>
<rectangle x1="0.71" y1="0.69" x2="0.91" y2="0.71" layer="25"/>
<rectangle x1="2.31" y1="0.69" x2="3.45" y2="0.71" layer="25"/>
<rectangle x1="0.69" y1="0.71" x2="0.91" y2="0.73" layer="25"/>
<rectangle x1="2.31" y1="0.71" x2="3.47" y2="0.73" layer="25"/>
<rectangle x1="0.67" y1="0.73" x2="0.89" y2="0.75" layer="25"/>
<rectangle x1="2.33" y1="0.73" x2="3.49" y2="0.75" layer="25"/>
<rectangle x1="0.65" y1="0.75" x2="0.89" y2="0.77" layer="25"/>
<rectangle x1="2.33" y1="0.75" x2="3.51" y2="0.77" layer="25"/>
<rectangle x1="0.65" y1="0.77" x2="0.87" y2="0.79" layer="25"/>
<rectangle x1="2.33" y1="0.77" x2="3.51" y2="0.79" layer="25"/>
<rectangle x1="0.63" y1="0.79" x2="0.87" y2="0.81" layer="25"/>
<rectangle x1="2.35" y1="0.79" x2="3.53" y2="0.81" layer="25"/>
<rectangle x1="0.61" y1="0.81" x2="0.85" y2="0.83" layer="25"/>
<rectangle x1="2.35" y1="0.81" x2="2.87" y2="0.83" layer="25"/>
<rectangle x1="2.91" y1="0.81" x2="3.55" y2="0.83" layer="25"/>
<rectangle x1="0.61" y1="0.83" x2="0.85" y2="0.85" layer="25"/>
<rectangle x1="2.35" y1="0.83" x2="2.85" y2="0.85" layer="25"/>
<rectangle x1="2.95" y1="0.83" x2="3.57" y2="0.85" layer="25"/>
<rectangle x1="0.59" y1="0.85" x2="0.85" y2="0.87" layer="25"/>
<rectangle x1="2.37" y1="0.85" x2="2.83" y2="0.87" layer="25"/>
<rectangle x1="2.97" y1="0.85" x2="3.57" y2="0.87" layer="25"/>
<rectangle x1="0.57" y1="0.87" x2="0.85" y2="0.89" layer="25"/>
<rectangle x1="2.37" y1="0.87" x2="2.81" y2="0.89" layer="25"/>
<rectangle x1="3.01" y1="0.87" x2="3.59" y2="0.89" layer="25"/>
<rectangle x1="0.55" y1="0.89" x2="0.83" y2="0.91" layer="25"/>
<rectangle x1="2.37" y1="0.89" x2="2.81" y2="0.91" layer="25"/>
<rectangle x1="3.03" y1="0.89" x2="3.59" y2="0.91" layer="25"/>
<rectangle x1="0.55" y1="0.91" x2="0.83" y2="0.93" layer="25"/>
<rectangle x1="2.37" y1="0.91" x2="2.77" y2="0.93" layer="25"/>
<rectangle x1="3.05" y1="0.91" x2="3.61" y2="0.93" layer="25"/>
<rectangle x1="0.53" y1="0.93" x2="0.83" y2="0.95" layer="25"/>
<rectangle x1="2.37" y1="0.93" x2="2.67" y2="0.95" layer="25"/>
<rectangle x1="3.09" y1="0.93" x2="3.63" y2="0.95" layer="25"/>
<rectangle x1="0.53" y1="0.95" x2="0.83" y2="0.97" layer="25"/>
<rectangle x1="2.37" y1="0.95" x2="2.55" y2="0.97" layer="25"/>
<rectangle x1="3.11" y1="0.95" x2="3.63" y2="0.97" layer="25"/>
<rectangle x1="0.51" y1="0.97" x2="0.83" y2="0.99" layer="25"/>
<rectangle x1="2.39" y1="0.97" x2="2.45" y2="0.99" layer="25"/>
<rectangle x1="3.13" y1="0.97" x2="3.65" y2="0.99" layer="25"/>
<rectangle x1="0.49" y1="0.99" x2="0.81" y2="1.01" layer="25"/>
<rectangle x1="3.11" y1="0.99" x2="3.65" y2="1.01" layer="25"/>
<rectangle x1="0.49" y1="1.01" x2="0.81" y2="1.03" layer="25"/>
<rectangle x1="3.09" y1="1.01" x2="3.67" y2="1.03" layer="25"/>
<rectangle x1="0.47" y1="1.03" x2="0.81" y2="1.05" layer="25"/>
<rectangle x1="3.07" y1="1.03" x2="3.67" y2="1.05" layer="25"/>
<rectangle x1="0.47" y1="1.05" x2="0.81" y2="1.07" layer="25"/>
<rectangle x1="3.07" y1="1.05" x2="3.69" y2="1.07" layer="25"/>
<rectangle x1="0.45" y1="1.07" x2="0.81" y2="1.09" layer="25"/>
<rectangle x1="3.05" y1="1.07" x2="3.69" y2="1.09" layer="25"/>
<rectangle x1="0.45" y1="1.09" x2="0.81" y2="1.11" layer="25"/>
<rectangle x1="3.03" y1="1.09" x2="3.71" y2="1.11" layer="25"/>
<rectangle x1="0.43" y1="1.11" x2="0.81" y2="1.13" layer="25"/>
<rectangle x1="3.01" y1="1.11" x2="3.71" y2="1.13" layer="25"/>
<rectangle x1="0.43" y1="1.13" x2="0.81" y2="1.15" layer="25"/>
<rectangle x1="3.01" y1="1.13" x2="3.73" y2="1.15" layer="25"/>
<rectangle x1="0.41" y1="1.15" x2="0.81" y2="1.17" layer="25"/>
<rectangle x1="2.99" y1="1.15" x2="3.73" y2="1.17" layer="25"/>
<rectangle x1="0.41" y1="1.17" x2="0.83" y2="1.19" layer="25"/>
<rectangle x1="2.97" y1="1.17" x2="3.75" y2="1.19" layer="25"/>
<rectangle x1="0.41" y1="1.19" x2="0.83" y2="1.21" layer="25"/>
<rectangle x1="2.95" y1="1.19" x2="3.75" y2="1.21" layer="25"/>
<rectangle x1="0.39" y1="1.21" x2="0.83" y2="1.23" layer="25"/>
<rectangle x1="2.95" y1="1.21" x2="3.75" y2="1.23" layer="25"/>
<rectangle x1="0.39" y1="1.23" x2="0.83" y2="1.25" layer="25"/>
<rectangle x1="2.51" y1="1.23" x2="2.55" y2="1.25" layer="25"/>
<rectangle x1="2.93" y1="1.23" x2="3.77" y2="1.25" layer="25"/>
<rectangle x1="0.37" y1="1.25" x2="0.83" y2="1.27" layer="25"/>
<rectangle x1="2.41" y1="1.25" x2="2.53" y2="1.27" layer="25"/>
<rectangle x1="2.91" y1="1.25" x2="3.77" y2="1.27" layer="25"/>
<rectangle x1="0.37" y1="1.27" x2="0.83" y2="1.29" layer="25"/>
<rectangle x1="2.35" y1="1.27" x2="2.51" y2="1.29" layer="25"/>
<rectangle x1="2.89" y1="1.27" x2="3.79" y2="1.29" layer="25"/>
<rectangle x1="0.37" y1="1.29" x2="0.85" y2="1.31" layer="25"/>
<rectangle x1="2.35" y1="1.29" x2="2.49" y2="1.31" layer="25"/>
<rectangle x1="2.89" y1="1.29" x2="3.79" y2="1.31" layer="25"/>
<rectangle x1="0.35" y1="1.31" x2="0.85" y2="1.33" layer="25"/>
<rectangle x1="2.33" y1="1.31" x2="2.49" y2="1.33" layer="25"/>
<rectangle x1="2.89" y1="1.31" x2="3.79" y2="1.33" layer="25"/>
<rectangle x1="0.35" y1="1.33" x2="0.85" y2="1.35" layer="25"/>
<rectangle x1="2.33" y1="1.33" x2="2.47" y2="1.35" layer="25"/>
<rectangle x1="2.89" y1="1.33" x2="3.81" y2="1.35" layer="25"/>
<rectangle x1="0.35" y1="1.35" x2="0.87" y2="1.37" layer="25"/>
<rectangle x1="2.33" y1="1.35" x2="2.45" y2="1.37" layer="25"/>
<rectangle x1="2.89" y1="1.35" x2="3.81" y2="1.37" layer="25"/>
<rectangle x1="0.33" y1="1.37" x2="0.87" y2="1.39" layer="25"/>
<rectangle x1="2.31" y1="1.37" x2="2.43" y2="1.39" layer="25"/>
<rectangle x1="2.89" y1="1.37" x2="3.81" y2="1.39" layer="25"/>
<rectangle x1="0.33" y1="1.39" x2="0.87" y2="1.41" layer="25"/>
<rectangle x1="2.31" y1="1.39" x2="2.43" y2="1.41" layer="25"/>
<rectangle x1="2.89" y1="1.39" x2="3.81" y2="1.41" layer="25"/>
<rectangle x1="0.33" y1="1.41" x2="0.89" y2="1.43" layer="25"/>
<rectangle x1="2.29" y1="1.41" x2="2.41" y2="1.43" layer="25"/>
<rectangle x1="2.89" y1="1.41" x2="3.83" y2="1.43" layer="25"/>
<rectangle x1="0.33" y1="1.43" x2="0.89" y2="1.45" layer="25"/>
<rectangle x1="2.29" y1="1.43" x2="2.39" y2="1.45" layer="25"/>
<rectangle x1="2.89" y1="1.43" x2="3.83" y2="1.45" layer="25"/>
<rectangle x1="0.31" y1="1.45" x2="0.91" y2="1.47" layer="25"/>
<rectangle x1="2.27" y1="1.45" x2="2.37" y2="1.47" layer="25"/>
<rectangle x1="2.89" y1="1.45" x2="3.83" y2="1.47" layer="25"/>
<rectangle x1="0.31" y1="1.47" x2="0.91" y2="1.49" layer="25"/>
<rectangle x1="2.27" y1="1.47" x2="2.35" y2="1.49" layer="25"/>
<rectangle x1="2.89" y1="1.47" x2="3.83" y2="1.49" layer="25"/>
<rectangle x1="0.31" y1="1.49" x2="0.93" y2="1.51" layer="25"/>
<rectangle x1="2.25" y1="1.49" x2="2.35" y2="1.51" layer="25"/>
<rectangle x1="2.89" y1="1.49" x2="3.85" y2="1.51" layer="25"/>
<rectangle x1="0.31" y1="1.51" x2="0.95" y2="1.53" layer="25"/>
<rectangle x1="2.23" y1="1.51" x2="2.33" y2="1.53" layer="25"/>
<rectangle x1="2.89" y1="1.51" x2="3.85" y2="1.53" layer="25"/>
<rectangle x1="0.29" y1="1.53" x2="0.95" y2="1.55" layer="25"/>
<rectangle x1="2.21" y1="1.53" x2="2.31" y2="1.55" layer="25"/>
<rectangle x1="2.89" y1="1.53" x2="3.85" y2="1.55" layer="25"/>
<rectangle x1="0.29" y1="1.55" x2="0.97" y2="1.57" layer="25"/>
<rectangle x1="2.21" y1="1.55" x2="2.29" y2="1.57" layer="25"/>
<rectangle x1="2.89" y1="1.55" x2="3.85" y2="1.57" layer="25"/>
<rectangle x1="0.29" y1="1.57" x2="0.99" y2="1.59" layer="25"/>
<rectangle x1="2.19" y1="1.57" x2="2.29" y2="1.59" layer="25"/>
<rectangle x1="2.89" y1="1.57" x2="3.85" y2="1.59" layer="25"/>
<rectangle x1="0.29" y1="1.59" x2="1.01" y2="1.61" layer="25"/>
<rectangle x1="2.17" y1="1.59" x2="2.27" y2="1.61" layer="25"/>
<rectangle x1="2.89" y1="1.59" x2="3.85" y2="1.61" layer="25"/>
<rectangle x1="0.29" y1="1.61" x2="1.03" y2="1.63" layer="25"/>
<rectangle x1="2.15" y1="1.61" x2="2.25" y2="1.63" layer="25"/>
<rectangle x1="2.63" y1="1.61" x2="2.65" y2="1.63" layer="25"/>
<rectangle x1="2.89" y1="1.61" x2="3.87" y2="1.63" layer="25"/>
<rectangle x1="0.27" y1="1.63" x2="1.05" y2="1.65" layer="25"/>
<rectangle x1="2.13" y1="1.63" x2="2.23" y2="1.65" layer="25"/>
<rectangle x1="2.61" y1="1.63" x2="2.65" y2="1.65" layer="25"/>
<rectangle x1="2.89" y1="1.63" x2="3.87" y2="1.65" layer="25"/>
<rectangle x1="0.27" y1="1.65" x2="1.07" y2="1.67" layer="25"/>
<rectangle x1="2.11" y1="1.65" x2="2.23" y2="1.67" layer="25"/>
<rectangle x1="2.61" y1="1.65" x2="2.65" y2="1.67" layer="25"/>
<rectangle x1="2.89" y1="1.65" x2="3.87" y2="1.67" layer="25"/>
<rectangle x1="0.27" y1="1.67" x2="1.09" y2="1.69" layer="25"/>
<rectangle x1="2.09" y1="1.67" x2="2.21" y2="1.69" layer="25"/>
<rectangle x1="2.59" y1="1.67" x2="2.65" y2="1.69" layer="25"/>
<rectangle x1="2.89" y1="1.67" x2="3.87" y2="1.69" layer="25"/>
<rectangle x1="0.27" y1="1.69" x2="1.11" y2="1.71" layer="25"/>
<rectangle x1="2.05" y1="1.69" x2="2.19" y2="1.71" layer="25"/>
<rectangle x1="2.57" y1="1.69" x2="2.65" y2="1.71" layer="25"/>
<rectangle x1="2.89" y1="1.69" x2="3.87" y2="1.71" layer="25"/>
<rectangle x1="0.27" y1="1.71" x2="1.13" y2="1.73" layer="25"/>
<rectangle x1="2.03" y1="1.71" x2="2.17" y2="1.73" layer="25"/>
<rectangle x1="2.55" y1="1.71" x2="2.65" y2="1.73" layer="25"/>
<rectangle x1="2.89" y1="1.71" x2="3.87" y2="1.73" layer="25"/>
<rectangle x1="0.27" y1="1.73" x2="1.17" y2="1.75" layer="25"/>
<rectangle x1="2.01" y1="1.73" x2="2.17" y2="1.75" layer="25"/>
<rectangle x1="2.55" y1="1.73" x2="2.65" y2="1.75" layer="25"/>
<rectangle x1="2.89" y1="1.73" x2="3.87" y2="1.75" layer="25"/>
<rectangle x1="0.27" y1="1.75" x2="1.19" y2="1.77" layer="25"/>
<rectangle x1="1.97" y1="1.75" x2="2.15" y2="1.77" layer="25"/>
<rectangle x1="2.53" y1="1.75" x2="2.65" y2="1.77" layer="25"/>
<rectangle x1="2.89" y1="1.75" x2="3.87" y2="1.77" layer="25"/>
<rectangle x1="0.27" y1="1.77" x2="1.23" y2="1.79" layer="25"/>
<rectangle x1="1.93" y1="1.77" x2="2.13" y2="1.79" layer="25"/>
<rectangle x1="2.51" y1="1.77" x2="2.65" y2="1.79" layer="25"/>
<rectangle x1="2.89" y1="1.77" x2="3.87" y2="1.79" layer="25"/>
<rectangle x1="0.27" y1="1.79" x2="1.29" y2="1.81" layer="25"/>
<rectangle x1="1.89" y1="1.79" x2="2.11" y2="1.81" layer="25"/>
<rectangle x1="2.49" y1="1.79" x2="2.65" y2="1.81" layer="25"/>
<rectangle x1="2.89" y1="1.79" x2="3.87" y2="1.81" layer="25"/>
<rectangle x1="0.27" y1="1.81" x2="1.33" y2="1.83" layer="25"/>
<rectangle x1="1.83" y1="1.81" x2="2.09" y2="1.83" layer="25"/>
<rectangle x1="2.49" y1="1.81" x2="2.65" y2="1.83" layer="25"/>
<rectangle x1="2.89" y1="1.81" x2="3.89" y2="1.83" layer="25"/>
<rectangle x1="0.27" y1="1.83" x2="1.41" y2="1.85" layer="25"/>
<rectangle x1="1.75" y1="1.83" x2="2.09" y2="1.85" layer="25"/>
<rectangle x1="2.47" y1="1.83" x2="2.65" y2="1.85" layer="25"/>
<rectangle x1="2.89" y1="1.83" x2="3.89" y2="1.85" layer="25"/>
<rectangle x1="0.25" y1="1.85" x2="1.55" y2="1.87" layer="25"/>
<rectangle x1="1.59" y1="1.85" x2="2.07" y2="1.87" layer="25"/>
<rectangle x1="2.45" y1="1.85" x2="2.65" y2="1.87" layer="25"/>
<rectangle x1="2.89" y1="1.85" x2="3.89" y2="1.87" layer="25"/>
<rectangle x1="0.25" y1="1.87" x2="2.05" y2="1.89" layer="25"/>
<rectangle x1="2.43" y1="1.87" x2="2.65" y2="1.89" layer="25"/>
<rectangle x1="2.89" y1="1.87" x2="3.89" y2="1.89" layer="25"/>
<rectangle x1="0.25" y1="1.89" x2="2.03" y2="1.91" layer="25"/>
<rectangle x1="2.43" y1="1.89" x2="2.65" y2="1.91" layer="25"/>
<rectangle x1="2.89" y1="1.89" x2="3.89" y2="1.91" layer="25"/>
<rectangle x1="0.25" y1="1.91" x2="2.03" y2="1.93" layer="25"/>
<rectangle x1="2.41" y1="1.91" x2="2.65" y2="1.93" layer="25"/>
<rectangle x1="2.89" y1="1.91" x2="3.89" y2="1.93" layer="25"/>
<rectangle x1="0.25" y1="1.93" x2="2.01" y2="1.95" layer="25"/>
<rectangle x1="2.39" y1="1.93" x2="2.65" y2="1.95" layer="25"/>
<rectangle x1="2.89" y1="1.93" x2="3.89" y2="1.95" layer="25"/>
<rectangle x1="0.25" y1="1.95" x2="1.99" y2="1.97" layer="25"/>
<rectangle x1="2.37" y1="1.95" x2="2.65" y2="1.97" layer="25"/>
<rectangle x1="2.89" y1="1.95" x2="3.87" y2="1.97" layer="25"/>
<rectangle x1="0.25" y1="1.97" x2="1.97" y2="1.99" layer="25"/>
<rectangle x1="2.35" y1="1.97" x2="2.65" y2="1.99" layer="25"/>
<rectangle x1="2.89" y1="1.97" x2="3.87" y2="1.99" layer="25"/>
<rectangle x1="0.27" y1="1.99" x2="1.97" y2="2.01" layer="25"/>
<rectangle x1="2.35" y1="1.99" x2="2.65" y2="2.01" layer="25"/>
<rectangle x1="2.89" y1="1.99" x2="3.87" y2="2.01" layer="25"/>
<rectangle x1="0.27" y1="2.01" x2="1.95" y2="2.03" layer="25"/>
<rectangle x1="2.33" y1="2.01" x2="2.65" y2="2.03" layer="25"/>
<rectangle x1="2.89" y1="2.01" x2="3.87" y2="2.03" layer="25"/>
<rectangle x1="0.27" y1="2.03" x2="1.93" y2="2.05" layer="25"/>
<rectangle x1="2.31" y1="2.03" x2="2.63" y2="2.05" layer="25"/>
<rectangle x1="2.97" y1="2.03" x2="3.87" y2="2.05" layer="25"/>
<rectangle x1="0.27" y1="2.05" x2="1.91" y2="2.07" layer="25"/>
<rectangle x1="2.29" y1="2.05" x2="2.59" y2="2.07" layer="25"/>
<rectangle x1="3.03" y1="2.05" x2="3.87" y2="2.07" layer="25"/>
<rectangle x1="0.27" y1="2.07" x2="1.89" y2="2.09" layer="25"/>
<rectangle x1="2.29" y1="2.07" x2="2.55" y2="2.09" layer="25"/>
<rectangle x1="3.07" y1="2.07" x2="3.87" y2="2.09" layer="25"/>
<rectangle x1="0.27" y1="2.09" x2="1.89" y2="2.11" layer="25"/>
<rectangle x1="2.27" y1="2.09" x2="2.51" y2="2.11" layer="25"/>
<rectangle x1="3.11" y1="2.09" x2="3.87" y2="2.11" layer="25"/>
<rectangle x1="0.27" y1="2.11" x2="1.41" y2="2.13" layer="25"/>
<rectangle x1="1.69" y1="2.11" x2="1.87" y2="2.13" layer="25"/>
<rectangle x1="2.25" y1="2.11" x2="2.47" y2="2.13" layer="25"/>
<rectangle x1="3.15" y1="2.11" x2="3.87" y2="2.13" layer="25"/>
<rectangle x1="0.27" y1="2.13" x2="1.35" y2="2.15" layer="25"/>
<rectangle x1="1.75" y1="2.13" x2="1.85" y2="2.15" layer="25"/>
<rectangle x1="2.23" y1="2.13" x2="2.45" y2="2.15" layer="25"/>
<rectangle x1="3.17" y1="2.13" x2="3.87" y2="2.15" layer="25"/>
<rectangle x1="0.27" y1="2.15" x2="1.31" y2="2.17" layer="25"/>
<rectangle x1="1.79" y1="2.15" x2="1.83" y2="2.17" layer="25"/>
<rectangle x1="2.23" y1="2.15" x2="2.43" y2="2.17" layer="25"/>
<rectangle x1="3.19" y1="2.15" x2="3.87" y2="2.17" layer="25"/>
<rectangle x1="0.27" y1="2.17" x2="1.27" y2="2.19" layer="25"/>
<rectangle x1="2.21" y1="2.17" x2="2.41" y2="2.19" layer="25"/>
<rectangle x1="3.21" y1="2.17" x2="3.85" y2="2.19" layer="25"/>
<rectangle x1="0.29" y1="2.19" x2="1.23" y2="2.21" layer="25"/>
<rectangle x1="2.19" y1="2.19" x2="2.39" y2="2.21" layer="25"/>
<rectangle x1="3.23" y1="2.19" x2="3.85" y2="2.21" layer="25"/>
<rectangle x1="0.29" y1="2.21" x2="1.21" y2="2.23" layer="25"/>
<rectangle x1="2.17" y1="2.21" x2="2.37" y2="2.23" layer="25"/>
<rectangle x1="3.25" y1="2.21" x2="3.85" y2="2.23" layer="25"/>
<rectangle x1="0.29" y1="2.23" x2="1.17" y2="2.25" layer="25"/>
<rectangle x1="2.17" y1="2.23" x2="2.35" y2="2.25" layer="25"/>
<rectangle x1="3.27" y1="2.23" x2="3.85" y2="2.25" layer="25"/>
<rectangle x1="0.29" y1="2.25" x2="1.15" y2="2.27" layer="25"/>
<rectangle x1="2.15" y1="2.25" x2="2.33" y2="2.27" layer="25"/>
<rectangle x1="3.29" y1="2.25" x2="3.85" y2="2.27" layer="25"/>
<rectangle x1="0.29" y1="2.27" x2="1.13" y2="2.29" layer="25"/>
<rectangle x1="2.13" y1="2.27" x2="2.33" y2="2.29" layer="25"/>
<rectangle x1="3.31" y1="2.27" x2="3.83" y2="2.29" layer="25"/>
<rectangle x1="0.29" y1="2.29" x2="1.11" y2="2.31" layer="25"/>
<rectangle x1="2.11" y1="2.29" x2="2.31" y2="2.31" layer="25"/>
<rectangle x1="3.31" y1="2.29" x2="3.83" y2="2.31" layer="25"/>
<rectangle x1="0.31" y1="2.31" x2="1.09" y2="2.33" layer="25"/>
<rectangle x1="2.09" y1="2.31" x2="2.31" y2="2.33" layer="25"/>
<rectangle x1="3.33" y1="2.31" x2="3.83" y2="2.33" layer="25"/>
<rectangle x1="0.31" y1="2.33" x2="1.09" y2="2.35" layer="25"/>
<rectangle x1="2.09" y1="2.33" x2="2.29" y2="2.35" layer="25"/>
<rectangle x1="3.33" y1="2.33" x2="3.83" y2="2.35" layer="25"/>
<rectangle x1="0.31" y1="2.35" x2="1.07" y2="2.37" layer="25"/>
<rectangle x1="2.07" y1="2.35" x2="2.29" y2="2.37" layer="25"/>
<rectangle x1="3.35" y1="2.35" x2="3.81" y2="2.37" layer="25"/>
<rectangle x1="0.31" y1="2.37" x2="1.05" y2="2.39" layer="25"/>
<rectangle x1="2.05" y1="2.37" x2="2.27" y2="2.39" layer="25"/>
<rectangle x1="3.35" y1="2.37" x2="3.81" y2="2.39" layer="25"/>
<rectangle x1="0.33" y1="2.39" x2="1.05" y2="2.41" layer="25"/>
<rectangle x1="2.05" y1="2.39" x2="2.27" y2="2.41" layer="25"/>
<rectangle x1="3.37" y1="2.39" x2="3.81" y2="2.41" layer="25"/>
<rectangle x1="0.33" y1="2.41" x2="1.03" y2="2.43" layer="25"/>
<rectangle x1="2.07" y1="2.41" x2="2.27" y2="2.43" layer="25"/>
<rectangle x1="3.37" y1="2.41" x2="3.81" y2="2.43" layer="25"/>
<rectangle x1="0.33" y1="2.43" x2="1.03" y2="2.45" layer="25"/>
<rectangle x1="2.07" y1="2.43" x2="2.27" y2="2.45" layer="25"/>
<rectangle x1="3.37" y1="2.43" x2="3.79" y2="2.45" layer="25"/>
<rectangle x1="0.33" y1="2.45" x2="1.01" y2="2.47" layer="25"/>
<rectangle x1="2.07" y1="2.45" x2="2.25" y2="2.47" layer="25"/>
<rectangle x1="3.39" y1="2.45" x2="3.79" y2="2.47" layer="25"/>
<rectangle x1="0.35" y1="2.47" x2="1.01" y2="2.49" layer="25"/>
<rectangle x1="2.09" y1="2.47" x2="2.25" y2="2.49" layer="25"/>
<rectangle x1="3.39" y1="2.47" x2="3.79" y2="2.49" layer="25"/>
<rectangle x1="0.35" y1="2.49" x2="0.99" y2="2.51" layer="25"/>
<rectangle x1="2.09" y1="2.49" x2="2.25" y2="2.51" layer="25"/>
<rectangle x1="3.39" y1="2.49" x2="3.77" y2="2.51" layer="25"/>
<rectangle x1="0.35" y1="2.51" x2="0.99" y2="2.53" layer="25"/>
<rectangle x1="2.09" y1="2.51" x2="2.25" y2="2.53" layer="25"/>
<rectangle x1="3.39" y1="2.51" x2="3.77" y2="2.53" layer="25"/>
<rectangle x1="0.37" y1="2.53" x2="0.99" y2="2.55" layer="25"/>
<rectangle x1="2.11" y1="2.53" x2="2.25" y2="2.55" layer="25"/>
<rectangle x1="3.39" y1="2.53" x2="3.77" y2="2.55" layer="25"/>
<rectangle x1="0.37" y1="2.55" x2="0.99" y2="2.57" layer="25"/>
<rectangle x1="2.11" y1="2.55" x2="2.25" y2="2.57" layer="25"/>
<rectangle x1="3.39" y1="2.55" x2="3.75" y2="2.57" layer="25"/>
<rectangle x1="0.39" y1="2.57" x2="0.97" y2="2.59" layer="25"/>
<rectangle x1="2.11" y1="2.57" x2="2.25" y2="2.59" layer="25"/>
<rectangle x1="3.41" y1="2.57" x2="3.75" y2="2.59" layer="25"/>
<rectangle x1="0.39" y1="2.59" x2="0.97" y2="2.61" layer="25"/>
<rectangle x1="2.11" y1="2.59" x2="2.25" y2="2.61" layer="25"/>
<rectangle x1="3.41" y1="2.59" x2="3.73" y2="2.61" layer="25"/>
<rectangle x1="0.39" y1="2.61" x2="0.97" y2="2.63" layer="25"/>
<rectangle x1="2.11" y1="2.61" x2="2.25" y2="2.63" layer="25"/>
<rectangle x1="3.41" y1="2.61" x2="3.73" y2="2.63" layer="25"/>
<rectangle x1="0.41" y1="2.63" x2="0.97" y2="2.65" layer="25"/>
<rectangle x1="2.11" y1="2.63" x2="2.25" y2="2.65" layer="25"/>
<rectangle x1="3.41" y1="2.63" x2="3.73" y2="2.65" layer="25"/>
<rectangle x1="0.41" y1="2.65" x2="0.97" y2="2.67" layer="25"/>
<rectangle x1="2.13" y1="2.65" x2="2.25" y2="2.67" layer="25"/>
<rectangle x1="3.39" y1="2.65" x2="3.71" y2="2.67" layer="25"/>
<rectangle x1="0.43" y1="2.67" x2="0.97" y2="2.69" layer="25"/>
<rectangle x1="2.13" y1="2.67" x2="2.25" y2="2.69" layer="25"/>
<rectangle x1="3.39" y1="2.67" x2="3.71" y2="2.69" layer="25"/>
<rectangle x1="0.43" y1="2.69" x2="0.97" y2="2.71" layer="25"/>
<rectangle x1="2.13" y1="2.69" x2="2.25" y2="2.71" layer="25"/>
<rectangle x1="3.39" y1="2.69" x2="3.69" y2="2.71" layer="25"/>
<rectangle x1="0.45" y1="2.71" x2="0.97" y2="2.73" layer="25"/>
<rectangle x1="2.11" y1="2.71" x2="2.27" y2="2.73" layer="25"/>
<rectangle x1="3.39" y1="2.71" x2="3.69" y2="2.73" layer="25"/>
<rectangle x1="0.45" y1="2.73" x2="0.97" y2="2.75" layer="25"/>
<rectangle x1="2.11" y1="2.73" x2="2.27" y2="2.75" layer="25"/>
<rectangle x1="3.39" y1="2.73" x2="3.67" y2="2.75" layer="25"/>
<rectangle x1="0.47" y1="2.75" x2="0.97" y2="2.77" layer="25"/>
<rectangle x1="2.11" y1="2.75" x2="2.27" y2="2.77" layer="25"/>
<rectangle x1="3.39" y1="2.75" x2="3.67" y2="2.77" layer="25"/>
<rectangle x1="0.47" y1="2.77" x2="0.97" y2="2.79" layer="25"/>
<rectangle x1="2.11" y1="2.77" x2="2.29" y2="2.79" layer="25"/>
<rectangle x1="3.37" y1="2.77" x2="3.65" y2="2.79" layer="25"/>
<rectangle x1="0.49" y1="2.79" x2="0.97" y2="2.81" layer="25"/>
<rectangle x1="2.11" y1="2.79" x2="2.29" y2="2.81" layer="25"/>
<rectangle x1="3.37" y1="2.79" x2="3.65" y2="2.81" layer="25"/>
<rectangle x1="0.49" y1="2.81" x2="0.99" y2="2.83" layer="25"/>
<rectangle x1="2.11" y1="2.81" x2="2.31" y2="2.83" layer="25"/>
<rectangle x1="3.37" y1="2.81" x2="3.63" y2="2.83" layer="25"/>
<rectangle x1="0.51" y1="2.83" x2="0.99" y2="2.85" layer="25"/>
<rectangle x1="2.09" y1="2.83" x2="2.31" y2="2.85" layer="25"/>
<rectangle x1="3.35" y1="2.83" x2="3.61" y2="2.85" layer="25"/>
<rectangle x1="0.51" y1="2.85" x2="0.99" y2="2.87" layer="25"/>
<rectangle x1="2.09" y1="2.85" x2="2.33" y2="2.87" layer="25"/>
<rectangle x1="3.35" y1="2.85" x2="3.61" y2="2.87" layer="25"/>
<rectangle x1="0.53" y1="2.87" x2="0.99" y2="2.89" layer="25"/>
<rectangle x1="2.09" y1="2.87" x2="2.33" y2="2.89" layer="25"/>
<rectangle x1="3.33" y1="2.87" x2="3.59" y2="2.89" layer="25"/>
<rectangle x1="0.55" y1="2.89" x2="1.01" y2="2.91" layer="25"/>
<rectangle x1="2.07" y1="2.89" x2="2.35" y2="2.91" layer="25"/>
<rectangle x1="3.33" y1="2.89" x2="3.59" y2="2.91" layer="25"/>
<rectangle x1="0.55" y1="2.91" x2="1.01" y2="2.93" layer="25"/>
<rectangle x1="2.07" y1="2.91" x2="2.35" y2="2.93" layer="25"/>
<rectangle x1="3.31" y1="2.91" x2="3.57" y2="2.93" layer="25"/>
<rectangle x1="0.57" y1="2.93" x2="1.03" y2="2.95" layer="25"/>
<rectangle x1="2.05" y1="2.93" x2="2.37" y2="2.95" layer="25"/>
<rectangle x1="3.29" y1="2.93" x2="3.55" y2="2.95" layer="25"/>
<rectangle x1="0.57" y1="2.95" x2="1.03" y2="2.97" layer="25"/>
<rectangle x1="2.05" y1="2.95" x2="2.39" y2="2.97" layer="25"/>
<rectangle x1="3.27" y1="2.95" x2="3.55" y2="2.97" layer="25"/>
<rectangle x1="0.59" y1="2.97" x2="1.05" y2="2.99" layer="25"/>
<rectangle x1="2.03" y1="2.97" x2="2.41" y2="2.99" layer="25"/>
<rectangle x1="3.27" y1="2.97" x2="3.53" y2="2.99" layer="25"/>
<rectangle x1="0.61" y1="2.99" x2="1.05" y2="3.01" layer="25"/>
<rectangle x1="2.03" y1="2.99" x2="2.43" y2="3.01" layer="25"/>
<rectangle x1="3.25" y1="2.99" x2="3.51" y2="3.01" layer="25"/>
<rectangle x1="0.63" y1="3.01" x2="1.07" y2="3.03" layer="25"/>
<rectangle x1="2.01" y1="3.01" x2="2.45" y2="3.03" layer="25"/>
<rectangle x1="3.23" y1="3.01" x2="3.49" y2="3.03" layer="25"/>
<rectangle x1="0.63" y1="3.03" x2="1.09" y2="3.05" layer="25"/>
<rectangle x1="1.99" y1="3.03" x2="2.47" y2="3.05" layer="25"/>
<rectangle x1="3.19" y1="3.03" x2="3.49" y2="3.05" layer="25"/>
<rectangle x1="0.65" y1="3.05" x2="1.09" y2="3.07" layer="25"/>
<rectangle x1="1.99" y1="3.05" x2="2.51" y2="3.07" layer="25"/>
<rectangle x1="3.17" y1="3.05" x2="3.47" y2="3.07" layer="25"/>
<rectangle x1="0.67" y1="3.07" x2="1.11" y2="3.09" layer="25"/>
<rectangle x1="1.97" y1="3.07" x2="2.53" y2="3.09" layer="25"/>
<rectangle x1="3.15" y1="3.07" x2="3.45" y2="3.09" layer="25"/>
<rectangle x1="0.69" y1="3.09" x2="1.13" y2="3.11" layer="25"/>
<rectangle x1="1.95" y1="3.09" x2="2.57" y2="3.11" layer="25"/>
<rectangle x1="3.11" y1="3.09" x2="3.43" y2="3.11" layer="25"/>
<rectangle x1="0.71" y1="3.11" x2="1.15" y2="3.13" layer="25"/>
<rectangle x1="1.93" y1="3.11" x2="2.61" y2="3.13" layer="25"/>
<rectangle x1="3.07" y1="3.11" x2="3.41" y2="3.13" layer="25"/>
<rectangle x1="0.73" y1="3.13" x2="1.17" y2="3.15" layer="25"/>
<rectangle x1="1.89" y1="3.13" x2="2.67" y2="3.15" layer="25"/>
<rectangle x1="3.01" y1="3.13" x2="3.39" y2="3.15" layer="25"/>
<rectangle x1="0.73" y1="3.15" x2="1.21" y2="3.17" layer="25"/>
<rectangle x1="1.87" y1="3.15" x2="2.77" y2="3.17" layer="25"/>
<rectangle x1="2.91" y1="3.15" x2="3.37" y2="3.17" layer="25"/>
<rectangle x1="0.75" y1="3.17" x2="1.23" y2="3.19" layer="25"/>
<rectangle x1="1.85" y1="3.17" x2="3.35" y2="3.19" layer="25"/>
<rectangle x1="0.77" y1="3.19" x2="1.27" y2="3.21" layer="25"/>
<rectangle x1="1.81" y1="3.19" x2="3.35" y2="3.21" layer="25"/>
<rectangle x1="0.79" y1="3.21" x2="1.31" y2="3.23" layer="25"/>
<rectangle x1="1.77" y1="3.21" x2="3.33" y2="3.23" layer="25"/>
<rectangle x1="0.81" y1="3.23" x2="1.37" y2="3.25" layer="25"/>
<rectangle x1="1.71" y1="3.23" x2="3.29" y2="3.25" layer="25"/>
<rectangle x1="0.83" y1="3.25" x2="1.45" y2="3.27" layer="25"/>
<rectangle x1="1.61" y1="3.25" x2="3.27" y2="3.27" layer="25"/>
<rectangle x1="0.85" y1="3.27" x2="3.25" y2="3.29" layer="25"/>
<rectangle x1="0.89" y1="3.29" x2="3.23" y2="3.31" layer="25"/>
<rectangle x1="0.91" y1="3.31" x2="3.21" y2="3.33" layer="25"/>
<rectangle x1="0.93" y1="3.33" x2="3.19" y2="3.35" layer="25"/>
<rectangle x1="0.95" y1="3.35" x2="3.15" y2="3.37" layer="25"/>
<rectangle x1="0.97" y1="3.37" x2="3.13" y2="3.39" layer="25"/>
<rectangle x1="1.01" y1="3.39" x2="3.11" y2="3.41" layer="25"/>
<rectangle x1="1.03" y1="3.41" x2="3.07" y2="3.43" layer="25"/>
<rectangle x1="1.07" y1="3.43" x2="3.05" y2="3.45" layer="25"/>
<rectangle x1="1.09" y1="3.45" x2="3.01" y2="3.47" layer="25"/>
<rectangle x1="1.13" y1="3.47" x2="2.99" y2="3.49" layer="25"/>
<rectangle x1="1.15" y1="3.49" x2="2.95" y2="3.51" layer="25"/>
<rectangle x1="1.19" y1="3.51" x2="2.91" y2="3.53" layer="25"/>
<rectangle x1="1.23" y1="3.53" x2="2.87" y2="3.55" layer="25"/>
<rectangle x1="1.27" y1="3.55" x2="2.83" y2="3.57" layer="25"/>
<rectangle x1="1.31" y1="3.57" x2="2.79" y2="3.59" layer="25"/>
<rectangle x1="1.35" y1="3.59" x2="2.75" y2="3.61" layer="25"/>
<rectangle x1="1.41" y1="3.61" x2="2.69" y2="3.63" layer="25"/>
<rectangle x1="1.47" y1="3.63" x2="2.65" y2="3.65" layer="25"/>
<rectangle x1="1.53" y1="3.65" x2="2.59" y2="3.67" layer="25"/>
<rectangle x1="1.59" y1="3.67" x2="2.51" y2="3.69" layer="25"/>
<rectangle x1="1.67" y1="3.69" x2="2.43" y2="3.71" layer="25"/>
<rectangle x1="1.77" y1="3.71" x2="2.33" y2="3.73" layer="25"/>
<rectangle x1="1.99" y1="3.73" x2="2.11" y2="3.75" layer="25"/>
<rectangle x1="16.53" y1="1.56" x2="16.63" y2="1.66" layer="25"/>
<rectangle x1="16.53" y1="2.98" x2="16.63" y2="3.08" layer="25"/>
</package>
</packages>
<packages3d>
<package3d name="SW_M" urn="urn:adsk.eagle:package:3097791/4" locally_modified="yes" type="box" library_version="6">
<description>WS-TSW-6x6 mm washable J-Bend SMD Tact Switch,4 pins</description>
<packageinstances>
<packageinstance name="SW_M"/>
</packageinstances>
</package3d>
<package3d name="DOCKING_CONNECTOR" urn="urn:adsk.eagle:package:14805565/2" locally_modified="yes" type="model" library_version="2">
<description>&lt;B&gt;WR-PHD &lt;/B&gt;&lt;BR&gt;2.54 mm Dual Socket Header, 12 Pins</description>
<packageinstances>
<packageinstance name="DOCKING_CONNECTOR"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="SW_M" library_version="27">
<wire x1="-2.54" y1="5.08" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-0.37" y1="-1.4" x2="-1.64" y2="-0.13" width="0.254" layer="94"/>
<circle x="0" y="1.87" radius="0.5" width="0.254" layer="94"/>
<circle x="0" y="-1.87" radius="0.5" width="0.254" layer="94"/>
<pin name="1" x="-5.08" y="5.08" visible="pad" length="short" direction="pas"/>
<pin name="3" x="-5.08" y="-5.08" visible="pad" length="short" direction="pas"/>
<pin name="2" x="5.08" y="5.08" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="4" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" rot="R180"/>
<text x="6.35" y="1.27" size="1.778" layer="95">&gt;Name</text>
<text x="6.35" y="-2.54" size="1.778" layer="96">&gt;Value</text>
</symbol>
<symbol name="CHARGING_PCB">
<pin name="OUT+" x="7.62" y="12.7" length="middle" rot="R180"/>
<pin name="B+" x="7.62" y="7.62" length="middle" rot="R180"/>
<pin name="B-" x="7.62" y="-5.08" length="middle" rot="R180"/>
<pin name="OUT-" x="7.62" y="-10.16" length="middle" rot="R180"/>
<pin name="IN+" x="-38.1" y="-10.16" length="middle"/>
<pin name="IN-" x="-38.1" y="12.7" length="middle"/>
</symbol>
<symbol name="BOOST">
<pin name="VIN+" x="30.48" y="12.7" length="middle" rot="R180"/>
<pin name="VIN-" x="30.48" y="0" length="middle" rot="R180"/>
<pin name="VOUT-" x="-15.24" y="0" length="middle"/>
<pin name="VOUT+" x="-15.24" y="12.7" length="middle"/>
</symbol>
<symbol name="DOCKING_CONNECTOR" library_version="72">
<description>6X2Row Socket Header THT</description>
<pin name="1" x="-7.62" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="2" x="-7.62" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="3" x="-5.08" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="4" x="-5.08" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="5" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="6" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="7" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="8" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="9" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="10" x="2.54" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="11" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="12" x="5.08" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<text x="-9.96" y="1.078" size="1.016" layer="95" align="bottom-right">&gt;NAME</text>
<text x="-9.91" y="-1.58" size="1.016" layer="96" align="bottom-right">&gt;VALUE</text>
<wire x1="-8.89" y1="2.54" x2="6.35" y2="2.54" width="0.254" layer="94"/>
<wire x1="6.35" y1="-2.54" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="-8.89" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-8.89" y1="-2.54" x2="-8.89" y2="2.54" width="0.254" layer="94"/>
<wire x1="6.35" y1="2.54" x2="6.35" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="1.705" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.705" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="1.705" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="1.705" x2="-7.62" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.705" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-1.705" x2="-7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-1.705" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-1.705" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-1.705" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.705" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="1.705" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-1.705" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<circle x="0" y="1.27" radius="0.4445" width="0.254" layer="94"/>
<circle x="-2.54" y="1.27" radius="0.4445" width="0.254" layer="94"/>
<circle x="-5.08" y="1.27" radius="0.4445" width="0.254" layer="94"/>
<circle x="-7.62" y="1.27" radius="0.4445" width="0.254" layer="94"/>
<circle x="2.54" y="1.27" radius="0.4445" width="0.254" layer="94"/>
<circle x="-7.62" y="-1.27" radius="0.4445" width="0.254" layer="94"/>
<circle x="-5.08" y="-1.27" radius="0.4445" width="0.254" layer="94"/>
<circle x="-2.54" y="-1.27" radius="0.4445" width="0.254" layer="94"/>
<circle x="0" y="-1.27" radius="0.4445" width="0.254" layer="94"/>
<circle x="2.54" y="-1.27" radius="0.4445" width="0.254" layer="94"/>
<circle x="5.08" y="1.27" radius="0.4445" width="0.254" layer="94"/>
<circle x="5.08" y="-1.27" radius="0.4445" width="0.254" layer="94"/>
</symbol>
<symbol name="GYRO" library_version="115">
<wire x1="-15.24" y1="17.78" x2="-15.24" y2="-22.86" width="0.6096" layer="94"/>
<wire x1="-15.24" y1="-22.86" x2="12.7" y2="-22.86" width="0.6096" layer="94"/>
<wire x1="12.7" y1="-22.86" x2="12.7" y2="17.78" width="0.6096" layer="94"/>
<wire x1="12.7" y1="17.78" x2="-15.24" y2="17.78" width="0.6096" layer="94"/>
<text x="8.636" y="-9.398" size="1.778" layer="94" rot="R270">GY-521</text>
<text x="10.16" y="-7.62" size="1.778" layer="94" rot="R90">ITG/MPU</text>
<pin name="VCC" x="-20.32" y="15.24" length="middle"/>
<pin name="GND" x="-20.32" y="10.16" length="middle"/>
<pin name="SCL" x="-20.32" y="5.08" length="middle"/>
<pin name="SDA" x="-20.32" y="0" length="middle"/>
<pin name="XDA" x="-20.32" y="-5.08" length="middle"/>
<pin name="XCL" x="-20.32" y="-10.16" length="middle"/>
<pin name="ADO" x="-20.32" y="-15.24" length="middle"/>
<pin name="INT" x="-20.32" y="-20.32" length="middle"/>
<wire x1="-2.54" y1="-20.32" x2="2.54" y2="-20.32" width="0.3048" layer="94"/>
<wire x1="2.54" y1="-20.32" x2="1.27" y2="-19.05" width="0.3048" layer="94"/>
<wire x1="2.54" y1="-20.32" x2="1.27" y2="-21.59" width="0.3048" layer="94"/>
<wire x1="-2.54" y1="-20.32" x2="-2.54" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="-2.54" y1="-15.24" x2="-3.81" y2="-16.51" width="0.3048" layer="94"/>
<wire x1="-2.54" y1="-15.24" x2="-1.27" y2="-16.51" width="0.3048" layer="94"/>
<text x="3.81" y="-21.082" size="1.778" layer="94">x</text>
<text x="0" y="-15.24" size="1.778" layer="94">y</text>
<text x="-6.604" y="14.732" size="1.778" layer="94">3.3V (or 5v)</text>
</symbol>
<symbol name="BME280">
<pin name="VCC" x="-7.62" y="5.08" length="middle"/>
<pin name="GND" x="-7.62" y="0" length="middle"/>
<pin name="SCL" x="-7.62" y="-5.08" length="middle"/>
<pin name="SDA" x="-7.62" y="-10.16" length="middle"/>
<pin name="CSB" x="-7.62" y="-15.24" length="middle"/>
<pin name="SDD" x="-7.62" y="-20.32" length="middle"/>
<wire x1="-5.08" y1="10.16" x2="27.94" y2="10.16" width="0.254" layer="94"/>
<wire x1="27.94" y1="10.16" x2="27.94" y2="-25.4" width="0.254" layer="94"/>
<wire x1="27.94" y1="-25.4" x2="-5.08" y2="-25.4" width="0.254" layer="94"/>
</symbol>
<symbol name="MUTANTC" library_version="120">
<text x="0" y="2.54" size="3.81" layer="94" font="vector" ratio="20">mutantc</text>
<rectangle x1="0" y1="0" x2="20.32" y2="2.54" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SW_M" prefix="S" uservalue="yes">
<description>&lt;b&gt;6x6 mm SMD J-Bend washable WS-TASV&lt;/b&gt;&lt;br&gt;
&lt;BR&gt;
&lt;BR&gt;
&lt;br&gt;&lt;a href="http://katalog.we-online.de/media/images/v2/Family_WS-TASV_6x6_J-Bend_SMD_Tact_Switch_4304830xx8xx.jpg" title="Enlarge picture"&gt;
&lt;img src="http://katalog.we-online.de/media/images/v2/Family_WS-TASV_6x6_J-Bend_SMD_Tact_Switch_4304830xx8xx.jpg" width="320"&gt;&lt;/a&gt;&lt;p&gt;
Details see: &lt;a href="http://katalog.we-online.de/em/datasheet/4304x3xxx8x6.pdf"&gt;http://katalog.we-online.de/em/datasheet/4304x3xxx8x6.pdf&lt;/a&gt;&lt;p&gt;

2015 (C) Wurth Elektronik</description>
<gates>
<gate name="G$1" symbol="SW_M" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SW_M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3097791/4"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PM" prefix="C_P">
<gates>
<gate name="G$1" symbol="CHARGING_PCB" x="20.32" y="-2.54"/>
</gates>
<devices>
<device name="" package="PC">
<connects>
<connect gate="G$1" pin="B+" pad="B+"/>
<connect gate="G$1" pin="B-" pad="B-"/>
<connect gate="G$1" pin="IN+" pad="IN+"/>
<connect gate="G$1" pin="IN-" pad="IN-"/>
<connect gate="G$1" pin="OUT+" pad="OUT+"/>
<connect gate="G$1" pin="OUT-" pad="OUT-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BOOST">
<gates>
<gate name="G$1" symbol="BOOST" x="-7.62" y="-7.62"/>
</gates>
<devices>
<device name="" package="BOOST">
<connects>
<connect gate="G$1" pin="VIN+" pad="VIN+"/>
<connect gate="G$1" pin="VIN-" pad="VIN-"/>
<connect gate="G$1" pin="VOUT+" pad="VOUT+"/>
<connect gate="G$1" pin="VOUT-" pad="VOUT-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DOCKING_CONNECTOR" prefix="J" uservalue="yes">
<description>&lt;b&gt;WR-PHD 2.54 mm Dual Socket Header &lt;/b&gt;&lt;BR&gt;
&lt;BR&gt;
&lt;b&gt;ARTICLE PROPERTIES:&lt;/b&gt;
&lt;BR&gt;
&lt;BR&gt;Pins:4;​ 6;​ 8;​ 10;​ 12;​ 14;​ 16;​ 18;​ 20;​ 22;​ 24;​ 26;​ 28;​ 30;​ 32;​ 34;​ 36;​ 40;​ 44;​ 64;​ 72
&lt;BR&gt;
&lt;BR&gt;&lt;B&gt;KIND PROPERTIES&lt;/B&gt;:
&lt;BR&gt;
&lt;BR&gt;Pitch：2.54 mm
&lt;BR&gt;Quality Class:3 as per CECC 75 301-802
&lt;BR&gt;Rows：Dual
&lt;BR&gt;Gender：Socket Header
&lt;BR&gt;Type：Straight
&lt;BR&gt;
&lt;BR&gt;&lt;B&gt;MATERIAL PROPERTIES:&lt;/B&gt;
&lt;BR&gt;
&lt;BR&gt;Insulator Material :PBT
&lt;BR&gt;Insulator Flammability Rating:UL94 V-0
&lt;BR&gt;Contact Material:Copper Alloy
&lt;BR&gt;Contact Plating:Gold
&lt;BR&gt;Contact Type:Stamped
&lt;BR&gt;
&lt;BR&gt;&lt;B&gt;GENERAL INFORMATION&lt;/B&gt;
&lt;BR&gt;Operating Temperature:-40 up to +125 °C
&lt;BR&gt;Compliance:Lead free / RoHS
&lt;BR&gt;
&lt;BR&gt;&lt;B&gt;ELECTRICAL PROPERTIES:&lt;/B&gt;
&lt;BR&gt;
&lt;BR&gt;Rated Current:3 A
&lt;BR&gt;IR 1:3 A
&lt;BR&gt;Withstanding Voltage:500 V (AC)
&lt;BR&gt;Contact Resistance:20 mΩ
&lt;BR&gt;RISO:1000 MΩ
&lt;BR&gt;
&lt;BR&gt;&lt;B&gt;CERTIFICATION&lt;/B&gt;
&lt;BR&gt;UL Approval:E323964
&lt;BR&gt;&lt;B&gt;PACKAGING PROPERTIES&lt;/B&gt;
&lt;BR&gt;Packaging:Tray
&lt;BR&gt;
&lt;BR&gt;
&lt;br&gt;&lt;a href="https://katalog.we-online.com/media/images/v2/o57948v209%20Family_Dual_Socket_Header_WR-PHD_6130xx21821.jpg" title="Enlarge picture"&gt;
&lt;img src="https://katalog.we-online.com/media/images/v2/o57948v209%20Family_Dual_Socket_Header_WR-PHD_6130xx21821.jpg" width="320"&gt;&lt;/a&gt;&lt;p&gt;

Details see: &lt;a href="https://katalog.we-online.com/en/em/PHD_2_54_DUAL_SOCKET_HEADER_6130XX21821"&gt;https://katalog.we-online.com/en/em/PHD_2_54_DUAL_SOCKET_HEADER_6130XX21821&lt;/a&gt;&lt;p&gt;
&lt;BR&gt;
Updated by Yingchun,Shan 2019-10-21
&lt;BR&gt;
2019 (C) Würth Elektronik</description>
<gates>
<gate name="G$1" symbol="DOCKING_CONNECTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DOCKING_CONNECTOR">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:14805565/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DATASHEET-URL" value="https://katalog.we-online.com/em/datasheet/61301221821.pdf" constant="no"/>
<attribute name="GENDER" value="Socket Header" constant="no"/>
<attribute name="IR" value="3A" constant="no"/>
<attribute name="MATES" value="WR-PHD 2.54mm SMT Dual Pin Header/THT Angled Dual Pin Header/THT Dual Board Stacker/THT Dual Pin Header" constant="no"/>
<attribute name="PACKAGING" value="Tray" constant="no"/>
<attribute name="PART-NUMBER" value=" 61301221821 " constant="no"/>
<attribute name="PINS" value=" 12 " constant="no"/>
<attribute name="ROWS" value="Dual" constant="no"/>
<attribute name="TYPE" value="Straight" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GYRO" library_version="115">
<gates>
<gate name="G$1" symbol="GYRO" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="GYRO">
<connects>
<connect gate="G$1" pin="ADO" pad="ADO"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="INT" pad="INT"/>
<connect gate="G$1" pin="SCL" pad="SCL"/>
<connect gate="G$1" pin="SDA" pad="SDA"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
<connect gate="G$1" pin="XCL" pad="XCL"/>
<connect gate="G$1" pin="XDA" pad="XDA"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="GYRO_SMD" package="GYRO_SMD">
<connects>
<connect gate="G$1" pin="ADO" pad="ADO"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="INT" pad="INT"/>
<connect gate="G$1" pin="SCL" pad="SCL"/>
<connect gate="G$1" pin="SDA" pad="SDA"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
<connect gate="G$1" pin="XCL" pad="XCL"/>
<connect gate="G$1" pin="XDA" pad="XDA"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BME280">
<gates>
<gate name="G$1" symbol="BME280" x="12.7" y="7.62"/>
</gates>
<devices>
<device name="" package="BME280">
<connects>
<connect gate="G$1" pin="CSB" pad="CSB"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="SCL" pad="SCL"/>
<connect gate="G$1" pin="SDA" pad="SDA"/>
<connect gate="G$1" pin="SDD" pad="SDD"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MUTANTC" library_version="120">
<gates>
<gate name="G$1" symbol="MUTANTC" x="22.86" y="2.54"/>
</gates>
<devices>
<device name="" package="MUTANTC_TEXT">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MUTANTC_LOGO" package="MUTANTC_LOGO">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MUTANTC_TEXT+LOGO" package="MUTANTC_TEXT+LOGO">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BeagleBone_Blue_R3" urn="urn:adsk.eagle:library:5828899">
<description>Generated from &lt;b&gt;BeagleBone_Blue.sch&lt;/b&gt;&lt;p&gt;
by exp-lbrs.ulp</description>
<packages>
<package name="TACTILE-SWITCH-1101NE" urn="urn:adsk.eagle:footprint:5829391/2" library_version="49">
<description>SparkFun SKU# COM-08229</description>
<wire x1="-3" y1="1.1" x2="-3" y2="-1.1" width="0.127" layer="51"/>
<wire x1="3" y1="1.1" x2="3" y2="-1.1" width="0.127" layer="51"/>
<wire x1="-2.75" y1="1.75" x2="-3" y2="1.5" width="0.2032" layer="21" curve="90"/>
<wire x1="-2.75" y1="1.75" x2="2.75" y2="1.75" width="0.2032" layer="21"/>
<wire x1="2.75" y1="1.75" x2="3" y2="1.5" width="0.2032" layer="21" curve="-90"/>
<wire x1="3" y1="-1.5" x2="2.75" y2="-1.75" width="0.2032" layer="21" curve="-90"/>
<wire x1="2.75" y1="-1.75" x2="-2.75" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="-3" y1="-1.5" x2="-2.75" y2="-1.75" width="0.2032" layer="21" curve="90"/>
<wire x1="-3" y1="-1.5" x2="-3" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-3" y1="1.1" x2="-3" y2="1.5" width="0.2032" layer="21"/>
<wire x1="3" y1="1.1" x2="3" y2="1.5" width="0.2032" layer="21"/>
<wire x1="3" y1="-1.5" x2="3" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="0.75" x2="1.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-0.75" x2="-1.5" y2="-0.75" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-0.75" x2="-1.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-0.75" x2="1.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-2" y1="0" x2="-1" y2="0" width="0.127" layer="51"/>
<wire x1="-1" y1="0" x2="0.1" y2="0.5" width="0.127" layer="51"/>
<wire x1="0.3" y1="0" x2="2" y2="0" width="0.127" layer="51"/>
<smd name="1" x="-3.15" y="0" dx="2.3" dy="1.6" layer="1" rot="R180"/>
<smd name="2" x="3.15" y="0" dx="2.3" dy="1.6" layer="1" rot="R180"/>
<text x="-3" y="2" size="0.762" layer="25">&gt;NAME</text>
<text x="-3" y="-2.7" size="0.762" layer="27">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="SPARKFUN-ELECTROMECHANICAL_TACTILE-SWITCH-1101NE" urn="urn:adsk.eagle:package:5829833/3" type="model" library_version="49">
<description>SparkFun SKU# COM-08229</description>
<packageinstances>
<packageinstance name="TACTILE-SWITCH-1101NE"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="SPARKFUN-ELECTROMECHANICAL_SWITCH-MOMENTARY-2" urn="urn:adsk.eagle:symbol:5828967/1" library_version="49">
<circle x="-2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<circle x="2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<wire x1="1.905" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="1.905" y2="1.27" width="0.254" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="2"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RS032G05A3SM-SPST-MOMENTARY-SW" urn="urn:adsk.eagle:component:5829909/3" prefix="S" library_version="49">
<description>6x3.5mm 2-pin Momentary Switch (Push-button)
&lt;p&gt;
&lt;a href="https://www.digikey.com/products/en?keywords=CKN10388TR-ND"&gt;Digikey Link&lt;/a&gt;
&lt;br&gt;
&lt;a href="https://media.digikey.com/pdf/Data%20Sheets/C&amp;K/RS-032G05_-SM_RT.pdf"&gt;Datasheet&lt;/a&gt;
&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="SPARKFUN-ELECTROMECHANICAL_SWITCH-MOMENTARY-2" x="0" y="0"/>
</gates>
<devices>
<device name="SMD-1101NE" package="TACTILE-SWITCH-1101NE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829833/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-00815" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Switches" urn="urn:adsk.eagle:library:14854419">
<description>This library contains various switch types such as DIP,Tactile,Toggle Push Button etc.

&lt;br&gt;
&lt;br&gt;If you would like to purchase any of our product please visit, &lt;a href="https://www.te.com/usa-en/home.html?te_bu=Cor&amp;te_type=disp&amp;te_campaign=ult_glo_cor-ult-global-disp-autodesk-models_sma-299_1&amp;elqCampaignId=84950"&gt;TE.com&lt;/a&gt;
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Terms and Conditions Disclaimer:&lt;/b&gt;
&lt;br&gt;
By using any of these models, you agree that this information has been provided to you free of charge for your use but remains the sole property of TE Connectivity Corporation (''TE'') or SnapEDA,  Inc. or Ultra Librarian/EMA Design Automation, Inc. (collectively, "Company"). While Company has used reasonable efforts to ensure its accuracy, Company does not guarantee that it is error-free, not makes any other representation, warranty, or guarantee that the information is completely accurate or up-to-date. In many cases, the CAD data has been simplified to remove proprietary detail while maintaining critical interface geometric detail for use by customers. Company expressly disclaims all implied warranties regarding this information, including but not limited to any implied warranties or merchantability or fitness for a particular purpose.</description>
<packages>
<package name="SW2_1825027-E" urn="urn:adsk.eagle:footprint:14854489/1" library_version="5">
<pad name="1" x="0" y="0" drill="0.9906" diameter="1.4986"/>
<pad name="2" x="4.4958" y="0" drill="0.9906" diameter="1.4986"/>
<pad name="3" x="-1.2446" y="2.4892" drill="1.2954" diameter="1.8034"/>
<pad name="4" x="5.7658" y="2.4892" drill="1.2954" diameter="1.8034"/>
<wire x1="4.0132" y1="-3.3528" x2="4.0132" y2="-6.9088" width="0.1524" layer="51"/>
<wire x1="0.508" y1="-3.3528" x2="0.508" y2="-6.9088" width="0.1524" layer="51"/>
<wire x1="0.508" y1="-6.9088" x2="4.0132" y2="-6.9088" width="0.1524" layer="51"/>
<wire x1="-1.2954" y1="-3.3528" x2="5.8166" y2="-3.3528" width="0.1524" layer="51"/>
<wire x1="5.8166" y1="-3.3528" x2="5.8166" y2="4.0132" width="0.1524" layer="51"/>
<wire x1="5.8166" y1="4.0132" x2="-1.2954" y2="4.0132" width="0.1524" layer="51"/>
<wire x1="-1.2954" y1="4.0132" x2="-1.2954" y2="-3.3528" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-3.4798" x2="5.9436" y2="-3.4798" width="0.1524" layer="21"/>
<wire x1="5.9436" y1="-3.4798" x2="5.9436" y2="1.143" width="0.1524" layer="21"/>
<wire x1="5.9436" y1="4.1402" x2="-1.4224" y2="4.1402" width="0.1524" layer="21"/>
<wire x1="-1.4224" y1="4.1402" x2="-1.4224" y2="3.683" width="0.1524" layer="21"/>
<wire x1="-1.4224" y1="1.143" x2="-1.4224" y2="-3.4798" width="0.1524" layer="21"/>
<wire x1="5.9436" y1="3.683" x2="5.9436" y2="4.1402" width="0.1524" layer="21"/>
<text x="0.6858" y="-0.635" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<text x="-2.3622" y="-0.635" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<text x="0.5334" y="2.6162" size="1.27" layer="21" ratio="6" rot="SR0">&gt;Value</text>
<text x="-1.016" y="2.6162" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
</package>
</packages>
<packages3d>
<package3d name="SW2_1825027-E" urn="urn:adsk.eagle:package:14854601/2" type="model" library_version="5">
<packageinstances>
<packageinstance name="SW2_1825027-E"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="SW2_1825027-A" urn="urn:adsk.eagle:symbol:14854440/2" library_version="5">
<pin name="1" x="-7.62" y="0" visible="pad" length="short" direction="pas"/>
<pin name="2" x="7.62" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="3" x="-5.08" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="4" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.2032" layer="94"/>
<wire x1="5.08" y1="0" x2="3.81" y2="0" width="0.2032" layer="94"/>
<wire x1="-3.175" y1="0" x2="3.81" y2="1.905" width="0.2032" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-3.81" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="-3.81" x2="0.889" y2="-3.81" width="0.2032" layer="94"/>
<wire x1="-0.635" y1="-4.191" x2="0.635" y2="-4.191" width="0.2032" layer="94"/>
<wire x1="-0.381" y1="-4.572" x2="0.381" y2="-4.572" width="0.2032" layer="94"/>
<wire x1="-0.127" y1="-4.953" x2="0.127" y2="-4.953" width="0.2032" layer="94"/>
<wire x1="3.81" y1="0" x2="2.54" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="2.54" y1="0" x2="3.81" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="-3.81" y1="0" x2="-2.54" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="-2.54" y1="0" x2="-3.81" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="0" y1="-2.54" x2="-2.54" y2="-2.54" width="0.2032" layer="94"/>
<wire x1="0" y1="-2.54" x2="2.54" y2="-2.54" width="0.2032" layer="94"/>
<text x="-7.8994" y="2.0828" size="3.4798" layer="96" ratio="10" rot="SR0">&gt;Value</text>
<text x="-7.8994" y="7.1628" size="3.4798" layer="95" ratio="10" rot="SR0">&gt;Name</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="1-1825027-4" urn="urn:adsk.eagle:component:14854724/3" prefix="SW" library_version="5">
<gates>
<gate name="A" symbol="SW2_1825027-A" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SW2_1825027-E">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:14854601/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="BUILT_BY" value="EMA_UL_Team" constant="no"/>
<attribute name="CATEGORY" value="Tactile Switches" constant="no"/>
<attribute name="CONFIGURATION__POLE-THROW_" value="Single Pole - Single Throw" constant="no"/>
<attribute name="CONTACT_CURRENT_RATING" value="50" constant="no"/>
<attribute name="COPYRIGHT" value="Copyright (C) 2016 Accelerated Designs. All rights reserved" constant="no"/>
<attribute name="DESCRIPTION" value="FSMRA5JH04=R/A,TACT PB SW,160G" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="1-1825027-4" constant="no"/>
<attribute name="MAX_SWITCHING_CURRENT" value="-" constant="no"/>
<attribute name="MAX_SWITCHING_VOLTAGE_AC_" value="-" constant="no"/>
<attribute name="PART_STATUS" value="active" constant="no"/>
<attribute name="PRODUCT_TYPE" value="Switch" constant="no"/>
<attribute name="SERIES" value="Unknown" constant="no"/>
<attribute name="SWITCH_TYPE" value="Tactile Switches" constant="no"/>
<attribute name="TE_INTERNAL_NUMBER" value="1-1825027-4" constant="no"/>
<attribute name="TE_PART_NUMBER" value="1-1825027-4" constant="no"/>
<attribute name="VENDOR" value="TE Connectivity" constant="no"/>
<attribute name="VOLTAGE_RATING" value="-" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="wirepad" urn="urn:adsk.eagle:library:412">
<description>&lt;b&gt;Single Pads&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="1,6/0,8" urn="urn:adsk.eagle:footprint:30809/1" library_version="2">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="-0.762" y1="0.762" x2="-0.508" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="0.762" x2="-0.762" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0.762" y1="0.762" x2="0.762" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0.762" y1="0.762" x2="0.508" y2="0.762" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-0.508" x2="0.762" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-0.762" x2="0.508" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="-0.762" x2="-0.762" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-0.762" x2="-0.762" y2="-0.508" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.635" width="0.1524" layer="51"/>
<pad name="1" x="0" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<text x="-0.762" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0.6" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="1,6/0,9" urn="urn:adsk.eagle:footprint:30812/1" library_version="2">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="-0.508" y1="0.762" x2="-0.762" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="0.762" x2="-0.762" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-0.508" x2="-0.762" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-0.762" x2="-0.508" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="0.508" y1="-0.762" x2="0.762" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-0.762" x2="0.762" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.762" y1="0.508" x2="0.762" y2="0.762" width="0.1524" layer="21"/>
<wire x1="0.762" y1="0.762" x2="0.508" y2="0.762" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.635" width="0.1524" layer="51"/>
<pad name="1" x="0" y="0" drill="0.9144" diameter="1.6002" shape="octagon"/>
<text x="-0.762" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0.6" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="2,15/1,0" urn="urn:adsk.eagle:footprint:30813/1" library_version="2">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="1.143" y1="-1.143" x2="1.143" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-1.143" x2="0.635" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.635" x2="1.143" y2="1.143" width="0.1524" layer="21"/>
<wire x1="1.143" y1="1.143" x2="0.635" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.143" x2="-1.143" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="1.143" x2="-1.143" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.635" x2="-1.143" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-1.143" x2="-0.635" y2="-1.143" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="2.159" shape="octagon"/>
<text x="-1.143" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="1" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="2,54/0,8" urn="urn:adsk.eagle:footprint:30820/1" library_version="2">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="-1.27" y1="1.27" x2="-0.762" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="0.762" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.762" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="0.762" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-1.27" x2="-1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.762" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.635" width="0.1524" layer="51"/>
<pad name="1" x="0" y="0" drill="0.8128" diameter="2.54" shape="octagon"/>
<text x="-1.27" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0.6" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="2,54/0,9" urn="urn:adsk.eagle:footprint:30821/1" library_version="2">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="-1.27" y1="1.27" x2="-0.762" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="0.762" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.762" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="0.762" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-1.27" x2="-1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.762" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.635" width="0.1524" layer="51"/>
<pad name="1" x="0" y="0" drill="0.9144" diameter="2.54" shape="octagon"/>
<text x="-1.27" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0.6" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="2,54/1,0" urn="urn:adsk.eagle:footprint:30810/1" library_version="2">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="0.762" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-0.762" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-0.762" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.762" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="-1.27" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="1" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="2,54/1,1" urn="urn:adsk.eagle:footprint:30818/1" library_version="2">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="0.762" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-0.762" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-0.762" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="0.762" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.762" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="2.54" shape="octagon"/>
<text x="-1.27" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="1" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="3,17/1,1" urn="urn:adsk.eagle:footprint:30814/1" library_version="2">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="1.524" y1="-1.016" x2="1.524" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-1.524" x2="1.016" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="-1.524" x2="-1.524" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-1.524" x2="-1.524" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="1.016" x2="-1.524" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="1.524" x2="-1.016" y2="1.524" width="0.1524" layer="21"/>
<wire x1="1.016" y1="1.524" x2="1.524" y2="1.524" width="0.1524" layer="21"/>
<wire x1="1.524" y1="1.524" x2="1.524" y2="1.016" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="3.175" shape="octagon"/>
<text x="-1.524" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="1.2" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="3,17/1,2" urn="urn:adsk.eagle:footprint:30824/1" library_version="2">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="1.524" y1="-1.016" x2="1.524" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-1.524" x2="1.016" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="-1.524" x2="-1.524" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-1.524" x2="-1.524" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="1.016" x2="-1.524" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="1.524" x2="-1.016" y2="1.524" width="0.1524" layer="21"/>
<wire x1="1.016" y1="1.524" x2="1.524" y2="1.524" width="0.1524" layer="21"/>
<wire x1="1.524" y1="1.524" x2="1.524" y2="1.016" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="0" y="0" drill="1.1938" diameter="3.175" shape="octagon"/>
<text x="-1.524" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="1.2" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="3,17/1,3" urn="urn:adsk.eagle:footprint:30815/1" library_version="2">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="1.524" y1="-1.016" x2="1.524" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-1.524" x2="1.016" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="-1.524" x2="-1.524" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-1.524" x2="-1.524" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="1.016" x2="-1.524" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="1.524" x2="-1.016" y2="1.524" width="0.1524" layer="21"/>
<wire x1="1.016" y1="1.524" x2="1.524" y2="1.524" width="0.1524" layer="21"/>
<wire x1="1.524" y1="1.524" x2="1.524" y2="1.016" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="0" y="0" drill="1.3208" diameter="3.175" shape="octagon"/>
<text x="-1.524" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="1.2" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="3,81/1,1" urn="urn:adsk.eagle:footprint:30811/1" library_version="2">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="1.905" y1="-1.27" x2="1.905" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.905" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.905" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.905" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.905" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.905" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="3.81" shape="octagon"/>
<text x="-1.905" y="2.286" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="1.2" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="3,81/1,3" urn="urn:adsk.eagle:footprint:30816/1" library_version="2">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="1.905" y1="-1.27" x2="1.905" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.905" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.905" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.905" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.905" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.905" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="0" y="0" drill="1.3208" diameter="3.81" shape="octagon"/>
<text x="-1.905" y="2.286" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="1.2" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="3,81/1,4" urn="urn:adsk.eagle:footprint:30817/1" library_version="2">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="1.905" y1="-1.27" x2="1.905" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.905" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.905" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.905" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.905" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.905" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="0" y="0" drill="1.397" diameter="3.81" shape="octagon"/>
<text x="-1.905" y="2.286" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="1.2" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="4,16O1,6" urn="urn:adsk.eagle:footprint:30825/1" library_version="2">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<pad name="1" x="0" y="0" drill="1.6002" diameter="4.1656" shape="octagon"/>
<text x="0" y="0" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-2.1" y="2.2" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="5-1,8" urn="urn:adsk.eagle:footprint:30826/1" library_version="2">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="1.1684" y1="2.794" x2="-1.1684" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.1684" y1="-2.794" x2="-1.1684" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.1684" y1="-2.794" x2="1.1684" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="1.1684" y1="2.794" x2="1.1684" y2="-2.794" width="0.1524" layer="21"/>
<smd name="1" x="0" y="0" dx="1.8288" dy="5.08" layer="1"/>
<text x="-1.524" y="-2.54" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-0.1" y="2.8" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="5-2,5" urn="urn:adsk.eagle:footprint:30827/1" library_version="2">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="1.524" y1="2.794" x2="-1.524" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-2.794" x2="-1.524" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-2.794" x2="1.524" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="1.524" y1="2.794" x2="1.524" y2="-2.794" width="0.1524" layer="21"/>
<smd name="1" x="0" y="0" dx="2.54" dy="5.08" layer="1"/>
<text x="-1.778" y="-2.54" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-0.1" y="2.8" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="SMD1,27-2,54" urn="urn:adsk.eagle:footprint:30822/1" library_version="2">
<description>&lt;b&gt;SMD PAD&lt;/b&gt;</description>
<smd name="1" x="0" y="0" dx="1.27" dy="2.54" layer="1"/>
<text x="0" y="0" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-0.8" y="-2.4" size="1.27" layer="25" rot="R90">&gt;NAME</text>
</package>
<package name="SMD2,54-5,08" urn="urn:adsk.eagle:footprint:30823/1" library_version="2">
<description>&lt;b&gt;SMD PAD&lt;/b&gt;</description>
<smd name="1" x="0" y="0" dx="2.54" dy="5.08" layer="1"/>
<text x="0" y="0" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-1.5" y="-2.5" size="1.27" layer="25" rot="R90">&gt;NAME</text>
</package>
</packages>
<packages3d>
<package3d name="1,6/0,8" urn="urn:adsk.eagle:package:30830/1" type="box" library_version="2">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="1,6/0,8"/>
</packageinstances>
</package3d>
<package3d name="1,6/0,9" urn="urn:adsk.eagle:package:30840/1" type="box" library_version="2">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="1,6/0,9"/>
</packageinstances>
</package3d>
<package3d name="2,15/1,0" urn="urn:adsk.eagle:package:30831/1" type="box" library_version="2">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="2,15/1,0"/>
</packageinstances>
</package3d>
<package3d name="2,54/0,8" urn="urn:adsk.eagle:package:30838/1" type="box" library_version="2">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="2,54/0,8"/>
</packageinstances>
</package3d>
<package3d name="2,54/0,9" urn="urn:adsk.eagle:package:30847/1" type="box" library_version="2">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="2,54/0,9"/>
</packageinstances>
</package3d>
<package3d name="2,54/1,0" urn="urn:adsk.eagle:package:30828/1" type="box" library_version="2">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="2,54/1,0"/>
</packageinstances>
</package3d>
<package3d name="2,54/1,1" urn="urn:adsk.eagle:package:30836/1" type="box" library_version="2">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="2,54/1,1"/>
</packageinstances>
</package3d>
<package3d name="3,17/1,1" urn="urn:adsk.eagle:package:30832/1" type="box" library_version="2">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="3,17/1,1"/>
</packageinstances>
</package3d>
<package3d name="3,17/1,2" urn="urn:adsk.eagle:package:30842/1" type="box" library_version="2">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="3,17/1,2"/>
</packageinstances>
</package3d>
<package3d name="3,17/1,3" urn="urn:adsk.eagle:package:30833/1" type="box" library_version="2">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="3,17/1,3"/>
</packageinstances>
</package3d>
<package3d name="3,81/1,1" urn="urn:adsk.eagle:package:30829/1" type="box" library_version="2">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="3,81/1,1"/>
</packageinstances>
</package3d>
<package3d name="3,81/1,3" urn="urn:adsk.eagle:package:30834/1" type="box" library_version="2">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="3,81/1,3"/>
</packageinstances>
</package3d>
<package3d name="3,81/1,4" urn="urn:adsk.eagle:package:30835/1" type="box" library_version="2">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="3,81/1,4"/>
</packageinstances>
</package3d>
<package3d name="4,16O1,6" urn="urn:adsk.eagle:package:30843/1" type="box" library_version="2">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="4,16O1,6"/>
</packageinstances>
</package3d>
<package3d name="5-1,8" urn="urn:adsk.eagle:package:30844/1" type="box" library_version="2">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="5-1,8"/>
</packageinstances>
</package3d>
<package3d name="5-2,5" urn="urn:adsk.eagle:package:30845/1" type="box" library_version="2">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="5-2,5"/>
</packageinstances>
</package3d>
<package3d name="SMD1,27-2,54" urn="urn:adsk.eagle:package:30839/1" type="box" library_version="2">
<description>SMD PAD</description>
<packageinstances>
<packageinstance name="SMD1,27-2,54"/>
</packageinstances>
</package3d>
<package3d name="SMD2,54-5,08" urn="urn:adsk.eagle:package:30841/1" type="box" library_version="2">
<description>SMD PAD</description>
<packageinstances>
<packageinstance name="SMD2,54-5,08"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PAD" urn="urn:adsk.eagle:symbol:30808/1" library_version="2">
<wire x1="-1.016" y1="1.016" x2="1.016" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-1.016" y1="-1.016" x2="1.016" y2="1.016" width="0.254" layer="94"/>
<text x="-1.143" y="1.8542" size="1.778" layer="95">&gt;NAME</text>
<text x="-1.143" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="P" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="WIREPAD" urn="urn:adsk.eagle:component:30861/2" prefix="PAD" library_version="2">
<description>&lt;b&gt;Wire PAD&lt;/b&gt; connect wire on PCB</description>
<gates>
<gate name="G$1" symbol="PAD" x="0" y="0"/>
</gates>
<devices>
<device name="1,6/0,8" package="1,6/0,8">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30830/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
</technology>
</technologies>
</device>
<device name="1,6/0,9" package="1,6/0,9">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30840/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="2,15/1,0" package="2,15/1,0">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30831/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="2" constant="no"/>
</technology>
</technologies>
</device>
<device name="2,54/0,8" package="2,54/0,8">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30838/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="2,54/0,9" package="2,54/0,9">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30847/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="6" constant="no"/>
</technology>
</technologies>
</device>
<device name="2,54/1,0" package="2,54/1,0">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30828/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="4" constant="no"/>
</technology>
</technologies>
</device>
<device name="2,54/1,1" package="2,54/1,1">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30836/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="2" constant="no"/>
</technology>
</technologies>
</device>
<device name="3,17/1,1" package="3,17/1,1">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30832/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="3,17/1,2" package="3,17/1,2">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30842/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
</technology>
</technologies>
</device>
<device name="3,17/1,3" package="3,17/1,3">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30833/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="5" constant="no"/>
</technology>
</technologies>
</device>
<device name="3,81/1,1" package="3,81/1,1">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30829/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="3,81/1,3" package="3,81/1,3">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30834/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="3,81/1,4" package="3,81/1,4">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30835/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
</technology>
</technologies>
</device>
<device name="4,16O1,6" package="4,16O1,6">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30843/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
</technology>
</technologies>
</device>
<device name="SMD5-1,8" package="5-1,8">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30844/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="4" constant="no"/>
</technology>
</technologies>
</device>
<device name="SMD5-2,5" package="5-2,5">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30845/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="3" constant="no"/>
</technology>
</technologies>
</device>
<device name="SMD1,27-254" package="SMD1,27-2,54">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30839/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="11" constant="no"/>
</technology>
</technologies>
</device>
<device name="SMD2,54-5,08" package="SMD2,54-5,08">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30841/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="2" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SMD2" urn="urn:adsk.eagle:component:30857/2" prefix="PAD" uservalue="yes" library_version="2">
<description>&lt;b&gt;SMD PAD&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="PAD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SMD1,27-2,54">
<connects>
<connect gate="1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30839/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="15" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Connector" urn="urn:adsk.eagle:library:16378166">
<description>&lt;b&gt;Pin Headers,Terminal blocks, D-Sub, Backplane, FFC/FPC, Socket</description>
<packages>
<package name="2X10" urn="urn:adsk.eagle:footprint:22268/1" library_version="3">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-12.7" y1="-1.905" x2="-12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-2.54" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.54" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.54" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-1.905" x2="-12.7" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.065" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="2.54" x2="-10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="2.54" x2="-10.16" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.905" x2="-9.525" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="2.54" x2="-8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="2.54" x2="-7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="2.54" x2="-5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="2.54" x2="6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="2.54" x2="7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="8.255" y1="2.54" x2="9.525" y2="2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="2.54" x2="10.16" y2="1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.905" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.905" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.54" x2="9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-2.54" x2="6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-2.54" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="-2.54" x2="-8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="-2.54" x2="-10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.905" x2="10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="2.54" x2="12.065" y2="2.54" width="0.1524" layer="21"/>
<wire x1="12.065" y1="2.54" x2="12.7" y2="1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="12.7" y1="1.905" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-2.54" x2="12.065" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-11.43" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-11.43" y="1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="-8.89" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="-8.89" y="1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="-6.35" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="-6.35" y="1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="-3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="-3.81" y="1.27" drill="1.016" shape="octagon"/>
<pad name="9" x="-1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="10" x="-1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="11" x="1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="12" x="1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="13" x="3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="14" x="3.81" y="1.27" drill="1.016" shape="octagon"/>
<pad name="15" x="6.35" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="16" x="6.35" y="1.27" drill="1.016" shape="octagon"/>
<pad name="17" x="8.89" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="18" x="8.89" y="1.27" drill="1.016" shape="octagon"/>
<pad name="19" x="11.43" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="20" x="11.43" y="1.27" drill="1.016" shape="octagon"/>
<text x="-12.7" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-12.7" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-11.684" y1="-1.524" x2="-11.176" y2="-1.016" layer="51"/>
<rectangle x1="-11.684" y1="1.016" x2="-11.176" y2="1.524" layer="51"/>
<rectangle x1="-9.144" y1="1.016" x2="-8.636" y2="1.524" layer="51"/>
<rectangle x1="-9.144" y1="-1.524" x2="-8.636" y2="-1.016" layer="51"/>
<rectangle x1="-6.604" y1="1.016" x2="-6.096" y2="1.524" layer="51"/>
<rectangle x1="-6.604" y1="-1.524" x2="-6.096" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
<rectangle x1="6.096" y1="1.016" x2="6.604" y2="1.524" layer="51"/>
<rectangle x1="6.096" y1="-1.524" x2="6.604" y2="-1.016" layer="51"/>
<rectangle x1="8.636" y1="1.016" x2="9.144" y2="1.524" layer="51"/>
<rectangle x1="8.636" y1="-1.524" x2="9.144" y2="-1.016" layer="51"/>
<rectangle x1="11.176" y1="1.016" x2="11.684" y2="1.524" layer="51"/>
<rectangle x1="11.176" y1="-1.524" x2="11.684" y2="-1.016" layer="51"/>
</package>
<package name="2X10/90" urn="urn:adsk.eagle:footprint:22269/1" library_version="3">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-12.7" y1="-1.905" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="0.635" x2="-12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="0.635" x2="-12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="6.985" x2="-11.43" y2="1.27" width="0.762" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="6.985" x2="-8.89" y2="1.27" width="0.762" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="6.985" x2="-6.35" y2="1.27" width="0.762" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="6.985" x2="-3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="6.985" x2="3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="6.985" x2="6.35" y2="1.27" width="0.762" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="0.635" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="8.89" y1="6.985" x2="8.89" y2="1.27" width="0.762" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="12.7" y1="0.635" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="11.43" y1="6.985" x2="11.43" y2="1.27" width="0.762" layer="21"/>
<pad name="2" x="-11.43" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="4" x="-8.89" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="6" x="-6.35" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="8" x="-3.81" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="10" x="-1.27" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="12" x="1.27" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="14" x="3.81" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="16" x="6.35" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="18" x="8.89" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="20" x="11.43" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="1" x="-11.43" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="3" x="-8.89" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="5" x="-6.35" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="7" x="-3.81" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="9" x="-1.27" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="11" x="1.27" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="13" x="3.81" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="15" x="6.35" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="17" x="8.89" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="19" x="11.43" y="-6.35" drill="1.016" shape="octagon"/>
<text x="-13.335" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="14.605" y="-4.445" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-11.811" y1="0.635" x2="-11.049" y2="1.143" layer="21"/>
<rectangle x1="-9.271" y1="0.635" x2="-8.509" y2="1.143" layer="21"/>
<rectangle x1="-6.731" y1="0.635" x2="-5.969" y2="1.143" layer="21"/>
<rectangle x1="-4.191" y1="0.635" x2="-3.429" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="3.429" y1="0.635" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="5.969" y1="0.635" x2="6.731" y2="1.143" layer="21"/>
<rectangle x1="8.509" y1="0.635" x2="9.271" y2="1.143" layer="21"/>
<rectangle x1="11.049" y1="0.635" x2="11.811" y2="1.143" layer="21"/>
<rectangle x1="-11.811" y1="-2.921" x2="-11.049" y2="-1.905" layer="21"/>
<rectangle x1="-9.271" y1="-2.921" x2="-8.509" y2="-1.905" layer="21"/>
<rectangle x1="-11.811" y1="-5.461" x2="-11.049" y2="-4.699" layer="21"/>
<rectangle x1="-11.811" y1="-4.699" x2="-11.049" y2="-2.921" layer="51"/>
<rectangle x1="-9.271" y1="-4.699" x2="-8.509" y2="-2.921" layer="51"/>
<rectangle x1="-9.271" y1="-5.461" x2="-8.509" y2="-4.699" layer="21"/>
<rectangle x1="-6.731" y1="-2.921" x2="-5.969" y2="-1.905" layer="21"/>
<rectangle x1="-4.191" y1="-2.921" x2="-3.429" y2="-1.905" layer="21"/>
<rectangle x1="-6.731" y1="-5.461" x2="-5.969" y2="-4.699" layer="21"/>
<rectangle x1="-6.731" y1="-4.699" x2="-5.969" y2="-2.921" layer="51"/>
<rectangle x1="-4.191" y1="-4.699" x2="-3.429" y2="-2.921" layer="51"/>
<rectangle x1="-4.191" y1="-5.461" x2="-3.429" y2="-4.699" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
<rectangle x1="-1.651" y1="-5.461" x2="-0.889" y2="-4.699" layer="21"/>
<rectangle x1="-1.651" y1="-4.699" x2="-0.889" y2="-2.921" layer="51"/>
<rectangle x1="0.889" y1="-4.699" x2="1.651" y2="-2.921" layer="51"/>
<rectangle x1="0.889" y1="-5.461" x2="1.651" y2="-4.699" layer="21"/>
<rectangle x1="3.429" y1="-2.921" x2="4.191" y2="-1.905" layer="21"/>
<rectangle x1="5.969" y1="-2.921" x2="6.731" y2="-1.905" layer="21"/>
<rectangle x1="3.429" y1="-5.461" x2="4.191" y2="-4.699" layer="21"/>
<rectangle x1="3.429" y1="-4.699" x2="4.191" y2="-2.921" layer="51"/>
<rectangle x1="5.969" y1="-4.699" x2="6.731" y2="-2.921" layer="51"/>
<rectangle x1="5.969" y1="-5.461" x2="6.731" y2="-4.699" layer="21"/>
<rectangle x1="8.509" y1="-2.921" x2="9.271" y2="-1.905" layer="21"/>
<rectangle x1="11.049" y1="-2.921" x2="11.811" y2="-1.905" layer="21"/>
<rectangle x1="8.509" y1="-5.461" x2="9.271" y2="-4.699" layer="21"/>
<rectangle x1="8.509" y1="-4.699" x2="9.271" y2="-2.921" layer="51"/>
<rectangle x1="11.049" y1="-4.699" x2="11.811" y2="-2.921" layer="51"/>
<rectangle x1="11.049" y1="-5.461" x2="11.811" y2="-4.699" layer="21"/>
</package>
<package name="1X03" urn="urn:adsk.eagle:footprint:22340/1" library_version="3">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-3.8862" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
</package>
<package name="1X03/90" urn="urn:adsk.eagle:footprint:22341/1" library_version="3">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.81" y1="-1.905" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="6.985" x2="-2.54" y2="1.27" width="0.762" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="6.985" x2="0" y2="1.27" width="0.762" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="6.985" x2="2.54" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-2.54" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="0" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-4.445" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="5.715" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.921" y1="0.635" x2="-2.159" y2="1.143" layer="21"/>
<rectangle x1="-0.381" y1="0.635" x2="0.381" y2="1.143" layer="21"/>
<rectangle x1="2.159" y1="0.635" x2="2.921" y2="1.143" layer="21"/>
<rectangle x1="-2.921" y1="-2.921" x2="-2.159" y2="-1.905" layer="21"/>
<rectangle x1="-0.381" y1="-2.921" x2="0.381" y2="-1.905" layer="21"/>
<rectangle x1="2.159" y1="-2.921" x2="2.921" y2="-1.905" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="2X10" urn="urn:adsk.eagle:package:22405/2" type="model" library_version="3">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="2X10"/>
</packageinstances>
</package3d>
<package3d name="2X10/90" urn="urn:adsk.eagle:package:22411/2" type="model" library_version="3">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="2X10/90"/>
</packageinstances>
</package3d>
<package3d name="1X03" urn="urn:adsk.eagle:package:22458/2" type="model" library_version="3">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="1X03"/>
</packageinstances>
</package3d>
<package3d name="1X03/90" urn="urn:adsk.eagle:package:22459/2" type="model" library_version="3">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="1X03/90"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PINH2X10" urn="urn:adsk.eagle:symbol:22266/1" library_version="3">
<wire x1="-6.35" y1="-15.24" x2="8.89" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-15.24" x2="8.89" y2="12.7" width="0.4064" layer="94"/>
<wire x1="8.89" y1="12.7" x2="-6.35" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="12.7" x2="-6.35" y2="-15.24" width="0.4064" layer="94"/>
<text x="-6.35" y="13.335" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="7" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="9" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="11" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="12" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="13" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="14" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="15" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="16" x="5.08" y="-7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="17" x="-2.54" y="-10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="18" x="5.08" y="-10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="19" x="-2.54" y="-12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="20" x="5.08" y="-12.7" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
<symbol name="PINHD3" urn="urn:adsk.eagle:symbol:22339/1" library_version="3">
<wire x1="-6.35" y1="-5.08" x2="1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-2X10" urn="urn:adsk.eagle:component:16494862/2" prefix="JP" library_version="4">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINH2X10" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X10">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="17" pad="17"/>
<connect gate="A" pin="18" pad="18"/>
<connect gate="A" pin="19" pad="19"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="20" pad="20"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22405/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="CATEGORY" value="CONNECTOR" constant="no"/>
<attribute name="DESCRIPTION" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="PIN-HEADER" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="/90" package="2X10/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="17" pad="17"/>
<connect gate="A" pin="18" pad="18"/>
<connect gate="A" pin="19" pad="19"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="20" pad="20"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22411/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="CATEGORY" value="CONNECTOR" constant="no"/>
<attribute name="DESCRIPTION" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="PIN-HEADER" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X3" urn="urn:adsk.eagle:component:16494881/2" prefix="JP" library_version="4">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINHD3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X03">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22458/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="CATEGORY" value="CONNECTOR" constant="no"/>
<attribute name="DESCRIPTION" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="PIN-HEADER" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="/90" package="1X03/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22459/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="CATEGORY" value="CONNECTOR" constant="no"/>
<attribute name="DESCRIPTION" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="PIN-HEADER" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Wurth_Connectors_WR-FPC" urn="urn:adsk.eagle:library:15778639">
<description>&lt;BR&gt;Wurth Elektronik - Connectors - FPC Connector and FFC Cable - WR-FPC &lt;br&gt;&lt;Hr&gt;

&lt;BR&gt;
&lt;TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0&gt;
&lt;TR&gt;   
&lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;BR&gt;&lt;br&gt;
      &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
&lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;br&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt; &lt;FONT FACE=ARIAL SIZE=3&gt;&lt;br&gt;
      ---------------------------&lt;BR&gt;
&lt;B&gt;&lt;I&gt;&lt;span style='font-size:26pt;
  color:#FF6600;'&gt;WE &lt;/span&gt;&lt;/i&gt;&lt;/b&gt;
&lt;BR&gt;
      ---------------------------&lt;BR&gt;&lt;b&gt;Würth Elektronik&lt;/b&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;br&gt;
      ---------O---&lt;BR&gt;
      ----O--------&lt;BR&gt;
      ---------O---&lt;BR&gt;
      ----O--------&lt;BR&gt;
      ---------O---&lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
   
&lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;BR&gt;
      &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;

  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  
&lt;/TABLE&gt;
&lt;B&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;More than you expect&lt;BR&gt;&lt;BR&gt;&lt;BR&gt;&lt;/B&gt;

&lt;HR&gt;&lt;BR&gt;
&lt;b&gt;Würth Elektronik eiSos GmbH &amp; Co. KG&lt;/b&gt;&lt;br&gt;
EMC &amp; Inductive Solutions&lt;br&gt;

Max-Eyth-Str.1&lt;br&gt;
D-74638 Waldenburg&lt;br&gt;
&lt;br&gt;
Tel: +49 (0)7942-945-0&lt;br&gt;
Fax:+49 (0)7942-945-5000&lt;br&gt;
&lt;br&gt;
&lt;a href="http://www.we-online.com/web/en/electronic_components/produkte_pb/bauteilebibliotheken/eagle_4.php"&gt;www.we-online.com/eagle&lt;/a&gt;&lt;br&gt;
&lt;a href="mailto:libraries@we-online.com"&gt;libraries@we-online.com&lt;/a&gt; &lt;BR&gt;&lt;BR&gt;
&lt;br&gt;&lt;HR&gt;&lt;BR&gt;
Neither Autodesk nor Würth Elektronik eiSos does warrant that this library is error-free or &lt;br&gt;
that it meets your specific requirements.&lt;br&gt;&lt;BR&gt;
Please contact us for more information.&lt;br&gt;
&lt;HR&gt;
&lt;br&gt;Eagle Version 9, Library Revision 2019b, 2019-11-15&lt;br&gt;
&lt;HR&gt;
Copyright: Würth Elektronik</description>
<packages>
<package name="68611614422" urn="urn:adsk.eagle:footprint:15743560/1" library_version="1">
<description>&lt;b&gt;WR-FPC&lt;/b&gt; &lt;br &gt; 1.00 mm SMT ZIF Horizontal Bottom Contact,  16 Pins</description>
<smd name="5" x="-3.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="4" x="-4.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="6" x="-2.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="3" x="-5.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="Z2" x="10.1" y="0.93" dx="1.8" dy="2.6" layer="1"/>
<smd name="Z1" x="-10.1" y="0.93" dx="1.8" dy="2.6" layer="1"/>
<smd name="2" x="-6.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="1" x="-7.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="7" x="-1.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="8" x="-0.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="9" x="0.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="10" x="1.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="11" x="2.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="12" x="3.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="13" x="4.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="14" x="5.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="15" x="6.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="16" x="7.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<wire x1="-10.5" y1="2.7" x2="10.5" y2="2.7" width="0.1" layer="51"/>
<wire x1="10.5" y1="2.7" x2="10.5" y2="2.1" width="0.1" layer="51"/>
<wire x1="10.5" y1="2.1" x2="10.5" y2="-1.6" width="0.1" layer="51"/>
<wire x1="-10.5" y1="-1.6" x2="-10.5" y2="2.1" width="0.1" layer="51"/>
<wire x1="-10.5" y1="2.1" x2="-10.5" y2="2.7" width="0.1" layer="51"/>
<wire x1="-11.5" y1="-1.6" x2="-10.73" y2="-1.6" width="0.1" layer="51"/>
<wire x1="-10.73" y1="-1.6" x2="-10.5" y2="-1.6" width="0.1" layer="51"/>
<wire x1="-10.5" y1="-1.6" x2="10.5" y2="-1.6" width="0.1" layer="51"/>
<wire x1="10.5" y1="-1.6" x2="10.73" y2="-1.6" width="0.1" layer="51"/>
<wire x1="10.73" y1="-1.6" x2="11.5" y2="-1.6" width="0.1" layer="51"/>
<wire x1="11.5" y1="-1.6" x2="11.5" y2="-2.7" width="0.1" layer="51"/>
<wire x1="11.5" y1="-2.7" x2="-11.5" y2="-2.7" width="0.1" layer="51"/>
<wire x1="-11.5" y1="-2.7" x2="-11.5" y2="-1.6" width="0.1" layer="51"/>
<wire x1="-10.5" y1="2.1" x2="-10.73" y2="2.1" width="0.1" layer="51"/>
<wire x1="-10.73" y1="2.1" x2="-10.73" y2="-1.6" width="0.1" layer="51"/>
<wire x1="10.5" y1="2.1" x2="10.73" y2="2.1" width="0.1" layer="51"/>
<wire x1="10.73" y1="2.1" x2="10.73" y2="-1.6" width="0.1" layer="51"/>
<wire x1="-8.1" y1="2.8" x2="-10.6" y2="2.8" width="0.2" layer="21"/>
<wire x1="-10.6" y1="2.8" x2="-10.6" y2="2.6" width="0.2" layer="21"/>
<wire x1="8.1" y1="2.8" x2="10.6" y2="2.8" width="0.2" layer="21"/>
<wire x1="10.6" y1="2.8" x2="10.6" y2="2.6" width="0.2" layer="21"/>
<wire x1="-10.83" y1="-0.7" x2="-10.83" y2="-1.5" width="0.2" layer="21"/>
<wire x1="-10.83" y1="-1.5" x2="-11.6" y2="-1.5" width="0.2" layer="21"/>
<wire x1="-11.6" y1="-1.5" x2="-11.6" y2="-2.8" width="0.2" layer="21"/>
<wire x1="-11.6" y1="-2.8" x2="11.6" y2="-2.8" width="0.2" layer="21"/>
<wire x1="11.6" y1="-2.8" x2="11.6" y2="-1.5" width="0.2" layer="21"/>
<wire x1="11.6" y1="-1.5" x2="10.83" y2="-1.5" width="0.2" layer="21"/>
<wire x1="10.83" y1="-1.5" x2="10.83" y2="-0.7" width="0.2" layer="21"/>
<text x="-11.6616" y="1.1185" size="0.8128" layer="25" align="bottom-right">&gt;NAME</text>
<text x="-11.6616" y="-0.6534" size="0.8128" layer="27" align="bottom-right">&gt;VALUE</text>
<circle x="-8.3" y="2.23" radius="0.1" width="0.2" layer="21"/>
<polygon width="0.1" layer="39" pour="solid">
<vertex x="-11.03" y="-0.599290625"/>
<vertex x="-11.03" y="-1.270709375"/>
<vertex x="-11.059290625" y="-1.3"/>
<vertex x="-11.8" y="-1.3"/>
<vertex x="-11.8" y="-3"/>
<vertex x="11.8" y="-3"/>
<vertex x="11.8" y="-1.3"/>
<vertex x="11.029290625" y="-1.3"/>
<vertex x="11" y="-1.270709375"/>
<vertex x="11" y="-0.599290625"/>
<vertex x="11.029290625" y="-0.57"/>
<vertex x="11.2" y="-0.57"/>
<vertex x="11.2" y="2.43"/>
<vertex x="10.829290625" y="2.43"/>
<vertex x="10.8" y="2.459290625"/>
<vertex x="10.8" y="3"/>
<vertex x="8.029290625" y="3"/>
<vertex x="8" y="3.029290625"/>
<vertex x="8" y="4.63"/>
<vertex x="-8" y="4.63"/>
<vertex x="-8" y="3.029290625"/>
<vertex x="-8.029290625" y="3"/>
<vertex x="-10.8" y="3"/>
<vertex x="-10.8" y="2.459290625"/>
<vertex x="-10.829290625" y="2.43"/>
<vertex x="-11.2" y="2.43"/>
<vertex x="-11.2" y="-0.57"/>
<vertex x="-11.059290625" y="-0.57"/>
</polygon>
</package>
<package name="68611014422" urn="urn:adsk.eagle:footprint:15743557/1" library_version="1">
<description>&lt;b&gt;WR-FPC&lt;/b&gt; &lt;br &gt;1.00 mm SMT ZIF Horizontal Bottom Contact, 10 Pins</description>
<smd name="5" x="-0.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="4" x="-1.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="6" x="0.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="3" x="-2.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="Z2" x="7.1" y="0.93" dx="1.8" dy="2.6" layer="1"/>
<smd name="Z1" x="-7.1" y="0.93" dx="1.8" dy="2.6" layer="1"/>
<smd name="2" x="-3.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="1" x="-4.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="7" x="1.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="8" x="2.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="9" x="3.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="10" x="4.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<wire x1="-7.5" y1="2.7" x2="7.5" y2="2.7" width="0.1" layer="51"/>
<wire x1="7.5" y1="2.7" x2="7.5" y2="2.1" width="0.1" layer="51"/>
<wire x1="7.5" y1="2.1" x2="7.5" y2="-1.6" width="0.1" layer="51"/>
<wire x1="-7.5" y1="-1.6" x2="-7.5" y2="2.1" width="0.1" layer="51"/>
<wire x1="-7.5" y1="2.1" x2="-7.5" y2="2.7" width="0.1" layer="51"/>
<wire x1="-8.5" y1="-1.6" x2="-7.73" y2="-1.6" width="0.1" layer="51"/>
<wire x1="-7.73" y1="-1.6" x2="-7.5" y2="-1.6" width="0.1" layer="51"/>
<wire x1="-7.5" y1="-1.6" x2="7.5" y2="-1.6" width="0.1" layer="51"/>
<wire x1="7.5" y1="-1.6" x2="7.73" y2="-1.6" width="0.1" layer="51"/>
<wire x1="7.73" y1="-1.6" x2="8.5" y2="-1.6" width="0.1" layer="51"/>
<wire x1="8.5" y1="-1.6" x2="8.5" y2="-2.7" width="0.1" layer="51"/>
<wire x1="8.5" y1="-2.7" x2="-8.5" y2="-2.7" width="0.1" layer="51"/>
<wire x1="-8.5" y1="-2.7" x2="-8.5" y2="-1.6" width="0.1" layer="51"/>
<wire x1="-7.5" y1="2.1" x2="-7.73" y2="2.1" width="0.1" layer="51"/>
<wire x1="-7.73" y1="2.1" x2="-7.73" y2="-1.6" width="0.1" layer="51"/>
<wire x1="7.5" y1="2.1" x2="7.73" y2="2.1" width="0.1" layer="51"/>
<wire x1="7.73" y1="2.1" x2="7.73" y2="-1.6" width="0.1" layer="51"/>
<wire x1="-5.1" y1="2.8" x2="-7.6" y2="2.8" width="0.2" layer="21"/>
<wire x1="-7.6" y1="2.8" x2="-7.6" y2="2.6" width="0.2" layer="21"/>
<wire x1="5.1" y1="2.8" x2="7.6" y2="2.8" width="0.2" layer="21"/>
<wire x1="7.6" y1="2.8" x2="7.6" y2="2.6" width="0.2" layer="21"/>
<wire x1="-7.83" y1="-0.7" x2="-7.83" y2="-1.5" width="0.2" layer="21"/>
<wire x1="-7.83" y1="-1.5" x2="-8.6" y2="-1.5" width="0.2" layer="21"/>
<wire x1="-8.6" y1="-1.5" x2="-8.6" y2="-2.8" width="0.2" layer="21"/>
<wire x1="-8.6" y1="-2.8" x2="8.6" y2="-2.8" width="0.2" layer="21"/>
<wire x1="8.6" y1="-2.8" x2="8.6" y2="-1.5" width="0.2" layer="21"/>
<wire x1="8.6" y1="-1.5" x2="7.83" y2="-1.5" width="0.2" layer="21"/>
<wire x1="7.83" y1="-1.5" x2="7.83" y2="-0.7" width="0.2" layer="21"/>
<text x="-8.6616" y="1.1185" size="0.8128" layer="25" align="bottom-right">&gt;NAME</text>
<text x="-8.6616" y="-0.6534" size="0.8128" layer="27" align="bottom-right">&gt;VALUE</text>
<circle x="-5.3" y="2.23" radius="0.1" width="0.2" layer="21"/>
<polygon width="0.1" layer="39" pour="solid">
<vertex x="-8.03" y="-0.599290625"/>
<vertex x="-8.03" y="-1.270709375"/>
<vertex x="-8.059290625" y="-1.3"/>
<vertex x="-8.8" y="-1.3"/>
<vertex x="-8.8" y="-3"/>
<vertex x="8.8" y="-3"/>
<vertex x="8.8" y="-1.3"/>
<vertex x="8.029290625" y="-1.3"/>
<vertex x="8" y="-1.270709375"/>
<vertex x="8" y="-0.599290625"/>
<vertex x="8.029290625" y="-0.57"/>
<vertex x="8.2" y="-0.57"/>
<vertex x="8.2" y="2.43"/>
<vertex x="7.829290625" y="2.43"/>
<vertex x="7.8" y="2.459290625"/>
<vertex x="7.8" y="3"/>
<vertex x="5.029290625" y="3"/>
<vertex x="5" y="3.029290625"/>
<vertex x="5" y="4.63"/>
<vertex x="-5" y="4.63"/>
<vertex x="-5" y="3.029290625"/>
<vertex x="-5.029290625" y="3"/>
<vertex x="-7.8" y="3"/>
<vertex x="-7.8" y="2.459290625"/>
<vertex x="-7.829290625" y="2.43"/>
<vertex x="-8.2" y="2.43"/>
<vertex x="-8.2" y="-0.57"/>
<vertex x="-8.059290625" y="-0.57"/>
</polygon>
</package>
</packages>
<packages3d>
<package3d name="68611614422" urn="urn:adsk.eagle:package:15743819/2" type="model" library_version="1">
<description>&lt;b&gt;WR-FPC&lt;/b&gt; &lt;br &gt; 1.00 mm SMT ZIF Horizontal Bottom Contact,  16 Pins</description>
<packageinstances>
<packageinstance name="68611614422"/>
</packageinstances>
</package3d>
<package3d name="68611014422" urn="urn:adsk.eagle:package:15743822/2" type="model" library_version="1">
<description>&lt;b&gt;WR-FPC&lt;/b&gt; &lt;br &gt;1.00 mm SMT ZIF Horizontal Bottom Contact, 10 Pins</description>
<packageinstances>
<packageinstance name="68611014422"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="16" urn="urn:adsk.eagle:symbol:15778659/1" library_version="1">
<text x="19.92" y="1.508" size="1.27" layer="95">&gt;NAME</text>
<text x="20.02" y="-1.27" size="1.27" layer="96">&gt;VALUE</text>
<pin name="1" x="17.78" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="2" x="15.24" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="3" x="12.7" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="4" x="10.16" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="5" x="7.62" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="6" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="7" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="8" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="9" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="10" x="-5.08" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="11" x="-7.62" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="12" x="-10.16" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="13" x="-12.7" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="14" x="-15.24" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="15" x="-17.78" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="16" x="-20.32" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<wire x1="19.05" y1="2.54" x2="-21.59" y2="2.54" width="0.254" layer="94"/>
<wire x1="-21.59" y1="2.54" x2="-21.59" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-21.59" y1="-2.54" x2="19.05" y2="-2.54" width="0.254" layer="94"/>
<wire x1="19.05" y1="-2.54" x2="19.05" y2="2.54" width="0.254" layer="94"/>
<wire x1="15.875" y1="-0.635" x2="15.24" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="15.24" y1="-1.27" x2="14.605" y2="-0.635" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="13.335" y1="-0.635" x2="12.7" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="12.7" y1="-1.27" x2="12.065" y2="-0.635" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="-2.54" width="0.254" layer="94"/>
<wire x1="15.24" y1="-1.27" x2="15.24" y2="-2.54" width="0.254" layer="94"/>
<wire x1="10.795" y1="-0.635" x2="10.16" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="10.16" y1="-1.27" x2="9.525" y2="-0.635" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="10.16" y1="-1.27" x2="10.16" y2="-2.54" width="0.254" layer="94"/>
<wire x1="18.415" y1="-0.635" x2="17.78" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="17.78" y1="-1.27" x2="17.145" y2="-0.635" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="17.78" y1="-1.27" x2="17.78" y2="-2.54" width="0.254" layer="94"/>
<wire x1="8.255" y1="-0.635" x2="7.62" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="7.62" y1="-1.27" x2="6.985" y2="-0.635" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="7.62" y1="-1.27" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.715" y1="-0.635" x2="5.08" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="5.08" y1="-1.27" x2="4.445" y2="-0.635" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="3.175" y1="-0.635" x2="2.54" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="2.54" y1="-1.27" x2="1.905" y2="-0.635" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.635" y1="-0.635" x2="0" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.27" x2="-0.635" y2="-0.635" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.27" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-0.635" x2="-2.54" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-2.54" y1="-1.27" x2="-3.175" y2="-0.635" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-0.635" x2="-5.08" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-5.08" y1="-1.27" x2="-5.715" y2="-0.635" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-5.08" y1="-1.27" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-6.985" y1="-0.635" x2="-7.62" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-7.62" y1="-1.27" x2="-8.255" y2="-0.635" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-7.62" y1="-1.27" x2="-7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-9.525" y1="-0.635" x2="-10.16" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-10.16" y1="-1.27" x2="-10.795" y2="-0.635" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-10.16" y1="-1.27" x2="-10.16" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-12.065" y1="-0.635" x2="-12.7" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-12.7" y1="-1.27" x2="-13.335" y2="-0.635" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-12.7" y1="-1.27" x2="-12.7" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-14.605" y1="-0.635" x2="-15.24" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-15.24" y1="-1.27" x2="-15.875" y2="-0.635" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-15.24" y1="-1.27" x2="-15.24" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-17.145" y1="-0.635" x2="-17.78" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-17.78" y1="-1.27" x2="-18.415" y2="-0.635" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-17.78" y1="-1.27" x2="-17.78" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-19.685" y1="-0.635" x2="-20.32" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-20.32" y1="-1.27" x2="-20.955" y2="-0.635" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-20.32" y1="-1.27" x2="-20.32" y2="-2.54" width="0.254" layer="94"/>
<rectangle x1="9.66" y1="-0.77" x2="10.66" y2="1.23" layer="94"/>
<rectangle x1="14.74" y1="-0.77" x2="15.74" y2="1.23" layer="94"/>
<rectangle x1="12.2" y1="-0.77" x2="13.2" y2="1.23" layer="94"/>
<rectangle x1="17.28" y1="-0.77" x2="18.28" y2="1.23" layer="94"/>
<rectangle x1="7.12" y1="-0.77" x2="8.12" y2="1.23" layer="94"/>
<rectangle x1="4.58" y1="-0.77" x2="5.58" y2="1.23" layer="94"/>
<rectangle x1="2.04" y1="-0.77" x2="3.04" y2="1.23" layer="94"/>
<rectangle x1="-0.5" y1="-0.77" x2="0.5" y2="1.23" layer="94"/>
<rectangle x1="-3.04" y1="-0.77" x2="-2.04" y2="1.23" layer="94"/>
<rectangle x1="-5.58" y1="-0.77" x2="-4.58" y2="1.23" layer="94"/>
<rectangle x1="-8.12" y1="-0.77" x2="-7.12" y2="1.23" layer="94"/>
<rectangle x1="-10.66" y1="-0.77" x2="-9.66" y2="1.23" layer="94"/>
<rectangle x1="-13.2" y1="-0.77" x2="-12.2" y2="1.23" layer="94"/>
<rectangle x1="-15.74" y1="-0.77" x2="-14.74" y2="1.23" layer="94"/>
<rectangle x1="-18.28" y1="-0.77" x2="-17.28" y2="1.23" layer="94"/>
<rectangle x1="-20.82" y1="-0.77" x2="-19.82" y2="1.23" layer="94"/>
</symbol>
<symbol name="10" urn="urn:adsk.eagle:symbol:15778664/1" library_version="1">
<text x="12.3" y="1.508" size="1.27" layer="95">&gt;NAME</text>
<text x="12.4" y="-1.27" size="1.27" layer="96">&gt;VALUE</text>
<pin name="1" x="10.16" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="2" x="7.62" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="3" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="4" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="5" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="6" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="7" x="-5.08" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="8" x="-7.62" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="9" x="-10.16" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="10" x="-12.7" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<wire x1="11.43" y1="2.54" x2="-13.97" y2="2.54" width="0.254" layer="94"/>
<wire x1="-13.97" y1="2.54" x2="-13.97" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-13.97" y1="-2.54" x2="11.43" y2="-2.54" width="0.254" layer="94"/>
<wire x1="11.43" y1="-2.54" x2="11.43" y2="2.54" width="0.254" layer="94"/>
<wire x1="8.255" y1="-0.635" x2="7.62" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="7.62" y1="-1.27" x2="6.985" y2="-0.635" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="5.715" y1="-0.635" x2="5.08" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="5.08" y1="-1.27" x2="4.445" y2="-0.635" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="-1.27" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="3.175" y1="-0.635" x2="2.54" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="2.54" y1="-1.27" x2="1.905" y2="-0.635" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="10.795" y1="-0.635" x2="10.16" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="10.16" y1="-1.27" x2="9.525" y2="-0.635" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="10.16" y1="-1.27" x2="10.16" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.635" y1="-0.635" x2="0" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.27" x2="-0.635" y2="-0.635" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.27" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-0.635" x2="-2.54" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-2.54" y1="-1.27" x2="-3.175" y2="-0.635" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-0.635" x2="-5.08" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-5.08" y1="-1.27" x2="-5.715" y2="-0.635" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-5.08" y1="-1.27" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-6.985" y1="-0.635" x2="-7.62" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-7.62" y1="-1.27" x2="-8.255" y2="-0.635" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-7.62" y1="-1.27" x2="-7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-9.525" y1="-0.635" x2="-10.16" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-10.16" y1="-1.27" x2="-10.795" y2="-0.635" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-10.16" y1="-1.27" x2="-10.16" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-12.065" y1="-0.635" x2="-12.7" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-12.7" y1="-1.27" x2="-13.335" y2="-0.635" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-12.7" y1="-1.27" x2="-12.7" y2="-2.54" width="0.254" layer="94"/>
<rectangle x1="2.04" y1="-0.77" x2="3.04" y2="1.23" layer="94"/>
<rectangle x1="7.12" y1="-0.77" x2="8.12" y2="1.23" layer="94"/>
<rectangle x1="4.58" y1="-0.77" x2="5.58" y2="1.23" layer="94"/>
<rectangle x1="9.66" y1="-0.77" x2="10.66" y2="1.23" layer="94"/>
<rectangle x1="-0.5" y1="-0.77" x2="0.5" y2="1.23" layer="94"/>
<rectangle x1="-3.04" y1="-0.77" x2="-2.04" y2="1.23" layer="94"/>
<rectangle x1="-5.58" y1="-0.77" x2="-4.58" y2="1.23" layer="94"/>
<rectangle x1="-8.12" y1="-0.77" x2="-7.12" y2="1.23" layer="94"/>
<rectangle x1="-10.66" y1="-0.77" x2="-9.66" y2="1.23" layer="94"/>
<rectangle x1="-13.2" y1="-0.77" x2="-12.2" y2="1.23" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="6861XX14422_68611614422" urn="urn:adsk.eagle:component:15778794/1" prefix="J" uservalue="yes" library_version="1">
<description>&lt;b&gt;WR-FPC 1.00 mm SMT ZIF Horizontal Bottom Contact&lt;br&gt; &lt;/b&gt;
&lt;br&gt;
&lt;b&gt;KIND PROPERTIES 
&lt;br&gt;&lt;br&gt;
&lt;/b&gt;Pitch 1 mm
&lt;br&gt;
&lt;b&gt;MATERIAL PROPERTIES 
&lt;br&gt;&lt;br&gt;
&lt;/b&gt;Insulator Material PPS
&lt;br&gt;Contact Material Phosphor Bronze
&lt;br&gt;Contact Plating 100 (µ") Tin over 50 (µ") Nickel
&lt;br&gt;Contact Type Stamped 
&lt;br&gt;
&lt;br&gt;&lt;a href="https://katalog.we-online.de/media/images/v2/o33099v209%20Family_WR-FPC_68611214422.jpg" title="Enlarge picture"&gt;
&lt;img src="https://katalog.we-online.de/media/images/v2/o33099v209%20Family_WR-FPC_68611214422.jpg"  width="320"&gt;&lt;/a&gt;&lt;p&gt;
Details see: &lt;a href="https://katalog.we-online.de/en/em/FPC_1_00_SMT_ZIF_HORIZONTAL_BOTTOM_CONTACT_6861XX14422"&gt;https://katalog.we-online.de/en/em/FPC_1_00_SMT_ZIF_HORIZONTAL_BOTTOM_CONTACT_6861XX14422&lt;/a&gt;&lt;p&gt;
Created by Yingchun,Shan  2019-11-15&lt;br&gt;
2019 (C) Würth Elektronik</description>
<gates>
<gate name="G$1" symbol="16" x="0" y="0"/>
</gates>
<devices>
<device name="" package="68611614422">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15743819/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="CONTACT-RESISTANCE" value="20Ohm" constant="no"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/68611614422.pdf" constant="no"/>
<attribute name="IR" value="1A" constant="no"/>
<attribute name="L" value="23mm" constant="no"/>
<attribute name="MATE-WITH" value="WR-FFC 1.00 mm Flat Flexible Cable Type 1 (Contacts on same side)/ Type 2 (Contacts opposite)" constant="no"/>
<attribute name="PACKAGING" value="Tape and Reel" constant="no"/>
<attribute name="PART-NUMBER" value=" 68611614422 " constant="no"/>
<attribute name="PINS" value=" 16 " constant="no"/>
<attribute name="PITCH" value="1mm" constant="no"/>
<attribute name="TOL-R" value="max." constant="no"/>
<attribute name="TYPE" value="Horizontal" constant="no"/>
<attribute name="WORKING-VOLTAGE" value="125V(AC)" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="6861XX14422_68611014422" urn="urn:adsk.eagle:component:15778827/1" prefix="J" uservalue="yes" library_version="1">
<description>&lt;b&gt;WR-FPC 1.00 mm SMT ZIF Horizontal Bottom Contact&lt;br&gt; &lt;/b&gt;
&lt;br&gt;
&lt;b&gt;KIND PROPERTIES 
&lt;br&gt;&lt;br&gt;
&lt;/b&gt;Pitch 1 mm
&lt;br&gt;
&lt;b&gt;MATERIAL PROPERTIES 
&lt;br&gt;&lt;br&gt;
&lt;/b&gt;Insulator Material PPS
&lt;br&gt;Contact Material Phosphor Bronze
&lt;br&gt;Contact Plating 100 (µ") Tin over 50 (µ") Nickel
&lt;br&gt;Contact Type Stamped 
&lt;br&gt;
&lt;br&gt;&lt;a href="https://katalog.we-online.de/media/images/v2/o33099v209%20Family_WR-FPC_68611214422.jpg" title="Enlarge picture"&gt;
&lt;img src="https://katalog.we-online.de/media/images/v2/o33099v209%20Family_WR-FPC_68611214422.jpg"  width="320"&gt;&lt;/a&gt;&lt;p&gt;
Details see: &lt;a href="https://katalog.we-online.de/en/em/FPC_1_00_SMT_ZIF_HORIZONTAL_BOTTOM_CONTACT_6861XX14422"&gt;https://katalog.we-online.de/en/em/FPC_1_00_SMT_ZIF_HORIZONTAL_BOTTOM_CONTACT_6861XX14422&lt;/a&gt;&lt;p&gt;
Created by Yingchun,Shan  2019-11-15&lt;br&gt;
2019 (C) Würth Elektronik</description>
<gates>
<gate name="G$1" symbol="10" x="0" y="0"/>
</gates>
<devices>
<device name="" package="68611014422">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15743822/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="CONTACT-RESISTANCE" value="20Ohm" constant="no"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/68611014422.pdf" constant="no"/>
<attribute name="IR" value="1A" constant="no"/>
<attribute name="L" value="17mm" constant="no"/>
<attribute name="MATE-WITH" value="WR-FFC 1.00 mm Flat Flexible Cable Type 1 (Contacts on same side)/ Type 2 (Contacts opposite)" constant="no"/>
<attribute name="PACKAGING" value="Tape and Reel" constant="no"/>
<attribute name="PART-NUMBER" value=" 68611014422 " constant="no"/>
<attribute name="PINS" value=" 10 " constant="no"/>
<attribute name="PITCH" value="1mm" constant="no"/>
<attribute name="TOL-R" value="max." constant="no"/>
<attribute name="TYPE" value="Horizontal" constant="no"/>
<attribute name="WORKING-VOLTAGE" value="125V(AC)" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="zetex" urn="urn:adsk.eagle:library:418">
<description>&lt;b&gt;Zetex Power MOS FETs, Bridges, Diodes&lt;/b&gt;&lt;p&gt;
http://www.zetex.com&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="MSOP8" urn="urn:adsk.eagle:footprint:30957/1" library_version="5">
<description>&lt;b&gt;Micro Small Outline Package&lt;/b&gt;</description>
<wire x1="-1.4" y1="1.4" x2="1.4" y2="1.4" width="0.2032" layer="21"/>
<wire x1="1.4" y1="1.4" x2="1.4" y2="-1.4" width="0.2032" layer="21"/>
<wire x1="1.4" y1="-1.4" x2="-1.4" y2="-1.4" width="0.2032" layer="21"/>
<wire x1="-1.4" y1="-1.4" x2="-1.4" y2="1.4" width="0.2032" layer="21"/>
<circle x="-1" y="-1" radius="0.2" width="0" layer="21"/>
<smd name="1" x="-0.975" y="-2.05" dx="0.45" dy="1" layer="1"/>
<smd name="2" x="-0.325" y="-2.05" dx="0.45" dy="1" layer="1"/>
<smd name="3" x="0.325" y="-2.05" dx="0.45" dy="1" layer="1"/>
<smd name="4" x="0.975" y="-2.05" dx="0.45" dy="1" layer="1"/>
<smd name="5" x="0.975" y="2.05" dx="0.45" dy="1" layer="1"/>
<smd name="6" x="0.325" y="2.05" dx="0.45" dy="1" layer="1"/>
<smd name="7" x="-0.325" y="2.05" dx="0.45" dy="1" layer="1"/>
<smd name="8" x="-0.975" y="2.05" dx="0.45" dy="1" layer="1"/>
<text x="-1.15" y="0.15" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.65" y="-1.05" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.1254" y1="-2.45" x2="-0.8254" y2="-1.5" layer="51"/>
<rectangle x1="-0.4751" y1="-2.45" x2="-0.1751" y2="-1.5" layer="51"/>
<rectangle x1="0.1751" y1="-2.45" x2="0.4751" y2="-1.5" layer="51"/>
<rectangle x1="0.8253" y1="-2.45" x2="1.1253" y2="-1.5" layer="51"/>
<rectangle x1="0.8254" y1="1.5" x2="1.1254" y2="2.45" layer="51"/>
<rectangle x1="0.1751" y1="1.5" x2="0.4751" y2="2.45" layer="51"/>
<rectangle x1="-0.4751" y1="1.5" x2="-0.1751" y2="2.45" layer="51"/>
<rectangle x1="-1.1253" y1="1.5" x2="-0.8253" y2="2.45" layer="51"/>
</package>
<package name="SO8" urn="urn:adsk.eagle:footprint:30958/1" library_version="5">
<description>&lt;b&gt;SMALL OUTLINE INTEGRATED CIRCUIT&lt;/b&gt; SOP-8L&lt;p&gt;
Source: http://www.diodes.com/datasheets/ds31262.pdf</description>
<wire x1="2.4" y1="1.9" x2="2.4" y2="-1.4" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-1.4" x2="2.4" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-1.9" x2="-2.4" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="-1.9" x2="-2.4" y2="-1.4" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="-1.4" x2="-2.4" y2="1.9" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="1.9" x2="2.4" y2="1.9" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-1.4" x2="-2.4" y2="-1.4" width="0.2032" layer="51"/>
<smd name="2" x="-0.635" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="7" x="-0.635" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="1" x="-1.905" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="3" x="0.635" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="4" x="1.905" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="8" x="-1.905" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="6" x="0.635" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="5" x="1.905" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<text x="-3.175" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="4.445" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.1501" y1="-3.1001" x2="-1.6599" y2="-2" layer="51"/>
<rectangle x1="-0.8801" y1="-3.1001" x2="-0.3899" y2="-2" layer="51"/>
<rectangle x1="0.3899" y1="-3.1001" x2="0.8801" y2="-2" layer="51"/>
<rectangle x1="1.6599" y1="-3.1001" x2="2.1501" y2="-2" layer="51"/>
<rectangle x1="1.6599" y1="2" x2="2.1501" y2="3.1001" layer="51"/>
<rectangle x1="0.3899" y1="2" x2="0.8801" y2="3.1001" layer="51"/>
<rectangle x1="-0.8801" y1="2" x2="-0.3899" y2="3.1001" layer="51"/>
<rectangle x1="-2.1501" y1="2" x2="-1.6599" y2="3.1001" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="MSOP8" urn="urn:adsk.eagle:package:30988/2" type="model" library_version="5">
<description>Micro Small Outline Package</description>
<packageinstances>
<packageinstance name="MSOP8"/>
</packageinstances>
</package3d>
<package3d name="SO8" urn="urn:adsk.eagle:package:30987/2" type="model" library_version="5">
<description>SMALL OUTLINE INTEGRATED CIRCUIT SOP-8L
Source: http://www.diodes.com/datasheets/ds31262.pdf</description>
<packageinstances>
<packageinstance name="SO8"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="P_MOSFET" urn="urn:adsk.eagle:symbol:30956/1" library_version="5">
<wire x1="-1.651" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-2.159" x2="0" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="1.397" y1="-0.508" x2="1.397" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="1.397" y1="-3.048" x2="0" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="2.159" x2="0" y2="2.159" width="0.1524" layer="94"/>
<wire x1="0" y1="2.159" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="3.048" x2="1.397" y2="3.048" width="0.1524" layer="94"/>
<wire x1="1.397" y1="3.048" x2="1.397" y2="0.381" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.159" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<text x="2.54" y="0" size="1.778" layer="96">&gt;VALUE</text>
<text x="2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<rectangle x1="-2.032" y1="1.397" x2="-1.524" y2="2.921" layer="94"/>
<rectangle x1="-2.032" y1="-0.762" x2="-1.524" y2="0.762" layer="94"/>
<rectangle x1="-2.032" y1="-2.921" x2="-1.524" y2="-1.397" layer="94"/>
<rectangle x1="0.762" y1="-0.635" x2="2.032" y2="-0.508" layer="94"/>
<pin name="D" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="S" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="G" x="-5.08" y="-2.54" visible="off" length="short" direction="pas"/>
<polygon width="0.1016" layer="94" pour="solid">
<vertex x="-1.143" y="0.635"/>
<vertex x="-1.143" y="-0.635"/>
<vertex x="-0.127" y="0"/>
</polygon>
<polygon width="0.1016" layer="94" pour="solid">
<vertex x="0.762" y="0.508"/>
<vertex x="1.397" y="-0.508"/>
<vertex x="2.032" y="0.508"/>
</polygon>
</symbol>
<symbol name="N_MOSFET" urn="urn:adsk.eagle:symbol:30955/1" library_version="5">
<wire x1="-0.508" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-2.159" x2="0" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="1.397" y1="-0.254" x2="1.397" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="1.397" y1="-3.048" x2="0" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="2.159" x2="0" y2="2.159" width="0.1524" layer="94"/>
<wire x1="0" y1="2.159" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="3.048" x2="1.397" y2="3.048" width="0.1524" layer="94"/>
<wire x1="1.397" y1="3.048" x2="1.397" y2="0.762" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.159" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<text x="2.54" y="0" size="1.778" layer="96">&gt;VALUE</text>
<text x="2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<rectangle x1="-2.032" y1="1.397" x2="-1.524" y2="2.921" layer="94"/>
<rectangle x1="-2.032" y1="-0.762" x2="-1.524" y2="0.762" layer="94"/>
<rectangle x1="-2.032" y1="-2.921" x2="-1.524" y2="-1.397" layer="94"/>
<rectangle x1="0.762" y1="0.762" x2="2.032" y2="0.889" layer="94"/>
<pin name="D" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="S" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="G" x="-5.08" y="-2.54" visible="off" length="short" direction="pas"/>
<polygon width="0.1016" layer="94" pour="solid">
<vertex x="-1.524" y="0"/>
<vertex x="-0.508" y="-0.635"/>
<vertex x="-0.508" y="0.635"/>
</polygon>
<polygon width="0.1016" layer="94" pour="solid">
<vertex x="0.762" y="-0.254"/>
<vertex x="2.032" y="-0.254"/>
<vertex x="1.397" y="0.762"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="PN_MOSFET-" urn="urn:adsk.eagle:component:31040/3" prefix="T" uservalue="yes" library_version="5">
<description>&lt;b&gt;DUAL N AND P-CHANNEL ENHANCEMENT MODE MOSFET&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.farnell.com/datasheets/1693880.pdf"&gt;Data sheet&lt;/a&gt;</description>
<gates>
<gate name="2" symbol="P_MOSFET" x="0" y="-7.62"/>
<gate name="1" symbol="N_MOSFET" x="0" y="7.62"/>
</gates>
<devices>
<device name="MSOP8" package="MSOP8">
<connects>
<connect gate="1" pin="D" pad="7 8" route="any"/>
<connect gate="1" pin="G" pad="2"/>
<connect gate="1" pin="S" pad="1"/>
<connect gate="2" pin="D" pad="5 6"/>
<connect gate="2" pin="G" pad="4"/>
<connect gate="2" pin="S" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30988/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="SO8" package="SO8">
<connects>
<connect gate="1" pin="D" pad="7 8" route="any"/>
<connect gate="1" pin="G" pad="2"/>
<connect gate="1" pin="S" pad="1"/>
<connect gate="2" pin="D" pad="5 6"/>
<connect gate="2" pin="G" pad="4"/>
<connect gate="2" pin="S" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30987/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="rcl" urn="urn:adsk.eagle:library:334">
<description>&lt;b&gt;Resistors, Capacitors, Inductors&lt;/b&gt;&lt;p&gt;
Based on the previous libraries:
&lt;ul&gt;
&lt;li&gt;r.lbr
&lt;li&gt;cap.lbr 
&lt;li&gt;cap-fe.lbr
&lt;li&gt;captant.lbr
&lt;li&gt;polcap.lbr
&lt;li&gt;ipc-smd.lbr
&lt;/ul&gt;
All SMD packages are defined according to the IPC specifications and  CECC&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;&lt;p&gt;
&lt;p&gt;
for Electrolyt Capacitors see also :&lt;p&gt;
www.bccomponents.com &lt;p&gt;
www.panasonic.com&lt;p&gt;
www.kemet.com&lt;p&gt;
http://www.secc.co.jp/pdf/os_e/2004/e_os_all.pdf &lt;b&gt;(SANYO)&lt;/b&gt;
&lt;p&gt;
for trimmer refence see : &lt;u&gt;www.electrospec-inc.com/cross_references/trimpotcrossref.asp&lt;/u&gt;&lt;p&gt;

&lt;table border=0 cellspacing=0 cellpadding=0 width="100%" cellpaddding=0&gt;
&lt;tr valign="top"&gt;

&lt;! &lt;td width="10"&gt;&amp;nbsp;&lt;/td&gt;
&lt;td width="90%"&gt;

&lt;b&gt;&lt;font color="#0000FF" size="4"&gt;TRIM-POT CROSS REFERENCE&lt;/font&gt;&lt;/b&gt;
&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=2&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;RECTANGULAR MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BOURNS&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BI&amp;nbsp;TECH&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;DALE-VISHAY&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PHILIPS/MEPCO&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MURATA&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PANASONIC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;SPECTROL&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MILSPEC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;&lt;TD&gt;&amp;nbsp;&lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3 &gt;
      3005P&lt;BR&gt;
      3006P&lt;BR&gt;
      3006W&lt;BR&gt;
      3006Y&lt;BR&gt;
      3009P&lt;BR&gt;
      3009W&lt;BR&gt;
      3009Y&lt;BR&gt;
      3057J&lt;BR&gt;
      3057L&lt;BR&gt;
      3057P&lt;BR&gt;
      3057Y&lt;BR&gt;
      3059J&lt;BR&gt;
      3059L&lt;BR&gt;
      3059P&lt;BR&gt;
      3059Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      89P&lt;BR&gt;
      89W&lt;BR&gt;
      89X&lt;BR&gt;
      89PH&lt;BR&gt;
      76P&lt;BR&gt;
      89XH&lt;BR&gt;
      78SLT&lt;BR&gt;
      78L&amp;nbsp;ALT&lt;BR&gt;
      56P&amp;nbsp;ALT&lt;BR&gt;
      78P&amp;nbsp;ALT&lt;BR&gt;
      T8S&lt;BR&gt;
      78L&lt;BR&gt;
      56P&lt;BR&gt;
      78P&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      T18/784&lt;BR&gt;
      783&lt;BR&gt;
      781&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2199&lt;BR&gt;
      1697/1897&lt;BR&gt;
      1680/1880&lt;BR&gt;
      2187&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      8035EKP/CT20/RJ-20P&lt;BR&gt;
      -&lt;BR&gt;
      RJ-20X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      1211L&lt;BR&gt;
      8012EKQ&amp;nbsp;ALT&lt;BR&gt;
      8012EKR&amp;nbsp;ALT&lt;BR&gt;
      1211P&lt;BR&gt;
      8012EKJ&lt;BR&gt;
      8012EKL&lt;BR&gt;
      8012EKQ&lt;BR&gt;
      8012EKR&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      2101P&lt;BR&gt;
      2101W&lt;BR&gt;
      2101Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2102L&lt;BR&gt;
      2102S&lt;BR&gt;
      2102Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVMCOG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      43P&lt;BR&gt;
      43W&lt;BR&gt;
      43Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      40L&lt;BR&gt;
      40P&lt;BR&gt;
      40Y&lt;BR&gt;
      70Y-T602&lt;BR&gt;
      70L&lt;BR&gt;
      70P&lt;BR&gt;
      70Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SQUARE MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
   &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3250L&lt;BR&gt;
      3250P&lt;BR&gt;
      3250W&lt;BR&gt;
      3250X&lt;BR&gt;
      3252P&lt;BR&gt;
      3252W&lt;BR&gt;
      3252X&lt;BR&gt;
      3260P&lt;BR&gt;
      3260W&lt;BR&gt;
      3260X&lt;BR&gt;
      3262P&lt;BR&gt;
      3262W&lt;BR&gt;
      3262X&lt;BR&gt;
      3266P&lt;BR&gt;
      3266W&lt;BR&gt;
      3266X&lt;BR&gt;
      3290H&lt;BR&gt;
      3290P&lt;BR&gt;
      3290W&lt;BR&gt;
      3292P&lt;BR&gt;
      3292W&lt;BR&gt;
      3292X&lt;BR&gt;
      3296P&lt;BR&gt;
      3296W&lt;BR&gt;
      3296X&lt;BR&gt;
      3296Y&lt;BR&gt;
      3296Z&lt;BR&gt;
      3299P&lt;BR&gt;
      3299W&lt;BR&gt;
      3299X&lt;BR&gt;
      3299Y&lt;BR&gt;
      3299Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64P&amp;nbsp;ALT&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      64X&amp;nbsp;ALT&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66P&lt;BR&gt;
      66W&lt;BR&gt;
      66X&lt;BR&gt;
      67P&lt;BR&gt;
      67W&lt;BR&gt;
      67X&lt;BR&gt;
      67Y&lt;BR&gt;
      67Z&lt;BR&gt;
      68P&lt;BR&gt;
      68W&lt;BR&gt;
      68X&lt;BR&gt;
      67Y&amp;nbsp;ALT&lt;BR&gt;
      67Z&amp;nbsp;ALT&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      5050&lt;BR&gt;
      5091&lt;BR&gt;
      5080&lt;BR&gt;
      5087&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T63YB&lt;BR&gt;
      T63XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      5887&lt;BR&gt;
      5891&lt;BR&gt;
      5880&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T93Z&lt;BR&gt;
      T93YA&lt;BR&gt;
      T93XA&lt;BR&gt;
      T93YB&lt;BR&gt;
      T93XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKW&lt;BR&gt;
      8026EKM&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKB&lt;BR&gt;
      8026EKM&lt;BR&gt;
      1309X&lt;BR&gt;
      1309P&lt;BR&gt;
      1309W&lt;BR&gt;
      8024EKP&lt;BR&gt;
      8024EKW&lt;BR&gt;
      8024EKN&lt;BR&gt;
      RJ-9P/CT9P&lt;BR&gt;
      RJ-9W&lt;BR&gt;
      RJ-9X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3105P/3106P&lt;BR&gt;
      3105W/3106W&lt;BR&gt;
      3105X/3106X&lt;BR&gt;
      3105Y/3106Y&lt;BR&gt;
      3105Z/3105Z&lt;BR&gt;
      3102P&lt;BR&gt;
      3102W&lt;BR&gt;
      3102X&lt;BR&gt;
      3102Y&lt;BR&gt;
      3102Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMCBG&lt;BR&gt;
      EVMCCG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      55-1-X&lt;BR&gt;
      55-4-X&lt;BR&gt;
      55-3-X&lt;BR&gt;
      55-2-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      50-2-X&lt;BR&gt;
      50-4-X&lt;BR&gt;
      50-3-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      64Y&lt;BR&gt;
      64Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3323P&lt;BR&gt;
      3323S&lt;BR&gt;
      3323W&lt;BR&gt;
      3329H&lt;BR&gt;
      3329P&lt;BR&gt;
      3329W&lt;BR&gt;
      3339H&lt;BR&gt;
      3339P&lt;BR&gt;
      3339W&lt;BR&gt;
      3352E&lt;BR&gt;
      3352H&lt;BR&gt;
      3352K&lt;BR&gt;
      3352P&lt;BR&gt;
      3352T&lt;BR&gt;
      3352V&lt;BR&gt;
      3352W&lt;BR&gt;
      3362H&lt;BR&gt;
      3362M&lt;BR&gt;
      3362P&lt;BR&gt;
      3362R&lt;BR&gt;
      3362S&lt;BR&gt;
      3362U&lt;BR&gt;
      3362W&lt;BR&gt;
      3362X&lt;BR&gt;
      3386B&lt;BR&gt;
      3386C&lt;BR&gt;
      3386F&lt;BR&gt;
      3386H&lt;BR&gt;
      3386K&lt;BR&gt;
      3386M&lt;BR&gt;
      3386P&lt;BR&gt;
      3386S&lt;BR&gt;
      3386W&lt;BR&gt;
      3386X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      25P&lt;BR&gt;
      25S&lt;BR&gt;
      25RX&lt;BR&gt;
      82P&lt;BR&gt;
      82M&lt;BR&gt;
      82PA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      91E&lt;BR&gt;
      91X&lt;BR&gt;
      91T&lt;BR&gt;
      91B&lt;BR&gt;
      91A&lt;BR&gt;
      91V&lt;BR&gt;
      91W&lt;BR&gt;
      25W&lt;BR&gt;
      25V&lt;BR&gt;
      25P&lt;BR&gt;
      -&lt;BR&gt;
      25S&lt;BR&gt;
      25U&lt;BR&gt;
      25RX&lt;BR&gt;
      25X&lt;BR&gt;
      72XW&lt;BR&gt;
      72XL&lt;BR&gt;
      72PM&lt;BR&gt;
      72RX&lt;BR&gt;
      -&lt;BR&gt;
      72PX&lt;BR&gt;
      72P&lt;BR&gt;
      72RXW&lt;BR&gt;
      72RXL&lt;BR&gt;
      72X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T7YB&lt;BR&gt;
      T7YA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      TXD&lt;BR&gt;
      TYA&lt;BR&gt;
      TYP&lt;BR&gt;
      -&lt;BR&gt;
      TYD&lt;BR&gt;
      TX&lt;BR&gt;
      -&lt;BR&gt;
      150SX&lt;BR&gt;
      100SX&lt;BR&gt;
      102T&lt;BR&gt;
      101S&lt;BR&gt;
      190T&lt;BR&gt;
      150TX&lt;BR&gt;
      101&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      101SX&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ET6P&lt;BR&gt;
      ET6S&lt;BR&gt;
      ET6X&lt;BR&gt;
      RJ-6W/8014EMW&lt;BR&gt;
      RJ-6P/8014EMP&lt;BR&gt;
      RJ-6X/8014EMX&lt;BR&gt;
      TM7W&lt;BR&gt;
      TM7P&lt;BR&gt;
      TM7X&lt;BR&gt;
      -&lt;BR&gt;
      8017SMS&lt;BR&gt;
      -&lt;BR&gt;
      8017SMB&lt;BR&gt;
      8017SMA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      CT-6W&lt;BR&gt;
      CT-6H&lt;BR&gt;
      CT-6P&lt;BR&gt;
      CT-6R&lt;BR&gt;
      -&lt;BR&gt;
      CT-6V&lt;BR&gt;
      CT-6X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKV&lt;BR&gt;
      -&lt;BR&gt;
      8038EKX&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKP&lt;BR&gt;
      8038EKZ&lt;BR&gt;
      8038EKW&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3321H&lt;BR&gt;
      3321P&lt;BR&gt;
      3321N&lt;BR&gt;
      1102H&lt;BR&gt;
      1102P&lt;BR&gt;
      1102T&lt;BR&gt;
      RVA0911V304A&lt;BR&gt;
      -&lt;BR&gt;
      RVA0911H413A&lt;BR&gt;
      RVG0707V100A&lt;BR&gt;
      RVA0607V(H)306A&lt;BR&gt;
      RVA1214H213A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3104B&lt;BR&gt;
      3104C&lt;BR&gt;
      3104F&lt;BR&gt;
      3104H&lt;BR&gt;
      -&lt;BR&gt;
      3104M&lt;BR&gt;
      3104P&lt;BR&gt;
      3104S&lt;BR&gt;
      3104W&lt;BR&gt;
      3104X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      EVMQ0G&lt;BR&gt;
      EVMQIG&lt;BR&gt;
      EVMQ3G&lt;BR&gt;
      EVMS0G&lt;BR&gt;
      EVMQ0G&lt;BR&gt;
      EVMG0G&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMK4GA00B&lt;BR&gt;
      EVM30GA00B&lt;BR&gt;
      EVMK0GA00B&lt;BR&gt;
      EVM38GA00B&lt;BR&gt;
      EVMB6&lt;BR&gt;
      EVLQ0&lt;BR&gt;
      -&lt;BR&gt;
      EVMMSG&lt;BR&gt;
      EVMMBG&lt;BR&gt;
      EVMMAG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMMCS&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM0&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM3&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      62-3-1&lt;BR&gt;
      62-1-2&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67R&lt;BR&gt;
      -&lt;BR&gt;
      67P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67X&lt;BR&gt;
      63V&lt;BR&gt;
      63S&lt;BR&gt;
      63M&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63H&lt;BR&gt;
      63P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;&amp;nbsp;&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=3&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT color="#0000FF" SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SMD TRIM-POT CROSS REFERENCE&lt;/B&gt;&lt;/FONT&gt;
      &lt;P&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3224G&lt;BR&gt;
      3224J&lt;BR&gt;
      3224W&lt;BR&gt;
      3269P&lt;BR&gt;
      3269W&lt;BR&gt;
      3269X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      44G&lt;BR&gt;
      44J&lt;BR&gt;
      44W&lt;BR&gt;
      84P&lt;BR&gt;
      84W&lt;BR&gt;
      84X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST63Z&lt;BR&gt;
      ST63Y&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST5P&lt;BR&gt;
      ST5W&lt;BR&gt;
      ST5X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3314G&lt;BR&gt;
      3314J&lt;BR&gt;
      3364A/B&lt;BR&gt;
      3364C/D&lt;BR&gt;
      3364W/X&lt;BR&gt;
      3313G&lt;BR&gt;
      3313J&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      23B&lt;BR&gt;
      23A&lt;BR&gt;
      21X&lt;BR&gt;
      21W&lt;BR&gt;
      -&lt;BR&gt;
      22B&lt;BR&gt;
      22A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST5YL/ST53YL&lt;BR&gt;
      ST5YJ/5T53YJ&lt;BR&gt;
      ST-23A&lt;BR&gt;
      ST-22B&lt;BR&gt;
      ST-22&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST-4B&lt;BR&gt;
      ST-4A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST-3B&lt;BR&gt;
      ST-3A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVM-6YS&lt;BR&gt;
      EVM-1E&lt;BR&gt;
      EVM-1G&lt;BR&gt;
      EVM-1D&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      G4B&lt;BR&gt;
      G4A&lt;BR&gt;
      TR04-3S1&lt;BR&gt;
      TRG04-2S1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      DVR-43A&lt;BR&gt;
      CVR-42C&lt;BR&gt;
      CVR-42A/C&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;
&lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;ALT =&amp;nbsp;ALTERNATE&lt;/B&gt;&lt;/FONT&gt;
&lt;P&gt;

&amp;nbsp;
&lt;P&gt;
&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;</description>
<packages>
<package name="R0402" urn="urn:adsk.eagle:footprint:23043/3" library_version="11">
<description>&lt;b&gt;Chip RESISTOR 0402 EIA (1005 Metric)&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1" y1="0.483" x2="1" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1" y1="0.483" x2="1" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1" y1="-0.483" x2="-1" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1" y1="-0.483" x2="-1" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.5" y="0" dx="0.6" dy="0.7" layer="1"/>
<smd name="2" x="0.5" y="0" dx="0.6" dy="0.7" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.35" x2="0.1999" y2="0.35" layer="35"/>
</package>
<package name="R0603" urn="urn:adsk.eagle:footprint:23044/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0805" urn="urn:adsk.eagle:footprint:23045/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R0805W" urn="urn:adsk.eagle:footprint:23046/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R1206" urn="urn:adsk.eagle:footprint:23047/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206W" urn="urn:adsk.eagle:footprint:23048/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1210" urn="urn:adsk.eagle:footprint:23049/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
</package>
<package name="R1210W" urn="urn:adsk.eagle:footprint:23050/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="R2010" urn="urn:adsk.eagle:footprint:23051/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2010W" urn="urn:adsk.eagle:footprint:23052/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2012" urn="urn:adsk.eagle:footprint:23053/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2012W" urn="urn:adsk.eagle:footprint:23054/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.94" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2512" urn="urn:adsk.eagle:footprint:23055/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R2512W" urn="urn:adsk.eagle:footprint:23056/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.896" y="0" dx="2" dy="2.1" layer="1"/>
<smd name="2" x="2.896" y="0" dx="2" dy="2.1" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R3216" urn="urn:adsk.eagle:footprint:23057/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3216W" urn="urn:adsk.eagle:footprint:23058/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3225" urn="urn:adsk.eagle:footprint:23059/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R3225W" urn="urn:adsk.eagle:footprint:23060/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R5025" urn="urn:adsk.eagle:footprint:23061/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R5025W" urn="urn:adsk.eagle:footprint:23062/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332" urn="urn:adsk.eagle:footprint:23063/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.1" y="0" dx="1" dy="3.2" layer="1"/>
<smd name="2" x="3.1" y="0" dx="1" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332W" urn="urn:adsk.eagle:footprint:25646/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<smd name="2" x="3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M0805" urn="urn:adsk.eagle:footprint:23065/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M1206" urn="urn:adsk.eagle:footprint:23066/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M1406" urn="urn:adsk.eagle:footprint:23067/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="M2012" urn="urn:adsk.eagle:footprint:23068/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M2309" urn="urn:adsk.eagle:footprint:23069/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M3216" urn="urn:adsk.eagle:footprint:23070/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M3516" urn="urn:adsk.eagle:footprint:23071/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="M5923" urn="urn:adsk.eagle:footprint:23072/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="0204/5" urn="urn:adsk.eagle:footprint:22991/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.0066" y="1.1684" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="0204/7" urn="urn:adsk.eagle:footprint:22998/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.2954" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
<package name="0204V" urn="urn:adsk.eagle:footprint:22999/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.508" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.508" layer="21"/>
<circle x="-1.27" y="0" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="0" radius="0.635" width="0.0508" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.1336" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/10" urn="urn:adsk.eagle:footprint:22992/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="0207/12" urn="urn:adsk.eagle:footprint:22993/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 12 mm</description>
<wire x1="6.35" y1="0" x2="5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="4.445" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-4.445" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.3086" y2="0.3048" layer="21"/>
<rectangle x1="-5.3086" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
</package>
<package name="0207/15" urn="urn:adsk.eagle:footprint:22997/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 15mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="5.715" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="5.715" y1="-0.3048" x2="6.5786" y2="0.3048" layer="21"/>
<rectangle x1="-6.5786" y1="-0.3048" x2="-5.715" y2="0.3048" layer="21"/>
</package>
<package name="0207/2V" urn="urn:adsk.eagle:footprint:22994/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.254" y1="0" x2="0.254" y2="0" width="0.6096" layer="21"/>
<wire x1="0.381" y1="0" x2="1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.27" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-0.0508" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.0508" y="-2.2352" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/5V" urn="urn:adsk.eagle:footprint:22995/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-0.889" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.762" y1="0" x2="0.762" y2="0" width="0.6096" layer="21"/>
<wire x1="0.889" y1="0" x2="2.54" y2="0" width="0.6096" layer="51"/>
<circle x="-2.54" y="0" radius="1.27" width="0.1016" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.143" y="0.889" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.143" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/7" urn="urn:adsk.eagle:footprint:22996/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 7.5 mm</description>
<wire x1="-3.81" y1="0" x2="-3.429" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.429" y1="0" x2="3.81" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.5588" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.429" y1="-0.3048" x2="-3.175" y2="0.3048" layer="51"/>
<rectangle x1="3.175" y1="-0.3048" x2="3.429" y2="0.3048" layer="51"/>
</package>
<package name="0309/10" urn="urn:adsk.eagle:footprint:23073/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 10mm</description>
<wire x1="-4.699" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="5.08" y1="0" x2="4.699" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.6228" y1="-0.3048" x2="-4.318" y2="0.3048" layer="51"/>
<rectangle x1="4.318" y1="-0.3048" x2="4.6228" y2="0.3048" layer="51"/>
</package>
<package name="0309/12" urn="urn:adsk.eagle:footprint:23074/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.318" y1="-0.3048" x2="5.1816" y2="0.3048" layer="21"/>
<rectangle x1="-5.1816" y1="-0.3048" x2="-4.318" y2="0.3048" layer="21"/>
</package>
<package name="0309V" urn="urn:adsk.eagle:footprint:23075/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 2.5 mm</description>
<wire x1="1.27" y1="0" x2="0.635" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.524" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="0.762" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="0.254" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.254" y="-2.2098" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.635" y1="-0.3048" x2="-0.3302" y2="0.3048" layer="51"/>
<rectangle x1="-0.3302" y1="-0.3048" x2="0.254" y2="0.3048" layer="21"/>
</package>
<package name="0411/12" urn="urn:adsk.eagle:footprint:23076/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.762" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.762" layer="51"/>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.3594" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
<rectangle x1="5.08" y1="-0.381" x2="5.3594" y2="0.381" layer="21"/>
</package>
<package name="0411/15" urn="urn:adsk.eagle:footprint:23077/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 15 mm</description>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0" x2="-6.35" y2="0" width="0.762" layer="51"/>
<wire x1="6.35" y1="0" x2="7.62" y2="0" width="0.762" layer="51"/>
<pad name="1" x="-7.62" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="5.08" y1="-0.381" x2="6.477" y2="0.381" layer="21"/>
<rectangle x1="-6.477" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
</package>
<package name="0411V" urn="urn:adsk.eagle:footprint:23078/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 3.81 mm</description>
<wire x1="1.27" y1="0" x2="0.3048" y2="0" width="0.762" layer="51"/>
<wire x1="-1.5748" y1="0" x2="-2.54" y2="0" width="0.762" layer="51"/>
<circle x="-2.54" y="0" radius="2.032" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.9144" shape="octagon"/>
<text x="-0.508" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.5334" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.4732" y1="-0.381" x2="0.2032" y2="0.381" layer="21"/>
</package>
<package name="0414/15" urn="urn:adsk.eagle:footprint:23079/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="1.905" x2="-5.842" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-5.842" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="-2.159" x2="6.096" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="2.159" x2="6.096" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-6.096" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="2.159" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="2.032" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-2.159" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="-4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.842" y1="2.159" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.842" y1="-2.159" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-1.905" x2="6.096" y2="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.5654" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.4064" x2="6.5024" y2="0.4064" layer="21"/>
<rectangle x1="-6.5024" y1="-0.4064" x2="-6.096" y2="0.4064" layer="21"/>
</package>
<package name="0414V" urn="urn:adsk.eagle:footprint:23080/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.159" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.381" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.381" y="-2.3622" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.2954" y2="0.4064" layer="21"/>
</package>
<package name="0617/17" urn="urn:adsk.eagle:footprint:23081/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 17.5 mm</description>
<wire x1="-8.89" y1="0" x2="-8.636" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.636" y1="0" x2="8.89" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-8.89" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.016" shape="octagon"/>
<text x="-8.128" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.096" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-8.5344" y1="-0.4064" x2="-8.2296" y2="0.4064" layer="51"/>
<rectangle x1="8.2296" y1="-0.4064" x2="8.5344" y2="0.4064" layer="51"/>
</package>
<package name="0617/22" urn="urn:adsk.eagle:footprint:23082/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 22.5 mm</description>
<wire x1="-10.287" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.287" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.255" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.1854" y1="-0.4064" x2="-8.255" y2="0.4064" layer="21"/>
<rectangle x1="8.255" y1="-0.4064" x2="10.1854" y2="0.4064" layer="21"/>
</package>
<package name="0617V" urn="urn:adsk.eagle:footprint:23083/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="3.048" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="0.635" y="1.4224" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.635" y="-2.6162" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.3208" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="0922/22" urn="urn:adsk.eagle:footprint:23084/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 22.5 mm</description>
<wire x1="11.43" y1="0" x2="10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-11.43" y1="0" x2="-10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-10.16" y1="-4.191" x2="-10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="4.572" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="4.318" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="-4.572" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="-4.318" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="-8.636" y2="4.318" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="-8.636" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="9.779" y1="4.572" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="9.779" y1="-4.572" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-4.191" x2="10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-4.191" x2="-9.779" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-10.16" y1="4.191" x2="-9.779" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="9.779" y1="-4.572" x2="10.16" y2="-4.191" width="0.1524" layer="21" curve="90"/>
<wire x1="9.779" y1="4.572" x2="10.16" y2="4.191" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-10.16" y="5.1054" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.7188" y1="-0.4064" x2="-10.16" y2="0.4064" layer="51"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-10.16" y2="0.4064" layer="21"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.7188" y2="0.4064" layer="51"/>
</package>
<package name="P0613V" urn="urn:adsk.eagle:footprint:23085/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.286" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.254" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.254" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="P0613/15" urn="urn:adsk.eagle:footprint:23086/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.032" x2="-6.223" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.032" x2="-6.223" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="-2.286" x2="6.477" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="2.286" x2="6.477" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.223" y1="2.286" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.159" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-6.223" y1="-2.286" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.159" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="-5.207" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="-5.207" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.223" y1="2.286" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-2.286" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="0.635" width="0.1524" layer="51"/>
<wire x1="6.477" y1="2.032" x2="6.477" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.032" x2="-6.477" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.477" y="2.6924" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.0358" y1="-0.4064" x2="-6.477" y2="0.4064" layer="51"/>
<rectangle x1="6.477" y1="-0.4064" x2="7.0358" y2="0.4064" layer="51"/>
</package>
<package name="P0817/22" urn="urn:adsk.eagle:footprint:23087/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 22.5 mm</description>
<wire x1="-10.414" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="-3.429" x2="-8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="3.81" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="3.556" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="-3.81" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-3.556" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="-6.985" y2="3.556" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="-6.985" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.128" y1="3.81" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="8.128" y1="-3.81" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.429" x2="8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.414" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="3.429" x2="-8.128" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.509" y1="-3.429" x2="-8.128" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="8.128" y1="3.81" x2="8.509" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.128" y1="-3.81" x2="8.509" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="4.2164" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.223" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="6.604" y="-2.2606" size="1.27" layer="51" ratio="10" rot="R90">0817</text>
<rectangle x1="8.509" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-8.509" y2="0.4064" layer="21"/>
</package>
<package name="P0817V" urn="urn:adsk.eagle:footprint:23088/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 6.35 mm</description>
<wire x1="-3.81" y1="0" x2="-5.08" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="3.81" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="octagon"/>
<text x="-1.016" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.016" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.032" size="1.016" layer="21" ratio="12">0817</text>
<rectangle x1="-3.81" y1="-0.4064" x2="0" y2="0.4064" layer="21"/>
</package>
<package name="V234/12" urn="urn:adsk.eagle:footprint:23089/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V234, grid 12.5 mm</description>
<wire x1="-4.953" y1="1.524" x2="-4.699" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.953" y1="-1.524" x2="-4.699" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="1.524" x2="-4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="-4.699" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.8128" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.016" shape="octagon"/>
<text x="-4.953" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.953" y1="-0.4064" x2="5.4102" y2="0.4064" layer="21"/>
<rectangle x1="-5.4102" y1="-0.4064" x2="-4.953" y2="0.4064" layer="21"/>
</package>
<package name="V235/17" urn="urn:adsk.eagle:footprint:23090/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V235, grid 17.78 mm</description>
<wire x1="-6.731" y1="2.921" x2="6.731" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-7.112" y1="2.54" x2="-7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.921" x2="-6.731" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="7.112" y1="2.54" x2="7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.89" y1="0" x2="7.874" y2="0" width="1.016" layer="51"/>
<wire x1="-7.874" y1="0" x2="-8.89" y2="0" width="1.016" layer="51"/>
<wire x1="-7.112" y1="-2.54" x2="-6.731" y2="-2.921" width="0.1524" layer="21" curve="90"/>
<wire x1="6.731" y1="2.921" x2="7.112" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="-2.921" x2="7.112" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-7.112" y1="2.54" x2="-6.731" y2="2.921" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-8.89" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.1938" shape="octagon"/>
<text x="-6.858" y="3.302" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.842" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="7.112" y1="-0.508" x2="7.747" y2="0.508" layer="21"/>
<rectangle x1="-7.747" y1="-0.508" x2="-7.112" y2="0.508" layer="21"/>
</package>
<package name="V526-0" urn="urn:adsk.eagle:footprint:23091/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V526-0, grid 2.5 mm</description>
<wire x1="-2.54" y1="1.016" x2="-2.286" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="1.27" x2="2.54" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="-1.27" x2="2.54" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.54" y1="-1.016" x2="-2.286" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.27" x2="-2.286" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.016" x2="2.54" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.27" x2="2.286" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="-2.54" y2="-1.016" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.413" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.413" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102R" urn="urn:adsk.eagle:footprint:23092/1" library_version="11">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102W" urn="urn:adsk.eagle:footprint:23093/1" library_version="11">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204R" urn="urn:adsk.eagle:footprint:25676/1" library_version="11">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.938" y1="0.6" x2="-0.938" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.938" y1="-0.6" x2="0.938" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204W" urn="urn:adsk.eagle:footprint:25677/1" library_version="11">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.684" y1="0.6" x2="-0.684" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.684" y1="-0.6" x2="0.684" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207R" urn="urn:adsk.eagle:footprint:25678/1" library_version="11">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.2125" y1="1" x2="-1.2125" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.2125" y1="-1" x2="1.2125" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<smd name="2" x="2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<text x="-2.2225" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.2225" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207W" urn="urn:adsk.eagle:footprint:25679/1" library_version="11">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.149" y1="1" x2="-1.149" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.149" y1="-1" x2="1.149" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<smd name="2" x="2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<text x="-2.54" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0922V" urn="urn:adsk.eagle:footprint:23098/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 7.5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="4.572" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.508" y="1.6764" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.508" y="-2.9972" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.54" size="1.016" layer="21" ratio="12">0922</text>
<rectangle x1="-3.81" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="RDH/15" urn="urn:adsk.eagle:footprint:23099/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type RDH, grid 15 mm</description>
<wire x1="-7.62" y1="0" x2="-6.858" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="3.048" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="2.794" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-3.048" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.794" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="-4.953" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="-4.953" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.667" x2="-6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-2.667" x2="6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.858" y1="0" x2="7.62" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.667" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="3.048" x2="6.477" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.667" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="6.096" y1="-3.048" x2="6.477" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.35" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="4.572" y="-1.7272" size="1.27" layer="51" ratio="10" rot="R90">RDH</text>
<rectangle x1="-6.7564" y1="-0.4064" x2="-6.4516" y2="0.4064" layer="51"/>
<rectangle x1="6.4516" y1="-0.4064" x2="6.7564" y2="0.4064" layer="51"/>
</package>
<package name="MINI_MELF-0102AX" urn="urn:adsk.eagle:footprint:23100/1" library_version="11">
<description>&lt;b&gt;Mini MELF 0102 Axial&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.6" width="0" layer="51"/>
<circle x="0" y="0" radius="0.6" width="0" layer="52"/>
<smd name="1" x="0" y="0" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="2" x="0" y="0" dx="1.9" dy="1.9" layer="16" roundness="100"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0" y="0" drill="1.3"/>
</package>
<package name="R0201" urn="urn:adsk.eagle:footprint:25683/1" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; chip&lt;p&gt;
Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<smd name="1" x="-0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<smd name="2" x="0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="0.15" layer="21"/>
</package>
<package name="VTA52" urn="urn:adsk.eagle:footprint:25684/1" library_version="11">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR52&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-15.24" y1="0" x2="-13.97" y2="0" width="0.6096" layer="51"/>
<wire x1="12.6225" y1="0.025" x2="12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="4.725" x2="-12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="4.725" x2="-12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="0.025" x2="-12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="-4.65" x2="12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="-4.65" x2="12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="13.97" y1="0" x2="15.24" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-15.24" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="15.24" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-13.97" y1="-0.3048" x2="-12.5675" y2="0.3048" layer="21"/>
<rectangle x1="12.5675" y1="-0.3048" x2="13.97" y2="0.3048" layer="21"/>
</package>
<package name="VTA53" urn="urn:adsk.eagle:footprint:25685/1" library_version="11">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR53&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="4.7" x2="-9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="4.7" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-4.675" x2="9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-4.675" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA54" urn="urn:adsk.eagle:footprint:25686/1" library_version="11">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR54&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="3.3" x2="-9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="3.3" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-3.3" x2="9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-3.3" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA55" urn="urn:adsk.eagle:footprint:25687/1" library_version="11">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-8.255" y1="0" x2="-6.985" y2="0" width="0.6096" layer="51"/>
<wire x1="6.405" y1="0" x2="6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="3.3" x2="-6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="3.3" x2="-6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="0" x2="-6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="-3.3" x2="6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="-3.3" x2="6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="6.985" y1="0" x2="8.255" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-8.255" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="8.255" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.985" y1="-0.3048" x2="-6.35" y2="0.3048" layer="21"/>
<rectangle x1="6.35" y1="-0.3048" x2="6.985" y2="0.3048" layer="21"/>
</package>
<package name="VTA56" urn="urn:adsk.eagle:footprint:25688/1" library_version="11">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR56&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="4.5" y1="0" x2="4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="3.3" x2="-4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="3.3" x2="-4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="0" x2="-4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="-3.3" x2="4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="-3.3" x2="4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.08" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.08" y2="0.3048" layer="21"/>
</package>
<package name="VMTA55" urn="urn:adsk.eagle:footprint:25689/1" library_version="11">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-5.08" y1="0" x2="-4.26" y2="0" width="0.6096" layer="51"/>
<wire x1="3.3375" y1="-1.45" x2="3.3375" y2="1.45" width="0.1524" layer="21"/>
<wire x1="3.3375" y1="1.45" x2="-3.3625" y2="1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="1.45" x2="-3.3625" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="-1.45" x2="3.3375" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="4.235" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.1" shape="octagon"/>
<text x="-3.175" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.26" y1="-0.3048" x2="-3.3075" y2="0.3048" layer="21"/>
<rectangle x1="3.2825" y1="-0.3048" x2="4.235" y2="0.3048" layer="21"/>
</package>
<package name="VMTB60" urn="urn:adsk.eagle:footprint:25690/1" library_version="11">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC60&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.585" y2="0" width="0.6096" layer="51"/>
<wire x1="4.6875" y1="-1.95" x2="4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="4.6875" y1="1.95" x2="-4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="1.95" x2="-4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="-1.95" x2="4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="5.585" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-4.445" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.445" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.585" y1="-0.3048" x2="-4.6325" y2="0.3048" layer="21"/>
<rectangle x1="4.6325" y1="-0.3048" x2="5.585" y2="0.3048" layer="21"/>
</package>
<package name="R4527" urn="urn:adsk.eagle:footprint:13246/1" library_version="11">
<description>&lt;b&gt;Package 4527&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com/docs/31059/wsrhigh.pdf</description>
<wire x1="-5.675" y1="-3.375" x2="5.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.65" y1="-3.375" x2="5.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.375" x2="-5.675" y2="3.375" width="0.2032" layer="21"/>
<wire x1="-5.675" y1="3.375" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<smd name="1" x="-4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.715" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.715" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0001" urn="urn:adsk.eagle:footprint:25692/1" library_version="11">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.075" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="1.606" x2="3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-2.544" y="2.229" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.501" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0002" urn="urn:adsk.eagle:footprint:25693/1" library_version="11">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.55" y1="3.375" x2="-5.55" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.55" y1="-3.375" x2="5.55" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.55" y1="-3.375" x2="5.55" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.55" y1="3.375" x2="-5.55" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.65" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.65" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC01/2" urn="urn:adsk.eagle:footprint:25694/1" library_version="11">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="-1.475" width="0.2032" layer="51"/>
<wire x1="-2.45" y1="-1.475" x2="2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="1.475" width="0.2032" layer="51"/>
<wire x1="2.45" y1="1.475" x2="-2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="1.106" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="-1.106" x2="-2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="1.106" x2="2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="-1.106" width="0.2032" layer="21"/>
<smd name="1" x="-2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<smd name="2" x="2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<text x="-2.544" y="1.904" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.176" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC2515" urn="urn:adsk.eagle:footprint:25695/1" library_version="11">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.05" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.05" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="1.606" x2="3.05" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-3.2" y="2.15" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.2" y="-3.4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC4527" urn="urn:adsk.eagle:footprint:25696/1" library_version="11">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.675" y1="3.4" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.675" y1="-3.375" x2="5.675" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.675" y1="-3.375" x2="5.675" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.675" y1="3.4" x2="-5.675" y2="3.4" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.775" y="3.925" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.775" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC6927" urn="urn:adsk.eagle:footprint:25697/1" library_version="11">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-8.65" y1="3.375" x2="-8.65" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-8.65" y1="-3.375" x2="8.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="8.65" y1="-3.375" x2="8.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="8.65" y1="3.375" x2="-8.65" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-7.95" y="0.025" dx="3.94" dy="5.97" layer="1"/>
<smd name="2" x="7.95" y="0" dx="3.94" dy="5.97" layer="1"/>
<text x="-8.75" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.75" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R1218" urn="urn:adsk.eagle:footprint:25698/1" library_version="11">
<description>&lt;b&gt;CRCW1218 Thick Film, Rectangular Chip Resistors&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com .. dcrcw.pdf</description>
<wire x1="-0.913" y1="-2.219" x2="0.939" y2="-2.219" width="0.1524" layer="51"/>
<wire x1="0.913" y1="2.219" x2="-0.939" y2="2.219" width="0.1524" layer="51"/>
<smd name="1" x="-1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<smd name="2" x="1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-2.3" x2="-0.9009" y2="2.3" layer="51"/>
<rectangle x1="0.9144" y1="-2.3" x2="1.6645" y2="2.3" layer="51"/>
</package>
<package name="1812X7R" urn="urn:adsk.eagle:footprint:25699/1" library_version="11">
<description>&lt;b&gt;Chip Monolithic Ceramic Capacitors&lt;/b&gt; Medium Voltage High Capacitance for General Use&lt;p&gt;
Source: http://www.murata.com .. GRM43DR72E224KW01.pdf</description>
<wire x1="-1.1" y1="1.5" x2="1.1" y2="1.5" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.5" x2="-1.1" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="-0.6" y1="1.5" x2="0.6" y2="1.5" width="0.2032" layer="21"/>
<wire x1="0.6" y1="-1.5" x2="-0.6" y2="-1.5" width="0.2032" layer="21"/>
<smd name="1" x="-1.425" y="0" dx="0.8" dy="3.5" layer="1"/>
<smd name="2" x="1.425" y="0" dx="0.8" dy="3.5" layer="1" rot="R180"/>
<text x="-1.9456" y="1.9958" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.9456" y="-3.7738" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.4" y1="-1.6" x2="-1.1" y2="1.6" layer="51"/>
<rectangle x1="1.1" y1="-1.6" x2="1.4" y2="1.6" layer="51" rot="R180"/>
</package>
<package name="PRL1632" urn="urn:adsk.eagle:footprint:25700/1" library_version="11">
<description>&lt;b&gt;PRL1632 are realized as 1W for 3.2 × 1.6mm(1206)&lt;/b&gt;&lt;p&gt;
Source: http://www.mouser.com/ds/2/392/products_18-2245.pdf</description>
<wire x1="0.7275" y1="-1.5228" x2="-0.7277" y2="-1.5228" width="0.1524" layer="51"/>
<wire x1="0.7275" y1="1.5228" x2="-0.7152" y2="1.5228" width="0.1524" layer="51"/>
<smd name="2" x="0.822" y="0" dx="1" dy="3.2" layer="1"/>
<smd name="1" x="-0.822" y="0" dx="1" dy="3.2" layer="1"/>
<text x="-1.4" y="1.8" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.4" y="-3" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-1.6" x2="-0.4" y2="1.6" layer="51"/>
<rectangle x1="0.4" y1="-1.6" x2="0.8" y2="1.6" layer="51"/>
</package>
<package name="R01005" urn="urn:adsk.eagle:footprint:25701/1" library_version="11">
<smd name="1" x="-0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<smd name="2" x="0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<text x="-0.4" y="0.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.4" y="-1.6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2" y1="-0.1" x2="-0.075" y2="0.1" layer="51"/>
<rectangle x1="0.075" y1="-0.1" x2="0.2" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="0.05" x2="0.15" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="-0.1" x2="0.15" y2="-0.05" layer="51"/>
</package>
<package name="C0402" urn="urn:adsk.eagle:footprint:23121/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0504" urn="urn:adsk.eagle:footprint:23122/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C0603" urn="urn:adsk.eagle:footprint:23123/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0805" urn="urn:adsk.eagle:footprint:23124/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C1206" urn="urn:adsk.eagle:footprint:23125/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1210" urn="urn:adsk.eagle:footprint:23126/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1310" urn="urn:adsk.eagle:footprint:23127/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.3" x2="0.1001" y2="0.3" layer="35"/>
</package>
<package name="C1608" urn="urn:adsk.eagle:footprint:23128/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1812" urn="urn:adsk.eagle:footprint:23129/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.3" y1="-0.4001" x2="0.3" y2="0.4001" layer="35"/>
</package>
<package name="C1825" urn="urn:adsk.eagle:footprint:23130/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C2012" urn="urn:adsk.eagle:footprint:23131/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C3216" urn="urn:adsk.eagle:footprint:23132/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.3" y1="-0.5001" x2="0.3" y2="0.5001" layer="35"/>
</package>
<package name="C3225" urn="urn:adsk.eagle:footprint:23133/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="C4532" urn="urn:adsk.eagle:footprint:23134/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="C4564" urn="urn:adsk.eagle:footprint:23135/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="C025-024X044" urn="urn:adsk.eagle:footprint:23136/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.778" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.778" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-025X050" urn="urn:adsk.eagle:footprint:23137/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.5 x 5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-030X050" urn="urn:adsk.eagle:footprint:23138/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 3 x 5 mm</description>
<wire x1="-2.159" y1="1.524" x2="2.159" y2="1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.524" x2="-2.159" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.27" x2="2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.27" x2="-2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.524" x2="2.413" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.27" x2="-2.159" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.524" x2="2.413" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.27" x2="-2.159" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-040X050" urn="urn:adsk.eagle:footprint:23139/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 4 x 5 mm</description>
<wire x1="-2.159" y1="1.905" x2="2.159" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.905" x2="-2.159" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.651" x2="2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.651" x2="-2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.905" x2="2.413" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.651" x2="-2.159" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.905" x2="2.413" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.651" x2="-2.159" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-050X050" urn="urn:adsk.eagle:footprint:23140/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 5 x 5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-060X050" urn="urn:adsk.eagle:footprint:23141/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 6 x 5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-024X070" urn="urn:adsk.eagle:footprint:23142/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm + 5 mm, outline 2.4 x 7 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.191" y1="-1.143" x2="-3.9624" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-4.191" y1="1.143" x2="-3.9624" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-0.635" x2="-4.191" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="0.635" x2="-4.191" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.699" y1="-0.635" x2="-4.699" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.143" x2="-2.5654" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.143" x2="-2.5654" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.81" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-025X075" urn="urn:adsk.eagle:footprint:23143/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.016" x2="4.953" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.27" x2="4.953" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.27" x2="4.953" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.27" x2="4.699" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.27" x2="2.794" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="0.762" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-0.762" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.254" x2="2.413" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-035X075" urn="urn:adsk.eagle:footprint:23144/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.778" x2="2.159" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.778" x2="-2.159" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.524" x2="-2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.778" x2="2.413" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.524" x2="-2.159" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.778" x2="2.413" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.524" x2="-2.159" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="2.794" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.524" x2="2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.508" x2="2.413" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.302" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-045X075" urn="urn:adsk.eagle:footprint:23145/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.032" x2="4.953" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.286" x2="4.953" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.286" x2="4.953" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.286" x2="4.699" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.286" x2="2.794" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.397" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-055X075" urn="urn:adsk.eagle:footprint:23146/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.794" x2="4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.794" x2="4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.794" x2="4.699" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.794" x2="2.794" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-2.032" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-024X044" urn="urn:adsk.eagle:footprint:23147/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.159" y1="-0.381" x2="2.54" y2="0.381" layer="51"/>
<rectangle x1="-2.54" y1="-0.381" x2="-2.159" y2="0.381" layer="51"/>
</package>
<package name="C050-025X075" urn="urn:adsk.eagle:footprint:23148/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.016" x2="-3.683" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.27" x2="3.429" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.016" x2="3.683" y2="1.016" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="-3.429" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="3.683" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.27" x2="3.683" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.016" x2="-3.429" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.016" x2="-3.429" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-045X075" urn="urn:adsk.eagle:footprint:23149/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.032" x2="-3.683" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.286" x2="3.429" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.032" x2="3.683" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="-3.429" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="3.683" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.286" x2="3.683" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.032" x2="-3.429" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.032" x2="-3.429" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-030X075" urn="urn:adsk.eagle:footprint:23150/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.27" x2="-3.683" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.27" x2="3.683" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="3.683" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.524" x2="3.683" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.27" x2="-3.429" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.27" x2="-3.429" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-050X075" urn="urn:adsk.eagle:footprint:23151/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.286" x2="-3.683" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.54" x2="3.429" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.286" x2="3.683" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="-3.429" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="3.683" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.54" x2="3.683" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.286" x2="-3.429" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.286" x2="-3.429" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-055X075" urn="urn:adsk.eagle:footprint:23152/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.54" x2="-3.683" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.794" x2="3.429" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.54" x2="3.683" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="-3.429" y2="2.794" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="3.683" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.794" x2="3.683" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.54" x2="-3.429" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.54" x2="-3.429" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.302" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-075X075" urn="urn:adsk.eagle:footprint:23153/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-1.524" y1="0" x2="-0.4572" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="0.762" width="0.4064" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0.762" x2="0.4318" y2="0" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.4318" y1="0" x2="0.4318" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="3.429" x2="-3.683" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-3.683" x2="3.429" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-3.429" x2="3.683" y2="3.429" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="-3.429" y2="3.683" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="3.683" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-3.683" x2="3.683" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-3.429" x2="-3.429" y2="-3.683" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="3.429" x2="-3.429" y2="3.683" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="4.064" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050H075X075" urn="urn:adsk.eagle:footprint:23154/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Horizontal, grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-3.683" y1="7.112" x2="-3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="0.508" x2="-3.302" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="0.508" x2="-1.778" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="0.508" x2="1.778" y2="0.508" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.508" x2="3.302" y2="0.508" width="0.1524" layer="51"/>
<wire x1="3.302" y1="0.508" x2="3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="3.683" y1="0.508" x2="3.683" y2="7.112" width="0.1524" layer="21"/>
<wire x1="3.175" y1="7.62" x2="-3.175" y2="7.62" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="2.413" x2="-0.3048" y2="1.778" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-0.3048" y2="1.143" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="2.413" x2="0.3302" y2="1.778" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="0.3302" y2="1.143" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="7.112" x2="-3.175" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.175" y1="7.62" x2="3.683" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.254" width="0.508" layer="51"/>
<wire x1="2.54" y1="0" x2="2.54" y2="0.254" width="0.508" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.302" y="8.001" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="3.175" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.794" y1="0.127" x2="-2.286" y2="0.508" layer="51"/>
<rectangle x1="2.286" y1="0.127" x2="2.794" y2="0.508" layer="51"/>
</package>
<package name="C075-032X103" urn="urn:adsk.eagle:footprint:23155/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 3.2 x 10.3 mm</description>
<wire x1="4.826" y1="1.524" x2="-4.826" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-1.524" x2="4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.826" y1="1.524" x2="5.08" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-1.524" x2="5.08" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.27" x2="-4.826" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.27" x2="-4.826" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.508" y1="0" x2="2.54" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0" x2="-0.508" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.889" x2="-0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-0.889" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0.889" x2="0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.889" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.826" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-042X103" urn="urn:adsk.eagle:footprint:23156/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 4.2 x 10.3 mm</description>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.778" x2="-5.08" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.778" x2="5.08" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="5.08" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-2.032" x2="5.08" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.778" x2="-4.826" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.778" x2="-4.826" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.699" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-052X106" urn="urn:adsk.eagle:footprint:23157/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 5.2 x 10.6 mm</description>
<wire x1="4.953" y1="2.54" x2="-4.953" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.286" x2="-5.207" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.286" x2="5.207" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.54" x2="5.207" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-2.54" x2="5.207" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.286" x2="-4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.286" x2="-4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-043X133" urn="urn:adsk.eagle:footprint:23158/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 4.3 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.032" x2="6.096" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.604" y1="1.524" x2="6.604" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.032" x2="-6.096" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-1.524" x2="-6.604" y2="1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.032" x2="6.604" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.032" x2="6.604" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-1.524" x2="-6.096" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="1.524" x2="-6.096" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-054X133" urn="urn:adsk.eagle:footprint:23159/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 5.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.54" x2="6.096" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.032" x2="6.604" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.54" x2="-6.096" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.032" x2="-6.604" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.54" x2="6.604" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.54" x2="6.604" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.032" x2="-6.096" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.032" x2="-6.096" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.905" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-064X133" urn="urn:adsk.eagle:footprint:23160/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 6.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.096" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.54" x2="6.604" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="6.604" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-3.048" x2="6.604" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102_152-062X184" urn="urn:adsk.eagle:footprint:23161/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm + 15.2 mm, outline 6.2 x 18.4 mm</description>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="3.683" y2="0" width="0.1524" layer="21"/>
<wire x1="6.477" y1="0" x2="8.636" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.223" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.223" y1="3.048" x2="6.731" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.223" y1="-3.048" x2="6.731" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="2.54" x2="6.731" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="11.176" y1="3.048" x2="11.684" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="11.176" y1="-3.048" x2="11.684" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="11.176" y1="-3.048" x2="7.112" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="7.112" y1="3.048" x2="11.176" y2="3.048" width="0.1524" layer="21"/>
<wire x1="11.684" y1="2.54" x2="11.684" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="3" x="10.033" y="0" drill="1.016" shape="octagon"/>
<text x="-5.969" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-054X183" urn="urn:adsk.eagle:footprint:23162/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 5.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.032" x2="9.017" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-2.54" x2="-8.509" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.032" x2="-9.017" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="2.54" x2="8.509" y2="2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="2.54" x2="9.017" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-2.54" x2="9.017" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.032" x2="-8.509" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.032" x2="-8.509" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-064X183" urn="urn:adsk.eagle:footprint:23163/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 6.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.54" x2="9.017" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.048" x2="-8.509" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.54" x2="-9.017" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.048" x2="8.509" y2="3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.048" x2="9.017" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.048" x2="9.017" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.54" x2="-8.509" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.54" x2="-8.509" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-072X183" urn="urn:adsk.eagle:footprint:23164/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 7.2 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.048" x2="9.017" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.556" x2="-8.509" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.048" x2="-9.017" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.556" x2="8.509" y2="3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.556" x2="9.017" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.556" x2="9.017" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.048" x2="-8.509" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.048" x2="-8.509" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.937" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-084X183" urn="urn:adsk.eagle:footprint:23165/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 8.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.556" x2="9.017" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.064" x2="-8.509" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.556" x2="-9.017" y2="3.556" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.064" x2="8.509" y2="4.064" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.064" x2="9.017" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.064" x2="9.017" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.556" x2="-8.509" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.556" x2="-8.509" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.445" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-091X182" urn="urn:adsk.eagle:footprint:23166/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 9.1 x 18.2 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.937" x2="9.017" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.445" x2="-8.509" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.937" x2="-9.017" y2="3.937" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.445" x2="8.509" y2="4.445" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.445" x2="9.017" y2="3.937" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.445" x2="9.017" y2="-3.937" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.937" x2="-8.509" y2="-4.445" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.937" x2="-8.509" y2="4.445" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.826" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-062X268" urn="urn:adsk.eagle:footprint:23167/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 6.2 x 26.8 mm</description>
<wire x1="-12.827" y1="3.048" x2="12.827" y2="3.048" width="0.1524" layer="21"/>
<wire x1="13.335" y1="2.54" x2="13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.048" x2="-12.827" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-2.54" x2="-13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.048" x2="13.335" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.048" x2="13.335" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-2.54" x2="-12.827" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="2.54" x2="-12.827" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.7" y="3.429" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-074X268" urn="urn:adsk.eagle:footprint:23168/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 7.4 x 26.8 mm</description>
<wire x1="-12.827" y1="3.556" x2="12.827" y2="3.556" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.048" x2="13.335" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.556" x2="-12.827" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.048" x2="-13.335" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.556" x2="13.335" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.556" x2="13.335" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.048" x2="-12.827" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.048" x2="-12.827" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="3.937" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-087X268" urn="urn:adsk.eagle:footprint:23169/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 8.7 x 26.8 mm</description>
<wire x1="-12.827" y1="4.318" x2="12.827" y2="4.318" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.81" x2="13.335" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-4.318" x2="-12.827" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.81" x2="-13.335" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="4.318" x2="13.335" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-4.318" x2="13.335" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.81" x2="-12.827" y2="-4.318" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.81" x2="-12.827" y2="4.318" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="4.699" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-108X268" urn="urn:adsk.eagle:footprint:23170/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 10.8 x 26.8 mm</description>
<wire x1="-12.827" y1="5.334" x2="12.827" y2="5.334" width="0.1524" layer="21"/>
<wire x1="13.335" y1="4.826" x2="13.335" y2="-4.826" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.334" x2="-12.827" y2="-5.334" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-4.826" x2="-13.335" y2="4.826" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.334" x2="13.335" y2="4.826" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.334" x2="13.335" y2="-4.826" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-4.826" x2="-12.827" y2="-5.334" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="4.826" x2="-12.827" y2="5.334" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.715" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-113X268" urn="urn:adsk.eagle:footprint:23171/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 11.3 x 26.8 mm</description>
<wire x1="-12.827" y1="5.588" x2="12.827" y2="5.588" width="0.1524" layer="21"/>
<wire x1="13.335" y1="5.08" x2="13.335" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.588" x2="-12.827" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-5.08" x2="-13.335" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.588" x2="13.335" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.588" x2="13.335" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-5.08" x2="-12.827" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="5.08" x2="-12.827" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-093X316" urn="urn:adsk.eagle:footprint:23172/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 9.3 x 31.6 mm</description>
<wire x1="-15.24" y1="4.572" x2="15.24" y2="4.572" width="0.1524" layer="21"/>
<wire x1="15.748" y1="4.064" x2="15.748" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-4.572" x2="-15.24" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-4.064" x2="-15.748" y2="4.064" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="4.572" x2="15.748" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-4.572" x2="15.748" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-4.064" x2="-15.24" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="4.064" x2="-15.24" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="4.953" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-113X316" urn="urn:adsk.eagle:footprint:23173/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 11.3 x 31.6 mm</description>
<wire x1="-15.24" y1="5.588" x2="15.24" y2="5.588" width="0.1524" layer="21"/>
<wire x1="15.748" y1="5.08" x2="15.748" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-5.588" x2="-15.24" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-5.08" x2="-15.748" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="5.588" x2="15.748" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-5.588" x2="15.748" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-5.08" x2="-15.24" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="5.08" x2="-15.24" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-134X316" urn="urn:adsk.eagle:footprint:23174/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 13.4 x 31.6 mm</description>
<wire x1="-15.24" y1="6.604" x2="15.24" y2="6.604" width="0.1524" layer="21"/>
<wire x1="15.748" y1="6.096" x2="15.748" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-6.604" x2="-15.24" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-6.096" x2="-15.748" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="6.604" x2="15.748" y2="6.096" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-6.604" x2="15.748" y2="-6.096" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-6.096" x2="-15.24" y2="-6.604" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="6.096" x2="-15.24" y2="6.604" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="6.985" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-205X316" urn="urn:adsk.eagle:footprint:23175/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 20.5 x 31.6 mm</description>
<wire x1="-15.24" y1="10.16" x2="15.24" y2="10.16" width="0.1524" layer="21"/>
<wire x1="15.748" y1="9.652" x2="15.748" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-10.16" x2="-15.24" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-9.652" x2="-15.748" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="10.16" x2="15.748" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-10.16" x2="15.748" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-9.652" x2="-15.24" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="9.652" x2="-15.24" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.318" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-137X374" urn="urn:adsk.eagle:footprint:23176/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 13.7 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="6.731" x2="-18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="18.542" y1="6.731" x2="-18.542" y2="6.731" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.2372" y="7.0612" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-162X374" urn="urn:adsk.eagle:footprint:23177/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 16.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="8.001" x2="-18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="18.542" y1="8.001" x2="-18.542" y2="8.001" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="8.3312" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-182X374" urn="urn:adsk.eagle:footprint:23178/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 18.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="9.017" x2="-18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="18.542" y1="9.017" x2="-18.542" y2="9.017" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="9.3472" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-192X418" urn="urn:adsk.eagle:footprint:23179/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 19.2 x 41.8 mm</description>
<wire x1="-20.32" y1="8.509" x2="20.32" y2="8.509" width="0.1524" layer="21"/>
<wire x1="20.828" y1="8.001" x2="20.828" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-8.509" x2="-20.32" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-8.001" x2="-20.828" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="8.509" x2="20.828" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-8.509" x2="20.828" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-8.001" x2="-20.32" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="8.001" x2="-20.32" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-203X418" urn="urn:adsk.eagle:footprint:23180/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 20.3 x 41.8 mm</description>
<wire x1="-20.32" y1="10.16" x2="20.32" y2="10.16" width="0.1524" layer="21"/>
<wire x1="20.828" y1="9.652" x2="20.828" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-10.16" x2="-20.32" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-9.652" x2="-20.828" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="10.16" x2="20.828" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-10.16" x2="20.828" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-9.652" x2="-20.32" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="9.652" x2="-20.32" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.32" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-035X075" urn="urn:adsk.eagle:footprint:23181/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.524" x2="-3.683" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.778" x2="3.429" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.524" x2="3.683" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="-3.429" y2="1.778" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="3.683" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.778" x2="3.683" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.524" x2="-3.429" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.524" x2="-3.429" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-155X418" urn="urn:adsk.eagle:footprint:23182/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 15.5 x 41.8 mm</description>
<wire x1="-20.32" y1="7.62" x2="20.32" y2="7.62" width="0.1524" layer="21"/>
<wire x1="20.828" y1="7.112" x2="20.828" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-7.62" x2="-20.32" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-7.112" x2="-20.828" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="7.62" x2="20.828" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-7.62" x2="20.828" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-7.112" x2="-20.32" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="7.112" x2="-20.32" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-063X106" urn="urn:adsk.eagle:footprint:23183/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 6.3 x 10.6 mm</description>
<wire x1="4.953" y1="3.048" x2="-4.953" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.794" x2="-5.207" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-3.048" x2="4.953" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.794" x2="5.207" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="3.048" x2="5.207" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-3.048" x2="5.207" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.794" x2="-4.953" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.794" x2="-4.953" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-154X316" urn="urn:adsk.eagle:footprint:23184/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 15.4 x 31.6 mm</description>
<wire x1="-15.24" y1="7.62" x2="15.24" y2="7.62" width="0.1524" layer="21"/>
<wire x1="15.748" y1="7.112" x2="15.748" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-7.62" x2="-15.24" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-7.112" x2="-15.748" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="7.62" x2="15.748" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-7.62" x2="15.748" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-7.112" x2="-15.24" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="7.112" x2="-15.24" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-173X316" urn="urn:adsk.eagle:footprint:23185/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 17.3 x 31.6 mm</description>
<wire x1="-15.24" y1="8.509" x2="15.24" y2="8.509" width="0.1524" layer="21"/>
<wire x1="15.748" y1="8.001" x2="15.748" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-8.509" x2="-15.24" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-8.001" x2="-15.748" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="8.509" x2="15.748" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-8.509" x2="15.748" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-8.001" x2="-15.24" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="8.001" x2="-15.24" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C0402K" urn="urn:adsk.eagle:footprint:23186/1" library_version="11">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0204 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1005</description>
<wire x1="-0.425" y1="0.2" x2="0.425" y2="0.2" width="0.1016" layer="51"/>
<wire x1="0.425" y1="-0.2" x2="-0.425" y2="-0.2" width="0.1016" layer="51"/>
<smd name="1" x="-0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<smd name="2" x="0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<text x="-0.5" y="0.425" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.5" y="-1.45" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.5" y1="-0.25" x2="-0.225" y2="0.25" layer="51"/>
<rectangle x1="0.225" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
</package>
<package name="C0603K" urn="urn:adsk.eagle:footprint:23187/1" library_version="11">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0603 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1608</description>
<wire x1="-0.725" y1="0.35" x2="0.725" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.725" y1="-0.35" x2="-0.725" y2="-0.35" width="0.1016" layer="51"/>
<smd name="1" x="-0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<smd name="2" x="0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<text x="-0.8" y="0.65" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.8" y="-1.65" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-0.4" x2="-0.45" y2="0.4" layer="51"/>
<rectangle x1="0.45" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
</package>
<package name="C0805K" urn="urn:adsk.eagle:footprint:23188/1" library_version="11">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0805 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 2012</description>
<wire x1="-0.925" y1="0.6" x2="0.925" y2="0.6" width="0.1016" layer="51"/>
<wire x1="0.925" y1="-0.6" x2="-0.925" y2="-0.6" width="0.1016" layer="51"/>
<smd name="1" x="-1" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="1" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1" y1="-0.65" x2="-0.5" y2="0.65" layer="51"/>
<rectangle x1="0.5" y1="-0.65" x2="1" y2="0.65" layer="51"/>
</package>
<package name="C1206K" urn="urn:adsk.eagle:footprint:23189/1" library_version="11">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1206 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3216</description>
<wire x1="-1.525" y1="0.75" x2="1.525" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-0.75" x2="-1.525" y2="-0.75" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2" layer="1"/>
<text x="-1.6" y="1.1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.1" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-0.8" x2="-1.1" y2="0.8" layer="51"/>
<rectangle x1="1.1" y1="-0.8" x2="1.6" y2="0.8" layer="51"/>
</package>
<package name="C1210K" urn="urn:adsk.eagle:footprint:23190/1" library_version="11">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1210 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3225</description>
<wire x1="-1.525" y1="1.175" x2="1.525" y2="1.175" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-1.175" x2="-1.525" y2="-1.175" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<text x="-1.6" y="1.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-1.25" x2="-1.1" y2="1.25" layer="51"/>
<rectangle x1="1.1" y1="-1.25" x2="1.6" y2="1.25" layer="51"/>
</package>
<package name="C1812K" urn="urn:adsk.eagle:footprint:23191/1" library_version="11">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1812 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4532</description>
<wire x1="-2.175" y1="1.525" x2="2.175" y2="1.525" width="0.1016" layer="51"/>
<wire x1="2.175" y1="-1.525" x2="-2.175" y2="-1.525" width="0.1016" layer="51"/>
<smd name="1" x="-2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="2" x="2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.25" y1="-1.6" x2="-1.65" y2="1.6" layer="51"/>
<rectangle x1="1.65" y1="-1.6" x2="2.25" y2="1.6" layer="51"/>
</package>
<package name="C1825K" urn="urn:adsk.eagle:footprint:23192/1" library_version="11">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1825 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4564</description>
<wire x1="-1.525" y1="3.125" x2="1.525" y2="3.125" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-3.125" x2="-1.525" y2="-3.125" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<text x="-1.6" y="3.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-4.625" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-3.2" x2="-1.1" y2="3.2" layer="51"/>
<rectangle x1="1.1" y1="-3.2" x2="1.6" y2="3.2" layer="51"/>
</package>
<package name="C2220K" urn="urn:adsk.eagle:footprint:23193/1" library_version="11">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2220 reflow solder&lt;/b&gt;&lt;p&gt;Metric Code Size 5650</description>
<wire x1="-2.725" y1="2.425" x2="2.725" y2="2.425" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-2.425" x2="-2.725" y2="-2.425" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<text x="-2.8" y="2.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-3.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-2.5" x2="-2.2" y2="2.5" layer="51"/>
<rectangle x1="2.2" y1="-2.5" x2="2.8" y2="2.5" layer="51"/>
</package>
<package name="C2225K" urn="urn:adsk.eagle:footprint:23194/1" library_version="11">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2225 reflow solder&lt;/b&gt;&lt;p&gt;Metric Code Size 5664</description>
<wire x1="-2.725" y1="3.075" x2="2.725" y2="3.075" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-3.075" x2="-2.725" y2="-3.075" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<text x="-2.8" y="3.6" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-4.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-3.15" x2="-2.2" y2="3.15" layer="51"/>
<rectangle x1="2.2" y1="-3.15" x2="2.8" y2="3.15" layer="51"/>
</package>
<package name="HPC0201" urn="urn:adsk.eagle:footprint:25783/1" library_version="11">
<description>&lt;b&gt; &lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com/docs/10129/hpc0201a.pdf</description>
<smd name="1" x="-0.18" y="0" dx="0.2" dy="0.35" layer="1"/>
<smd name="2" x="0.18" y="0" dx="0.2" dy="0.35" layer="1"/>
<text x="-0.75" y="0.74" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.785" y="-1.865" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.305" y1="-0.15" x2="0.305" y2="0.15" layer="51"/>
</package>
<package name="C0201" urn="urn:adsk.eagle:footprint:23196/1" library_version="11">
<description>Source: http://www.avxcorp.com/docs/catalogs/cx5r.pdf</description>
<smd name="1" x="-0.25" y="0" dx="0.25" dy="0.35" layer="1"/>
<smd name="2" x="0.25" y="0" dx="0.25" dy="0.35" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="0.1" x2="0.15" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="-0.1" layer="51"/>
</package>
<package name="C1808" urn="urn:adsk.eagle:footprint:23197/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Source: AVX .. aphvc.pdf</description>
<wire x1="-1.4732" y1="0.9502" x2="1.4732" y2="0.9502" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-0.9502" x2="1.4732" y2="-0.9502" width="0.1016" layer="51"/>
<smd name="1" x="-1.95" y="0" dx="1.6" dy="2.2" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.6" dy="2.2" layer="1"/>
<text x="-2.233" y="1.827" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.233" y="-2.842" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.275" y1="-1.015" x2="-1.225" y2="1.015" layer="51"/>
<rectangle x1="1.225" y1="-1.015" x2="2.275" y2="1.015" layer="51"/>
</package>
<package name="C3640" urn="urn:adsk.eagle:footprint:23198/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Source: AVX .. aphvc.pdf</description>
<wire x1="-3.8322" y1="5.0496" x2="3.8322" y2="5.0496" width="0.1016" layer="51"/>
<wire x1="-3.8322" y1="-5.0496" x2="3.8322" y2="-5.0496" width="0.1016" layer="51"/>
<smd name="1" x="-4.267" y="0" dx="2.6" dy="10.7" layer="1"/>
<smd name="2" x="4.267" y="0" dx="2.6" dy="10.7" layer="1"/>
<text x="-4.647" y="6.465" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.647" y="-7.255" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.57" y1="-5.1" x2="-3.05" y2="5.1" layer="51"/>
<rectangle x1="3.05" y1="-5.1" x2="4.5688" y2="5.1" layer="51"/>
</package>
<package name="C01005" urn="urn:adsk.eagle:footprint:23199/1" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2" y1="-0.1" x2="-0.075" y2="0.1" layer="51"/>
<rectangle x1="0.075" y1="-0.1" x2="0.2" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="0.05" x2="0.15" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="-0.1" x2="0.15" y2="-0.05" layer="51"/>
<smd name="1" x="-0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<smd name="2" x="0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<text x="-0.4" y="0.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.4" y="-1.6" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="R0402" urn="urn:adsk.eagle:package:23547/3" type="model" library_version="11">
<description>Chip RESISTOR 0402 EIA (1005 Metric)</description>
<packageinstances>
<packageinstance name="R0402"/>
</packageinstances>
</package3d>
<package3d name="R0603" urn="urn:adsk.eagle:package:23555/3" type="model" library_version="11">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R0603"/>
</packageinstances>
</package3d>
<package3d name="R0805" urn="urn:adsk.eagle:package:23553/2" type="model" library_version="11">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R0805"/>
</packageinstances>
</package3d>
<package3d name="R0805W" urn="urn:adsk.eagle:package:23537/2" type="model" library_version="11">
<description>RESISTOR wave soldering</description>
<packageinstances>
<packageinstance name="R0805W"/>
</packageinstances>
</package3d>
<package3d name="R1206" urn="urn:adsk.eagle:package:23540/2" type="model" library_version="11">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R1206"/>
</packageinstances>
</package3d>
<package3d name="R1206W" urn="urn:adsk.eagle:package:23539/2" type="model" library_version="11">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R1206W"/>
</packageinstances>
</package3d>
<package3d name="R1210" urn="urn:adsk.eagle:package:23554/2" type="model" library_version="11">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R1210"/>
</packageinstances>
</package3d>
<package3d name="R1210W" urn="urn:adsk.eagle:package:23541/2" type="model" library_version="11">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R1210W"/>
</packageinstances>
</package3d>
<package3d name="R2010" urn="urn:adsk.eagle:package:23551/2" type="model" library_version="11">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R2010"/>
</packageinstances>
</package3d>
<package3d name="R2010W" urn="urn:adsk.eagle:package:23542/2" type="model" library_version="11">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R2010W"/>
</packageinstances>
</package3d>
<package3d name="R2012" urn="urn:adsk.eagle:package:23543/2" type="model" library_version="11">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R2012"/>
</packageinstances>
</package3d>
<package3d name="R2012W" urn="urn:adsk.eagle:package:23544/2" type="model" library_version="11">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R2012W"/>
</packageinstances>
</package3d>
<package3d name="R2512" urn="urn:adsk.eagle:package:23545/2" type="model" library_version="11">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R2512"/>
</packageinstances>
</package3d>
<package3d name="R2512W" urn="urn:adsk.eagle:package:23565/2" type="model" library_version="11">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R2512W"/>
</packageinstances>
</package3d>
<package3d name="R3216" urn="urn:adsk.eagle:package:23557/2" type="model" library_version="11">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R3216"/>
</packageinstances>
</package3d>
<package3d name="R3216W" urn="urn:adsk.eagle:package:23548/2" type="model" library_version="11">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R3216W"/>
</packageinstances>
</package3d>
<package3d name="R3225" urn="urn:adsk.eagle:package:23549/2" type="model" library_version="11">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R3225"/>
</packageinstances>
</package3d>
<package3d name="R3225W" urn="urn:adsk.eagle:package:23550/2" type="model" library_version="11">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R3225W"/>
</packageinstances>
</package3d>
<package3d name="R5025" urn="urn:adsk.eagle:package:23552/2" type="model" library_version="11">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R5025"/>
</packageinstances>
</package3d>
<package3d name="R5025W" urn="urn:adsk.eagle:package:23558/2" type="model" library_version="11">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R5025W"/>
</packageinstances>
</package3d>
<package3d name="R6332" urn="urn:adsk.eagle:package:23559/2" type="model" library_version="11">
<description>RESISTOR
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<packageinstances>
<packageinstance name="R6332"/>
</packageinstances>
</package3d>
<package3d name="R6332W" urn="urn:adsk.eagle:package:26078/2" type="model" library_version="11">
<description>RESISTOR wave soldering
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<packageinstances>
<packageinstance name="R6332W"/>
</packageinstances>
</package3d>
<package3d name="M0805" urn="urn:adsk.eagle:package:23556/2" type="model" library_version="11">
<description>RESISTOR
MELF 0.10 W</description>
<packageinstances>
<packageinstance name="M0805"/>
</packageinstances>
</package3d>
<package3d name="M1206" urn="urn:adsk.eagle:package:23566/2" type="model" library_version="11">
<description>RESISTOR
MELF 0.25 W</description>
<packageinstances>
<packageinstance name="M1206"/>
</packageinstances>
</package3d>
<package3d name="M1406" urn="urn:adsk.eagle:package:23569/2" type="model" library_version="11">
<description>RESISTOR
MELF 0.12 W</description>
<packageinstances>
<packageinstance name="M1406"/>
</packageinstances>
</package3d>
<package3d name="M2012" urn="urn:adsk.eagle:package:23561/2" type="model" library_version="11">
<description>RESISTOR
MELF 0.10 W</description>
<packageinstances>
<packageinstance name="M2012"/>
</packageinstances>
</package3d>
<package3d name="M2309" urn="urn:adsk.eagle:package:23562/2" type="model" library_version="11">
<description>RESISTOR
MELF 0.25 W</description>
<packageinstances>
<packageinstance name="M2309"/>
</packageinstances>
</package3d>
<package3d name="M3216" urn="urn:adsk.eagle:package:23563/2" type="model" library_version="11">
<description>RESISTOR
MELF 0.25 W</description>
<packageinstances>
<packageinstance name="M3216"/>
</packageinstances>
</package3d>
<package3d name="M3516" urn="urn:adsk.eagle:package:23573/2" type="model" library_version="11">
<description>RESISTOR
MELF 0.12 W</description>
<packageinstances>
<packageinstance name="M3516"/>
</packageinstances>
</package3d>
<package3d name="M5923" urn="urn:adsk.eagle:package:23564/3" type="model" library_version="11">
<description>RESISTOR
MELF 0.25 W</description>
<packageinstances>
<packageinstance name="M5923"/>
</packageinstances>
</package3d>
<package3d name="0204/5" urn="urn:adsk.eagle:package:23488/1" type="box" library_version="11">
<description>RESISTOR
type 0204, grid 5 mm</description>
<packageinstances>
<packageinstance name="0204/5"/>
</packageinstances>
</package3d>
<package3d name="0204/7" urn="urn:adsk.eagle:package:23498/2" type="model" library_version="11">
<description>RESISTOR
type 0204, grid 7.5 mm</description>
<packageinstances>
<packageinstance name="0204/7"/>
</packageinstances>
</package3d>
<package3d name="0204V" urn="urn:adsk.eagle:package:23495/1" type="box" library_version="11">
<description>RESISTOR
type 0204, grid 2.5 mm</description>
<packageinstances>
<packageinstance name="0204V"/>
</packageinstances>
</package3d>
<package3d name="0207/10" urn="urn:adsk.eagle:package:23491/2" type="model" library_version="11">
<description>RESISTOR
type 0207, grid 10 mm</description>
<packageinstances>
<packageinstance name="0207/10"/>
</packageinstances>
</package3d>
<package3d name="0207/12" urn="urn:adsk.eagle:package:23489/1" type="box" library_version="11">
<description>RESISTOR
type 0207, grid 12 mm</description>
<packageinstances>
<packageinstance name="0207/12"/>
</packageinstances>
</package3d>
<package3d name="0207/15" urn="urn:adsk.eagle:package:23492/1" type="box" library_version="11">
<description>RESISTOR
type 0207, grid 15mm</description>
<packageinstances>
<packageinstance name="0207/15"/>
</packageinstances>
</package3d>
<package3d name="0207/2V" urn="urn:adsk.eagle:package:23490/1" type="box" library_version="11">
<description>RESISTOR
type 0207, grid 2.5 mm</description>
<packageinstances>
<packageinstance name="0207/2V"/>
</packageinstances>
</package3d>
<package3d name="0207/5V" urn="urn:adsk.eagle:package:23502/1" type="box" library_version="11">
<description>RESISTOR
type 0207, grid 5 mm</description>
<packageinstances>
<packageinstance name="0207/5V"/>
</packageinstances>
</package3d>
<package3d name="0207/7" urn="urn:adsk.eagle:package:23493/2" type="model" library_version="11">
<description>RESISTOR
type 0207, grid 7.5 mm</description>
<packageinstances>
<packageinstance name="0207/7"/>
</packageinstances>
</package3d>
<package3d name="0309/10" urn="urn:adsk.eagle:package:23567/2" type="model" library_version="11">
<description>RESISTOR
type 0309, grid 10mm</description>
<packageinstances>
<packageinstance name="0309/10"/>
</packageinstances>
</package3d>
<package3d name="0309/12" urn="urn:adsk.eagle:package:23571/1" type="box" library_version="11">
<description>RESISTOR
type 0309, grid 12.5 mm</description>
<packageinstances>
<packageinstance name="0309/12"/>
</packageinstances>
</package3d>
<package3d name="0309V" urn="urn:adsk.eagle:package:23572/1" type="box" library_version="11">
<description>RESISTOR
type 0309, grid 2.5 mm</description>
<packageinstances>
<packageinstance name="0309V"/>
</packageinstances>
</package3d>
<package3d name="0411/12" urn="urn:adsk.eagle:package:23578/1" type="box" library_version="11">
<description>RESISTOR
type 0411, grid 12.5 mm</description>
<packageinstances>
<packageinstance name="0411/12"/>
</packageinstances>
</package3d>
<package3d name="0411/15" urn="urn:adsk.eagle:package:23568/2" type="model" library_version="11">
<description>RESISTOR
type 0411, grid 15 mm</description>
<packageinstances>
<packageinstance name="0411/15"/>
</packageinstances>
</package3d>
<package3d name="0411V" urn="urn:adsk.eagle:package:23570/1" type="box" library_version="11">
<description>RESISTOR
type 0411, grid 3.81 mm</description>
<packageinstances>
<packageinstance name="0411V"/>
</packageinstances>
</package3d>
<package3d name="0414/15" urn="urn:adsk.eagle:package:23579/2" type="model" library_version="11">
<description>RESISTOR
type 0414, grid 15 mm</description>
<packageinstances>
<packageinstance name="0414/15"/>
</packageinstances>
</package3d>
<package3d name="0414V" urn="urn:adsk.eagle:package:23574/1" type="box" library_version="11">
<description>RESISTOR
type 0414, grid 5 mm</description>
<packageinstances>
<packageinstance name="0414V"/>
</packageinstances>
</package3d>
<package3d name="0617/17" urn="urn:adsk.eagle:package:23575/2" type="model" library_version="11">
<description>RESISTOR
type 0617, grid 17.5 mm</description>
<packageinstances>
<packageinstance name="0617/17"/>
</packageinstances>
</package3d>
<package3d name="0617/22" urn="urn:adsk.eagle:package:23577/1" type="box" library_version="11">
<description>RESISTOR
type 0617, grid 22.5 mm</description>
<packageinstances>
<packageinstance name="0617/22"/>
</packageinstances>
</package3d>
<package3d name="0617V" urn="urn:adsk.eagle:package:23576/1" type="box" library_version="11">
<description>RESISTOR
type 0617, grid 5 mm</description>
<packageinstances>
<packageinstance name="0617V"/>
</packageinstances>
</package3d>
<package3d name="0922/22" urn="urn:adsk.eagle:package:23580/2" type="model" library_version="11">
<description>RESISTOR
type 0922, grid 22.5 mm</description>
<packageinstances>
<packageinstance name="0922/22"/>
</packageinstances>
</package3d>
<package3d name="P0613V" urn="urn:adsk.eagle:package:23582/1" type="box" library_version="11">
<description>RESISTOR
type 0613, grid 5 mm</description>
<packageinstances>
<packageinstance name="P0613V"/>
</packageinstances>
</package3d>
<package3d name="P0613/15" urn="urn:adsk.eagle:package:23581/2" type="model" library_version="11">
<description>RESISTOR
type 0613, grid 15 mm</description>
<packageinstances>
<packageinstance name="P0613/15"/>
</packageinstances>
</package3d>
<package3d name="P0817/22" urn="urn:adsk.eagle:package:23583/1" type="box" library_version="11">
<description>RESISTOR
type 0817, grid 22.5 mm</description>
<packageinstances>
<packageinstance name="P0817/22"/>
</packageinstances>
</package3d>
<package3d name="P0817V" urn="urn:adsk.eagle:package:23608/1" type="box" library_version="11">
<description>RESISTOR
type 0817, grid 6.35 mm</description>
<packageinstances>
<packageinstance name="P0817V"/>
</packageinstances>
</package3d>
<package3d name="V234/12" urn="urn:adsk.eagle:package:23592/1" type="box" library_version="11">
<description>RESISTOR
type V234, grid 12.5 mm</description>
<packageinstances>
<packageinstance name="V234/12"/>
</packageinstances>
</package3d>
<package3d name="V235/17" urn="urn:adsk.eagle:package:23586/2" type="model" library_version="11">
<description>RESISTOR
type V235, grid 17.78 mm</description>
<packageinstances>
<packageinstance name="V235/17"/>
</packageinstances>
</package3d>
<package3d name="V526-0" urn="urn:adsk.eagle:package:23590/1" type="box" library_version="11">
<description>RESISTOR
type V526-0, grid 2.5 mm</description>
<packageinstances>
<packageinstance name="V526-0"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0102R" urn="urn:adsk.eagle:package:23591/2" type="model" library_version="11">
<description>CECC Size RC2211 Reflow Soldering
source Beyschlag</description>
<packageinstances>
<packageinstance name="MINI_MELF-0102R"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0102W" urn="urn:adsk.eagle:package:23588/2" type="model" library_version="11">
<description>CECC Size RC2211 Wave Soldering
source Beyschlag</description>
<packageinstances>
<packageinstance name="MINI_MELF-0102W"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0204R" urn="urn:adsk.eagle:package:26109/2" type="model" library_version="11">
<description>CECC Size RC3715 Reflow Soldering
source Beyschlag</description>
<packageinstances>
<packageinstance name="MINI_MELF-0204R"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0204W" urn="urn:adsk.eagle:package:26111/2" type="model" library_version="11">
<description>CECC Size RC3715 Wave Soldering
source Beyschlag</description>
<packageinstances>
<packageinstance name="MINI_MELF-0204W"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0207R" urn="urn:adsk.eagle:package:26113/2" type="model" library_version="11">
<description>CECC Size RC6123 Reflow Soldering
source Beyschlag</description>
<packageinstances>
<packageinstance name="MINI_MELF-0207R"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0207W" urn="urn:adsk.eagle:package:26112/2" type="model" library_version="11">
<description>CECC Size RC6123 Wave Soldering
source Beyschlag</description>
<packageinstances>
<packageinstance name="MINI_MELF-0207W"/>
</packageinstances>
</package3d>
<package3d name="0922V" urn="urn:adsk.eagle:package:23589/1" type="box" library_version="11">
<description>RESISTOR
type 0922, grid 7.5 mm</description>
<packageinstances>
<packageinstance name="0922V"/>
</packageinstances>
</package3d>
<package3d name="RDH/15" urn="urn:adsk.eagle:package:23595/1" type="box" library_version="11">
<description>RESISTOR
type RDH, grid 15 mm</description>
<packageinstances>
<packageinstance name="RDH/15"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0102AX" urn="urn:adsk.eagle:package:23594/1" type="box" library_version="11">
<description>Mini MELF 0102 Axial</description>
<packageinstances>
<packageinstance name="MINI_MELF-0102AX"/>
</packageinstances>
</package3d>
<package3d name="R0201" urn="urn:adsk.eagle:package:26117/2" type="model" library_version="11">
<description>RESISTOR chip
Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<packageinstances>
<packageinstance name="R0201"/>
</packageinstances>
</package3d>
<package3d name="VTA52" urn="urn:adsk.eagle:package:26116/2" type="model" library_version="11">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RBR52
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VTA52"/>
</packageinstances>
</package3d>
<package3d name="VTA53" urn="urn:adsk.eagle:package:26118/2" type="model" library_version="11">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RBR53
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VTA53"/>
</packageinstances>
</package3d>
<package3d name="VTA54" urn="urn:adsk.eagle:package:26119/2" type="model" library_version="11">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RBR54
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VTA54"/>
</packageinstances>
</package3d>
<package3d name="VTA55" urn="urn:adsk.eagle:package:26120/2" type="model" library_version="11">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RBR55
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VTA55"/>
</packageinstances>
</package3d>
<package3d name="VTA56" urn="urn:adsk.eagle:package:26129/3" type="model" library_version="11">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RBR56
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VTA56"/>
</packageinstances>
</package3d>
<package3d name="VMTA55" urn="urn:adsk.eagle:package:26121/2" type="model" library_version="11">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RNC55
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VMTA55"/>
</packageinstances>
</package3d>
<package3d name="VMTB60" urn="urn:adsk.eagle:package:26122/2" type="model" library_version="11">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RNC60
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VMTB60"/>
</packageinstances>
</package3d>
<package3d name="R4527" urn="urn:adsk.eagle:package:13310/2" type="model" library_version="11">
<description>Package 4527
Source: http://www.vishay.com/docs/31059/wsrhigh.pdf</description>
<packageinstances>
<packageinstance name="R4527"/>
</packageinstances>
</package3d>
<package3d name="WSC0001" urn="urn:adsk.eagle:package:26123/2" type="model" library_version="11">
<description>Wirewound Resistors, Precision Power
Source: VISHAY wscwsn.pdf</description>
<packageinstances>
<packageinstance name="WSC0001"/>
</packageinstances>
</package3d>
<package3d name="WSC0002" urn="urn:adsk.eagle:package:26125/2" type="model" library_version="11">
<description>Wirewound Resistors, Precision Power
Source: VISHAY wscwsn.pdf</description>
<packageinstances>
<packageinstance name="WSC0002"/>
</packageinstances>
</package3d>
<package3d name="WSC01/2" urn="urn:adsk.eagle:package:26127/2" type="model" library_version="11">
<description>Wirewound Resistors, Precision Power
Source: VISHAY wscwsn.pdf</description>
<packageinstances>
<packageinstance name="WSC01/2"/>
</packageinstances>
</package3d>
<package3d name="WSC2515" urn="urn:adsk.eagle:package:26134/2" type="model" library_version="11">
<description>Wirewound Resistors, Precision Power
Source: VISHAY wscwsn.pdf</description>
<packageinstances>
<packageinstance name="WSC2515"/>
</packageinstances>
</package3d>
<package3d name="WSC4527" urn="urn:adsk.eagle:package:26126/2" type="model" library_version="11">
<description>Wirewound Resistors, Precision Power
Source: VISHAY wscwsn.pdf</description>
<packageinstances>
<packageinstance name="WSC4527"/>
</packageinstances>
</package3d>
<package3d name="WSC6927" urn="urn:adsk.eagle:package:26128/2" type="model" library_version="11">
<description>Wirewound Resistors, Precision Power
Source: VISHAY wscwsn.pdf</description>
<packageinstances>
<packageinstance name="WSC6927"/>
</packageinstances>
</package3d>
<package3d name="R1218" urn="urn:adsk.eagle:package:26131/2" type="model" library_version="11">
<description>CRCW1218 Thick Film, Rectangular Chip Resistors
Source: http://www.vishay.com .. dcrcw.pdf</description>
<packageinstances>
<packageinstance name="R1218"/>
</packageinstances>
</package3d>
<package3d name="1812X7R" urn="urn:adsk.eagle:package:26130/2" type="model" library_version="11">
<description>Chip Monolithic Ceramic Capacitors Medium Voltage High Capacitance for General Use
Source: http://www.murata.com .. GRM43DR72E224KW01.pdf</description>
<packageinstances>
<packageinstance name="1812X7R"/>
</packageinstances>
</package3d>
<package3d name="PRL1632" urn="urn:adsk.eagle:package:26132/2" type="model" library_version="11">
<description>PRL1632 are realized as 1W for 3.2 × 1.6mm(1206)
Source: http://www.mouser.com/ds/2/392/products_18-2245.pdf</description>
<packageinstances>
<packageinstance name="PRL1632"/>
</packageinstances>
</package3d>
<package3d name="R01005" urn="urn:adsk.eagle:package:26133/2" type="model" library_version="11">
<description>Chip, 0.40 X 0.20 X 0.16 mm body
&lt;p&gt;Chip package with body size 0.40 X 0.20 X 0.16 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="R01005"/>
</packageinstances>
</package3d>
<package3d name="CAPC1005X60" urn="urn:adsk.eagle:package:23626/2" type="model" library_version="11">
<description>Chip, 1.00 X 0.50 X 0.60 mm body
&lt;p&gt;Chip package with body size 1.00 X 0.50 X 0.60 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="C0402"/>
</packageinstances>
</package3d>
<package3d name="C0504" urn="urn:adsk.eagle:package:23624/2" type="model" library_version="11">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C0504"/>
</packageinstances>
</package3d>
<package3d name="C0603" urn="urn:adsk.eagle:package:23616/2" type="model" library_version="11">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C0603"/>
</packageinstances>
</package3d>
<package3d name="C0805" urn="urn:adsk.eagle:package:23617/2" type="model" library_version="11">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C0805"/>
</packageinstances>
</package3d>
<package3d name="C1206" urn="urn:adsk.eagle:package:23618/2" type="model" library_version="11">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C1206"/>
</packageinstances>
</package3d>
<package3d name="C1210" urn="urn:adsk.eagle:package:23619/2" type="model" library_version="11">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C1210"/>
</packageinstances>
</package3d>
<package3d name="C1310" urn="urn:adsk.eagle:package:23620/2" type="model" library_version="11">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C1310"/>
</packageinstances>
</package3d>
<package3d name="C1608" urn="urn:adsk.eagle:package:23621/2" type="model" library_version="11">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C1608"/>
</packageinstances>
</package3d>
<package3d name="C1812" urn="urn:adsk.eagle:package:23622/2" type="model" library_version="11">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C1812"/>
</packageinstances>
</package3d>
<package3d name="C1825" urn="urn:adsk.eagle:package:23623/2" type="model" library_version="11">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C1825"/>
</packageinstances>
</package3d>
<package3d name="C2012" urn="urn:adsk.eagle:package:23625/2" type="model" library_version="11">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C2012"/>
</packageinstances>
</package3d>
<package3d name="C3216" urn="urn:adsk.eagle:package:23628/2" type="model" library_version="11">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C3216"/>
</packageinstances>
</package3d>
<package3d name="C3225" urn="urn:adsk.eagle:package:23655/2" type="model" library_version="11">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C3225"/>
</packageinstances>
</package3d>
<package3d name="C4532" urn="urn:adsk.eagle:package:23627/2" type="model" library_version="11">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C4532"/>
</packageinstances>
</package3d>
<package3d name="C4564" urn="urn:adsk.eagle:package:23648/2" type="model" library_version="11">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C4564"/>
</packageinstances>
</package3d>
<package3d name="C025-024X044" urn="urn:adsk.eagle:package:23630/1" type="box" library_version="11">
<description>CAPACITOR
grid 2.5 mm, outline 2.4 x 4.4 mm</description>
<packageinstances>
<packageinstance name="C025-024X044"/>
</packageinstances>
</package3d>
<package3d name="C025-025X050" urn="urn:adsk.eagle:package:23629/2" type="model" library_version="11">
<description>CAPACITOR
grid 2.5 mm, outline 2.5 x 5 mm</description>
<packageinstances>
<packageinstance name="C025-025X050"/>
</packageinstances>
</package3d>
<package3d name="C025-030X050" urn="urn:adsk.eagle:package:23631/1" type="box" library_version="11">
<description>CAPACITOR
grid 2.5 mm, outline 3 x 5 mm</description>
<packageinstances>
<packageinstance name="C025-030X050"/>
</packageinstances>
</package3d>
<package3d name="C025-040X050" urn="urn:adsk.eagle:package:23634/1" type="box" library_version="11">
<description>CAPACITOR
grid 2.5 mm, outline 4 x 5 mm</description>
<packageinstances>
<packageinstance name="C025-040X050"/>
</packageinstances>
</package3d>
<package3d name="C025-050X050" urn="urn:adsk.eagle:package:23633/1" type="box" library_version="11">
<description>CAPACITOR
grid 2.5 mm, outline 5 x 5 mm</description>
<packageinstances>
<packageinstance name="C025-050X050"/>
</packageinstances>
</package3d>
<package3d name="C025-060X050" urn="urn:adsk.eagle:package:23632/1" type="box" library_version="11">
<description>CAPACITOR
grid 2.5 mm, outline 6 x 5 mm</description>
<packageinstances>
<packageinstance name="C025-060X050"/>
</packageinstances>
</package3d>
<package3d name="C025_050-024X070" urn="urn:adsk.eagle:package:23639/1" type="box" library_version="11">
<description>CAPACITOR
grid 2.5 mm + 5 mm, outline 2.4 x 7 mm</description>
<packageinstances>
<packageinstance name="C025_050-024X070"/>
</packageinstances>
</package3d>
<package3d name="C025_050-025X075" urn="urn:adsk.eagle:package:23641/1" type="box" library_version="11">
<description>CAPACITOR
grid 2.5 + 5 mm, outline 2.5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C025_050-025X075"/>
</packageinstances>
</package3d>
<package3d name="C025_050-035X075" urn="urn:adsk.eagle:package:23651/1" type="box" library_version="11">
<description>CAPACITOR
grid 2.5 + 5 mm, outline 3.5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C025_050-035X075"/>
</packageinstances>
</package3d>
<package3d name="C025_050-045X075" urn="urn:adsk.eagle:package:23635/1" type="box" library_version="11">
<description>CAPACITOR
grid 2.5 + 5 mm, outline 4.5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C025_050-045X075"/>
</packageinstances>
</package3d>
<package3d name="C025_050-055X075" urn="urn:adsk.eagle:package:23636/1" type="box" library_version="11">
<description>CAPACITOR
grid 2.5 + 5 mm, outline 5.5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C025_050-055X075"/>
</packageinstances>
</package3d>
<package3d name="C050-024X044" urn="urn:adsk.eagle:package:23643/1" type="box" library_version="11">
<description>CAPACITOR
grid 5 mm, outline 2.4 x 4.4 mm</description>
<packageinstances>
<packageinstance name="C050-024X044"/>
</packageinstances>
</package3d>
<package3d name="C050-025X075" urn="urn:adsk.eagle:package:23637/1" type="box" library_version="11">
<description>CAPACITOR
grid 5 mm, outline 2.5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C050-025X075"/>
</packageinstances>
</package3d>
<package3d name="C050-045X075" urn="urn:adsk.eagle:package:23638/1" type="box" library_version="11">
<description>CAPACITOR
grid 5 mm, outline 4.5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C050-045X075"/>
</packageinstances>
</package3d>
<package3d name="C050-030X075" urn="urn:adsk.eagle:package:23640/1" type="box" library_version="11">
<description>CAPACITOR
grid 5 mm, outline 3 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C050-030X075"/>
</packageinstances>
</package3d>
<package3d name="C050-050X075" urn="urn:adsk.eagle:package:23665/1" type="box" library_version="11">
<description>CAPACITOR
grid 5 mm, outline 5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C050-050X075"/>
</packageinstances>
</package3d>
<package3d name="C050-055X075" urn="urn:adsk.eagle:package:23642/1" type="box" library_version="11">
<description>CAPACITOR
grid 5 mm, outline 5.5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C050-055X075"/>
</packageinstances>
</package3d>
<package3d name="C050-075X075" urn="urn:adsk.eagle:package:23645/1" type="box" library_version="11">
<description>CAPACITOR
grid 5 mm, outline 7.5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C050-075X075"/>
</packageinstances>
</package3d>
<package3d name="C050H075X075" urn="urn:adsk.eagle:package:23644/1" type="box" library_version="11">
<description>CAPACITOR
Horizontal, grid 5 mm, outline 7.5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C050H075X075"/>
</packageinstances>
</package3d>
<package3d name="C075-032X103" urn="urn:adsk.eagle:package:23646/1" type="box" library_version="11">
<description>CAPACITOR
grid 7.5 mm, outline 3.2 x 10.3 mm</description>
<packageinstances>
<packageinstance name="C075-032X103"/>
</packageinstances>
</package3d>
<package3d name="C075-042X103" urn="urn:adsk.eagle:package:23656/1" type="box" library_version="11">
<description>CAPACITOR
grid 7.5 mm, outline 4.2 x 10.3 mm</description>
<packageinstances>
<packageinstance name="C075-042X103"/>
</packageinstances>
</package3d>
<package3d name="C075-052X106" urn="urn:adsk.eagle:package:23650/1" type="box" library_version="11">
<description>CAPACITOR
grid 7.5 mm, outline 5.2 x 10.6 mm</description>
<packageinstances>
<packageinstance name="C075-052X106"/>
</packageinstances>
</package3d>
<package3d name="C102-043X133" urn="urn:adsk.eagle:package:23647/1" type="box" library_version="11">
<description>CAPACITOR
grid 10.2 mm, outline 4.3 x 13.3 mm</description>
<packageinstances>
<packageinstance name="C102-043X133"/>
</packageinstances>
</package3d>
<package3d name="C102-054X133" urn="urn:adsk.eagle:package:23649/1" type="box" library_version="11">
<description>CAPACITOR
grid 10.2 mm, outline 5.4 x 13.3 mm</description>
<packageinstances>
<packageinstance name="C102-054X133"/>
</packageinstances>
</package3d>
<package3d name="C102-064X133" urn="urn:adsk.eagle:package:23653/1" type="box" library_version="11">
<description>CAPACITOR
grid 10.2 mm, outline 6.4 x 13.3 mm</description>
<packageinstances>
<packageinstance name="C102-064X133"/>
</packageinstances>
</package3d>
<package3d name="C102_152-062X184" urn="urn:adsk.eagle:package:23652/1" type="box" library_version="11">
<description>CAPACITOR
grid 10.2 mm + 15.2 mm, outline 6.2 x 18.4 mm</description>
<packageinstances>
<packageinstance name="C102_152-062X184"/>
</packageinstances>
</package3d>
<package3d name="C150-054X183" urn="urn:adsk.eagle:package:23669/1" type="box" library_version="11">
<description>CAPACITOR
grid 15 mm, outline 5.4 x 18.3 mm</description>
<packageinstances>
<packageinstance name="C150-054X183"/>
</packageinstances>
</package3d>
<package3d name="C150-064X183" urn="urn:adsk.eagle:package:23654/1" type="box" library_version="11">
<description>CAPACITOR
grid 15 mm, outline 6.4 x 18.3 mm</description>
<packageinstances>
<packageinstance name="C150-064X183"/>
</packageinstances>
</package3d>
<package3d name="C150-072X183" urn="urn:adsk.eagle:package:23657/1" type="box" library_version="11">
<description>CAPACITOR
grid 15 mm, outline 7.2 x 18.3 mm</description>
<packageinstances>
<packageinstance name="C150-072X183"/>
</packageinstances>
</package3d>
<package3d name="C150-084X183" urn="urn:adsk.eagle:package:23658/1" type="box" library_version="11">
<description>CAPACITOR
grid 15 mm, outline 8.4 x 18.3 mm</description>
<packageinstances>
<packageinstance name="C150-084X183"/>
</packageinstances>
</package3d>
<package3d name="C150-091X182" urn="urn:adsk.eagle:package:23659/1" type="box" library_version="11">
<description>CAPACITOR
grid 15 mm, outline 9.1 x 18.2 mm</description>
<packageinstances>
<packageinstance name="C150-091X182"/>
</packageinstances>
</package3d>
<package3d name="C225-062X268" urn="urn:adsk.eagle:package:23661/1" type="box" library_version="11">
<description>CAPACITOR
grid 22.5 mm, outline 6.2 x 26.8 mm</description>
<packageinstances>
<packageinstance name="C225-062X268"/>
</packageinstances>
</package3d>
<package3d name="C225-074X268" urn="urn:adsk.eagle:package:23660/1" type="box" library_version="11">
<description>CAPACITOR
grid 22.5 mm, outline 7.4 x 26.8 mm</description>
<packageinstances>
<packageinstance name="C225-074X268"/>
</packageinstances>
</package3d>
<package3d name="C225-087X268" urn="urn:adsk.eagle:package:23662/1" type="box" library_version="11">
<description>CAPACITOR
grid 22.5 mm, outline 8.7 x 26.8 mm</description>
<packageinstances>
<packageinstance name="C225-087X268"/>
</packageinstances>
</package3d>
<package3d name="C225-108X268" urn="urn:adsk.eagle:package:23663/1" type="box" library_version="11">
<description>CAPACITOR
grid 22.5 mm, outline 10.8 x 26.8 mm</description>
<packageinstances>
<packageinstance name="C225-108X268"/>
</packageinstances>
</package3d>
<package3d name="C225-113X268" urn="urn:adsk.eagle:package:23667/1" type="box" library_version="11">
<description>CAPACITOR
grid 22.5 mm, outline 11.3 x 26.8 mm</description>
<packageinstances>
<packageinstance name="C225-113X268"/>
</packageinstances>
</package3d>
<package3d name="C275-093X316" urn="urn:adsk.eagle:package:23701/1" type="box" library_version="11">
<description>CAPACITOR
grid 27.5 mm, outline 9.3 x 31.6 mm</description>
<packageinstances>
<packageinstance name="C275-093X316"/>
</packageinstances>
</package3d>
<package3d name="C275-113X316" urn="urn:adsk.eagle:package:23673/1" type="box" library_version="11">
<description>CAPACITOR
grid 27.5 mm, outline 11.3 x 31.6 mm</description>
<packageinstances>
<packageinstance name="C275-113X316"/>
</packageinstances>
</package3d>
<package3d name="C275-134X316" urn="urn:adsk.eagle:package:23664/1" type="box" library_version="11">
<description>CAPACITOR
grid 27.5 mm, outline 13.4 x 31.6 mm</description>
<packageinstances>
<packageinstance name="C275-134X316"/>
</packageinstances>
</package3d>
<package3d name="C275-205X316" urn="urn:adsk.eagle:package:23666/1" type="box" library_version="11">
<description>CAPACITOR
grid 27.5 mm, outline 20.5 x 31.6 mm</description>
<packageinstances>
<packageinstance name="C275-205X316"/>
</packageinstances>
</package3d>
<package3d name="C325-137X374" urn="urn:adsk.eagle:package:23672/1" type="box" library_version="11">
<description>CAPACITOR
grid 32.5 mm, outline 13.7 x 37.4 mm</description>
<packageinstances>
<packageinstance name="C325-137X374"/>
</packageinstances>
</package3d>
<package3d name="C325-162X374" urn="urn:adsk.eagle:package:23670/1" type="box" library_version="11">
<description>CAPACITOR
grid 32.5 mm, outline 16.2 x 37.4 mm</description>
<packageinstances>
<packageinstance name="C325-162X374"/>
</packageinstances>
</package3d>
<package3d name="C325-182X374" urn="urn:adsk.eagle:package:23668/1" type="box" library_version="11">
<description>CAPACITOR
grid 32.5 mm, outline 18.2 x 37.4 mm</description>
<packageinstances>
<packageinstance name="C325-182X374"/>
</packageinstances>
</package3d>
<package3d name="C375-192X418" urn="urn:adsk.eagle:package:23674/1" type="box" library_version="11">
<description>CAPACITOR
grid 37.5 mm, outline 19.2 x 41.8 mm</description>
<packageinstances>
<packageinstance name="C375-192X418"/>
</packageinstances>
</package3d>
<package3d name="C375-203X418" urn="urn:adsk.eagle:package:23671/1" type="box" library_version="11">
<description>CAPACITOR
grid 37.5 mm, outline 20.3 x 41.8 mm</description>
<packageinstances>
<packageinstance name="C375-203X418"/>
</packageinstances>
</package3d>
<package3d name="C050-035X075" urn="urn:adsk.eagle:package:23677/1" type="box" library_version="11">
<description>CAPACITOR
grid 5 mm, outline 3.5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C050-035X075"/>
</packageinstances>
</package3d>
<package3d name="C375-155X418" urn="urn:adsk.eagle:package:23675/1" type="box" library_version="11">
<description>CAPACITOR
grid 37.5 mm, outline 15.5 x 41.8 mm</description>
<packageinstances>
<packageinstance name="C375-155X418"/>
</packageinstances>
</package3d>
<package3d name="C075-063X106" urn="urn:adsk.eagle:package:23678/1" type="box" library_version="11">
<description>CAPACITOR
grid 7.5 mm, outline 6.3 x 10.6 mm</description>
<packageinstances>
<packageinstance name="C075-063X106"/>
</packageinstances>
</package3d>
<package3d name="C275-154X316" urn="urn:adsk.eagle:package:23685/1" type="box" library_version="11">
<description>CAPACITOR
grid 27.5 mm, outline 15.4 x 31.6 mm</description>
<packageinstances>
<packageinstance name="C275-154X316"/>
</packageinstances>
</package3d>
<package3d name="C275-173X316" urn="urn:adsk.eagle:package:23676/1" type="box" library_version="11">
<description>CAPACITOR
grid 27.5 mm, outline 17.3 x 31.6 mm</description>
<packageinstances>
<packageinstance name="C275-173X316"/>
</packageinstances>
</package3d>
<package3d name="C0402K" urn="urn:adsk.eagle:package:23679/2" type="model" library_version="11">
<description>Ceramic Chip Capacitor KEMET 0204 reflow solder
Metric Code Size 1005</description>
<packageinstances>
<packageinstance name="C0402K"/>
</packageinstances>
</package3d>
<package3d name="C0603K" urn="urn:adsk.eagle:package:23680/2" type="model" library_version="11">
<description>Ceramic Chip Capacitor KEMET 0603 reflow solder
Metric Code Size 1608</description>
<packageinstances>
<packageinstance name="C0603K"/>
</packageinstances>
</package3d>
<package3d name="C0805K" urn="urn:adsk.eagle:package:23681/2" type="model" library_version="11">
<description>Ceramic Chip Capacitor KEMET 0805 reflow solder
Metric Code Size 2012</description>
<packageinstances>
<packageinstance name="C0805K"/>
</packageinstances>
</package3d>
<package3d name="C1206K" urn="urn:adsk.eagle:package:23682/2" type="model" library_version="11">
<description>Ceramic Chip Capacitor KEMET 1206 reflow solder
Metric Code Size 3216</description>
<packageinstances>
<packageinstance name="C1206K"/>
</packageinstances>
</package3d>
<package3d name="C1210K" urn="urn:adsk.eagle:package:23683/2" type="model" library_version="11">
<description>Ceramic Chip Capacitor KEMET 1210 reflow solder
Metric Code Size 3225</description>
<packageinstances>
<packageinstance name="C1210K"/>
</packageinstances>
</package3d>
<package3d name="C1812K" urn="urn:adsk.eagle:package:23686/2" type="model" library_version="11">
<description>Ceramic Chip Capacitor KEMET 1812 reflow solder
Metric Code Size 4532</description>
<packageinstances>
<packageinstance name="C1812K"/>
</packageinstances>
</package3d>
<package3d name="C1825K" urn="urn:adsk.eagle:package:23684/2" type="model" library_version="11">
<description>Ceramic Chip Capacitor KEMET 1825 reflow solder
Metric Code Size 4564</description>
<packageinstances>
<packageinstance name="C1825K"/>
</packageinstances>
</package3d>
<package3d name="C2220K" urn="urn:adsk.eagle:package:23687/2" type="model" library_version="11">
<description>Ceramic Chip Capacitor KEMET 2220 reflow solderMetric Code Size 5650</description>
<packageinstances>
<packageinstance name="C2220K"/>
</packageinstances>
</package3d>
<package3d name="C2225K" urn="urn:adsk.eagle:package:23692/2" type="model" library_version="11">
<description>Ceramic Chip Capacitor KEMET 2225 reflow solderMetric Code Size 5664</description>
<packageinstances>
<packageinstance name="C2225K"/>
</packageinstances>
</package3d>
<package3d name="HPC0201" urn="urn:adsk.eagle:package:26213/1" type="box" library_version="11">
<description> 
Source: http://www.vishay.com/docs/10129/hpc0201a.pdf</description>
<packageinstances>
<packageinstance name="HPC0201"/>
</packageinstances>
</package3d>
<package3d name="C0201" urn="urn:adsk.eagle:package:23690/2" type="model" library_version="11">
<description>Source: http://www.avxcorp.com/docs/catalogs/cx5r.pdf</description>
<packageinstances>
<packageinstance name="C0201"/>
</packageinstances>
</package3d>
<package3d name="C1808" urn="urn:adsk.eagle:package:23689/2" type="model" library_version="11">
<description>CAPACITOR
Source: AVX .. aphvc.pdf</description>
<packageinstances>
<packageinstance name="C1808"/>
</packageinstances>
</package3d>
<package3d name="C3640" urn="urn:adsk.eagle:package:23693/2" type="model" library_version="11">
<description>CAPACITOR
Source: AVX .. aphvc.pdf</description>
<packageinstances>
<packageinstance name="C3640"/>
</packageinstances>
</package3d>
<package3d name="C01005" urn="urn:adsk.eagle:package:23691/1" type="box" library_version="11">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C01005"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="R-EU" urn="urn:adsk.eagle:symbol:23042/1" library_version="11">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="C-EU" urn="urn:adsk.eagle:symbol:23120/1" library_version="11">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="R-EU_" urn="urn:adsk.eagle:component:23791/21" prefix="R" uservalue="yes" library_version="11">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="R-EU" x="0" y="0"/>
</gates>
<devices>
<device name="R0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23547/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23555/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="70" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23553/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="86" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R0805W" package="R0805W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23537/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="3" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23540/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="41" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R1206W" package="R1206W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23539/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="3" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R1210" package="R1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23554/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R1210W" package="R1210W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23541/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23551/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="3" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R2010W" package="R2010W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23542/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R2012" package="R2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23543/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R2012W" package="R2012W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23544/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23545/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R2512W" package="R2512W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23565/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R3216" package="R3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23557/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R3216W" package="R3216W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23548/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="5" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R3225" package="R3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23549/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R3225W" package="R3225W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23550/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R5025" package="R5025">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23552/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R5025W" package="R5025W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23558/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R6332" package="R6332">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23559/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R6332W" package="R6332W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26078/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M0805" package="M0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23556/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="45" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M1206" package="M1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23566/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="17" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M1406" package="M1406">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23569/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M2012" package="M2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23561/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M2309" package="M2309">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23562/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M3216" package="M3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23563/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M3516" package="M3516">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23573/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M5923" package="M5923">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23564/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0204/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23488/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="35" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0204/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23498/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="79" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0204/2V" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23495/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="11" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0207/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23491/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="81" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0207/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23489/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="9" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0207/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23492/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="2" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0207/2V" package="0207/2V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23490/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="17" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0207/5V" package="0207/5V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23502/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="4" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0207/7" package="0207/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23493/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="46" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0309/10" package="0309/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23567/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="2" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0309/12" package="0309/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23571/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="6" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0309/V" package="0309V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23572/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="2" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0411/12" package="0411/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23578/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="5" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0411/15" package="0411/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23568/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0411/3V" package="0411V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23570/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="4" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0414/15" package="0414/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23579/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0414/5V" package="0414V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23574/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0617/17" package="0617/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23575/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0617/22" package="0617/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23577/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="2" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0617/5V" package="0617V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23576/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0922/22" package="0922/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23580/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0613/5V" package="P0613V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23582/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0613/15" package="P0613/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23581/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0817/22" package="P0817/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23583/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0817/7V" package="P0817V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23608/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="V234/12" package="V234/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23592/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="V235/17" package="V235/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23586/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="3" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="V526-0" package="V526-0">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23590/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF0102R" package="MINI_MELF-0102R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23591/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF0102W" package="MINI_MELF-0102W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23588/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF0204R" package="MINI_MELF-0204R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26109/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF0204W" package="MINI_MELF-0204W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26111/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF0207R" package="MINI_MELF-0207R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26113/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF0207W" package="MINI_MELF-0207W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26112/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0922V" package="0922V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23589/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="RDH/15" package="RDH/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23595/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF0102AX" package="MINI_MELF-0102AX">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23594/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R0201" package="R0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26117/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="VTA52" package="VTA52">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26116/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="VTA53" package="VTA53">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26118/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="VTA54" package="VTA54">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26119/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="VTA55" package="VTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26120/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="VTA56" package="VTA56">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26129/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="VMTA55" package="VMTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26121/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="VMTB60" package="VMTB60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26122/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R4527" package="R4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:13310/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="WSC0001" package="WSC0001">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26123/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="WSC0002" package="WSC0002">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26125/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="WSC01/2" package="WSC01/2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26127/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="WSC2515" package="WSC2515">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26134/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="WSC4527" package="WSC4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26126/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="WSC6927" package="WSC6927">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26128/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R1218" package="R1218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26131/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="1812X7R" package="1812X7R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26130/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="PRL1632" package="PRL1632">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26132/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="01005" package="R01005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26133/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C-EU" urn="urn:adsk.eagle:component:23793/46" prefix="C" uservalue="yes" library_version="11">
<description>&lt;B&gt;CAPACITOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="C-EU" x="0" y="0"/>
</gates>
<devices>
<device name="C0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23626/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="18" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C0504" package="C0504">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23624/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="2" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23616/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="73" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23617/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="88" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C1206" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23618/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="54" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C1210" package="C1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23619/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C1310" package="C1310">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23620/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C1608" package="C1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23621/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="3" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C1812" package="C1812">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23622/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="3" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C1825" package="C1825">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23623/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C2012" package="C2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23625/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C3216" package="C3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23628/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="4" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C3225" package="C3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23655/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C4532" package="C4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23627/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C4564" package="C4564">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23648/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="025-024X044" package="C025-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23630/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="56" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="025-025X050" package="C025-025X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23629/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="65" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="025-030X050" package="C025-030X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23631/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="14" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="025-040X050" package="C025-040X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23634/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="4" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="025-050X050" package="C025-050X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23633/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="16" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="025-060X050" package="C025-060X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23632/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C025_050-024X070" package="C025_050-024X070">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23639/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="025_050-025X075" package="C025_050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23641/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="025_050-035X075" package="C025_050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23651/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="025_050-045X075" package="C025_050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23635/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="025_050-055X075" package="C025_050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23636/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="050-024X044" package="C050-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23643/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="33" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="050-025X075" package="C050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23637/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="29" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="050-045X075" package="C050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23638/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="050-030X075" package="C050-030X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23640/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="9" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="050-050X075" package="C050-050X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23665/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="050-055X075" package="C050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23642/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="050-075X075" package="C050-075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23645/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="050H075X075" package="C050H075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23644/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="075-032X103" package="C075-032X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23646/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="075-042X103" package="C075-042X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23656/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="075-052X106" package="C075-052X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23650/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="4" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="102-043X133" package="C102-043X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23647/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="102-054X133" package="C102-054X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23649/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="102-064X133" package="C102-064X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23653/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="102_152-062X184" package="C102_152-062X184">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23652/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="150-054X183" package="C150-054X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23669/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="3" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="150-064X183" package="C150-064X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23654/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="150-072X183" package="C150-072X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23657/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="150-084X183" package="C150-084X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23658/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="150-091X182" package="C150-091X182">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23659/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="225-062X268" package="C225-062X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23661/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="225-074X268" package="C225-074X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23660/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="2" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="225-087X268" package="C225-087X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23662/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="225-108X268" package="C225-108X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23663/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="225-113X268" package="C225-113X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23667/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="275-093X316" package="C275-093X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23701/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="275-113X316" package="C275-113X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23673/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="275-134X316" package="C275-134X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23664/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="275-205X316" package="C275-205X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23666/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="325-137X374" package="C325-137X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23672/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="325-162X374" package="C325-162X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23670/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="325-182X374" package="C325-182X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23668/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="375-192X418" package="C375-192X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23674/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="375-203X418" package="C375-203X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23671/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="050-035X075" package="C050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23677/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="2" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="375-155X418" package="C375-155X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23675/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="075-063X106" package="C075-063X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23678/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="275-154X316" package="C275-154X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23685/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="275-173X316" package="C275-173X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23676/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C0402K" package="C0402K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23679/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="15" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C0603K" package="C0603K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23680/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="30" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C0805K" package="C0805K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23681/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="52" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C1206K" package="C1206K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23682/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="13" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C1210K" package="C1210K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23683/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C1812K" package="C1812K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23686/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C1825K" package="C1825K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23684/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C2220K" package="C2220K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23687/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C2225K" package="C2225K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23692/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="HPC0201" package="HPC0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26213/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C0201" package="C0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23690/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C1808" package="C1808">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23689/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C3640" package="C3640">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23693/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="01005" package="C01005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23691/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
</devices>
<spice>
<pinmapping spiceprefix="C">
<pinmap gate="G$1" pin="1" pinorder="1"/>
<pinmap gate="G$1" pin="2" pinorder="2"/>
</pinmapping>
</spice>
</deviceset>
</devicesets>
</library>
<library name="adafruit" urn="urn:adsk.eagle:library:420">
<packages>
<package name="SO16W" urn="urn:adsk.eagle:footprint:6240108/1" library_version="2">
<description>&lt;b&gt;SMALL OUTLINE INTEGRATED CIRCUIT&lt;/b&gt;&lt;p&gt;
wide body 7.5 mm/JEDEC MS-013AA</description>
<wire x1="-5.395" y1="5.9" x2="5.395" y2="5.9" width="0.1998" layer="39"/>
<wire x1="5.395" y1="-5.9" x2="-5.395" y2="-5.9" width="0.1998" layer="39"/>
<wire x1="-5.395" y1="-5.9" x2="-5.395" y2="5.9" width="0.1998" layer="39"/>
<wire x1="5.19" y1="-3.7" x2="-5.19" y2="-3.7" width="0.2032" layer="51"/>
<wire x1="-5.19" y1="-3.7" x2="-5.19" y2="-3.2" width="0.2032" layer="51"/>
<wire x1="-5.19" y1="-3.2" x2="-5.19" y2="3.7" width="0.2032" layer="51"/>
<wire x1="-5.19" y1="3.7" x2="5.19" y2="3.7" width="0.2032" layer="51"/>
<wire x1="5.19" y1="-3.2" x2="-5.19" y2="-3.2" width="0.2032" layer="51"/>
<wire x1="5.19" y1="3.7" x2="5.19" y2="-3.2" width="0.2032" layer="51"/>
<wire x1="5.19" y1="-3.2" x2="5.19" y2="-3.7" width="0.2032" layer="51"/>
<wire x1="5.395" y1="5.9" x2="5.395" y2="-5.9" width="0.1998" layer="39"/>
<smd name="2" x="-3.175" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="13" x="-0.635" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="1" x="-4.445" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="3" x="-1.905" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="4" x="-0.635" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="14" x="-1.905" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="12" x="0.635" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="11" x="1.905" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="6" x="1.905" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="9" x="4.445" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="5" x="0.635" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="7" x="3.175" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="10" x="3.175" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="8" x="4.445" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="15" x="-3.175" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="16" x="-4.445" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<text x="-4.445" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.445" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.6901" y1="-5.32" x2="-4.1999" y2="-3.8001" layer="51"/>
<rectangle x1="-3.4201" y1="-5.32" x2="-2.9299" y2="-3.8001" layer="51"/>
<rectangle x1="-2.1501" y1="-5.32" x2="-1.6599" y2="-3.8001" layer="51"/>
<rectangle x1="-0.8801" y1="-5.32" x2="-0.3899" y2="-3.8001" layer="51"/>
<rectangle x1="0.3899" y1="-5.32" x2="0.8801" y2="-3.8001" layer="51"/>
<rectangle x1="1.6599" y1="-5.32" x2="2.1501" y2="-3.8001" layer="51"/>
<rectangle x1="2.9299" y1="-5.32" x2="3.4201" y2="-3.8001" layer="51"/>
<rectangle x1="4.1999" y1="-5.32" x2="4.6901" y2="-3.8001" layer="51"/>
<rectangle x1="4.1999" y1="3.8001" x2="4.6901" y2="5.32" layer="51"/>
<rectangle x1="2.9299" y1="3.8001" x2="3.4201" y2="5.32" layer="51"/>
<rectangle x1="1.6599" y1="3.8001" x2="2.1501" y2="5.32" layer="51"/>
<rectangle x1="0.3899" y1="3.8001" x2="0.8801" y2="5.32" layer="51"/>
<rectangle x1="-0.8801" y1="3.8001" x2="-0.3899" y2="5.32" layer="51"/>
<rectangle x1="-2.1501" y1="3.8001" x2="-1.6599" y2="5.32" layer="51"/>
<rectangle x1="-3.4201" y1="3.8001" x2="-2.9299" y2="5.32" layer="51"/>
<rectangle x1="-4.6901" y1="3.8001" x2="-4.1999" y2="5.32" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="SO16W" urn="urn:adsk.eagle:package:6240754/1" type="box" library_version="2">
<description>&lt;b&gt;SMALL OUTLINE INTEGRATED CIRCUIT&lt;/b&gt;&lt;p&gt;
wide body 7.5 mm/JEDEC MS-013AA</description>
<packageinstances>
<packageinstance name="SO16W"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="DS3231" urn="urn:adsk.eagle:symbol:6239637/1" library_version="2">
<wire x1="-10.16" y1="12.7" x2="10.16" y2="12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="12.7" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="-10.16" y2="12.7" width="0.254" layer="94"/>
<text x="-10.16" y="-12.7" size="1.778" layer="96" font="vector">DS3231</text>
<text x="10.16" y="15.24" size="1.778" layer="95" font="vector" rot="R180">&gt;NAME</text>
<pin name="SCL" x="15.24" y="10.16" length="middle" rot="R180"/>
<pin name="SDA" x="15.24" y="7.62" length="middle" rot="R180"/>
<pin name="/RST" x="-15.24" y="2.54" length="middle" direction="pas" function="dot"/>
<pin name="VBAT" x="15.24" y="5.08" length="middle" direction="pwr" rot="R180"/>
<pin name="32KHZ" x="-15.24" y="10.16" length="middle" direction="out"/>
<pin name="SQW/INT" x="-15.24" y="5.08" length="middle" direction="out"/>
<pin name="VCC" x="-15.24" y="7.62" length="middle" direction="pwr"/>
<pin name="GND" x="15.24" y="2.54" length="middle" direction="pwr" rot="R180"/>
<pin name="NC" x="-15.24" y="0" length="middle" direction="nc"/>
<pin name="NC1" x="-15.24" y="-2.54" length="middle" direction="nc"/>
<pin name="NC2" x="-15.24" y="-5.08" length="middle" direction="nc"/>
<pin name="NC3" x="-15.24" y="-7.62" length="middle" direction="nc"/>
<pin name="NC4" x="15.24" y="-7.62" length="middle" direction="nc" rot="R180"/>
<pin name="NC5" x="15.24" y="-5.08" length="middle" direction="nc" rot="R180"/>
<pin name="NC6" x="15.24" y="-2.54" length="middle" direction="nc" rot="R180"/>
<pin name="NC7" x="15.24" y="0" length="middle" direction="nc" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DS3231" urn="urn:adsk.eagle:component:6241127/1" prefix="IC" library_version="2">
<gates>
<gate name="G$1" symbol="DS3231" x="0" y="0"/>
</gates>
<devices>
<device name="/SO" package="SO16W">
<connects>
<connect gate="G$1" pin="/RST" pad="4"/>
<connect gate="G$1" pin="32KHZ" pad="1"/>
<connect gate="G$1" pin="GND" pad="13"/>
<connect gate="G$1" pin="NC" pad="5"/>
<connect gate="G$1" pin="NC1" pad="6"/>
<connect gate="G$1" pin="NC2" pad="7"/>
<connect gate="G$1" pin="NC3" pad="8"/>
<connect gate="G$1" pin="NC4" pad="9"/>
<connect gate="G$1" pin="NC5" pad="10"/>
<connect gate="G$1" pin="NC6" pad="11"/>
<connect gate="G$1" pin="NC7" pad="12"/>
<connect gate="G$1" pin="SCL" pad="16"/>
<connect gate="G$1" pin="SDA" pad="15"/>
<connect gate="G$1" pin="SQW/INT" pad="3"/>
<connect gate="G$1" pin="VBAT" pad="14"/>
<connect gate="G$1" pin="VCC" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6240754/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="transistor-small-signal" urn="urn:adsk.eagle:library:401">
<description>&lt;b&gt;Small Signal Transistors&lt;/b&gt;&lt;p&gt;
Packages from :&lt;br&gt;
www.infineon.com; &lt;br&gt;
www.semiconductors.com;&lt;br&gt;
www.irf.com&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SOT23" urn="urn:adsk.eagle:footprint:28669/1" library_version="5">
<description>&lt;b&gt;SOT-23&lt;/b&gt;</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<smd name="3" x="0" y="1.1" dx="1" dy="1.4" layer="1"/>
<smd name="2" x="0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<smd name="1" x="-0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="SOT23" urn="urn:adsk.eagle:package:28738/2" type="model" library_version="5">
<description>SOT-23</description>
<packageinstances>
<packageinstance name="SOT23"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="P-MOS" urn="urn:adsk.eagle:symbol:29640/1" library_version="5">
<wire x1="0" y1="0" x2="-1.016" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="0.381" x2="-1.016" y2="-0.381" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-0.381" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="2.032" x2="0" y2="2.794" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="0" x2="-0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="0" x2="-0.381" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-3.048" x2="1.27" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-3.048" x2="1.27" y2="-0.254" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-0.254" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="2.794" width="0.1524" layer="94"/>
<wire x1="1.27" y1="2.794" x2="0" y2="2.794" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-0.762" x2="1.778" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-0.762" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="0.762" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="0.762" y1="0" x2="1.778" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-2.032" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.032" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="2.032" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="0" x2="-0.889" y2="-0.127" width="0.254" layer="94"/>
<wire x1="-0.889" y1="-0.127" x2="-0.889" y2="0.127" width="0.254" layer="94"/>
<wire x1="-0.889" y1="0.127" x2="-0.508" y2="0" width="0.254" layer="94"/>
<wire x1="1.016" y1="-0.635" x2="1.524" y2="-0.635" width="0.254" layer="94"/>
<wire x1="1.524" y1="-0.635" x2="1.27" y2="-0.254" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.254" x2="1.016" y2="-0.635" width="0.254" layer="94"/>
<circle x="0" y="2.794" radius="0.3592" width="0" layer="94"/>
<circle x="0" y="2.032" radius="0.3592" width="0" layer="94"/>
<circle x="0" y="-3.048" radius="0.3592" width="0" layer="94"/>
<text x="2.54" y="0" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.54" x2="-1.524" y2="-1.27" layer="94"/>
<rectangle x1="-2.032" y1="1.27" x2="-1.524" y2="2.54" layer="94"/>
<rectangle x1="-2.032" y1="-0.762" x2="-1.524" y2="0.762" layer="94"/>
<pin name="G" x="-5.08" y="2.54" visible="off" length="short" direction="pas"/>
<pin name="D" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BSS84" urn="urn:adsk.eagle:component:29809/4" prefix="Q" library_version="5">
<description>&lt;b&gt;P-CHANNEL MOS FET&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="P-MOS" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:28738/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="8" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pcf8575c">
<description>&lt;b&gt;PCF8575 16-bit I/O expander for I2C-bus&lt;/b&gt;
&lt;br&gt;&lt;br&gt;
This library contains one device - PCF8575 chip. I had to create entire package drawing by myself, because there was no chip in standard library that is packaged in SOP24 (or I was unable to find it.)
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Warning!&lt;/b&gt; Phillips datasheet says that PCF8575 is made only in SSOP24 package - it seems that the chip I used in my project was not made by Phillips, and the package &lt;b&gt;is not&lt;/b&gt; SSOP24, for sure. 
&lt;br&gt;&lt;br&gt;
Author: Piotr Nikiel &lt;br&gt;
E-mail: nikl@op.pl
&lt;br&gt;
Student of Cracow University of Technology, Cracow, Poland.
&lt;br&gt;&lt;br&gt;
Feel free to modify and add new packages to this library, but leave
information about original author. Please let me know (by email) 
about your modifications/upgrades.</description>
<packages>
<package name="SOP24">
<wire x1="0" y1="0" x2="7.62" y2="0" width="0.127" layer="21"/>
<wire x1="7.62" y1="0" x2="7.62" y2="15.24" width="0.127" layer="21"/>
<wire x1="7.62" y1="15.24" x2="0" y2="15.24" width="0.127" layer="21"/>
<wire x1="0" y1="15.24" x2="0" y2="0" width="0.127" layer="21"/>
<circle x="1.27" y="13.97" radius="0.635" width="0.127" layer="21"/>
<smd name="P$1" x="-1.27" y="14.605" dx="1.27" dy="0.635" layer="1"/>
<smd name="P$2" x="-1.27" y="13.335" dx="1.27" dy="0.635" layer="1"/>
<smd name="P$3" x="-1.27" y="12.065" dx="1.27" dy="0.635" layer="1"/>
<smd name="P$4" x="-1.27" y="10.795" dx="1.27" dy="0.635" layer="1"/>
<smd name="P$5" x="-1.27" y="9.525" dx="1.27" dy="0.635" layer="1"/>
<smd name="P$6" x="-1.27" y="8.255" dx="1.27" dy="0.635" layer="1"/>
<smd name="P$7" x="-1.27" y="6.985" dx="1.27" dy="0.635" layer="1"/>
<smd name="P$8" x="-1.27" y="5.715" dx="1.27" dy="0.635" layer="1"/>
<smd name="P$9" x="-1.27" y="4.445" dx="1.27" dy="0.635" layer="1"/>
<smd name="P$10" x="-1.27" y="3.175" dx="1.27" dy="0.635" layer="1"/>
<smd name="P$11" x="-1.27" y="1.905" dx="1.27" dy="0.635" layer="1"/>
<smd name="P$12" x="-1.27" y="0.635" dx="1.27" dy="0.635" layer="1"/>
<smd name="P$13" x="8.89" y="0.635" dx="1.27" dy="0.635" layer="1"/>
<smd name="P$14" x="8.89" y="1.905" dx="1.27" dy="0.635" layer="1"/>
<smd name="P$15" x="8.89" y="3.175" dx="1.27" dy="0.635" layer="1"/>
<smd name="P$16" x="8.89" y="4.445" dx="1.27" dy="0.635" layer="1"/>
<smd name="P$17" x="8.89" y="5.715" dx="1.27" dy="0.635" layer="1"/>
<smd name="P$18" x="8.89" y="6.985" dx="1.27" dy="0.635" layer="1"/>
<smd name="P$19" x="8.89" y="8.255" dx="1.27" dy="0.635" layer="1"/>
<smd name="P$20" x="8.89" y="9.525" dx="1.27" dy="0.635" layer="1"/>
<smd name="P$21" x="8.89" y="10.795" dx="1.27" dy="0.635" layer="1"/>
<smd name="P$22" x="8.89" y="12.065" dx="1.27" dy="0.635" layer="1"/>
<smd name="P$23" x="8.89" y="13.335" dx="1.27" dy="0.635" layer="1"/>
<smd name="P$24" x="8.89" y="14.605" dx="1.27" dy="0.635" layer="1"/>
<text x="4.445" y="8.255" size="1.27" layer="21" rot="R270">PCF8575</text>
<text x="2.54" y="6.985" size="1.27" layer="25" rot="R270">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="PCF8575">
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="-35.56" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-35.56" x2="10.16" y2="-35.56" width="0.254" layer="94"/>
<wire x1="10.16" y1="-35.56" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<text x="2.54" y="-27.94" size="1.778" layer="94" rot="R90">PCF8575</text>
<text x="2.54" y="-10.16" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<pin name="P00" x="-12.7" y="7.62" visible="pin" length="short"/>
<pin name="P01" x="-12.7" y="5.08" visible="pin" length="short"/>
<pin name="P02" x="-12.7" y="2.54" visible="pin" length="short"/>
<pin name="P03" x="-12.7" y="0" visible="pin" length="short"/>
<pin name="P04" x="-12.7" y="-2.54" visible="pin" length="short"/>
<pin name="P05" x="-12.7" y="-5.08" visible="pin" length="short"/>
<pin name="P06" x="-12.7" y="-7.62" visible="pin" length="short"/>
<pin name="P07" x="-12.7" y="-10.16" visible="pin" length="short"/>
<pin name="P10" x="-12.7" y="-15.24" visible="pin" length="short"/>
<pin name="P11" x="-12.7" y="-17.78" visible="pin" length="short"/>
<pin name="P12" x="-12.7" y="-20.32" visible="pin" length="short"/>
<pin name="P13" x="-12.7" y="-22.86" visible="pin" length="short"/>
<pin name="P14" x="-12.7" y="-25.4" visible="pin" length="short"/>
<pin name="P15" x="-12.7" y="-27.94" visible="pin" length="short"/>
<pin name="P16" x="-12.7" y="-30.48" visible="pin" length="short"/>
<pin name="P17" x="-12.7" y="-33.02" visible="pin" length="short"/>
<pin name="A0" x="12.7" y="7.62" visible="pin" length="short" direction="in" rot="R180"/>
<pin name="A1" x="12.7" y="5.08" visible="pin" length="short" direction="in" rot="R180"/>
<pin name="A2" x="12.7" y="2.54" visible="pin" length="short" direction="in" rot="R180"/>
<pin name="!INT" x="12.7" y="-2.54" visible="pin" length="short" direction="oc" rot="R180"/>
<pin name="SCL" x="12.7" y="-10.16" visible="pin" length="short" direction="in" rot="R180"/>
<pin name="SDA" x="12.7" y="-12.7" visible="pin" length="short" rot="R180"/>
<pin name="VDD" x="12.7" y="-17.78" visible="pin" length="short" direction="pwr" rot="R180"/>
<pin name="VSS" x="12.7" y="-22.86" visible="pin" length="short" direction="pwr" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PCF8575C">
<gates>
<gate name="G$1" symbol="PCF8575" x="-22.86" y="20.32"/>
</gates>
<devices>
<device name="" package="SOP24">
<connects>
<connect gate="G$1" pin="!INT" pad="P$1"/>
<connect gate="G$1" pin="A0" pad="P$21"/>
<connect gate="G$1" pin="A1" pad="P$2"/>
<connect gate="G$1" pin="A2" pad="P$3"/>
<connect gate="G$1" pin="P00" pad="P$4"/>
<connect gate="G$1" pin="P01" pad="P$5"/>
<connect gate="G$1" pin="P02" pad="P$6"/>
<connect gate="G$1" pin="P03" pad="P$7"/>
<connect gate="G$1" pin="P04" pad="P$8"/>
<connect gate="G$1" pin="P05" pad="P$9"/>
<connect gate="G$1" pin="P06" pad="P$10"/>
<connect gate="G$1" pin="P07" pad="P$11"/>
<connect gate="G$1" pin="P10" pad="P$13"/>
<connect gate="G$1" pin="P11" pad="P$14"/>
<connect gate="G$1" pin="P12" pad="P$15"/>
<connect gate="G$1" pin="P13" pad="P$16"/>
<connect gate="G$1" pin="P14" pad="P$17"/>
<connect gate="G$1" pin="P15" pad="P$18"/>
<connect gate="G$1" pin="P16" pad="P$19"/>
<connect gate="G$1" pin="P17" pad="P$20"/>
<connect gate="G$1" pin="SCL" pad="P$22"/>
<connect gate="G$1" pin="SDA" pad="P$23"/>
<connect gate="G$1" pin="VDD" pad="P$24"/>
<connect gate="G$1" pin="VSS" pad="P$12"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Boards" urn="urn:adsk.eagle:library:12590577">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
This library contains footprints for SparkFun breakout boards, microcontrollers (Arduino, Particle, Teensy, etc.),  breadboards, non-RF modules, etc.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="SPARKFUN_PRO_MICRO" urn="urn:adsk.eagle:footprint:12590637/1" library_version="1">
<description>&lt;h3&gt;SparkFun Pro Mico Footprint (with USB connector)&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 24&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1"&lt;/li&gt;
&lt;li&gt;Area:0.7x1.3"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;SparkFun Pro Micro&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-8.89" y1="16.51" x2="-8.89" y2="-16.51" width="0.127" layer="51"/>
<wire x1="-8.89" y1="-16.51" x2="8.89" y2="-16.51" width="0.127" layer="51"/>
<wire x1="8.89" y1="-16.51" x2="8.89" y2="16.51" width="0.127" layer="51"/>
<wire x1="8.89" y1="16.51" x2="-8.89" y2="16.51" width="0.127" layer="51"/>
<wire x1="-3.81" y1="16.51" x2="-3.81" y2="17.78" width="0.127" layer="51"/>
<wire x1="-3.81" y1="17.78" x2="3.81" y2="17.78" width="0.127" layer="51"/>
<wire x1="3.81" y1="17.78" x2="3.81" y2="16.51" width="0.127" layer="51"/>
<pad name="1" x="-7.62" y="12.7" drill="1.016" diameter="1.8796"/>
<pad name="2" x="-7.62" y="10.16" drill="1.016" diameter="1.8796"/>
<pad name="3" x="-7.62" y="7.62" drill="1.016" diameter="1.8796"/>
<pad name="4" x="-7.62" y="5.08" drill="1.016" diameter="1.8796"/>
<pad name="5" x="-7.62" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="6" x="-7.62" y="0" drill="1.016" diameter="1.8796"/>
<pad name="7" x="-7.62" y="-2.54" drill="1.016" diameter="1.8796"/>
<pad name="8" x="-7.62" y="-5.08" drill="1.016" diameter="1.8796"/>
<pad name="9" x="-7.62" y="-7.62" drill="1.016" diameter="1.8796"/>
<pad name="10" x="-7.62" y="-10.16" drill="1.016" diameter="1.8796"/>
<pad name="11" x="-7.62" y="-12.7" drill="1.016" diameter="1.8796"/>
<pad name="12" x="-7.62" y="-15.24" drill="1.016" diameter="1.8796"/>
<pad name="13" x="7.62" y="-15.24" drill="1.016" diameter="1.8796"/>
<pad name="14" x="7.62" y="-12.7" drill="1.016" diameter="1.8796"/>
<pad name="15" x="7.62" y="-10.16" drill="1.016" diameter="1.8796"/>
<pad name="16" x="7.62" y="-7.62" drill="1.016" diameter="1.8796"/>
<pad name="17" x="7.62" y="-5.08" drill="1.016" diameter="1.8796"/>
<pad name="18" x="7.62" y="-2.54" drill="1.016" diameter="1.8796"/>
<pad name="19" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<pad name="20" x="7.62" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="21" x="7.62" y="5.08" drill="1.016" diameter="1.8796"/>
<pad name="22" x="7.62" y="7.62" drill="1.016" diameter="1.8796"/>
<pad name="23" x="7.62" y="10.16" drill="1.016" diameter="1.8796"/>
<pad name="24" x="7.62" y="12.7" drill="1.016" diameter="1.8796"/>
<text x="0" y="18.034" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-16.764" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<text x="-1.27" y="16.51" size="0.8128" layer="51" font="vector" ratio="20">USB</text>
</package>
</packages>
<packages3d>
<package3d name="SPARKFUN_PRO_MICRO" urn="urn:adsk.eagle:package:12590700/1" type="box" library_version="1">
<description>&lt;h3&gt;SparkFun Pro Mico Footprint (with USB connector)&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 24&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1"&lt;/li&gt;
&lt;li&gt;Area:0.7x1.3"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;SparkFun Pro Micro&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SPARKFUN_PRO_MICRO"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="SPARKFUN_PRO_MICRO" urn="urn:adsk.eagle:symbol:12590598/1" library_version="1">
<description>&lt;h3&gt;SparkFun Pro Micro&lt;/h3&gt;
&lt;p&gt;3.3V/5V at 8MHz/16MHz. Does not require a USB to serial converter board for programming.  &lt;/p&gt;</description>
<wire x1="-7.62" y1="17.78" x2="-7.62" y2="-15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="-15.24" x2="10.16" y2="17.78" width="0.254" layer="94"/>
<wire x1="10.16" y1="17.78" x2="-7.62" y2="17.78" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-15.24" x2="10.16" y2="-15.24" width="0.254" layer="94"/>
<text x="-7.62" y="18.542" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="-10.16" y="5.08" length="short"/>
<pin name="*3" x="-10.16" y="2.54" length="short"/>
<pin name="4" x="-10.16" y="0" length="short"/>
<pin name="*5" x="-10.16" y="-2.54" length="short"/>
<pin name="*6" x="-10.16" y="-5.08" length="short"/>
<pin name="7" x="-10.16" y="-7.62" length="short"/>
<pin name="8" x="-10.16" y="-10.16" length="short"/>
<pin name="*9" x="-10.16" y="-12.7" length="short"/>
<pin name="*10" x="12.7" y="-12.7" length="short" rot="R180"/>
<pin name="11(MOSI)" x="12.7" y="-10.16" length="short" rot="R180"/>
<pin name="12(MISO)" x="12.7" y="-7.62" length="short" rot="R180"/>
<pin name="13(SCK)" x="12.7" y="-5.08" length="short" rot="R180"/>
<pin name="A0" x="12.7" y="-2.54" length="short" rot="R180"/>
<pin name="A1" x="12.7" y="0" length="short" rot="R180"/>
<pin name="A2" x="12.7" y="2.54" length="short" rot="R180"/>
<pin name="A3" x="12.7" y="5.08" length="short" rot="R180"/>
<pin name="GND" x="-10.16" y="7.62" length="short" direction="pwr"/>
<pin name="GND@2" x="12.7" y="12.7" length="short" direction="pwr" rot="R180"/>
<pin name="RAW" x="12.7" y="15.24" length="short" direction="pwr" rot="R180"/>
<pin name="GND@1" x="-10.16" y="10.16" length="short" direction="pwr"/>
<pin name="RESET" x="12.7" y="10.16" length="short" rot="R180"/>
<pin name="RXI" x="-10.16" y="12.7" length="short"/>
<pin name="TXO" x="-10.16" y="15.24" length="short"/>
<pin name="VCC" x="12.7" y="7.62" length="short" direction="pwr" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SPARKFUN_PRO_MICRO" urn="urn:adsk.eagle:component:12590751/1" prefix="B" library_version="1">
<description>&lt;h3&gt;SparkFun Pro Micro &lt;/h3&gt;
&lt;p&gt;Atmega32U4 compatible footprint.&lt;/p&gt;

&lt;b&gt;&lt;p&gt;SparkFun Products:&lt;/b&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/12587”&gt;3.3V/8MHz&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/12640”&gt;5V/16MHz&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="SPARKFUN_PRO_MICRO" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="SPARKFUN_PRO_MICRO">
<connects>
<connect gate="G$1" pin="*10" pad="13"/>
<connect gate="G$1" pin="*3" pad="6"/>
<connect gate="G$1" pin="*5" pad="8"/>
<connect gate="G$1" pin="*6" pad="9"/>
<connect gate="G$1" pin="*9" pad="12"/>
<connect gate="G$1" pin="11(MOSI)" pad="14"/>
<connect gate="G$1" pin="12(MISO)" pad="15"/>
<connect gate="G$1" pin="13(SCK)" pad="16"/>
<connect gate="G$1" pin="2" pad="5"/>
<connect gate="G$1" pin="4" pad="7"/>
<connect gate="G$1" pin="7" pad="10"/>
<connect gate="G$1" pin="8" pad="11"/>
<connect gate="G$1" pin="A0" pad="17"/>
<connect gate="G$1" pin="A1" pad="18"/>
<connect gate="G$1" pin="A2" pad="19"/>
<connect gate="G$1" pin="A3" pad="20"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="GND@1" pad="3"/>
<connect gate="G$1" pin="GND@2" pad="23"/>
<connect gate="G$1" pin="RAW" pad="24"/>
<connect gate="G$1" pin="RESET" pad="22"/>
<connect gate="G$1" pin="RXI" pad="2"/>
<connect gate="G$1" pin="TXO" pad="1"/>
<connect gate="G$1" pin="VCC" pad="21"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12590700/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="diode" urn="urn:adsk.eagle:library:210">
<description>&lt;b&gt;Diodes&lt;/b&gt;&lt;p&gt;
Based on the following sources:
&lt;ul&gt;
&lt;li&gt;Motorola : www.onsemi.com
&lt;li&gt;Fairchild : www.fairchildsemi.com
&lt;li&gt;Philips : www.semiconductors.com
&lt;li&gt;Vishay : www.vishay.de
&lt;/ul&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SOT23" urn="urn:adsk.eagle:footprint:43155/1" library_version="8">
<description>&lt;B&gt;DIODE&lt;/B&gt;</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.1524" x2="-1.4224" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="-1.4224" y1="0.6604" x2="-0.8636" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.1524" width="0.1524" layer="21"/>
<wire x1="0.8636" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="21"/>
<smd name="3" x="0" y="1.1" dx="1" dy="1.4" layer="1"/>
<smd name="2" x="0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<smd name="1" x="-0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="SOT23" urn="urn:adsk.eagle:package:43389/2" type="model" library_version="8">
<description>DIODE</description>
<packageinstances>
<packageinstance name="SOT23"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="SDD_AKKA" urn="urn:adsk.eagle:symbol:43242/2" library_version="8">
<wire x1="-3.81" y1="1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-3.81" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-1.27" x2="-3.81" y2="0" width="0.254" layer="94"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="1.27" width="0.254" layer="94"/>
<wire x1="3.81" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="3.81" y2="1.27" width="0.254" layer="94"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="0" width="0.254" layer="94"/>
<wire x1="3.81" y1="0" x2="3.81" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-0.635" y1="-1.016" x2="-0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.905" y1="1.016" x2="-1.905" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.905" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="-1.016" x2="1.905" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0.635" y1="1.016" x2="0.635" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="0.635" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0" x2="3.81" y2="0" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="0.127" width="0.4064" layer="94"/>
<text x="0.762" y="2.0066" size="1.778" layer="95">&gt;NAME</text>
<text x="-4.318" y="-3.9624" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A1" x="-5.08" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="A2" x="5.08" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="CC" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BAT54C" urn="urn:adsk.eagle:component:43609/4" prefix="D" library_version="8">
<description>&lt;b&gt;Schottky Diodes&lt;/b&gt;&lt;p&gt;
Source: Fairchild .. BAT54.pdf</description>
<gates>
<gate name="G$1" symbol="SDD_AKKA" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23">
<connects>
<connect gate="G$1" pin="A1" pad="1"/>
<connect gate="G$1" pin="A2" pad="2"/>
<connect gate="G$1" pin="CC" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:43389/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="6" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="GOLD_ORB_SM1" library="oshw" deviceset="OSHWLOGO" device="LOGO5MM"/>
<part name="C1" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="C2" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="C3" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="C4" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="C5" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="C6" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="C7" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="C8" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="C9" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="D1" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="D2" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="D3" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="D4" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="D5" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="D6" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="D7" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="D8" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="D9" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="E1" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="E2" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="E3" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="E4" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="E5" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="E6" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="E7" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="E8" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="E9" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="A2" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="A3" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="A4" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="A5" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="A6" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="A7" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="A8" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="A9" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="B1" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="S2" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="B3" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="B4" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="B5" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="B6" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="B7" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="B8" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="B9" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="B11" library="Switches" library_urn="urn:adsk.eagle:library:14854419" deviceset="1-1825027-4" device="" package3d_urn="urn:adsk.eagle:package:14854601/2" value="s"/>
<part name="C10" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="D10" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="E10" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="A10" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="B10" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="D11" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="E11" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="C11" library="Switches" library_urn="urn:adsk.eagle:library:14854419" deviceset="1-1825027-4" device="" package3d_urn="urn:adsk.eagle:package:14854601/2" value="s"/>
<part name="C_P1" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="PM" device=""/>
<part name="BAT-" library="wirepad" library_urn="urn:adsk.eagle:library:412" deviceset="WIREPAD" device="SMD5-1,8" package3d_urn="urn:adsk.eagle:package:30844/1"/>
<part name="BAT+" library="wirepad" library_urn="urn:adsk.eagle:library:412" deviceset="WIREPAD" device="SMD5-1,8" package3d_urn="urn:adsk.eagle:package:30844/1"/>
<part name="J1" library="Wurth_Connectors_WR-FPC" library_urn="urn:adsk.eagle:library:15778639" deviceset="6861XX14422_68611614422" device="" package3d_urn="urn:adsk.eagle:package:15743819/2"/>
<part name="JP2" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-2X20" device="" package3d_urn="urn:adsk.eagle:package:22443/2" value="Pi Header"/>
<part name="JP1" library="Connector" library_urn="urn:adsk.eagle:library:16378166" deviceset="PINHD-2X10" device="" package3d_urn="urn:adsk.eagle:package:22405/2"/>
<part name="AO4622" library="zetex" library_urn="urn:adsk.eagle:library:418" deviceset="PN_MOSFET-" device="SO8" package3d_urn="urn:adsk.eagle:package:30987/2"/>
<part name="R0_100K" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2"/>
<part name="R1_100K" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2"/>
<part name="POW" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="U$3" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="BOOST" device=""/>
<part name="G.5V.5V" library="Connector" library_urn="urn:adsk.eagle:library:16378166" deviceset="PINHD-1X3" device="" package3d_urn="urn:adsk.eagle:package:22458/2"/>
<part name="R5_100K" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2"/>
<part name="STEMMA1" library="SparkFun-Connectors" library_urn="urn:adsk.eagle:library:513" deviceset="QWIIC_CONNECTOR" device="JS-1MM" package3d_urn="urn:adsk.eagle:package:38096/1" override_package3d_urn="urn:adsk.eagle:package:16855367/2" override_package_urn="urn:adsk.eagle:footprint:37714/1"/>
<part name="IC1" library="adafruit" library_urn="urn:adsk.eagle:library:420" deviceset="DS3231" device="/SO" package3d_urn="urn:adsk.eagle:package:6240754/1"/>
<part name="GND" library="wirepad" library_urn="urn:adsk.eagle:library:412" deviceset="SMD2" device="" package3d_urn="urn:adsk.eagle:package:30839/1"/>
<part name="B_BATTERY+" library="wirepad" library_urn="urn:adsk.eagle:library:412" deviceset="SMD2" device="" package3d_urn="urn:adsk.eagle:package:30839/1"/>
<part name="R3_10K" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2"/>
<part name="R4_10K" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2"/>
<part name="Q1" library="transistor-small-signal" library_urn="urn:adsk.eagle:library:401" deviceset="BSS84" device="" package3d_urn="urn:adsk.eagle:package:28738/2"/>
<part name="SI2301" library="transistor-small-signal" library_urn="urn:adsk.eagle:library:401" deviceset="BSS84" device="" package3d_urn="urn:adsk.eagle:package:28738/2"/>
<part name="U$2" library="pcf8575c" deviceset="PCF8575C" device=""/>
<part name="B2" library="SparkFun-Boards" library_urn="urn:adsk.eagle:library:12590577" deviceset="SPARKFUN_PRO_MICRO" device="" package3d_urn="urn:adsk.eagle:package:12590700/1"/>
<part name="R8_100K" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2"/>
<part name="Q3" library="transistor-small-signal" library_urn="urn:adsk.eagle:library:401" deviceset="BSS84" device="" package3d_urn="urn:adsk.eagle:package:28738/2"/>
<part name="R7_10K" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2"/>
<part name="R6_10K" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2"/>
<part name="C1_100NF" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="C-EU" device="C1206" package3d_urn="urn:adsk.eagle:package:23618/2"/>
<part name="C3_100NF" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="C-EU" device="C1206" package3d_urn="urn:adsk.eagle:package:23618/2"/>
<part name="C2_100NF" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="C-EU" device="C1206" package3d_urn="urn:adsk.eagle:package:23618/2"/>
<part name="J5" library="Wurth_Connectors_WR-FPC" library_urn="urn:adsk.eagle:library:15778639" deviceset="6861XX14422_68611014422" device="" package3d_urn="urn:adsk.eagle:package:15743822/2"/>
<part name="D12" library="diode" library_urn="urn:adsk.eagle:library:210" deviceset="BAT54C" device="" package3d_urn="urn:adsk.eagle:package:43389/2"/>
<part name="J3" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="DOCKING_CONNECTOR" device="" package3d_urn="urn:adsk.eagle:package:14805565/2"/>
<part name="R8_100K1" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2"/>
<part name="Q2" library="transistor-small-signal" library_urn="urn:adsk.eagle:library:401" deviceset="BSS84" device="" package3d_urn="urn:adsk.eagle:package:28738/2"/>
<part name="U$4" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="GYRO" device="GYRO_SMD"/>
<part name="R8_100K3" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2"/>
<part name="Q5" library="transistor-small-signal" library_urn="urn:adsk.eagle:library:401" deviceset="BSS84" device="" package3d_urn="urn:adsk.eagle:package:28738/2"/>
<part name="U$5" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="BME280" device=""/>
<part name="R8_100K2" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2"/>
<part name="Q4" library="transistor-small-signal" library_urn="urn:adsk.eagle:library:401" deviceset="BSS84" device="" package3d_urn="urn:adsk.eagle:package:28738/2"/>
<part name="U$1" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="MUTANTC" device="MUTANTC_TEXT+LOGO"/>
<part name="PAD1" library="wirepad" library_urn="urn:adsk.eagle:library:412" deviceset="WIREPAD" device="1,6/0,8" package3d_urn="urn:adsk.eagle:package:30830/1"/>
<part name="PAD2" library="wirepad" library_urn="urn:adsk.eagle:library:412" deviceset="WIREPAD" device="1,6/0,8" package3d_urn="urn:adsk.eagle:package:30830/1"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="GOLD_ORB_SM1" gate="G$1" x="-218.44" y="-38.1" smashed="yes"/>
<instance part="C1" gate="G$1" x="35.56" y="-121.92" smashed="yes">
<attribute name="NAME" x="41.91" y="-120.65" size="1.778" layer="95"/>
<attribute name="VALUE" x="41.91" y="-124.46" size="1.778" layer="96"/>
</instance>
<instance part="C2" gate="G$1" x="50.8" y="-121.92" smashed="yes">
<attribute name="NAME" x="57.15" y="-120.65" size="1.778" layer="95"/>
<attribute name="VALUE" x="57.15" y="-124.46" size="1.778" layer="96"/>
</instance>
<instance part="C3" gate="G$1" x="68.58" y="-121.92" smashed="yes">
<attribute name="NAME" x="74.93" y="-120.65" size="1.778" layer="95"/>
<attribute name="VALUE" x="74.93" y="-124.46" size="1.778" layer="96"/>
</instance>
<instance part="C4" gate="G$1" x="86.36" y="-121.92" smashed="yes">
<attribute name="NAME" x="92.71" y="-120.65" size="1.778" layer="95"/>
<attribute name="VALUE" x="92.71" y="-124.46" size="1.778" layer="96"/>
</instance>
<instance part="C5" gate="G$1" x="106.68" y="-121.92" smashed="yes">
<attribute name="NAME" x="113.03" y="-120.65" size="1.778" layer="95"/>
<attribute name="VALUE" x="113.03" y="-124.46" size="1.778" layer="96"/>
</instance>
<instance part="C6" gate="G$1" x="124.46" y="-121.92" smashed="yes">
<attribute name="NAME" x="130.81" y="-120.65" size="1.778" layer="95"/>
<attribute name="VALUE" x="130.81" y="-124.46" size="1.778" layer="96"/>
</instance>
<instance part="C7" gate="G$1" x="142.24" y="-121.92" smashed="yes">
<attribute name="NAME" x="148.59" y="-120.65" size="1.778" layer="95"/>
<attribute name="VALUE" x="148.59" y="-124.46" size="1.778" layer="96"/>
</instance>
<instance part="C8" gate="G$1" x="160.02" y="-121.92" smashed="yes">
<attribute name="NAME" x="166.37" y="-120.65" size="1.778" layer="95"/>
<attribute name="VALUE" x="166.37" y="-124.46" size="1.778" layer="96"/>
</instance>
<instance part="C9" gate="G$1" x="177.8" y="-121.92" smashed="yes">
<attribute name="NAME" x="184.15" y="-120.65" size="1.778" layer="95"/>
<attribute name="VALUE" x="184.15" y="-124.46" size="1.778" layer="96"/>
</instance>
<instance part="D1" gate="G$1" x="35.56" y="-137.16" smashed="yes">
<attribute name="NAME" x="41.91" y="-135.89" size="1.778" layer="95"/>
<attribute name="VALUE" x="41.91" y="-139.7" size="1.778" layer="96"/>
</instance>
<instance part="D2" gate="G$1" x="50.8" y="-137.16" smashed="yes">
<attribute name="NAME" x="57.15" y="-135.89" size="1.778" layer="95"/>
<attribute name="VALUE" x="57.15" y="-139.7" size="1.778" layer="96"/>
</instance>
<instance part="D3" gate="G$1" x="68.58" y="-137.16" smashed="yes">
<attribute name="NAME" x="74.93" y="-135.89" size="1.778" layer="95"/>
<attribute name="VALUE" x="74.93" y="-139.7" size="1.778" layer="96"/>
</instance>
<instance part="D4" gate="G$1" x="86.36" y="-137.16" smashed="yes">
<attribute name="NAME" x="92.71" y="-135.89" size="1.778" layer="95"/>
<attribute name="VALUE" x="92.71" y="-139.7" size="1.778" layer="96"/>
</instance>
<instance part="D5" gate="G$1" x="106.68" y="-137.16" smashed="yes">
<attribute name="NAME" x="113.03" y="-135.89" size="1.778" layer="95"/>
<attribute name="VALUE" x="113.03" y="-139.7" size="1.778" layer="96"/>
</instance>
<instance part="D6" gate="G$1" x="124.46" y="-137.16" smashed="yes">
<attribute name="NAME" x="130.81" y="-135.89" size="1.778" layer="95"/>
<attribute name="VALUE" x="130.81" y="-139.7" size="1.778" layer="96"/>
</instance>
<instance part="D7" gate="G$1" x="142.24" y="-137.16" smashed="yes">
<attribute name="NAME" x="148.59" y="-135.89" size="1.778" layer="95"/>
<attribute name="VALUE" x="148.59" y="-139.7" size="1.778" layer="96"/>
</instance>
<instance part="D8" gate="G$1" x="160.02" y="-137.16" smashed="yes">
<attribute name="NAME" x="166.37" y="-135.89" size="1.778" layer="95"/>
<attribute name="VALUE" x="166.37" y="-139.7" size="1.778" layer="96"/>
</instance>
<instance part="D9" gate="G$1" x="177.8" y="-137.16" smashed="yes">
<attribute name="NAME" x="184.15" y="-135.89" size="1.778" layer="95"/>
<attribute name="VALUE" x="184.15" y="-139.7" size="1.778" layer="96"/>
</instance>
<instance part="E1" gate="G$1" x="35.56" y="-154.94" smashed="yes">
<attribute name="NAME" x="41.91" y="-153.67" size="1.778" layer="95"/>
<attribute name="VALUE" x="41.91" y="-157.48" size="1.778" layer="96"/>
</instance>
<instance part="E2" gate="G$1" x="50.8" y="-154.94" smashed="yes">
<attribute name="NAME" x="57.15" y="-153.67" size="1.778" layer="95"/>
<attribute name="VALUE" x="57.15" y="-157.48" size="1.778" layer="96"/>
</instance>
<instance part="E3" gate="G$1" x="68.58" y="-154.94" smashed="yes">
<attribute name="NAME" x="74.93" y="-153.67" size="1.778" layer="95"/>
<attribute name="VALUE" x="74.93" y="-157.48" size="1.778" layer="96"/>
</instance>
<instance part="E4" gate="G$1" x="86.36" y="-154.94" smashed="yes">
<attribute name="NAME" x="92.71" y="-153.67" size="1.778" layer="95"/>
<attribute name="VALUE" x="92.71" y="-157.48" size="1.778" layer="96"/>
</instance>
<instance part="E5" gate="G$1" x="106.68" y="-154.94" smashed="yes">
<attribute name="NAME" x="113.03" y="-153.67" size="1.778" layer="95"/>
<attribute name="VALUE" x="113.03" y="-157.48" size="1.778" layer="96"/>
</instance>
<instance part="E6" gate="G$1" x="124.46" y="-154.94" smashed="yes">
<attribute name="NAME" x="130.81" y="-153.67" size="1.778" layer="95"/>
<attribute name="VALUE" x="130.81" y="-157.48" size="1.778" layer="96"/>
</instance>
<instance part="E7" gate="G$1" x="142.24" y="-154.94" smashed="yes">
<attribute name="NAME" x="148.59" y="-153.67" size="1.778" layer="95"/>
<attribute name="VALUE" x="148.59" y="-157.48" size="1.778" layer="96"/>
</instance>
<instance part="E8" gate="G$1" x="160.02" y="-154.94" smashed="yes">
<attribute name="NAME" x="166.37" y="-153.67" size="1.778" layer="95"/>
<attribute name="VALUE" x="166.37" y="-157.48" size="1.778" layer="96"/>
</instance>
<instance part="E9" gate="G$1" x="175.26" y="-154.94" smashed="yes">
<attribute name="VALUE" x="181.61" y="-157.48" size="1.778" layer="96"/>
</instance>
<instance part="A2" gate="G$1" x="50.8" y="-86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="48.26" y="-88.9" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="53.34" y="-88.9" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="A3" gate="G$1" x="68.58" y="-86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="66.04" y="-88.9" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="71.12" y="-88.9" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="A4" gate="G$1" x="86.36" y="-86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="83.82" y="-88.9" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="88.9" y="-88.9" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="A5" gate="G$1" x="106.68" y="-86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="104.14" y="-88.9" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="109.22" y="-88.9" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="A6" gate="G$1" x="121.92" y="-86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="119.38" y="-88.9" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="124.46" y="-88.9" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="A7" gate="G$1" x="139.7" y="-86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="137.16" y="-88.9" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="142.24" y="-88.9" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="A8" gate="G$1" x="157.48" y="-86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="154.94" y="-88.9" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="160.02" y="-88.9" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="A9" gate="G$1" x="175.26" y="-86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="172.72" y="-88.9" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="177.8" y="-88.9" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="B1" gate="G$1" x="38.1" y="-104.14" smashed="yes" rot="R90">
<attribute name="NAME" x="35.56" y="-106.68" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="40.64" y="-106.68" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="S2" gate="G$1" x="50.8" y="-104.14" smashed="yes" rot="R90">
<attribute name="NAME" x="48.26" y="-106.68" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="53.34" y="-106.68" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="B3" gate="G$1" x="68.58" y="-104.14" smashed="yes" rot="R90">
<attribute name="NAME" x="66.04" y="-106.68" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="71.12" y="-106.68" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="B4" gate="G$1" x="86.36" y="-104.14" smashed="yes" rot="R90">
<attribute name="NAME" x="83.82" y="-106.68" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="88.9" y="-106.68" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="B5" gate="G$1" x="106.68" y="-104.14" smashed="yes" rot="R90">
<attribute name="NAME" x="104.14" y="-106.68" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="109.22" y="-106.68" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="B6" gate="G$1" x="121.92" y="-104.14" smashed="yes" rot="R90">
<attribute name="NAME" x="119.38" y="-106.68" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="124.46" y="-106.68" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="B7" gate="G$1" x="139.7" y="-104.14" smashed="yes" rot="R90">
<attribute name="NAME" x="137.16" y="-106.68" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="142.24" y="-106.68" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="B8" gate="G$1" x="157.48" y="-104.14" smashed="yes" rot="R90">
<attribute name="NAME" x="154.94" y="-106.68" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="160.02" y="-106.68" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="B9" gate="G$1" x="175.26" y="-104.14" smashed="yes" rot="R90">
<attribute name="NAME" x="172.72" y="-106.68" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="177.8" y="-106.68" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="B11" gate="A" x="218.44" y="-106.68" smashed="yes">
<attribute name="NAME" x="218.44" y="-105.156" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="218.44" y="-107.188" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="C10" gate="G$1" x="198.12" y="-121.92" smashed="yes">
<attribute name="NAME" x="204.47" y="-120.65" size="1.778" layer="95"/>
<attribute name="VALUE" x="204.47" y="-124.46" size="1.778" layer="96"/>
</instance>
<instance part="D10" gate="G$1" x="198.12" y="-137.16" smashed="yes">
<attribute name="NAME" x="204.47" y="-135.89" size="1.778" layer="95"/>
<attribute name="VALUE" x="204.47" y="-139.7" size="1.778" layer="96"/>
</instance>
<instance part="E10" gate="G$1" x="198.12" y="-154.94" smashed="yes">
<attribute name="VALUE" x="204.47" y="-157.48" size="1.778" layer="96"/>
</instance>
<instance part="A10" gate="G$1" x="195.58" y="-86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="193.04" y="-88.9" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="198.12" y="-88.9" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="B10" gate="G$1" x="195.58" y="-104.14" smashed="yes" rot="R90">
<attribute name="NAME" x="193.04" y="-106.68" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="198.12" y="-106.68" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="D11" gate="G$1" x="220.98" y="-137.16" smashed="yes">
<attribute name="NAME" x="227.33" y="-135.89" size="1.778" layer="95"/>
<attribute name="VALUE" x="227.33" y="-139.7" size="1.778" layer="96"/>
</instance>
<instance part="E11" gate="G$1" x="220.98" y="-154.94" smashed="yes">
<attribute name="VALUE" x="227.33" y="-157.48" size="1.778" layer="96"/>
</instance>
<instance part="C11" gate="A" x="218.44" y="-119.38" smashed="yes">
<attribute name="NAME" x="218.44" y="-117.856" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="218.44" y="-119.888" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="C_P1" gate="G$1" x="203.2" y="-200.66" smashed="yes" rot="R180"/>
<instance part="BAT-" gate="G$1" x="177.8" y="-185.42" smashed="yes">
<attribute name="NAME" x="176.657" y="-183.5658" size="1.778" layer="95"/>
</instance>
<instance part="BAT+" gate="G$1" x="177.8" y="-177.8" smashed="yes">
<attribute name="NAME" x="176.657" y="-175.9458" size="1.778" layer="95"/>
</instance>
<instance part="J1" gate="G$1" x="-218.44" y="-73.66" smashed="yes" rot="MR270">
<attribute name="NAME" x="-214.868" y="-96.12" size="1.27" layer="95" rot="MR270"/>
<attribute name="VALUE" x="-217.17" y="-93.68" size="1.27" layer="96" rot="MR270"/>
</instance>
<instance part="JP2" gate="A" x="-27.94" y="-175.26" smashed="yes">
<attribute name="VALUE" x="-34.29" y="-205.74" size="1.778" layer="96"/>
</instance>
<instance part="JP1" gate="A" x="-30.48" y="-96.52" smashed="yes">
<attribute name="NAME" x="-36.83" y="-83.185" size="1.778" layer="95"/>
<attribute name="VALUE" x="-36.83" y="-114.3" size="1.778" layer="96"/>
</instance>
<instance part="AO4622" gate="2" x="104.14" y="-187.96" smashed="yes" rot="MR180">
<attribute name="VALUE" x="106.68" y="-187.96" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="106.68" y="-187.96" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="AO4622" gate="1" x="73.66" y="-200.66" smashed="yes">
<attribute name="VALUE" x="76.2" y="-200.66" size="1.778" layer="96"/>
<attribute name="NAME" x="60.96" y="-198.12" size="1.778" layer="95"/>
</instance>
<instance part="R0_100K" gate="G$1" x="60.96" y="-210.82" smashed="yes" rot="R90">
<attribute name="NAME" x="59.4614" y="-214.63" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="64.262" y="-214.63" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R1_100K" gate="G$1" x="91.44" y="-180.34" smashed="yes" rot="R90">
<attribute name="NAME" x="89.9414" y="-184.15" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="94.742" y="-184.15" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="POW" gate="G$1" x="88.9" y="-203.2" smashed="yes" rot="R90">
<attribute name="NAME" x="86.36" y="-205.74" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="91.44" y="-205.74" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="U$3" gate="G$1" x="139.7" y="-208.28" smashed="yes"/>
<instance part="G.5V.5V" gate="A" x="-220.98" y="-177.8" smashed="yes" rot="R180">
<attribute name="NAME" x="-214.63" y="-183.515" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="R5_100K" gate="G$1" x="208.28" y="-38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="209.7786" y="-34.29" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="204.978" y="-34.29" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="STEMMA1" gate="J1" x="-218.44" y="-200.66" smashed="yes">
<attribute name="VALUE" x="-223.52" y="-205.994" size="1.778" layer="96" font="vector" align="top-left"/>
<attribute name="NAME" x="-218.44" y="-192.786" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="IC1" gate="G$1" x="-132.08" y="-104.14" smashed="yes">
<attribute name="NAME" x="-121.92" y="-88.9" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="GND" gate="1" x="-218.44" y="-162.56" smashed="yes">
<attribute name="NAME" x="-219.583" y="-160.7058" size="1.778" layer="95"/>
<attribute name="VALUE" x="-219.583" y="-165.862" size="1.778" layer="96"/>
</instance>
<instance part="B_BATTERY+" gate="1" x="-218.44" y="-157.48" smashed="yes">
<attribute name="NAME" x="-219.583" y="-155.6258" size="1.778" layer="95"/>
<attribute name="VALUE" x="-219.583" y="-160.782" size="1.778" layer="96"/>
</instance>
<instance part="R3_10K" gate="G$1" x="-109.22" y="-86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="-110.7186" y="-90.17" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-105.918" y="-90.17" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R4_10K" gate="G$1" x="-96.52" y="-86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="-98.0186" y="-90.17" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-93.218" y="-90.17" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="Q1" gate="G$1" x="215.9" y="-30.48" smashed="yes" rot="MR180">
<attribute name="NAME" x="218.44" y="-30.48" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="218.44" y="-27.94" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="SI2301" gate="G$1" x="246.38" y="-30.48" smashed="yes" rot="MR180">
<attribute name="NAME" x="248.92" y="-30.48" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="248.92" y="-27.94" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="U$2" gate="G$1" x="-132.08" y="-139.7" smashed="yes">
<attribute name="NAME" x="-129.54" y="-149.86" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="B2" gate="G$1" x="-116.84" y="-205.74" smashed="yes">
<attribute name="NAME" x="-124.46" y="-187.198" size="1.778" layer="95"/>
<attribute name="VALUE" x="-124.46" y="-223.52" size="1.778" layer="96"/>
</instance>
<instance part="R8_100K" gate="G$1" x="167.64" y="-38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="169.1386" y="-34.29" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="164.338" y="-34.29" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="Q3" gate="G$1" x="175.26" y="-30.48" smashed="yes" rot="MR180">
<attribute name="NAME" x="177.8" y="-30.48" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="177.8" y="-27.94" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="R7_10K" gate="G$1" x="-86.36" y="-162.56" smashed="yes" rot="R270">
<attribute name="NAME" x="-84.8614" y="-158.75" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="-89.662" y="-158.75" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R6_10K" gate="G$1" x="-93.98" y="-142.24" smashed="yes" rot="R90">
<attribute name="NAME" x="-95.4786" y="-146.05" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-90.678" y="-146.05" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C1_100NF" gate="G$1" x="-152.4" y="-88.9" smashed="yes" rot="R180">
<attribute name="NAME" x="-153.924" y="-89.281" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-153.924" y="-84.201" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C3_100NF" gate="G$1" x="-101.6" y="-165.1" smashed="yes" rot="R180">
<attribute name="NAME" x="-103.124" y="-165.481" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-103.124" y="-160.401" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C2_100NF" gate="G$1" x="-99.06" y="-106.68" smashed="yes" rot="R180">
<attribute name="NAME" x="-100.584" y="-107.061" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-100.584" y="-101.981" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="J5" gate="G$1" x="-218.44" y="-114.3" smashed="yes" rot="R90">
<attribute name="NAME" x="-219.948" y="-102" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="-217.17" y="-101.9" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="D12" gate="G$1" x="88.9" y="-193.04" smashed="yes" rot="R180">
<attribute name="NAME" x="88.138" y="-195.0466" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="93.218" y="-189.0776" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="J3" gate="G$1" x="-27.94" y="-129.54" smashed="yes" rot="R90">
<attribute name="NAME" x="-29.018" y="-139.5" size="1.016" layer="95" rot="R90" align="bottom-right"/>
<attribute name="VALUE" x="-26.36" y="-139.45" size="1.016" layer="96" rot="R90" align="bottom-right"/>
</instance>
<instance part="R8_100K1" gate="G$1" x="132.08" y="-38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="133.5786" y="-34.29" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="128.778" y="-34.29" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="Q2" gate="G$1" x="139.7" y="-30.48" smashed="yes" rot="MR180">
<attribute name="NAME" x="142.24" y="-30.48" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="142.24" y="-27.94" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="U$4" gate="G$1" x="-106.68" y="-38.1" smashed="yes"/>
<instance part="R8_100K3" gate="G$1" x="91.44" y="-38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="92.9386" y="-34.29" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="88.138" y="-34.29" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="Q5" gate="G$1" x="99.06" y="-30.48" smashed="yes" rot="MR180">
<attribute name="NAME" x="101.6" y="-30.48" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="101.6" y="-27.94" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="U$5" gate="G$1" x="-43.18" y="-30.48" smashed="yes"/>
<instance part="R8_100K2" gate="G$1" x="48.26" y="-38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="49.7586" y="-34.29" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="44.958" y="-34.29" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="Q4" gate="G$1" x="55.88" y="-30.48" smashed="yes" rot="MR180">
<attribute name="NAME" x="58.42" y="-30.48" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="58.42" y="-27.94" size="1.778" layer="96" rot="MR180" align="center-left"/>
</instance>
<instance part="U$1" gate="G$1" x="-218.44" y="-22.86" smashed="yes"/>
<instance part="PAD1" gate="G$1" x="-218.44" y="-139.7" smashed="yes"/>
<instance part="PAD2" gate="G$1" x="-218.44" y="-137.16" smashed="yes"/>
</instances>
<busses>
</busses>
<nets>
<net name="3V" class="0">
<segment>
<pinref part="JP2" gate="A" pin="17"/>
<wire x1="-30.48" y1="-172.72" x2="-38.1" y2="-172.72" width="0.1524" layer="91"/>
<label x="-38.1" y="-172.72" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JP2" gate="A" pin="1"/>
<wire x1="-30.48" y1="-152.4" x2="-38.1" y2="-152.4" width="0.1524" layer="91"/>
<label x="-38.1" y="-152.4" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-213.36" y1="-86.36" x2="-208.28" y2="-86.36" width="0.1524" layer="91"/>
<label x="-208.28" y="-86.36" size="1.778" layer="95" xref="yes"/>
<pinref part="J1" gate="G$1" pin="3"/>
<junction x="-213.36" y="-86.36"/>
</segment>
<segment>
<wire x1="-213.36" y1="-78.74" x2="-193.04" y2="-78.74" width="0.1524" layer="91"/>
<label x="-193.04" y="-78.74" size="1.778" layer="95" xref="yes"/>
<pinref part="J1" gate="G$1" pin="6"/>
<junction x="-213.36" y="-78.74"/>
</segment>
<segment>
<wire x1="139.7" y1="-35.56" x2="139.7" y2="-45.72" width="0.1524" layer="91"/>
<label x="139.7" y="-48.26" size="1.778" layer="95" rot="R270" xref="yes"/>
<pinref part="R8_100K1" gate="G$1" pin="2"/>
<wire x1="139.7" y1="-45.72" x2="139.7" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-43.18" x2="132.08" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-45.72" x2="139.7" y2="-45.72" width="0.1524" layer="91"/>
<junction x="139.7" y="-45.72"/>
<pinref part="Q2" gate="G$1" pin="S"/>
</segment>
<segment>
<wire x1="55.88" y1="-35.56" x2="55.88" y2="-45.72" width="0.1524" layer="91"/>
<label x="55.88" y="-48.26" size="1.778" layer="95" rot="R270" xref="yes"/>
<pinref part="R8_100K2" gate="G$1" pin="2"/>
<wire x1="55.88" y1="-45.72" x2="55.88" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="48.26" y1="-43.18" x2="48.26" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="48.26" y1="-45.72" x2="55.88" y2="-45.72" width="0.1524" layer="91"/>
<junction x="55.88" y="-45.72"/>
<pinref part="Q4" gate="G$1" pin="S"/>
</segment>
<segment>
<wire x1="99.06" y1="-35.56" x2="99.06" y2="-45.72" width="0.1524" layer="91"/>
<label x="99.06" y="-48.26" size="1.778" layer="95" rot="R270" xref="yes"/>
<pinref part="R8_100K3" gate="G$1" pin="2"/>
<wire x1="99.06" y1="-45.72" x2="99.06" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-43.18" x2="91.44" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-45.72" x2="99.06" y2="-45.72" width="0.1524" layer="91"/>
<junction x="99.06" y="-45.72"/>
<pinref part="Q5" gate="G$1" pin="S"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<pinref part="JP2" gate="A" pin="2"/>
<wire x1="-22.86" y1="-152.4" x2="-15.24" y2="-152.4" width="0.1524" layer="91"/>
<label x="-15.24" y="-152.4" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="JP2" gate="A" pin="4"/>
<wire x1="-22.86" y1="-154.94" x2="-2.54" y2="-154.94" width="0.1524" layer="91"/>
<label x="-2.54" y="-154.94" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="AO4622" gate="2" pin="D"/>
<wire x1="104.14" y1="-193.04" x2="104.14" y2="-223.52" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-223.52" x2="48.26" y2="-223.52" width="0.1524" layer="91"/>
<label x="48.26" y="-223.52" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="G.5V.5V" gate="A" pin="1"/>
<wire x1="-218.44" y1="-180.34" x2="-200.66" y2="-180.34" width="0.1524" layer="91"/>
<label x="-200.66" y="-180.34" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="215.9" y1="-35.56" x2="215.9" y2="-45.72" width="0.1524" layer="91"/>
<label x="215.9" y="-48.26" size="1.778" layer="95" rot="R270" xref="yes"/>
<pinref part="R5_100K" gate="G$1" pin="2"/>
<wire x1="215.9" y1="-45.72" x2="215.9" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="208.28" y1="-43.18" x2="208.28" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="208.28" y1="-45.72" x2="215.9" y2="-45.72" width="0.1524" layer="91"/>
<junction x="215.9" y="-45.72"/>
<pinref part="Q1" gate="G$1" pin="S"/>
</segment>
<segment>
<pinref part="STEMMA1" gate="J1" pin="VCC"/>
<wire x1="-210.82" y1="-200.66" x2="-205.74" y2="-200.66" width="0.1524" layer="91"/>
<label x="-205.74" y="-200.66" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<label x="-96.52" y="-78.74" size="1.778" layer="95" rot="R90" xref="yes"/>
<pinref part="R4_10K" gate="G$1" pin="2"/>
<wire x1="-96.52" y1="-81.28" x2="-96.52" y2="-78.74" width="0.1524" layer="91"/>
</segment>
<segment>
<label x="-109.22" y="-78.74" size="1.778" layer="95" rot="R90" xref="yes"/>
<pinref part="R3_10K" gate="G$1" pin="2"/>
<wire x1="-109.22" y1="-81.28" x2="-109.22" y2="-78.74" width="0.1524" layer="91"/>
</segment>
<segment>
<label x="-154.94" y="-96.52" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="IC1" gate="G$1" pin="VCC"/>
<wire x1="-147.32" y1="-96.52" x2="-152.4" y2="-96.52" width="0.1524" layer="91"/>
<pinref part="C1_100NF" gate="G$1" pin="1"/>
<wire x1="-152.4" y1="-96.52" x2="-154.94" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="-152.4" y1="-91.44" x2="-152.4" y2="-96.52" width="0.1524" layer="91"/>
<junction x="-152.4" y="-96.52"/>
</segment>
<segment>
<wire x1="-119.38" y1="-157.48" x2="-101.6" y2="-157.48" width="0.1524" layer="91"/>
<label x="-96.52" y="-157.48" size="1.778" layer="95" xref="yes"/>
<pinref part="U$2" gate="G$1" pin="VDD"/>
<pinref part="C3_100NF" gate="G$1" pin="2"/>
<wire x1="-101.6" y1="-157.48" x2="-96.52" y2="-157.48" width="0.1524" layer="91"/>
<wire x1="-101.6" y1="-160.02" x2="-101.6" y2="-157.48" width="0.1524" layer="91"/>
<junction x="-101.6" y="-157.48"/>
</segment>
<segment>
<wire x1="175.26" y1="-35.56" x2="175.26" y2="-45.72" width="0.1524" layer="91"/>
<label x="175.26" y="-48.26" size="1.778" layer="95" rot="R270" xref="yes"/>
<pinref part="R8_100K" gate="G$1" pin="2"/>
<wire x1="175.26" y1="-45.72" x2="175.26" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="167.64" y1="-43.18" x2="167.64" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="167.64" y1="-45.72" x2="175.26" y2="-45.72" width="0.1524" layer="91"/>
<junction x="175.26" y="-45.72"/>
<pinref part="Q3" gate="G$1" pin="S"/>
</segment>
<segment>
<label x="-93.98" y="-134.62" size="1.778" layer="95" rot="R90" xref="yes"/>
<pinref part="R6_10K" gate="G$1" pin="2"/>
<wire x1="-93.98" y1="-137.16" x2="-93.98" y2="-134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<label x="-86.36" y="-172.72" size="1.778" layer="95" rot="R270" xref="yes"/>
<pinref part="R7_10K" gate="G$1" pin="2"/>
<wire x1="-86.36" y1="-167.64" x2="-86.36" y2="-172.72" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="-104.14" y1="-198.12" x2="-99.06" y2="-198.12" width="0.1524" layer="91"/>
<label x="-99.06" y="-198.12" size="1.778" layer="95" xref="yes"/>
<pinref part="B2" gate="G$1" pin="VCC"/>
</segment>
<segment>
<wire x1="-218.44" y1="-177.8" x2="-208.28" y2="-177.8" width="0.1524" layer="91"/>
<label x="-208.28" y="-177.8" size="1.778" layer="95" xref="yes"/>
<pinref part="G.5V.5V" gate="A" pin="2"/>
</segment>
<segment>
<wire x1="-33.02" y1="-134.62" x2="-50.8" y2="-134.62" width="0.1524" layer="91"/>
<label x="-50.8" y="-134.62" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="J3" gate="G$1" pin="4"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<wire x1="-215.9" y1="-137.16" x2="-213.36" y2="-137.16" width="0.1524" layer="91"/>
<label x="-200.66" y="-137.16" size="1.778" layer="95" xref="yes"/>
<pinref part="PAD2" gate="G$1" pin="P"/>
<wire x1="-213.36" y1="-137.16" x2="-200.66" y2="-137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP2" gate="A" pin="14"/>
<wire x1="-22.86" y1="-167.64" x2="-15.24" y2="-167.64" width="0.1524" layer="91"/>
<label x="-15.24" y="-167.64" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="JP2" gate="A" pin="20"/>
<label x="-2.54" y="-175.26" size="1.778" layer="95" xref="yes"/>
<wire x1="-22.86" y1="-175.26" x2="-2.54" y2="-175.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP2" gate="A" pin="30"/>
<wire x1="-22.86" y1="-187.96" x2="-15.24" y2="-187.96" width="0.1524" layer="91"/>
<label x="-15.24" y="-187.96" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="JP2" gate="A" pin="25"/>
<wire x1="-30.48" y1="-182.88" x2="-38.1" y2="-182.88" width="0.1524" layer="91"/>
<label x="-38.1" y="-182.88" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JP2" gate="A" pin="39"/>
<wire x1="-30.48" y1="-200.66" x2="-48.26" y2="-200.66" width="0.1524" layer="91"/>
<label x="-48.26" y="-200.66" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JP2" gate="A" pin="34"/>
<wire x1="-22.86" y1="-193.04" x2="-15.24" y2="-193.04" width="0.1524" layer="91"/>
<label x="-15.24" y="-193.04" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="JP2" gate="A" pin="6"/>
<wire x1="-22.86" y1="-157.48" x2="-15.24" y2="-157.48" width="0.1524" layer="91"/>
<label x="-15.24" y="-157.48" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="JP2" gate="A" pin="9"/>
<wire x1="-30.48" y1="-162.56" x2="-38.1" y2="-162.56" width="0.1524" layer="91"/>
<label x="-38.1" y="-162.56" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-213.36" y1="-83.82" x2="-193.04" y2="-83.82" width="0.1524" layer="91"/>
<label x="-193.04" y="-83.82" size="1.778" layer="95" xref="yes"/>
<pinref part="J1" gate="G$1" pin="4"/>
<junction x="-213.36" y="-83.82"/>
</segment>
<segment>
<wire x1="-213.36" y1="-71.12" x2="-208.28" y2="-71.12" width="0.1524" layer="91"/>
<label x="-208.28" y="-71.12" size="1.778" layer="95" xref="yes"/>
<pinref part="J1" gate="G$1" pin="9"/>
<junction x="-213.36" y="-71.12"/>
</segment>
<segment>
<wire x1="-33.02" y1="-93.98" x2="-40.64" y2="-93.98" width="0.1524" layer="91"/>
<label x="-40.64" y="-93.98" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP1" gate="A" pin="7"/>
</segment>
<segment>
<label x="-15.24" y="-86.36" size="1.778" layer="95" xref="yes"/>
<wire x1="-25.4" y1="-86.36" x2="-15.24" y2="-86.36" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="2"/>
</segment>
<segment>
<pinref part="R0_100K" gate="G$1" pin="1"/>
<wire x1="60.96" y1="-215.9" x2="73.66" y2="-215.9" width="0.1524" layer="91"/>
<pinref part="AO4622" gate="1" pin="S"/>
<wire x1="73.66" y1="-215.9" x2="73.66" y2="-205.74" width="0.1524" layer="91"/>
<wire x1="60.96" y1="-215.9" x2="48.26" y2="-215.9" width="0.1524" layer="91"/>
<junction x="60.96" y="-215.9"/>
<wire x1="124.46" y1="-208.28" x2="124.46" y2="-215.9" width="0.1524" layer="91"/>
<wire x1="124.46" y1="-215.9" x2="88.9" y2="-215.9" width="0.1524" layer="91"/>
<junction x="73.66" y="-215.9"/>
<pinref part="POW" gate="G$1" pin="1"/>
<wire x1="88.9" y1="-215.9" x2="73.66" y2="-215.9" width="0.1524" layer="91"/>
<wire x1="88.9" y1="-208.28" x2="88.9" y2="-215.9" width="0.1524" layer="91"/>
<junction x="88.9" y="-215.9"/>
<label x="48.26" y="-215.9" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U$3" gate="G$1" pin="VOUT-"/>
</segment>
<segment>
<pinref part="STEMMA1" gate="J1" pin="GND"/>
<wire x1="-193.04" y1="-203.2" x2="-210.82" y2="-203.2" width="0.1524" layer="91"/>
<label x="-193.04" y="-203.2" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<label x="-99.06" y="-114.3" size="1.778" layer="95" rot="R270" xref="yes"/>
<wire x1="-99.06" y1="-109.22" x2="-99.06" y2="-114.3" width="0.1524" layer="91"/>
<pinref part="C2_100NF" gate="G$1" pin="1"/>
</segment>
<segment>
<label x="-109.22" y="-101.6" size="1.778" layer="95" xref="yes"/>
<pinref part="IC1" gate="G$1" pin="GND"/>
<wire x1="-116.84" y1="-101.6" x2="-109.22" y2="-101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="-119.38" y1="-162.56" x2="-114.3" y2="-162.56" width="0.1524" layer="91"/>
<label x="-114.3" y="-162.56" size="1.778" layer="95" xref="yes"/>
<pinref part="U$2" gate="G$1" pin="VSS"/>
</segment>
<segment>
<wire x1="-119.38" y1="-137.16" x2="-114.3" y2="-137.16" width="0.1524" layer="91"/>
<label x="-114.3" y="-137.16" size="1.778" layer="95" xref="yes"/>
<pinref part="U$2" gate="G$1" pin="A2"/>
</segment>
<segment>
<wire x1="-119.38" y1="-134.62" x2="-106.68" y2="-134.62" width="0.1524" layer="91"/>
<label x="-106.68" y="-134.62" size="1.778" layer="95" xref="yes"/>
<pinref part="U$2" gate="G$1" pin="A1"/>
</segment>
<segment>
<wire x1="-119.38" y1="-132.08" x2="-114.3" y2="-132.08" width="0.1524" layer="91"/>
<label x="-114.3" y="-132.08" size="1.778" layer="95" xref="yes"/>
<pinref part="U$2" gate="G$1" pin="A0"/>
</segment>
<segment>
<label x="-152.4" y="-78.74" size="1.778" layer="95" rot="R90" xref="yes"/>
<wire x1="-152.4" y1="-83.82" x2="-152.4" y2="-78.74" width="0.1524" layer="91"/>
<pinref part="C1_100NF" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-101.6" y1="-167.64" x2="-101.6" y2="-170.18" width="0.1524" layer="91"/>
<label x="-101.6" y="-170.18" size="1.778" layer="95" rot="R270" xref="yes"/>
<pinref part="C3_100NF" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-127" y1="-195.58" x2="-132.08" y2="-195.58" width="0.1524" layer="91"/>
<label x="-132.08" y="-195.58" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="B2" gate="G$1" pin="GND@1"/>
</segment>
<segment>
<wire x1="-127" y1="-198.12" x2="-149.86" y2="-198.12" width="0.1524" layer="91"/>
<label x="-149.86" y="-198.12" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="B2" gate="G$1" pin="GND"/>
</segment>
<segment>
<wire x1="-104.14" y1="-193.04" x2="-99.06" y2="-193.04" width="0.1524" layer="91"/>
<label x="-99.06" y="-193.04" size="1.778" layer="95" xref="yes"/>
<pinref part="B2" gate="G$1" pin="GND@2"/>
</segment>
<segment>
<wire x1="-213.36" y1="-127" x2="-203.2" y2="-127" width="0.1524" layer="91"/>
<label x="-203.2" y="-127" size="1.778" layer="95" xref="yes"/>
<pinref part="J5" gate="G$1" pin="10"/>
</segment>
<segment>
<pinref part="GND" gate="1" pin="P"/>
<wire x1="-200.66" y1="-162.56" x2="-215.9" y2="-162.56" width="0.1524" layer="91"/>
<label x="-200.66" y="-162.56" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="G.5V.5V" gate="A" pin="3"/>
<wire x1="-218.44" y1="-175.26" x2="-200.66" y2="-175.26" width="0.1524" layer="91"/>
<label x="-200.66" y="-175.26" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-33.02" y1="-137.16" x2="-38.1" y2="-137.16" width="0.1524" layer="91"/>
<label x="-38.1" y="-137.16" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="J3" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-127" y1="-27.94" x2="-134.62" y2="-27.94" width="0.1524" layer="91"/>
<label x="-134.62" y="-27.94" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U$4" gate="G$1" pin="GND"/>
</segment>
<segment>
<wire x1="-50.8" y1="-30.48" x2="-58.42" y2="-30.48" width="0.1524" layer="91"/>
<label x="-58.42" y="-30.48" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U$5" gate="G$1" pin="GND"/>
</segment>
</net>
<net name="LED_1" class="0">
<segment>
<wire x1="-213.36" y1="-53.34" x2="-193.04" y2="-53.34" width="0.1524" layer="91"/>
<label x="-193.04" y="-53.34" size="1.778" layer="95" xref="yes"/>
<pinref part="J1" gate="G$1" pin="16"/>
<junction x="-213.36" y="-53.34"/>
</segment>
<segment>
<wire x1="-99.06" y1="-218.44" x2="-104.14" y2="-218.44" width="0.1524" layer="91"/>
<label x="-99.06" y="-218.44" size="1.778" layer="95" xref="yes"/>
<pinref part="B2" gate="G$1" pin="*10"/>
</segment>
</net>
<net name="P_SCL" class="0">
<segment>
<pinref part="JP2" gate="A" pin="5"/>
<wire x1="-30.48" y1="-157.48" x2="-38.1" y2="-157.48" width="0.1524" layer="91"/>
<label x="-38.1" y="-157.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-15.24" y1="-91.44" x2="-25.4" y2="-91.44" width="0.1524" layer="91"/>
<label x="-15.24" y="-91.44" size="1.778" layer="95" xref="yes"/>
<pinref part="JP1" gate="A" pin="6"/>
</segment>
<segment>
<pinref part="STEMMA1" gate="J1" pin="SCL"/>
<wire x1="-210.82" y1="-195.58" x2="-205.74" y2="-195.58" width="0.1524" layer="91"/>
<label x="-205.74" y="-195.58" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="SCL"/>
<label x="-88.9" y="-93.98" size="1.778" layer="95" xref="yes"/>
<wire x1="-116.84" y1="-93.98" x2="-96.52" y2="-93.98" width="0.1524" layer="91"/>
<pinref part="R4_10K" gate="G$1" pin="1"/>
<wire x1="-96.52" y1="-93.98" x2="-88.9" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="-91.44" x2="-96.52" y2="-93.98" width="0.1524" layer="91"/>
<junction x="-96.52" y="-93.98"/>
</segment>
<segment>
<wire x1="-33.02" y1="-127" x2="-38.1" y2="-127" width="0.1524" layer="91"/>
<label x="-38.1" y="-127" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="J3" gate="G$1" pin="10"/>
</segment>
<segment>
<wire x1="-127" y1="-33.02" x2="-142.24" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="U$4" gate="G$1" pin="SCL"/>
<label x="-142.24" y="-33.02" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-50.8" y1="-35.56" x2="-58.42" y2="-35.56" width="0.1524" layer="91"/>
<label x="-58.42" y="-35.56" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U$5" gate="G$1" pin="SCL"/>
</segment>
</net>
<net name="PRX" class="0">
<segment>
<pinref part="JP2" gate="A" pin="10"/>
<wire x1="-22.86" y1="-162.56" x2="-15.24" y2="-162.56" width="0.1524" layer="91"/>
<label x="-15.24" y="-162.56" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-15.24" y1="-96.52" x2="-25.4" y2="-96.52" width="0.1524" layer="91"/>
<label x="-15.24" y="-96.52" size="1.778" layer="95" xref="yes"/>
<pinref part="JP1" gate="A" pin="10"/>
</segment>
<segment>
<wire x1="-33.02" y1="-124.46" x2="-50.8" y2="-124.46" width="0.1524" layer="91"/>
<label x="-50.8" y="-124.46" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="J3" gate="G$1" pin="12"/>
</segment>
</net>
<net name="GP27" class="0">
<segment>
<pinref part="JP2" gate="A" pin="13"/>
<wire x1="-30.48" y1="-167.64" x2="-38.1" y2="-167.64" width="0.1524" layer="91"/>
<label x="-38.1" y="-167.64" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-50.8" y1="-96.52" x2="-33.02" y2="-96.52" width="0.1524" layer="91"/>
<label x="-50.8" y="-96.52" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP1" gate="A" pin="9"/>
</segment>
</net>
<net name="GP22" class="0">
<segment>
<pinref part="JP2" gate="A" pin="15"/>
<wire x1="-30.48" y1="-170.18" x2="-48.26" y2="-170.18" width="0.1524" layer="91"/>
<label x="-48.26" y="-170.18" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-33.02" y1="-99.06" x2="-40.64" y2="-99.06" width="0.1524" layer="91"/>
<label x="-40.64" y="-99.06" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP1" gate="A" pin="11"/>
</segment>
</net>
<net name="ID_SD" class="0">
<segment>
<pinref part="JP2" gate="A" pin="27"/>
<wire x1="-30.48" y1="-185.42" x2="-48.26" y2="-185.42" width="0.1524" layer="91"/>
<label x="-48.26" y="-185.42" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-25.4" y1="-101.6" x2="-15.24" y2="-101.6" width="0.1524" layer="91"/>
<label x="-15.24" y="-101.6" size="1.778" layer="95" xref="yes"/>
<pinref part="JP1" gate="A" pin="14"/>
</segment>
</net>
<net name="PTX" class="0">
<segment>
<wire x1="-2.54" y1="-160.02" x2="-22.86" y2="-160.02" width="0.1524" layer="91"/>
<pinref part="JP2" gate="A" pin="8"/>
<label x="-2.54" y="-160.02" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-5.08" y1="-93.98" x2="-25.4" y2="-93.98" width="0.1524" layer="91"/>
<label x="-5.08" y="-93.98" size="1.778" layer="95" xref="yes"/>
<pinref part="JP1" gate="A" pin="8"/>
</segment>
<segment>
<wire x1="-22.86" y1="-124.46" x2="-15.24" y2="-124.46" width="0.1524" layer="91"/>
<label x="-15.24" y="-124.46" size="1.778" layer="95" xref="yes"/>
<pinref part="J3" gate="G$1" pin="11"/>
</segment>
</net>
<net name="GP23" class="0">
<segment>
<pinref part="JP2" gate="A" pin="16"/>
<wire x1="-22.86" y1="-170.18" x2="-2.54" y2="-170.18" width="0.1524" layer="91"/>
<label x="-2.54" y="-170.18" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-5.08" y1="-99.06" x2="-25.4" y2="-99.06" width="0.1524" layer="91"/>
<label x="-5.08" y="-99.06" size="1.778" layer="95" xref="yes"/>
<pinref part="JP1" gate="A" pin="12"/>
</segment>
</net>
<net name="ID_SC" class="0">
<segment>
<pinref part="JP2" gate="A" pin="28"/>
<wire x1="-22.86" y1="-185.42" x2="-2.54" y2="-185.42" width="0.1524" layer="91"/>
<label x="-2.54" y="-185.42" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-5.08" y1="-104.14" x2="-25.4" y2="-104.14" width="0.1524" layer="91"/>
<label x="-5.08" y="-104.14" size="1.778" layer="95" xref="yes"/>
<pinref part="JP1" gate="A" pin="16"/>
</segment>
</net>
<net name="GP19" class="0">
<segment>
<pinref part="JP2" gate="A" pin="35"/>
<wire x1="-30.48" y1="-195.58" x2="-48.26" y2="-195.58" width="0.1524" layer="91"/>
<label x="-48.26" y="-195.58" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-78.74" y1="-200.66" x2="-104.14" y2="-200.66" width="0.1524" layer="91"/>
<label x="-78.74" y="-200.66" size="1.778" layer="95" xref="yes"/>
<pinref part="B2" gate="G$1" pin="A3"/>
</segment>
</net>
<net name="GP26" class="0">
<segment>
<pinref part="JP2" gate="A" pin="37"/>
<wire x1="-38.1" y1="-198.12" x2="-30.48" y2="-198.12" width="0.1524" layer="91"/>
<label x="-38.1" y="-198.12" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-5.08" y1="-127" x2="-22.86" y2="-127" width="0.1524" layer="91"/>
<label x="-5.08" y="-127" size="1.778" layer="95" xref="yes"/>
<pinref part="J3" gate="G$1" pin="9"/>
</segment>
</net>
<net name="GP6" class="0">
<segment>
<pinref part="JP2" gate="A" pin="31"/>
<wire x1="-30.48" y1="-190.5" x2="-48.26" y2="-190.5" width="0.1524" layer="91"/>
<label x="-48.26" y="-190.5" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-5.08" y1="-109.22" x2="-25.4" y2="-109.22" width="0.1524" layer="91"/>
<label x="-5.08" y="-109.22" size="1.778" layer="95" xref="yes"/>
<pinref part="JP1" gate="A" pin="20"/>
</segment>
</net>
<net name="GP16" class="0">
<segment>
<pinref part="JP2" gate="A" pin="36"/>
<wire x1="-2.54" y1="-195.58" x2="-22.86" y2="-195.58" width="0.1524" layer="91"/>
<label x="-2.54" y="-195.58" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-50.8" y1="-106.68" x2="-33.02" y2="-106.68" width="0.1524" layer="91"/>
<label x="-50.8" y="-106.68" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP1" gate="A" pin="17"/>
</segment>
</net>
<net name="GP13" class="0">
<segment>
<pinref part="JP2" gate="A" pin="33"/>
<wire x1="-30.48" y1="-193.04" x2="-38.1" y2="-193.04" width="0.1524" layer="91"/>
<label x="-38.1" y="-193.04" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-33.02" y1="-109.22" x2="-40.64" y2="-109.22" width="0.1524" layer="91"/>
<label x="-40.64" y="-109.22" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP1" gate="A" pin="19"/>
</segment>
</net>
<net name="GP4" class="0">
<segment>
<pinref part="JP2" gate="A" pin="7"/>
<wire x1="-30.48" y1="-160.02" x2="-48.26" y2="-160.02" width="0.1524" layer="91"/>
<label x="-48.26" y="-160.02" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-33.02" y1="-91.44" x2="-50.8" y2="-91.44" width="0.1524" layer="91"/>
<label x="-50.8" y="-91.44" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP1" gate="A" pin="5"/>
</segment>
</net>
<net name="P_SDA" class="0">
<segment>
<pinref part="JP2" gate="A" pin="3"/>
<wire x1="-30.48" y1="-154.94" x2="-48.26" y2="-154.94" width="0.1524" layer="91"/>
<label x="-48.26" y="-154.94" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-40.64" y1="-88.9" x2="-33.02" y2="-88.9" width="0.1524" layer="91"/>
<label x="-40.64" y="-88.9" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP1" gate="A" pin="3"/>
</segment>
<segment>
<pinref part="STEMMA1" gate="J1" pin="SDA"/>
<wire x1="-193.04" y1="-198.12" x2="-210.82" y2="-198.12" width="0.1524" layer="91"/>
<label x="-193.04" y="-198.12" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="SDA"/>
<wire x1="-116.84" y1="-96.52" x2="-109.22" y2="-96.52" width="0.1524" layer="91"/>
<label x="-101.6" y="-96.52" size="1.778" layer="95" xref="yes"/>
<pinref part="R3_10K" gate="G$1" pin="1"/>
<wire x1="-109.22" y1="-96.52" x2="-101.6" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="-96.52" x2="-109.22" y2="-91.44" width="0.1524" layer="91"/>
<junction x="-109.22" y="-96.52"/>
</segment>
<segment>
<wire x1="-38.1" y1="-132.08" x2="-33.02" y2="-132.08" width="0.1524" layer="91"/>
<label x="-38.1" y="-132.08" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="J3" gate="G$1" pin="6"/>
</segment>
<segment>
<wire x1="-127" y1="-38.1" x2="-152.4" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="U$4" gate="G$1" pin="SDA"/>
<label x="-152.4" y="-38.1" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-50.8" y1="-40.64" x2="-58.42" y2="-40.64" width="0.1524" layer="91"/>
<label x="-58.42" y="-40.64" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U$5" gate="G$1" pin="SDA"/>
</segment>
</net>
<net name="GP12" class="0">
<segment>
<pinref part="JP2" gate="A" pin="32"/>
<wire x1="-22.86" y1="-190.5" x2="-2.54" y2="-190.5" width="0.1524" layer="91"/>
<label x="-2.54" y="-190.5" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-40.64" y1="-104.14" x2="-33.02" y2="-104.14" width="0.1524" layer="91"/>
<label x="-40.64" y="-104.14" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP1" gate="A" pin="15"/>
</segment>
</net>
<net name="GP5" class="0">
<segment>
<pinref part="JP2" gate="A" pin="29"/>
<wire x1="-30.48" y1="-187.96" x2="-38.1" y2="-187.96" width="0.1524" layer="91"/>
<label x="-38.1" y="-187.96" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-15.24" y1="-106.68" x2="-25.4" y2="-106.68" width="0.1524" layer="91"/>
<label x="-15.24" y="-106.68" size="1.778" layer="95" xref="yes"/>
<pinref part="JP1" gate="A" pin="18"/>
</segment>
</net>
<net name="GP21" class="0">
<segment>
<pinref part="JP2" gate="A" pin="40"/>
<wire x1="-22.86" y1="-200.66" x2="-2.54" y2="-200.66" width="0.1524" layer="91"/>
<label x="-2.54" y="-200.66" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-22.86" y1="-132.08" x2="-5.08" y2="-132.08" width="0.1524" layer="91"/>
<label x="-5.08" y="-132.08" size="1.778" layer="95" xref="yes"/>
<pinref part="J3" gate="G$1" pin="5"/>
</segment>
</net>
<net name="GP20" class="0">
<segment>
<pinref part="JP2" gate="A" pin="38"/>
<wire x1="-22.86" y1="-198.12" x2="-15.24" y2="-198.12" width="0.1524" layer="91"/>
<label x="-15.24" y="-198.12" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-15.24" y1="-129.54" x2="-22.86" y2="-129.54" width="0.1524" layer="91"/>
<label x="-15.24" y="-129.54" size="1.778" layer="95" xref="yes"/>
<pinref part="J3" gate="G$1" pin="7"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="C1" gate="G$1" pin="4"/>
<pinref part="C2" gate="G$1" pin="3"/>
<wire x1="40.64" y1="-127" x2="45.72" y2="-127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="C2" gate="G$1" pin="4"/>
<pinref part="C3" gate="G$1" pin="3"/>
<wire x1="55.88" y1="-127" x2="63.5" y2="-127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="C3" gate="G$1" pin="4"/>
<pinref part="C4" gate="G$1" pin="3"/>
<wire x1="73.66" y1="-127" x2="81.28" y2="-127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="C4" gate="G$1" pin="4"/>
<pinref part="C5" gate="G$1" pin="3"/>
<wire x1="91.44" y1="-127" x2="101.6" y2="-127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="C5" gate="G$1" pin="4"/>
<pinref part="C6" gate="G$1" pin="3"/>
<wire x1="111.76" y1="-127" x2="119.38" y2="-127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="C6" gate="G$1" pin="4"/>
<pinref part="C7" gate="G$1" pin="3"/>
<wire x1="129.54" y1="-127" x2="137.16" y2="-127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="C7" gate="G$1" pin="4"/>
<pinref part="C8" gate="G$1" pin="3"/>
<wire x1="147.32" y1="-127" x2="154.94" y2="-127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="C8" gate="G$1" pin="4"/>
<pinref part="C9" gate="G$1" pin="3"/>
<wire x1="165.1" y1="-127" x2="172.72" y2="-127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="4"/>
<pinref part="D2" gate="G$1" pin="3"/>
<wire x1="40.64" y1="-142.24" x2="45.72" y2="-142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="4"/>
<pinref part="D3" gate="G$1" pin="3"/>
<wire x1="55.88" y1="-142.24" x2="63.5" y2="-142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="4"/>
<pinref part="D4" gate="G$1" pin="3"/>
<wire x1="73.66" y1="-142.24" x2="81.28" y2="-142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="D4" gate="G$1" pin="4"/>
<pinref part="D5" gate="G$1" pin="3"/>
<wire x1="91.44" y1="-142.24" x2="101.6" y2="-142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="D5" gate="G$1" pin="4"/>
<pinref part="D6" gate="G$1" pin="3"/>
<wire x1="111.76" y1="-142.24" x2="119.38" y2="-142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="D6" gate="G$1" pin="4"/>
<pinref part="D7" gate="G$1" pin="3"/>
<wire x1="129.54" y1="-142.24" x2="137.16" y2="-142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="D7" gate="G$1" pin="4"/>
<pinref part="D8" gate="G$1" pin="3"/>
<wire x1="147.32" y1="-142.24" x2="154.94" y2="-142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="D8" gate="G$1" pin="4"/>
<pinref part="D9" gate="G$1" pin="3"/>
<wire x1="165.1" y1="-142.24" x2="172.72" y2="-142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="E1" gate="G$1" pin="4"/>
<pinref part="E2" gate="G$1" pin="3"/>
<wire x1="40.64" y1="-160.02" x2="45.72" y2="-160.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="E2" gate="G$1" pin="4"/>
<pinref part="E3" gate="G$1" pin="3"/>
<wire x1="55.88" y1="-160.02" x2="63.5" y2="-160.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="E3" gate="G$1" pin="4"/>
<pinref part="E4" gate="G$1" pin="3"/>
<wire x1="73.66" y1="-160.02" x2="81.28" y2="-160.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="E4" gate="G$1" pin="4"/>
<pinref part="E5" gate="G$1" pin="3"/>
<wire x1="91.44" y1="-160.02" x2="101.6" y2="-160.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="E5" gate="G$1" pin="4"/>
<pinref part="E6" gate="G$1" pin="3"/>
<wire x1="111.76" y1="-160.02" x2="119.38" y2="-160.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="E6" gate="G$1" pin="4"/>
<pinref part="E7" gate="G$1" pin="3"/>
<wire x1="129.54" y1="-160.02" x2="137.16" y2="-160.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="E7" gate="G$1" pin="4"/>
<pinref part="E8" gate="G$1" pin="3"/>
<wire x1="147.32" y1="-160.02" x2="154.94" y2="-160.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="E8" gate="G$1" pin="4"/>
<pinref part="E9" gate="G$1" pin="3"/>
<wire x1="165.1" y1="-160.02" x2="170.18" y2="-160.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="B" class="0">
<segment>
<label x="238.76" y="-99.06" size="1.778" layer="95" xref="yes"/>
<pinref part="B10" gate="G$1" pin="2"/>
<wire x1="238.76" y1="-99.06" x2="210.82" y2="-99.06" width="0.1524" layer="91"/>
<wire x1="210.82" y1="-99.06" x2="195.58" y2="-99.06" width="0.1524" layer="91"/>
<wire x1="195.58" y1="-99.06" x2="175.26" y2="-99.06" width="0.1524" layer="91"/>
<junction x="195.58" y="-99.06"/>
<pinref part="B9" gate="G$1" pin="2"/>
<pinref part="B8" gate="G$1" pin="2"/>
<wire x1="175.26" y1="-99.06" x2="157.48" y2="-99.06" width="0.1524" layer="91"/>
<junction x="175.26" y="-99.06"/>
<pinref part="B7" gate="G$1" pin="2"/>
<wire x1="157.48" y1="-99.06" x2="139.7" y2="-99.06" width="0.1524" layer="91"/>
<junction x="157.48" y="-99.06"/>
<pinref part="B6" gate="G$1" pin="2"/>
<wire x1="139.7" y1="-99.06" x2="121.92" y2="-99.06" width="0.1524" layer="91"/>
<junction x="139.7" y="-99.06"/>
<pinref part="B5" gate="G$1" pin="2"/>
<wire x1="121.92" y1="-99.06" x2="106.68" y2="-99.06" width="0.1524" layer="91"/>
<junction x="121.92" y="-99.06"/>
<wire x1="106.68" y1="-99.06" x2="86.36" y2="-99.06" width="0.1524" layer="91"/>
<junction x="106.68" y="-99.06"/>
<pinref part="B4" gate="G$1" pin="2"/>
<pinref part="B3" gate="G$1" pin="2"/>
<wire x1="86.36" y1="-99.06" x2="68.58" y2="-99.06" width="0.1524" layer="91"/>
<junction x="86.36" y="-99.06"/>
<pinref part="S2" gate="G$1" pin="2"/>
<wire x1="68.58" y1="-99.06" x2="50.8" y2="-99.06" width="0.1524" layer="91"/>
<junction x="68.58" y="-99.06"/>
<pinref part="B1" gate="G$1" pin="2"/>
<wire x1="38.1" y1="-99.06" x2="50.8" y2="-99.06" width="0.1524" layer="91"/>
<junction x="50.8" y="-99.06"/>
<pinref part="B11" gate="A" pin="1"/>
<wire x1="210.82" y1="-106.68" x2="210.82" y2="-99.06" width="0.1524" layer="91"/>
<junction x="210.82" y="-99.06"/>
</segment>
<segment>
<wire x1="-157.48" y1="-172.72" x2="-144.78" y2="-172.72" width="0.1524" layer="91"/>
<label x="-157.48" y="-172.72" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U$2" gate="G$1" pin="P17"/>
</segment>
</net>
<net name="E" class="0">
<segment>
<pinref part="E11" gate="G$1" pin="4"/>
<wire x1="226.06" y1="-160.02" x2="238.76" y2="-160.02" width="0.1524" layer="91"/>
<label x="238.76" y="-160.02" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-149.86" y1="-144.78" x2="-144.78" y2="-144.78" width="0.1524" layer="91"/>
<label x="-149.86" y="-144.78" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U$2" gate="G$1" pin="P05"/>
</segment>
</net>
<net name="1" class="0">
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="D1" gate="G$1" pin="2"/>
<pinref part="E1" gate="G$1" pin="2"/>
<pinref part="B1" gate="G$1" pin="1"/>
<wire x1="38.1" y1="-109.22" x2="43.18" y2="-109.22" width="0.1524" layer="91"/>
<junction x="43.18" y="-109.22"/>
<label x="43.18" y="-71.12" size="1.778" layer="95" xref="yes"/>
<wire x1="43.18" y1="-109.22" x2="43.18" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="43.18" y1="-109.22" x2="43.18" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="43.18" y1="-116.84" x2="43.18" y2="-132.08" width="0.1524" layer="91"/>
<wire x1="43.18" y1="-132.08" x2="43.18" y2="-149.86" width="0.1524" layer="91"/>
<wire x1="40.64" y1="-149.86" x2="43.18" y2="-149.86" width="0.1524" layer="91"/>
<wire x1="40.64" y1="-132.08" x2="43.18" y2="-132.08" width="0.1524" layer="91"/>
<junction x="43.18" y="-132.08"/>
<wire x1="40.64" y1="-116.84" x2="43.18" y2="-116.84" width="0.1524" layer="91"/>
<junction x="43.18" y="-116.84"/>
</segment>
<segment>
<wire x1="-144.78" y1="-147.32" x2="-157.48" y2="-147.32" width="0.1524" layer="91"/>
<label x="-157.48" y="-147.32" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U$2" gate="G$1" pin="P06"/>
</segment>
<segment>
<wire x1="-190.5" y1="-114.3" x2="-213.36" y2="-114.3" width="0.1524" layer="91"/>
<label x="-190.5" y="-114.3" size="1.778" layer="95" xref="yes"/>
<pinref part="J5" gate="G$1" pin="5"/>
</segment>
</net>
<net name="4" class="0">
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="D4" gate="G$1" pin="2"/>
<pinref part="E4" gate="G$1" pin="2"/>
<pinref part="B4" gate="G$1" pin="1"/>
<wire x1="86.36" y1="-109.22" x2="93.98" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="A4" gate="G$1" pin="1"/>
<wire x1="86.36" y1="-91.44" x2="93.98" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-91.44" x2="93.98" y2="-109.22" width="0.1524" layer="91"/>
<junction x="93.98" y="-109.22"/>
<wire x1="93.98" y1="-91.44" x2="93.98" y2="-71.12" width="0.1524" layer="91"/>
<junction x="93.98" y="-91.44"/>
<label x="93.98" y="-71.12" size="1.778" layer="95" xref="yes"/>
<wire x1="93.98" y1="-109.22" x2="93.98" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-116.84" x2="93.98" y2="-132.08" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-132.08" x2="93.98" y2="-149.86" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-149.86" x2="93.98" y2="-149.86" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-132.08" x2="93.98" y2="-132.08" width="0.1524" layer="91"/>
<junction x="93.98" y="-132.08"/>
<wire x1="91.44" y1="-116.84" x2="93.98" y2="-116.84" width="0.1524" layer="91"/>
<junction x="93.98" y="-116.84"/>
</segment>
<segment>
<wire x1="-149.86" y1="-149.86" x2="-144.78" y2="-149.86" width="0.1524" layer="91"/>
<label x="-149.86" y="-149.86" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U$2" gate="G$1" pin="P07"/>
</segment>
</net>
<net name="5" class="0">
<segment>
<pinref part="E5" gate="G$1" pin="2"/>
<pinref part="D5" gate="G$1" pin="2"/>
<pinref part="C5" gate="G$1" pin="2"/>
<pinref part="B5" gate="G$1" pin="1"/>
<wire x1="106.68" y1="-109.22" x2="114.3" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="A5" gate="G$1" pin="1"/>
<wire x1="106.68" y1="-91.44" x2="114.3" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="114.3" y1="-91.44" x2="114.3" y2="-109.22" width="0.1524" layer="91"/>
<junction x="114.3" y="-109.22"/>
<wire x1="114.3" y1="-91.44" x2="114.3" y2="-71.12" width="0.1524" layer="91"/>
<junction x="114.3" y="-91.44"/>
<label x="114.3" y="-71.12" size="1.778" layer="95" xref="yes"/>
<wire x1="114.3" y1="-109.22" x2="114.3" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="114.3" y1="-116.84" x2="114.3" y2="-132.08" width="0.1524" layer="91"/>
<wire x1="114.3" y1="-132.08" x2="114.3" y2="-149.86" width="0.1524" layer="91"/>
<wire x1="111.76" y1="-149.86" x2="114.3" y2="-149.86" width="0.1524" layer="91"/>
<wire x1="111.76" y1="-132.08" x2="114.3" y2="-132.08" width="0.1524" layer="91"/>
<junction x="114.3" y="-132.08"/>
<wire x1="111.76" y1="-116.84" x2="114.3" y2="-116.84" width="0.1524" layer="91"/>
<junction x="114.3" y="-116.84"/>
</segment>
<segment>
<wire x1="-144.78" y1="-160.02" x2="-149.86" y2="-160.02" width="0.1524" layer="91"/>
<label x="-149.86" y="-160.02" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U$2" gate="G$1" pin="P12"/>
</segment>
</net>
<net name="6" class="0">
<segment>
<pinref part="C6" gate="G$1" pin="2"/>
<pinref part="D6" gate="G$1" pin="2"/>
<pinref part="E6" gate="G$1" pin="2"/>
<pinref part="B6" gate="G$1" pin="1"/>
<wire x1="121.92" y1="-109.22" x2="132.08" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="A6" gate="G$1" pin="1"/>
<wire x1="121.92" y1="-91.44" x2="132.08" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-91.44" x2="132.08" y2="-109.22" width="0.1524" layer="91"/>
<junction x="132.08" y="-109.22"/>
<wire x1="132.08" y1="-91.44" x2="132.08" y2="-71.12" width="0.1524" layer="91"/>
<junction x="132.08" y="-91.44"/>
<label x="132.08" y="-71.12" size="1.778" layer="95" xref="yes"/>
<wire x1="132.08" y1="-109.22" x2="132.08" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-116.84" x2="132.08" y2="-132.08" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-132.08" x2="132.08" y2="-149.86" width="0.1524" layer="91"/>
<wire x1="129.54" y1="-149.86" x2="132.08" y2="-149.86" width="0.1524" layer="91"/>
<wire x1="129.54" y1="-132.08" x2="132.08" y2="-132.08" width="0.1524" layer="91"/>
<junction x="132.08" y="-132.08"/>
<wire x1="129.54" y1="-116.84" x2="132.08" y2="-116.84" width="0.1524" layer="91"/>
<junction x="132.08" y="-116.84"/>
</segment>
<segment>
<wire x1="-144.78" y1="-165.1" x2="-149.86" y2="-165.1" width="0.1524" layer="91"/>
<label x="-149.86" y="-165.1" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U$2" gate="G$1" pin="P14"/>
</segment>
</net>
<net name="7" class="0">
<segment>
<pinref part="E7" gate="G$1" pin="2"/>
<pinref part="D7" gate="G$1" pin="2"/>
<pinref part="C7" gate="G$1" pin="2"/>
<pinref part="B7" gate="G$1" pin="1"/>
<wire x1="139.7" y1="-109.22" x2="149.86" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="A7" gate="G$1" pin="1"/>
<wire x1="139.7" y1="-91.44" x2="149.86" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="149.86" y1="-91.44" x2="149.86" y2="-109.22" width="0.1524" layer="91"/>
<junction x="149.86" y="-109.22"/>
<wire x1="149.86" y1="-91.44" x2="149.86" y2="-71.12" width="0.1524" layer="91"/>
<junction x="149.86" y="-91.44"/>
<label x="149.86" y="-71.12" size="1.778" layer="95" xref="yes"/>
<wire x1="149.86" y1="-109.22" x2="149.86" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="149.86" y1="-116.84" x2="149.86" y2="-132.08" width="0.1524" layer="91"/>
<wire x1="149.86" y1="-132.08" x2="149.86" y2="-149.86" width="0.1524" layer="91"/>
<wire x1="147.32" y1="-149.86" x2="149.86" y2="-149.86" width="0.1524" layer="91"/>
<wire x1="147.32" y1="-132.08" x2="149.86" y2="-132.08" width="0.1524" layer="91"/>
<junction x="149.86" y="-132.08"/>
<wire x1="147.32" y1="-116.84" x2="149.86" y2="-116.84" width="0.1524" layer="91"/>
<junction x="149.86" y="-116.84"/>
</segment>
<segment>
<wire x1="-157.48" y1="-167.64" x2="-144.78" y2="-167.64" width="0.1524" layer="91"/>
<label x="-157.48" y="-167.64" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U$2" gate="G$1" pin="P15"/>
</segment>
</net>
<net name="9" class="0">
<segment>
<pinref part="E9" gate="G$1" pin="2"/>
<pinref part="D9" gate="G$1" pin="2"/>
<pinref part="C9" gate="G$1" pin="2"/>
<pinref part="B9" gate="G$1" pin="1"/>
<wire x1="175.26" y1="-109.22" x2="185.42" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="A9" gate="G$1" pin="1"/>
<wire x1="175.26" y1="-91.44" x2="185.42" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="185.42" y1="-91.44" x2="185.42" y2="-109.22" width="0.1524" layer="91"/>
<junction x="185.42" y="-109.22"/>
<wire x1="185.42" y1="-91.44" x2="185.42" y2="-71.12" width="0.1524" layer="91"/>
<junction x="185.42" y="-91.44"/>
<label x="185.42" y="-71.12" size="1.778" layer="95" xref="yes"/>
<wire x1="180.34" y1="-149.86" x2="185.42" y2="-149.86" width="0.1524" layer="91"/>
<wire x1="185.42" y1="-109.22" x2="185.42" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="185.42" y1="-116.84" x2="185.42" y2="-132.08" width="0.1524" layer="91"/>
<wire x1="185.42" y1="-132.08" x2="185.42" y2="-149.86" width="0.1524" layer="91"/>
<wire x1="182.88" y1="-132.08" x2="185.42" y2="-132.08" width="0.1524" layer="91"/>
<junction x="185.42" y="-132.08"/>
<wire x1="182.88" y1="-116.84" x2="185.42" y2="-116.84" width="0.1524" layer="91"/>
<junction x="185.42" y="-116.84"/>
</segment>
<segment>
<wire x1="-149.86" y1="-139.7" x2="-144.78" y2="-139.7" width="0.1524" layer="91"/>
<label x="-149.86" y="-139.7" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U$2" gate="G$1" pin="P03"/>
</segment>
</net>
<net name="2" class="0">
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<pinref part="D2" gate="G$1" pin="2"/>
<pinref part="E2" gate="G$1" pin="2"/>
<pinref part="S2" gate="G$1" pin="1"/>
<wire x1="50.8" y1="-109.22" x2="58.42" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="A2" gate="G$1" pin="1"/>
<wire x1="50.8" y1="-91.44" x2="58.42" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="58.42" y1="-91.44" x2="58.42" y2="-109.22" width="0.1524" layer="91"/>
<junction x="58.42" y="-109.22"/>
<wire x1="58.42" y1="-91.44" x2="58.42" y2="-71.12" width="0.1524" layer="91"/>
<junction x="58.42" y="-91.44"/>
<label x="58.42" y="-71.12" size="1.778" layer="95" xref="yes"/>
<label x="58.42" y="-71.12" size="1.778" layer="95" xref="yes"/>
<wire x1="58.42" y1="-109.22" x2="58.42" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="58.42" y1="-116.84" x2="58.42" y2="-132.08" width="0.1524" layer="91"/>
<wire x1="58.42" y1="-132.08" x2="58.42" y2="-149.86" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-149.86" x2="58.42" y2="-149.86" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-132.08" x2="58.42" y2="-132.08" width="0.1524" layer="91"/>
<junction x="58.42" y="-132.08"/>
<wire x1="55.88" y1="-116.84" x2="58.42" y2="-116.84" width="0.1524" layer="91"/>
<junction x="58.42" y="-116.84"/>
</segment>
<segment>
<wire x1="-144.78" y1="-157.48" x2="-157.48" y2="-157.48" width="0.1524" layer="91"/>
<label x="-157.48" y="-157.48" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U$2" gate="G$1" pin="P11"/>
</segment>
</net>
<net name="3" class="0">
<segment>
<pinref part="E3" gate="G$1" pin="2"/>
<pinref part="D3" gate="G$1" pin="2"/>
<pinref part="C3" gate="G$1" pin="2"/>
<pinref part="B3" gate="G$1" pin="1"/>
<wire x1="68.58" y1="-109.22" x2="76.2" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="A3" gate="G$1" pin="1"/>
<wire x1="68.58" y1="-91.44" x2="76.2" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-91.44" x2="76.2" y2="-109.22" width="0.1524" layer="91"/>
<junction x="76.2" y="-109.22"/>
<wire x1="76.2" y1="-91.44" x2="76.2" y2="-71.12" width="0.1524" layer="91"/>
<junction x="76.2" y="-91.44"/>
<label x="76.2" y="-71.12" size="1.778" layer="95" xref="yes"/>
<wire x1="76.2" y1="-109.22" x2="76.2" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-116.84" x2="76.2" y2="-132.08" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-132.08" x2="76.2" y2="-149.86" width="0.1524" layer="91"/>
<wire x1="73.66" y1="-149.86" x2="76.2" y2="-149.86" width="0.1524" layer="91"/>
<wire x1="73.66" y1="-132.08" x2="76.2" y2="-132.08" width="0.1524" layer="91"/>
<junction x="76.2" y="-132.08"/>
<wire x1="73.66" y1="-116.84" x2="76.2" y2="-116.84" width="0.1524" layer="91"/>
<junction x="76.2" y="-116.84"/>
</segment>
<segment>
<wire x1="-149.86" y1="-154.94" x2="-144.78" y2="-154.94" width="0.1524" layer="91"/>
<label x="-149.86" y="-154.94" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U$2" gate="G$1" pin="P10"/>
</segment>
</net>
<net name="A" class="0">
<segment>
<pinref part="A10" gate="G$1" pin="2"/>
<wire x1="195.58" y1="-81.28" x2="238.76" y2="-81.28" width="0.1524" layer="91"/>
<junction x="195.58" y="-81.28"/>
<pinref part="A2" gate="G$1" pin="2"/>
<pinref part="A3" gate="G$1" pin="2"/>
<wire x1="68.58" y1="-81.28" x2="50.8" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="A4" gate="G$1" pin="2"/>
<wire x1="68.58" y1="-81.28" x2="86.36" y2="-81.28" width="0.1524" layer="91"/>
<junction x="68.58" y="-81.28"/>
<pinref part="A5" gate="G$1" pin="2"/>
<wire x1="86.36" y1="-81.28" x2="106.68" y2="-81.28" width="0.1524" layer="91"/>
<junction x="86.36" y="-81.28"/>
<pinref part="A6" gate="G$1" pin="2"/>
<wire x1="106.68" y1="-81.28" x2="121.92" y2="-81.28" width="0.1524" layer="91"/>
<junction x="106.68" y="-81.28"/>
<pinref part="A7" gate="G$1" pin="2"/>
<junction x="121.92" y="-81.28"/>
<pinref part="A8" gate="G$1" pin="2"/>
<wire x1="121.92" y1="-81.28" x2="139.7" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="139.7" y1="-81.28" x2="157.48" y2="-81.28" width="0.1524" layer="91"/>
<junction x="139.7" y="-81.28"/>
<pinref part="A9" gate="G$1" pin="2"/>
<wire x1="157.48" y1="-81.28" x2="175.26" y2="-81.28" width="0.1524" layer="91"/>
<junction x="157.48" y="-81.28"/>
<wire x1="175.26" y1="-81.28" x2="195.58" y2="-81.28" width="0.1524" layer="91"/>
<junction x="175.26" y="-81.28"/>
<label x="238.76" y="-81.28" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-144.78" y1="-162.56" x2="-157.48" y2="-162.56" width="0.1524" layer="91"/>
<label x="-157.48" y="-162.56" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U$2" gate="G$1" pin="P13"/>
</segment>
<segment>
<wire x1="-190.5" y1="-104.14" x2="-213.36" y2="-104.14" width="0.1524" layer="91"/>
<label x="-190.5" y="-104.14" size="1.778" layer="95" xref="yes"/>
<pinref part="J5" gate="G$1" pin="1"/>
</segment>
</net>
<net name="C" class="0">
<segment>
<pinref part="C10" gate="G$1" pin="4"/>
<wire x1="203.2" y1="-127" x2="210.82" y2="-127" width="0.1524" layer="91"/>
<label x="238.76" y="-127" size="1.778" layer="95" xref="yes"/>
<pinref part="C11" gate="A" pin="1"/>
<wire x1="210.82" y1="-127" x2="238.76" y2="-127" width="0.1524" layer="91"/>
<wire x1="210.82" y1="-119.38" x2="210.82" y2="-127" width="0.1524" layer="91"/>
<junction x="210.82" y="-127"/>
</segment>
<segment>
<wire x1="-144.78" y1="-137.16" x2="-157.48" y2="-137.16" width="0.1524" layer="91"/>
<label x="-157.48" y="-137.16" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U$2" gate="G$1" pin="P02"/>
</segment>
</net>
<net name="D" class="0">
<segment>
<pinref part="D11" gate="G$1" pin="4"/>
<wire x1="238.76" y1="-142.24" x2="226.06" y2="-142.24" width="0.1524" layer="91"/>
<label x="238.76" y="-142.24" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-144.78" y1="-142.24" x2="-157.48" y2="-142.24" width="0.1524" layer="91"/>
<label x="-157.48" y="-142.24" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U$2" gate="G$1" pin="P04"/>
</segment>
</net>
<net name="8" class="0">
<segment>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="D8" gate="G$1" pin="2"/>
<pinref part="E8" gate="G$1" pin="2"/>
<pinref part="B8" gate="G$1" pin="1"/>
<wire x1="157.48" y1="-109.22" x2="167.64" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="A8" gate="G$1" pin="1"/>
<wire x1="167.64" y1="-109.22" x2="167.64" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="167.64" y1="-91.44" x2="157.48" y2="-91.44" width="0.1524" layer="91"/>
<junction x="167.64" y="-109.22"/>
<wire x1="167.64" y1="-91.44" x2="167.64" y2="-71.12" width="0.1524" layer="91"/>
<junction x="167.64" y="-91.44"/>
<label x="167.64" y="-71.12" size="1.778" layer="95" xref="yes"/>
<wire x1="167.64" y1="-149.86" x2="167.64" y2="-132.08" width="0.1524" layer="91"/>
<wire x1="167.64" y1="-132.08" x2="167.64" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="167.64" y1="-116.84" x2="167.64" y2="-109.22" width="0.1524" layer="91"/>
<wire x1="165.1" y1="-116.84" x2="167.64" y2="-116.84" width="0.1524" layer="91"/>
<junction x="167.64" y="-116.84"/>
<wire x1="165.1" y1="-132.08" x2="167.64" y2="-132.08" width="0.1524" layer="91"/>
<junction x="167.64" y="-132.08"/>
<wire x1="165.1" y1="-149.86" x2="167.64" y2="-149.86" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="-144.78" y1="-170.18" x2="-149.86" y2="-170.18" width="0.1524" layer="91"/>
<label x="-149.86" y="-170.18" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U$2" gate="G$1" pin="P16"/>
</segment>
</net>
<net name="10" class="0">
<segment>
<pinref part="E10" gate="G$1" pin="2"/>
<pinref part="D10" gate="G$1" pin="2"/>
<pinref part="C10" gate="G$1" pin="2"/>
<pinref part="B10" gate="G$1" pin="1"/>
<wire x1="195.58" y1="-109.22" x2="205.74" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="A10" gate="G$1" pin="1"/>
<wire x1="195.58" y1="-91.44" x2="205.74" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="205.74" y1="-91.44" x2="205.74" y2="-109.22" width="0.1524" layer="91"/>
<junction x="205.74" y="-109.22"/>
<wire x1="205.74" y1="-91.44" x2="205.74" y2="-71.12" width="0.1524" layer="91"/>
<junction x="205.74" y="-91.44"/>
<label x="205.74" y="-71.12" size="1.778" layer="95" xref="yes"/>
<wire x1="203.2" y1="-149.86" x2="205.74" y2="-149.86" width="0.1524" layer="91"/>
<wire x1="205.74" y1="-109.22" x2="205.74" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="205.74" y1="-116.84" x2="205.74" y2="-132.08" width="0.1524" layer="91"/>
<wire x1="205.74" y1="-132.08" x2="205.74" y2="-149.86" width="0.1524" layer="91"/>
<wire x1="203.2" y1="-132.08" x2="205.74" y2="-132.08" width="0.1524" layer="91"/>
<junction x="205.74" y="-132.08"/>
<wire x1="203.2" y1="-116.84" x2="205.74" y2="-116.84" width="0.1524" layer="91"/>
<junction x="205.74" y="-116.84"/>
</segment>
<segment>
<wire x1="-149.86" y1="-134.62" x2="-144.78" y2="-134.62" width="0.1524" layer="91"/>
<label x="-149.86" y="-134.62" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U$2" gate="G$1" pin="P01"/>
</segment>
</net>
<net name="11" class="0">
<segment>
<pinref part="E11" gate="G$1" pin="2"/>
<pinref part="D11" gate="G$1" pin="2"/>
<junction x="228.6" y="-106.68"/>
<wire x1="226.06" y1="-106.68" x2="228.6" y2="-106.68" width="0.1524" layer="91"/>
<label x="228.6" y="-71.12" size="1.778" layer="95" xref="yes"/>
<wire x1="228.6" y1="-106.68" x2="228.6" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="226.06" y1="-149.86" x2="228.6" y2="-149.86" width="0.1524" layer="91"/>
<wire x1="228.6" y1="-106.68" x2="228.6" y2="-119.38" width="0.1524" layer="91"/>
<wire x1="228.6" y1="-119.38" x2="228.6" y2="-132.08" width="0.1524" layer="91"/>
<wire x1="228.6" y1="-132.08" x2="228.6" y2="-149.86" width="0.1524" layer="91"/>
<wire x1="226.06" y1="-132.08" x2="228.6" y2="-132.08" width="0.1524" layer="91"/>
<junction x="228.6" y="-132.08"/>
<wire x1="226.06" y1="-119.38" x2="228.6" y2="-119.38" width="0.1524" layer="91"/>
<junction x="228.6" y="-119.38"/>
<pinref part="C11" gate="A" pin="2"/>
<pinref part="B11" gate="A" pin="2"/>
</segment>
<segment>
<wire x1="-144.78" y1="-132.08" x2="-157.48" y2="-132.08" width="0.1524" layer="91"/>
<label x="-157.48" y="-132.08" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U$2" gate="G$1" pin="P00"/>
</segment>
<segment>
<wire x1="-213.36" y1="-116.84" x2="-203.2" y2="-116.84" width="0.1524" layer="91"/>
<label x="-203.2" y="-116.84" size="1.778" layer="95" xref="yes"/>
<pinref part="J5" gate="G$1" pin="6"/>
</segment>
</net>
<net name="N$256" class="0">
<segment>
<pinref part="C9" gate="G$1" pin="4"/>
<wire x1="182.88" y1="-127" x2="193.04" y2="-127" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="3"/>
</segment>
</net>
<net name="N$26RE" class="0">
<segment>
<pinref part="D9" gate="G$1" pin="4"/>
<wire x1="193.04" y1="-142.24" x2="182.88" y2="-142.24" width="0.1524" layer="91"/>
<pinref part="D10" gate="G$1" pin="3"/>
</segment>
</net>
<net name="WF" class="0">
<segment>
<pinref part="E9" gate="G$1" pin="4"/>
<pinref part="E10" gate="G$1" pin="3"/>
<wire x1="180.34" y1="-160.02" x2="193.04" y2="-160.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="WFEW" class="0">
<segment>
<pinref part="E10" gate="G$1" pin="4"/>
<pinref part="E11" gate="G$1" pin="3"/>
<wire x1="215.9" y1="-160.02" x2="203.2" y2="-160.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="EF2" class="0">
<segment>
<pinref part="D10" gate="G$1" pin="4"/>
<pinref part="D11" gate="G$1" pin="3"/>
<wire x1="215.9" y1="-142.24" x2="203.2" y2="-142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="POWER_MAIN" class="0">
<segment>
<pinref part="R0_100K" gate="G$1" pin="2"/>
<pinref part="AO4622" gate="1" pin="G"/>
<wire x1="60.96" y1="-205.74" x2="60.96" y2="-203.2" width="0.1524" layer="91"/>
<wire x1="60.96" y1="-203.2" x2="68.58" y2="-203.2" width="0.1524" layer="91"/>
<wire x1="60.96" y1="-203.2" x2="48.26" y2="-203.2" width="0.1524" layer="91"/>
<junction x="60.96" y="-203.2"/>
<label x="48.26" y="-203.2" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-78.74" y1="-210.82" x2="-104.14" y2="-210.82" width="0.1524" layer="91"/>
<label x="-78.74" y="-210.82" size="1.778" layer="95" xref="yes"/>
<pinref part="B2" gate="G$1" pin="13(SCK)"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="C_P1" gate="G$1" pin="B-"/>
<wire x1="195.58" y1="-195.58" x2="182.88" y2="-195.58" width="0.1524" layer="91"/>
<wire x1="182.88" y1="-195.58" x2="182.88" y2="-185.42" width="0.1524" layer="91"/>
<pinref part="BAT-" gate="G$1" pin="P"/>
<wire x1="182.88" y1="-185.42" x2="180.34" y2="-185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BAT+" class="0">
<segment>
<pinref part="C_P1" gate="G$1" pin="B+"/>
<wire x1="195.58" y1="-208.28" x2="190.5" y2="-208.28" width="0.1524" layer="91"/>
<pinref part="BAT+" gate="G$1" pin="P"/>
<wire x1="190.5" y1="-208.28" x2="190.5" y2="-177.8" width="0.1524" layer="91"/>
<wire x1="190.5" y1="-177.8" x2="180.34" y2="-177.8" width="0.1524" layer="91"/>
<label x="177.8" y="-177.8" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<label x="246.38" y="-48.26" size="1.778" layer="95" rot="R270" xref="yes"/>
<pinref part="SI2301" gate="G$1" pin="S"/>
<wire x1="246.38" y1="-48.26" x2="246.38" y2="-35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TP_IRQ" class="0">
<segment>
<pinref part="JP2" gate="A" pin="11"/>
<wire x1="-30.48" y1="-165.1" x2="-48.26" y2="-165.1" width="0.1524" layer="91"/>
<label x="-48.26" y="-165.1" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-213.36" y1="-81.28" x2="-208.28" y2="-81.28" width="0.1524" layer="91"/>
<label x="-208.28" y="-81.28" size="1.778" layer="95" xref="yes"/>
<pinref part="J1" gate="G$1" pin="5"/>
<junction x="-213.36" y="-81.28"/>
</segment>
</net>
<net name="LCD_RS" class="0">
<segment>
<pinref part="JP2" gate="A" pin="18"/>
<wire x1="-22.86" y1="-172.72" x2="-15.24" y2="-172.72" width="0.1524" layer="91"/>
<label x="-15.24" y="-172.72" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-213.36" y1="-76.2" x2="-208.28" y2="-76.2" width="0.1524" layer="91"/>
<label x="-208.28" y="-76.2" size="1.778" layer="95" xref="yes"/>
<pinref part="J1" gate="G$1" pin="7"/>
<junction x="-213.36" y="-76.2"/>
</segment>
</net>
<net name="LCD_SI" class="0">
<segment>
<pinref part="JP2" gate="A" pin="19"/>
<wire x1="-30.48" y1="-175.26" x2="-48.26" y2="-175.26" width="0.1524" layer="91"/>
<label x="-48.26" y="-175.26" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-213.36" y1="-73.66" x2="-193.04" y2="-73.66" width="0.1524" layer="91"/>
<label x="-193.04" y="-73.66" size="1.778" layer="95" xref="yes"/>
<pinref part="J1" gate="G$1" pin="8"/>
<junction x="-213.36" y="-73.66"/>
</segment>
</net>
<net name="TP_SO" class="0">
<segment>
<pinref part="JP2" gate="A" pin="21"/>
<wire x1="-30.48" y1="-177.8" x2="-38.1" y2="-177.8" width="0.1524" layer="91"/>
<label x="-38.1" y="-177.8" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-213.36" y1="-68.58" x2="-193.04" y2="-68.58" width="0.1524" layer="91"/>
<label x="-193.04" y="-68.58" size="1.778" layer="95" xref="yes"/>
<pinref part="J1" gate="G$1" pin="10"/>
<junction x="-213.36" y="-68.58"/>
</segment>
</net>
<net name="RST" class="0">
<segment>
<pinref part="JP2" gate="A" pin="22"/>
<wire x1="-22.86" y1="-177.8" x2="-15.24" y2="-177.8" width="0.1524" layer="91"/>
<label x="-15.24" y="-177.8" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-213.36" y1="-66.04" x2="-208.28" y2="-66.04" width="0.1524" layer="91"/>
<label x="-208.28" y="-66.04" size="1.778" layer="95" xref="yes"/>
<pinref part="J1" gate="G$1" pin="11"/>
<junction x="-213.36" y="-66.04"/>
</segment>
</net>
<net name="LCD_SCK" class="0">
<segment>
<pinref part="JP2" gate="A" pin="23"/>
<wire x1="-30.48" y1="-180.34" x2="-48.26" y2="-180.34" width="0.1524" layer="91"/>
<label x="-48.26" y="-180.34" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-213.36" y1="-63.5" x2="-193.04" y2="-63.5" width="0.1524" layer="91"/>
<label x="-193.04" y="-63.5" size="1.778" layer="95" xref="yes"/>
<pinref part="J1" gate="G$1" pin="12"/>
<junction x="-213.36" y="-63.5"/>
</segment>
</net>
<net name="LCD_CS" class="0">
<segment>
<pinref part="JP2" gate="A" pin="24"/>
<wire x1="-22.86" y1="-180.34" x2="-2.54" y2="-180.34" width="0.1524" layer="91"/>
<label x="-2.54" y="-180.34" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-213.36" y1="-60.96" x2="-208.28" y2="-60.96" width="0.1524" layer="91"/>
<label x="-208.28" y="-60.96" size="1.778" layer="95" xref="yes"/>
<pinref part="J1" gate="G$1" pin="13"/>
<junction x="-213.36" y="-60.96"/>
</segment>
</net>
<net name="TP_CS" class="0">
<segment>
<pinref part="JP2" gate="A" pin="26"/>
<wire x1="-22.86" y1="-182.88" x2="-15.24" y2="-182.88" width="0.1524" layer="91"/>
<label x="-15.24" y="-182.88" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-213.36" y1="-58.42" x2="-193.04" y2="-58.42" width="0.1524" layer="91"/>
<label x="-193.04" y="-58.42" size="1.778" layer="95" xref="yes"/>
<pinref part="J1" gate="G$1" pin="14"/>
<junction x="-213.36" y="-58.42"/>
</segment>
</net>
<net name="GP18" class="0">
<segment>
<pinref part="JP2" gate="A" pin="12"/>
<wire x1="-22.86" y1="-165.1" x2="-2.54" y2="-165.1" width="0.1524" layer="91"/>
<label x="-2.54" y="-165.1" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-213.36" y1="-55.88" x2="-208.28" y2="-55.88" width="0.1524" layer="91"/>
<label x="-208.28" y="-55.88" size="1.778" layer="95" xref="yes"/>
<pinref part="J1" gate="G$1" pin="15"/>
<junction x="-213.36" y="-55.88"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="R1_100K" gate="G$1" pin="2"/>
<wire x1="91.44" y1="-175.26" x2="104.14" y2="-175.26" width="0.1524" layer="91"/>
<pinref part="AO4622" gate="2" pin="S"/>
<wire x1="104.14" y1="-175.26" x2="104.14" y2="-182.88" width="0.1524" layer="91"/>
<junction x="104.14" y="-175.26"/>
<wire x1="104.14" y1="-175.26" x2="124.46" y2="-175.26" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="VOUT+"/>
<wire x1="124.46" y1="-175.26" x2="124.46" y2="-195.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="C_P1" gate="G$1" pin="OUT+"/>
<wire x1="195.58" y1="-213.36" x2="177.8" y2="-213.36" width="0.1524" layer="91"/>
<wire x1="177.8" y1="-213.36" x2="177.8" y2="-195.58" width="0.1524" layer="91"/>
<wire x1="177.8" y1="-195.58" x2="170.18" y2="-195.58" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="VIN+"/>
</segment>
</net>
<net name="LCD_5V" class="0">
<segment>
<wire x1="215.9" y1="-25.4" x2="215.9" y2="-20.32" width="0.1524" layer="91"/>
<label x="215.9" y="-20.32" size="1.778" layer="95" rot="R90" xref="yes"/>
<pinref part="Q1" gate="G$1" pin="D"/>
</segment>
<segment>
<wire x1="-213.36" y1="-91.44" x2="-208.28" y2="-91.44" width="0.1524" layer="91"/>
<label x="-208.28" y="-91.44" size="1.778" layer="95" xref="yes"/>
<pinref part="J1" gate="G$1" pin="1"/>
<junction x="-213.36" y="-91.44"/>
</segment>
<segment>
<wire x1="-213.36" y1="-88.9" x2="-193.04" y2="-88.9" width="0.1524" layer="91"/>
<label x="-193.04" y="-88.9" size="1.778" layer="95" xref="yes"/>
<pinref part="J1" gate="G$1" pin="2"/>
<junction x="-213.36" y="-88.9"/>
</segment>
</net>
<net name="POWER_LCD" class="0">
<segment>
<wire x1="210.82" y1="-33.02" x2="208.28" y2="-33.02" width="0.1524" layer="91"/>
<label x="205.74" y="-33.02" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="R5_100K" gate="G$1" pin="1"/>
<wire x1="208.28" y1="-33.02" x2="205.74" y2="-33.02" width="0.1524" layer="91"/>
<junction x="208.28" y="-33.02"/>
<pinref part="Q1" gate="G$1" pin="G"/>
</segment>
<segment>
<wire x1="-99.06" y1="-213.36" x2="-104.14" y2="-213.36" width="0.1524" layer="91"/>
<label x="-99.06" y="-213.36" size="1.778" layer="95" xref="yes"/>
<pinref part="B2" gate="G$1" pin="12(MISO)"/>
</segment>
</net>
<net name="SQW" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="SQW/INT"/>
<wire x1="-162.56" y1="-99.06" x2="-147.32" y2="-99.06" width="0.1524" layer="91"/>
<label x="-162.56" y="-99.06" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R1_100K" gate="G$1" pin="1"/>
<pinref part="AO4622" gate="2" pin="G"/>
<pinref part="AO4622" gate="1" pin="D"/>
<wire x1="91.44" y1="-185.42" x2="99.06" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="73.66" y1="-195.58" x2="73.66" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="73.66" y1="-185.42" x2="81.28" y2="-185.42" width="0.1524" layer="91"/>
<junction x="91.44" y="-185.42"/>
<wire x1="81.28" y1="-185.42" x2="91.44" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="73.66" y1="-185.42" x2="48.26" y2="-185.42" width="0.1524" layer="91"/>
<junction x="73.66" y="-185.42"/>
<label x="48.26" y="-185.42" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="D12" gate="G$1" pin="A2"/>
<wire x1="83.82" y1="-193.04" x2="81.28" y2="-193.04" width="0.1524" layer="91"/>
<wire x1="81.28" y1="-193.04" x2="81.28" y2="-185.42" width="0.1524" layer="91"/>
<junction x="81.28" y="-185.42"/>
</segment>
<segment>
<pinref part="SI2301" gate="G$1" pin="G"/>
<label x="236.22" y="-33.02" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="241.3" y1="-33.02" x2="236.22" y2="-33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BAT_M" class="0">
<segment>
<pinref part="SI2301" gate="G$1" pin="D"/>
<wire x1="246.38" y1="-25.4" x2="246.38" y2="-20.32" width="0.1524" layer="91"/>
<label x="246.38" y="-20.32" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<label x="-149.86" y="-218.44" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="B2" gate="G$1" pin="*9"/>
<wire x1="-149.86" y1="-218.44" x2="-127" y2="-218.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A_SDA" class="0">
<segment>
<wire x1="-127" y1="-200.66" x2="-132.08" y2="-200.66" width="0.1524" layer="91"/>
<label x="-132.08" y="-200.66" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="B2" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-119.38" y1="-152.4" x2="-86.36" y2="-152.4" width="0.1524" layer="91"/>
<label x="-81.28" y="-152.4" size="1.778" layer="95" xref="yes"/>
<pinref part="U$2" gate="G$1" pin="SDA"/>
<pinref part="R7_10K" gate="G$1" pin="1"/>
<wire x1="-86.36" y1="-152.4" x2="-81.28" y2="-152.4" width="0.1524" layer="91"/>
<wire x1="-86.36" y1="-157.48" x2="-86.36" y2="-152.4" width="0.1524" layer="91"/>
<junction x="-86.36" y="-152.4"/>
</segment>
</net>
<net name="A_SCL" class="0">
<segment>
<wire x1="-149.86" y1="-203.2" x2="-127" y2="-203.2" width="0.1524" layer="91"/>
<label x="-149.86" y="-203.2" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="B2" gate="G$1" pin="*3"/>
</segment>
<segment>
<wire x1="-91.44" y1="-149.86" x2="-93.98" y2="-149.86" width="0.1524" layer="91"/>
<label x="-91.44" y="-149.86" size="1.778" layer="95" xref="yes"/>
<pinref part="U$2" gate="G$1" pin="SCL"/>
<pinref part="R6_10K" gate="G$1" pin="1"/>
<wire x1="-93.98" y1="-149.86" x2="-119.38" y2="-149.86" width="0.1524" layer="91"/>
<wire x1="-93.98" y1="-147.32" x2="-93.98" y2="-149.86" width="0.1524" layer="91"/>
<junction x="-93.98" y="-149.86"/>
</segment>
</net>
<net name="U1" class="0">
<segment>
<wire x1="-127" y1="-205.74" x2="-132.08" y2="-205.74" width="0.1524" layer="91"/>
<label x="-132.08" y="-205.74" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="B2" gate="G$1" pin="4"/>
</segment>
<segment>
<wire x1="-213.36" y1="-106.68" x2="-203.2" y2="-106.68" width="0.1524" layer="91"/>
<label x="-203.2" y="-106.68" size="1.778" layer="95" xref="yes"/>
<pinref part="J5" gate="G$1" pin="2"/>
</segment>
</net>
<net name="U2" class="0">
<segment>
<wire x1="-127" y1="-208.28" x2="-149.86" y2="-208.28" width="0.1524" layer="91"/>
<label x="-149.86" y="-208.28" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="B2" gate="G$1" pin="*5"/>
</segment>
<segment>
<wire x1="-190.5" y1="-109.22" x2="-213.36" y2="-109.22" width="0.1524" layer="91"/>
<label x="-190.5" y="-109.22" size="1.778" layer="95" xref="yes"/>
<pinref part="J5" gate="G$1" pin="3"/>
</segment>
</net>
<net name="U3" class="0">
<segment>
<wire x1="-127" y1="-213.36" x2="-149.86" y2="-213.36" width="0.1524" layer="91"/>
<label x="-149.86" y="-213.36" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="B2" gate="G$1" pin="7"/>
</segment>
<segment>
<label x="-190.5" y="-119.38" size="1.778" layer="95" xref="yes"/>
<wire x1="-213.36" y1="-119.38" x2="-190.5" y2="-119.38" width="0.1524" layer="91"/>
<pinref part="J5" gate="G$1" pin="7"/>
</segment>
</net>
<net name="LED_2" class="0">
<segment>
<wire x1="-127" y1="-210.82" x2="-132.08" y2="-210.82" width="0.1524" layer="91"/>
<label x="-132.08" y="-210.82" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="B2" gate="G$1" pin="*6"/>
</segment>
<segment>
<wire x1="-213.36" y1="-111.76" x2="-203.2" y2="-111.76" width="0.1524" layer="91"/>
<label x="-203.2" y="-111.76" size="1.778" layer="95" xref="yes"/>
<pinref part="J5" gate="G$1" pin="4"/>
</segment>
</net>
<net name="U5" class="0">
<segment>
<wire x1="-127" y1="-215.9" x2="-132.08" y2="-215.9" width="0.1524" layer="91"/>
<label x="-132.08" y="-215.9" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="B2" gate="G$1" pin="8"/>
</segment>
<segment>
<wire x1="-213.36" y1="-121.92" x2="-203.2" y2="-121.92" width="0.1524" layer="91"/>
<label x="-203.2" y="-121.92" size="1.778" layer="95" xref="yes"/>
<pinref part="J5" gate="G$1" pin="8"/>
</segment>
</net>
<net name="EX_5V" class="0">
<segment>
<wire x1="175.26" y1="-25.4" x2="175.26" y2="-20.32" width="0.1524" layer="91"/>
<label x="175.26" y="-20.32" size="1.778" layer="95" rot="R90" xref="yes"/>
<pinref part="Q3" gate="G$1" pin="D"/>
</segment>
<segment>
<wire x1="-50.8" y1="-86.36" x2="-33.02" y2="-86.36" width="0.1524" layer="91"/>
<label x="-50.8" y="-86.36" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP1" gate="A" pin="1"/>
</segment>
<segment>
<wire x1="-50.8" y1="-101.6" x2="-33.02" y2="-101.6" width="0.1524" layer="91"/>
<label x="-50.8" y="-101.6" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP1" gate="A" pin="13"/>
</segment>
</net>
<net name="POWER_EX" class="0">
<segment>
<wire x1="-78.74" y1="-215.9" x2="-104.14" y2="-215.9" width="0.1524" layer="91"/>
<label x="-78.74" y="-215.9" size="1.778" layer="95" xref="yes"/>
<pinref part="B2" gate="G$1" pin="11(MOSI)"/>
</segment>
<segment>
<wire x1="170.18" y1="-33.02" x2="167.64" y2="-33.02" width="0.1524" layer="91"/>
<label x="165.1" y="-33.02" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="R8_100K" gate="G$1" pin="1"/>
<wire x1="167.64" y1="-33.02" x2="165.1" y2="-33.02" width="0.1524" layer="91"/>
<junction x="167.64" y="-33.02"/>
<pinref part="Q3" gate="G$1" pin="G"/>
</segment>
<segment>
<wire x1="134.62" y1="-33.02" x2="132.08" y2="-33.02" width="0.1524" layer="91"/>
<label x="129.54" y="-33.02" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="R8_100K1" gate="G$1" pin="1"/>
<wire x1="132.08" y1="-33.02" x2="129.54" y2="-33.02" width="0.1524" layer="91"/>
<junction x="132.08" y="-33.02"/>
<pinref part="Q2" gate="G$1" pin="G"/>
</segment>
</net>
<net name="BUZZER" class="0">
<segment>
<label x="-193.04" y="-139.7" size="1.778" layer="95" xref="yes"/>
<pinref part="PAD1" gate="G$1" pin="P"/>
<wire x1="-215.9" y1="-139.7" x2="-193.04" y2="-139.7" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="-99.06" y1="-203.2" x2="-104.14" y2="-203.2" width="0.1524" layer="91"/>
<label x="-99.06" y="-203.2" size="1.778" layer="95" xref="yes"/>
<pinref part="B2" gate="G$1" pin="A2"/>
</segment>
</net>
<net name="U4" class="0">
<segment>
<wire x1="-213.36" y1="-124.46" x2="-190.5" y2="-124.46" width="0.1524" layer="91"/>
<label x="-190.5" y="-124.46" size="1.778" layer="95" xref="yes"/>
<pinref part="J5" gate="G$1" pin="9"/>
</segment>
<segment>
<wire x1="-78.74" y1="-205.74" x2="-104.14" y2="-205.74" width="0.1524" layer="91"/>
<label x="-78.74" y="-205.74" size="1.778" layer="95" xref="yes"/>
<pinref part="B2" gate="G$1" pin="A1"/>
</segment>
</net>
<net name="B_BATTERY+" class="0">
<segment>
<pinref part="B_BATTERY+" gate="1" pin="P"/>
<wire x1="-215.9" y1="-157.48" x2="-200.66" y2="-157.48" width="0.1524" layer="91"/>
<label x="-200.66" y="-157.48" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="VBAT"/>
<wire x1="-116.84" y1="-99.06" x2="-99.06" y2="-99.06" width="0.1524" layer="91"/>
<wire x1="-99.06" y1="-99.06" x2="-99.06" y2="-101.6" width="0.1524" layer="91"/>
<pinref part="C2_100NF" gate="G$1" pin="2"/>
<wire x1="-99.06" y1="-99.06" x2="-88.9" y2="-99.06" width="0.1524" layer="91"/>
<junction x="-99.06" y="-99.06"/>
<label x="-88.9" y="-99.06" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="POW" gate="G$1" pin="2"/>
<pinref part="D12" gate="G$1" pin="CC"/>
<wire x1="88.9" y1="-195.58" x2="88.9" y2="-198.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="POWER_OFF" class="0">
<segment>
<wire x1="48.26" y1="-172.72" x2="96.52" y2="-172.72" width="0.1524" layer="91"/>
<label x="48.26" y="-172.72" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="96.52" y1="-172.72" x2="96.52" y2="-193.04" width="0.1524" layer="91"/>
<pinref part="D12" gate="G$1" pin="A1"/>
<wire x1="96.52" y1="-193.04" x2="93.98" y2="-193.04" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="-99.06" y1="-208.28" x2="-104.14" y2="-208.28" width="0.1524" layer="91"/>
<label x="-99.06" y="-208.28" size="1.778" layer="95" xref="yes"/>
<pinref part="B2" gate="G$1" pin="A0"/>
</segment>
</net>
<net name="IN+" class="0">
<segment>
<pinref part="C_P1" gate="G$1" pin="IN+"/>
<wire x1="241.3" y1="-190.5" x2="248.92" y2="-190.5" width="0.1524" layer="91"/>
<label x="248.92" y="-190.5" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-5.08" y1="-137.16" x2="-22.86" y2="-137.16" width="0.1524" layer="91"/>
<label x="-5.08" y="-137.16" size="1.778" layer="95" xref="yes"/>
<pinref part="J3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="IN-" class="0">
<segment>
<pinref part="C_P1" gate="G$1" pin="IN-"/>
<wire x1="248.92" y1="-213.36" x2="241.3" y2="-213.36" width="0.1524" layer="91"/>
<label x="248.92" y="-213.36" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-22.86" y1="-134.62" x2="-15.24" y2="-134.62" width="0.1524" layer="91"/>
<label x="-15.24" y="-134.62" size="1.778" layer="95" xref="yes"/>
<pinref part="J3" gate="G$1" pin="3"/>
</segment>
</net>
<net name="EX_3V" class="0">
<segment>
<wire x1="-5.08" y1="-88.9" x2="-25.4" y2="-88.9" width="0.1524" layer="91"/>
<label x="-5.08" y="-88.9" size="1.778" layer="95" xref="yes"/>
<pinref part="JP1" gate="A" pin="4"/>
</segment>
<segment>
<wire x1="139.7" y1="-25.4" x2="139.7" y2="-20.32" width="0.1524" layer="91"/>
<label x="139.7" y="-20.32" size="1.778" layer="95" rot="R90" xref="yes"/>
<pinref part="Q2" gate="G$1" pin="D"/>
</segment>
</net>
<net name="POWER_SENSORS" class="0">
<segment>
<wire x1="-127" y1="-193.04" x2="-149.86" y2="-193.04" width="0.1524" layer="91"/>
<label x="-149.86" y="-193.04" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="B2" gate="G$1" pin="RXI"/>
</segment>
<segment>
<wire x1="93.98" y1="-33.02" x2="91.44" y2="-33.02" width="0.1524" layer="91"/>
<label x="88.9" y="-33.02" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="R8_100K3" gate="G$1" pin="1"/>
<wire x1="91.44" y1="-33.02" x2="88.9" y2="-33.02" width="0.1524" layer="91"/>
<junction x="91.44" y="-33.02"/>
<pinref part="Q5" gate="G$1" pin="G"/>
</segment>
<segment>
<wire x1="50.8" y1="-33.02" x2="48.26" y2="-33.02" width="0.1524" layer="91"/>
<label x="45.72" y="-33.02" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="R8_100K2" gate="G$1" pin="1"/>
<wire x1="48.26" y1="-33.02" x2="45.72" y2="-33.02" width="0.1524" layer="91"/>
<junction x="48.26" y="-33.02"/>
<pinref part="Q4" gate="G$1" pin="G"/>
</segment>
</net>
<net name="DOCK/A_TX" class="0">
<segment>
<wire x1="-127" y1="-190.5" x2="-132.08" y2="-190.5" width="0.1524" layer="91"/>
<label x="-132.08" y="-190.5" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="B2" gate="G$1" pin="TXO"/>
</segment>
<segment>
<wire x1="-33.02" y1="-129.54" x2="-50.8" y2="-129.54" width="0.1524" layer="91"/>
<label x="-50.8" y="-129.54" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="J3" gate="G$1" pin="8"/>
</segment>
</net>
<net name="THP_3V" class="0">
<segment>
<wire x1="55.88" y1="-25.4" x2="55.88" y2="-20.32" width="0.1524" layer="91"/>
<label x="55.88" y="-20.32" size="1.778" layer="95" rot="R90" xref="yes"/>
<pinref part="Q4" gate="G$1" pin="D"/>
</segment>
<segment>
<wire x1="-50.8" y1="-25.4" x2="-58.42" y2="-25.4" width="0.1524" layer="91"/>
<label x="-58.42" y="-25.4" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U$5" gate="G$1" pin="VCC"/>
</segment>
</net>
<net name="GYRO_3V" class="0">
<segment>
<wire x1="99.06" y1="-25.4" x2="99.06" y2="-20.32" width="0.1524" layer="91"/>
<label x="99.06" y="-20.32" size="1.778" layer="95" rot="R90" xref="yes"/>
<pinref part="Q5" gate="G$1" pin="D"/>
</segment>
<segment>
<wire x1="-127" y1="-22.86" x2="-134.62" y2="-22.86" width="0.1524" layer="91"/>
<label x="-134.62" y="-22.86" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U$4" gate="G$1" pin="VCC"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="C_P1" gate="G$1" pin="OUT-"/>
<wire x1="195.58" y1="-190.5" x2="180.34" y2="-190.5" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="VIN-"/>
<wire x1="180.34" y1="-190.5" x2="180.34" y2="-208.28" width="0.1524" layer="91"/>
<wire x1="180.34" y1="-208.28" x2="170.18" y2="-208.28" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
<note version="9.4" severity="warning">
Since Version 9.4, EAGLE supports the overriding of 3D packages
in schematics and board files. Those overridden 3d packages
will not be understood (or retained) with this version.
</note>
<note version="8.4" severity="warning">
Since Version 8.4, EAGLE supports properties for SPICE simulation. 
Probes in schematics and SPICE mapping objects found in parts and library devices
will not be understood with this version. Update EAGLE to the latest version
for full support of SPICE simulation. 
</note>
</compatibility>
</eagle>
