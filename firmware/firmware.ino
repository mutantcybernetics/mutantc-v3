// firmware V3.4
// mutantC v3

#include <Keypad.h>
/*
 * Depending on which i2c expander chip is used different i2c keypad library has to be used.
 * https://github.com/joeyoung/arduino_keypads is used here.
 * 
 * For PCF8575  inlucde <Keypad_I2C.h>
 * For PCA9555D include <Keypad_I2Ca.h>
 */
#include <Keypad_I2C.h>
#include <HID-Project.h>
#include <Wire.h>

#define FIRMWARE_VERSION 3410

#define LED_BODY        6
#define LED_DISPLAY     10
#define POWER_MAIN      15
#define POWER_OFF_BTN   A0
#define BATTERY_VOLTAGE A9
#define BUZZER          A2

#define POWER_LCD     14
#define POWER_EXP     16
#define POWER_SENSORS 0

#define PI_POWEROFF_SIGNAL A3

#define DOCK_PIN 1

/* Thumbstick buttons */
#define THUMB_UP     7
#define THUMB_DOWN   A1
#define THUMB_LEFT   5
#define THUMB_RIGHT  8
#define THUMB_MIDDLE 4

// Switch Stats
static bool keymapState = 0;

// For Keypad
#define KBD_ROWS 5
#define KBD_COLS 11

#define KEYS_TILDE      KEY_TILDE|MOD_LEFT_SHIFT       /* ~ */
#define KEYS_EXCL       KEY_1|MOD_LEFT_SHIFT           /* ! */
#define KEYS_AT         KEY_2|MOD_LEFT_SHIFT           /* @ */
#define KEYS_HASH       KEY_3|MOD_LEFT_SHIFT           /* # */
#define KEYS_DOLLAR     KEY_4|MOD_LEFT_SHIFT           /* $ */
#define KEYS_PERCENT    KEY_5|MOD_LEFT_SHIFT           /* % */
#define KEYS_CARET      KEY_6|MOD_LEFT_SHIFT           /* ^ */
#define KEYS_AMP        KEY_7|MOD_LEFT_SHIFT           /* & */
#define KEYS_ASTERISK   KEY_8|MOD_LEFT_SHIFT           /* * */
#define KEYS_LRBRACKET  KEY_9|MOD_LEFT_SHIFT           /* ( */
#define KEYS_RRBRACKET  KEY_0|MOD_LEFT_SHIFT           /* ) */
#define KEYS_UNDERSCORE KEY_MINUS|MOD_LEFT_SHIFT       /* _ */
#define KEYS_PLUS       KEY_EQUAL|MOD_LEFT_SHIFT       /* + */
#define KEYS_LCBRACKET  KEY_LEFT_BRACE|MOD_LEFT_SHIFT  /* { */
#define KEYS_RCBRACKET  KEY_RIGHT_BRACE|MOD_LEFT_SHIFT /* } */
#define KEYS_VBAR       KEY_BACKSLASH|MOD_LEFT_SHIFT   /* \ */
#define KEYS_COLON      KEY_SEMICOLON|MOD_LEFT_SHIFT   /* : */
#define KEYS_DQUOTE     KEY_QUOTE|MOD_LEFT_SHIFT       /* " */
#define KEYS_LABRACKET  KEY_COMMA|MOD_LEFT_SHIFT       /* < */
#define KEYS_RABRACKET  KEY_PERIOD|MOD_LEFT_SHIFT      /* > */
#define KEYS_QMARK      KEY_SLASH|MOD_LEFT_SHIFT       /* ? */

#define KMAP_SWITCH           0xffff
#define LCD_SWITCH            0xfffe
#define KMOUSE_MIDDLE         0xffef
#define KMOUSE_RIGHT          0xfeff

static uint16_t keymapSymbols[] = {
    KMOUSE_RIGHT,   LCD_SWITCH,              'E',             'E',             'E',             'E',            'E',            'E',            'E',    KMAP_SWITCH,  KMOUSE_MIDDLE,
        KEY_ESC,    KEYS_EXCL,           KEYS_AT,       KEYS_HASH,      KEYS_DOLLAR,    KEYS_PERCENT,     KEYS_CARET,       KEYS_AMP, KEYS_LRBRACKET, KEYS_RRBRACKET,      KEY_LEFT,
        KEY_TAB,    KEYS_VBAR,            KEY_UP, KEYS_UNDERSCORE,       KEYS_TILDE,      KEYS_COLON,  KEY_SEMICOLON,    KEYS_DQUOTE,      KEY_QUOTE,      KEY_TILDE,     KEY_RIGHT,
  KEY_CAPS_LOCK,     KEY_LEFT,          KEY_DOWN,       KEY_RIGHT,   KEY_LEFT_BRACE, KEY_RIGHT_BRACE, KEYS_LCBRACKET, KEYS_RCBRACKET, KEYS_LABRACKET, KEYS_RABRACKET, KEY_BACKSPACE,
  KEY_LEFT_CTRL, KEY_LEFT_GUI,         KEYS_PLUS,       KEY_MINUS,        KEY_SLASH,   KEYS_ASTERISK,      KEY_EQUAL,      KEY_COMMA,     KEY_PERIOD,     KEYS_QMARK,     KEY_ENTER
};

static uint16_t keymapAlpha[] = {
   KMOUSE_RIGHT,   LCD_SWITCH,   KEY_PRINTSCREEN, KEY_INSERT, KEY_DELETE, KEY_HOME, KEY_END, KEY_PAGE_UP, KEY_PAGE_DOWN, KMAP_SWITCH,   KMOUSE_MIDDLE,
          KEY_1,        KEY_2,             KEY_3,      KEY_4,      KEY_5,    KEY_6,   KEY_7,       KEY_8,         KEY_9,       KEY_0,        KEY_LEFT,
          KEY_Q,        KEY_W,             KEY_E,      KEY_R,      KEY_T,    KEY_Y,   KEY_U,       KEY_I,         KEY_O,       KEY_P,       KEY_RIGHT,
  KEY_CAPS_LOCK,        KEY_A,             KEY_S,      KEY_D,      KEY_F,    KEY_G,   KEY_H,       KEY_J,         KEY_K,       KEY_L,   KEY_BACKSPACE,
  KEY_LEFT_CTRL, KEY_LEFT_GUI,             KEY_Z,      KEY_X,      KEY_C,    KEY_V,   KEY_B,       KEY_N,         KEY_M,   KEY_SPACE,       KEY_ENTER
};

static char dummyKeypad[KBD_ROWS][KBD_COLS] = {
  { 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10},
  {11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21},
  {22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32},
  {33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43},
  {44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54}
};

/*
 * Pin mappings on the I2C 16bit expander for the matrix keyboard.
 */
static byte kbd_row_pins[KBD_ROWS] = {11, 15, 2, 4, 5};
static byte kbd_col_pins[KBD_COLS] = {6, 9, 8, 7, 10, 12, 13, 14, 3, 1, 0};

/*
 * Depending on which I2C expander is used different I2C keypad library has to be used.
 *
 * For PCF8575  the object is called Keypad_I2C
 * For PCA9555D the object is called Keypad_I2Ca
 *
 * The bus address is the same in both cases.
 */
#define I2C_EXP_ADDR 0x20

Keypad_I2C Dummy(makeKeymap(dummyKeypad), kbd_row_pins, kbd_col_pins, KBD_ROWS, KBD_COLS, I2C_EXP_ADDR, 2);

/*
 * Temperature sensor offset should be accordingly to the datasheet between -10 and +10.
 *
 * To callibrate the offset do following:
 *
 * - Let the device sit down powered off at a room temperature for 20 minutes
 * - Turn it on, read temperature using mutantctl
 * - Adjust this constant so that the result matches room temp
 */
#define TEMP_OFFSET -7
/* Temperature in Kelvins when to power off */
static uint16_t temp_thresh = 323;
/* Last temperature measured */
static uint16_t last_temp;

/* Voltage 5V = 10bit, set to ~2750mV */
static uint16_t bat_threshold = 563;
static uint16_t bat_voltage;

void setup(void)
{
  /*  Turn on the FET that powers on the board and RPI first */
  pinMode(POWER_MAIN, OUTPUT);
  digitalWrite(POWER_MAIN, HIGH);

  /* Slow down CPU gets the arduino power consumption to 16mA */
  noInterrupts();
  CLKPR = 0x80;  // enable change of the clock prescaler
  CLKPR = 0x03;  // divide frequency by 8
  interrupts();

  /* Set up ADC MUX to temperature sensor at start */
  adc_select_temp();

  Wire.begin();
  /*
   * The I2C clock depends on the CPU clock so if we slow down CPU we have to adjust
   * the timings. Normally TWBR is set to 72 for 16Mhz CPU clock for 100kHz I2C clock,
   * we set it to 0 which yields 125kHz on 2Mhz CPU clock (since we set prescaler to 8)
   * which is good enough for us.
   */
  TWBR=0;

  Serial.begin(9600);

  pinMode(LED_BODY, OUTPUT);
  pinMode(LED_DISPLAY, OUTPUT);
  pinMode(POWER_OFF_BTN, INPUT_PULLUP);
  pinMode(POWER_EXP, OUTPUT);
  pinMode(POWER_LCD, OUTPUT);
  pinMode(POWER_SENSORS, OUTPUT);
  pinMode(BUZZER, OUTPUT);
  pinMode(BATTERY_VOLTAGE, INPUT);
  pinMode(PI_POWEROFF_SIGNAL, INPUT);
  pinMode(THUMB_UP, INPUT_PULLUP);
  pinMode(THUMB_DOWN, INPUT_PULLUP);
  pinMode(THUMB_LEFT, INPUT_PULLUP);
  pinMode(THUMB_RIGHT, INPUT_PULLUP);
  pinMode(THUMB_MIDDLE, INPUT_PULLUP);

  digitalWrite(POWER_EXP, HIGH);
  digitalWrite(POWER_LCD, LOW);
  digitalWrite(POWER_SENSORS, HIGH);
  digitalWrite(BUZZER, LOW);

  Mouse.begin();
  Dummy.begin( );

  /* Compensate for the slow CPU */
  Dummy.setHoldTime(1);
  Dummy.setDebounceTime(0);
  Dummy.addEventListener(keypadEvent);

  /* Enable 1ms timer interrupts */
  OCR0A = 0xAF;
  TIMSK0 |= 1<<OCIE0A;
}

static void notify(void)
{
  start_buzzer(10);
  start_led();
}

static void poweroff(void)
{
  uint8_t i;

  notify();

  /* Ask RPi to powerdown */
  Keyboard.write(CONSUMER_POWER);

  /* Wait for RPi to halt OS, about 16 seconds */
  for (i = 0; i < 20; i++) {
    if (digitalRead(PI_POWEROFF_SIGNAL))
      break;

    digitalWrite(LED_DISPLAY, HIGH);
    delay(25);

    if (digitalRead(PI_POWEROFF_SIGNAL))
      break;

    digitalWrite(LED_DISPLAY, LOW);
    delay(25);
  }

  /* And finally turn off the power */
  digitalWrite(POWER_MAIN, LOW);
}

static uint8_t exp_board_power_get(void)
{
  return !digitalRead(POWER_EXP);
}

static void exp_board_power_set(uint8_t val)
{
  if (val)
    digitalWrite(POWER_EXP, LOW);
  else
    digitalWrite(POWER_EXP, HIGH);
}

static uint8_t sensors_power_get(void)
{
  return !digitalRead(POWER_SENSORS);
}

static void sensors_power_set(uint8_t val)
{
  if (val)
    digitalWrite(POWER_SENSORS, LOW);
  else
    digitalWrite(POWER_SENSORS, HIGH);
}

static void lcd_power_toggle(void)
{
  if (digitalRead(POWER_LCD))
    digitalWrite(POWER_LCD, LOW);
  else
    digitalWrite(POWER_LCD, HIGH);
}

static void lcd_power_set(uint8_t val)
{
  if (val)
    digitalWrite(POWER_LCD, LOW);
  else
    digitalWrite(POWER_LCD, HIGH);
}

static uint8_t lcd_power_get(void)
{
  return !digitalRead(POWER_LCD);
}

static void adc_start(void)
{
  ADCSRA |= 1<<ADSC;
}

static void adc_wait(void)
{
  while (bit_is_set(ADCSRA, ADSC));
}

static void adc_select_temp(void)
{
  /* Set reference to 2.56V and MUX to temp sensor */

  ADMUX = (1<<REFS1) | (1<<REFS0) | (1<<MUX2) | (1<<MUX1) | (1<<MUX0);
  ADCSRB |= 1<<MUX5;
}

static void adc_select_bat(void)
{
  /* External 5V refrence and MUX to ADC9 -> battery voltage */
  ADMUX = (1<<REFS0) | (1<<MUX2);
  ADCSRB |= 1<<MUX5;
}

static inline void read_bat_temp(void)
{
  /* Read temperature first */
  adc_start();
  adc_wait();

  last_temp = ADC + TEMP_OFFSET;

  if (last_temp > temp_thresh)
    poweroff();

  adc_select_bat();
  adc_start();
  adc_wait();

  bat_voltage = ADC;

  if (bat_voltage < bat_threshold)
    poweroff();

  /*
   * The internal temperature sensor needs some time to settle down
   * so we configure the MUX for next measurement here.
   */
   adc_select_temp();
}

/*
 * Simple UART protocol for setting and getting data.
 * The message consists of a few bytes followed by a newline.
 *
 * To setup the serial port on RPi use:
 *
 * stty -F /dev/ttyACM0 9600 raw -clocal -echo icrnl
 *
 * Then you can read results and write commands with:
 *
 * cat /dev/ttyACM0
 * cat > /dev/ttyACM0
 *
 * cmd[0]:
 * ? - get value
 * ! - set value
 *
 * cmd[1]
 * T - internal temperature in Kelvin
 * P - poweroff threshold temperature in Kelvin
 * V - mutant hardware revision
 * B - battery voltage (1024 == 5V)
 * Q - poweroff threshold battery voltage (1024 == 5V)
 * L - display led
 * M - body led
 * F - firmware version
 * Z - buzzer turn on for n ns
 *
 * l - LCD screen power
 * e - expansion board power
 * s - sensors power
 *
 * cmd[2] - cmd[4]
 *  - optional command parameter
 *
 * '\n' - message end
 */

static void write_u16_res(char type, uint16_t val)
{
  Serial.write(type);
  Serial.print(val);
  Serial.write('\n');
}

static void process_serial(void)
{
  static uint8_t cmd[6];
  static uint8_t lb = 0;;
  int b;

  while ((b = Serial.read()) > 0) {
    cmd[lb] = b;

    if (cmd[lb] == '\n')
      break;

    lb = (lb + 1) % 6;
  }

  if (cmd[lb] != '\n')
    return;

  switch (cmd[0]) {
  case '?':
    switch (cmd[1]) {
    case 'T':
      write_u16_res('T', last_temp);
    break;
    case 'P':
      write_u16_res('P', temp_thresh);
    break;
    case 'V':
      write_u16_res('V', 3);
    break;
    case 'B':
      write_u16_res('B', bat_voltage);
    break;
    case 'Q':
      write_u16_res('Q', bat_threshold);
    break;
    case 'F':
      write_u16_res('F', FIRMWARE_VERSION);
    break;
    case 'L':
      write_u16_res('L', digitalRead(LED_DISPLAY));
    break;
    case 'M':
      write_u16_res('M', digitalRead(LED_BODY));
    break;
    case 'Z':
      write_u16_res('Z', digitalRead(BUZZER));
    break;
    case 'l':
      write_u16_res('l', lcd_power_get());
    break;
    case 'e':
      write_u16_res('e', exp_board_power_get());
    break;
    case 's':
      write_u16_res('s', sensors_power_get());
    break;
    }
  break;
  case '!':
    cmd[lb] = 0;
    switch (cmd[1]) {
      case 'P':
        temp_thresh = atoi(cmd+2);
      break;
      case 'Q':
        bat_threshold = atoi(cmd+2);
      break;
      case 'L':
        if (atoi(cmd+2))
          digitalWrite(LED_DISPLAY, HIGH);
        else
          digitalWrite(LED_DISPLAY, LOW);
      break;
      case 'M':
        switch (atoi(cmd+2)) {
        case 0:
          digitalWrite(LED_BODY, LOW);
        break;
        case 1:
          digitalWrite(LED_BODY, HIGH);
        break;
        case 2:
          start_led();
        break;
        }
      break;
      case 'Z':
        start_buzzer(atoi(cmd+2));
      break;
      case 'l':
        lcd_power_set(atoi(cmd+2));
      break;
      case 'e':
        exp_board_power_set(atoi(cmd+2));
      break;
      case 's':
        sensors_power_set(atoi(cmd+2));
      break;
    }
  break;
  }

  lb = 0;
}

static void selectAlphabet(void)
{
  digitalWrite(LED_DISPLAY, LOW);
  keymapState = 0;
}

static void selectSymbols(void)
{
  digitalWrite(LED_DISPLAY, HIGH);
  keymapState = 1;
}

static void switchKeymap(void)
{
  if (keymapState)
    selectAlphabet();
  else
    selectSymbols();
}

static uint16_t mapKey(char key)
{
  if (keymapState)
    return keymapSymbols[key];

  return keymapAlpha[key];
}

static void keypadEvent(KeypadEvent key)
{
  if (Dummy.getState() != PRESSED)
    return;

  uint16_t kcode = mapKey(key);

  switch (kcode) {
  case KMAP_SWITCH:
    switchKeymap();
  break;
  case LCD_SWITCH:
    lcd_power_toggle();
  break;
  }
}

static int left_shift_ref_cnt;

static void process_keys(void)
{
  if (!Dummy.getKeys())
    return;

  int i;

  for (i = 0; i < LIST_MAX; i++) {
    if (!Dummy.key[i].stateChanged)
      continue;

    uint16_t key = mapKey(Dummy.key[i].kchar);

    if (key == KMAP_SWITCH || key == LCD_SWITCH)
      continue;

    switch (Dummy.key[i].kstate) {
    case PRESSED:
      switch (key) {
      case KMOUSE_MIDDLE:
        Mouse.press(MOUSE_MIDDLE);
      break;
      case KMOUSE_RIGHT:
        Mouse.press(MOUSE_RIGHT);
      break;
      default:
        if (key & MOD_LEFT_SHIFT) {
          if (!(left_shift_ref_cnt++))
            Keyboard.press(KEY_LEFT_SHIFT);
        }
        Keyboard.press((KeyboardKeycode)key);
      break;
      }
    break;
    case RELEASED:
      switch (key) {
      case KMOUSE_MIDDLE:
        Mouse.release(MOUSE_MIDDLE);
      break;
      case KMOUSE_RIGHT:
        Mouse.release(MOUSE_RIGHT);
      break;
      default:
        if (key & MOD_LEFT_SHIFT) {
          if (!(--left_shift_ref_cnt))
            Keyboard.release(KEY_LEFT_SHIFT);
        }
        Keyboard.release((KeyboardKeycode)key);
      break;
      }
    break;
    }
  }
}

static void thumbstick(void)
{
  static int8_t x_accel = 8;
  static int8_t y_accel = 8;
  static uint8_t btn_state = 1;

  uint8_t btn = digitalRead(THUMB_MIDDLE);

  if (btn != btn_state) {
    btn_state = btn;
    if (!btn_state)
      Mouse.press(MOUSE_LEFT);
   else
      Mouse.release(MOUSE_LEFT);
  }

  int8_t up = digitalRead(THUMB_UP);
  int8_t down = digitalRead(THUMB_DOWN);
  int8_t right = digitalRead(THUMB_RIGHT);
  int8_t left = digitalRead(THUMB_LEFT);

  int8_t x_dist = (left - right) * (x_accel/8);
  int8_t y_dist = (up - down) * (y_accel/8);

  if (x_dist) {
    if (x_accel < 80)
      x_accel++;
  } else {
    x_accel = 8;
  }

  if (y_dist) {
    if (y_accel < 80)
      y_accel++;
  } else {
    y_accel = 8;
  }

  if (x_dist || y_dist)
    Mouse.move(x_dist, y_dist, 0);
}

static uint16_t buzzer_rem_ms;

static inline void process_buzzer(void)
{
  if (!buzzer_rem_ms)
    return;

  if (--buzzer_rem_ms == 0)
    digitalWrite(BUZZER, LOW);
}

static void start_buzzer(uint16_t interval_ms)
{
  buzzer_rem_ms = interval_ms;
  digitalWrite(BUZZER, HIGH);
}

static uint16_t led_state;

static void start_led(void)
{
  led_state = 30;
}

static inline void process_led(void)
{
  if (!led_state)
    return;

  switch (led_state) {
  case 30:
    digitalWrite(LED_BODY, HIGH);
  break;
  case 20:
    digitalWrite(LED_BODY, LOW);
  break;
  case 10:
    digitalWrite(LED_BODY, HIGH);
  break;
  case 1:
    digitalWrite(LED_BODY, LOW);
  break;
  }

  led_state--;
}

SIGNAL(TIMER0_COMPA_vect)
{
  process_buzzer();
  process_led();
  thumbstick();
}

static uint16_t powered_on = 1000;

void loop(void)
{
  /* Ignore the poweroff button just after it has been pressed to turn the power on */
  if (powered_on > 0) {
    powered_on--;
  } else {
    if (!digitalRead(POWER_OFF_BTN))
      poweroff();
  }

  /* If RPi is halted beep and cut the power off */
  if (digitalRead(PI_POWEROFF_SIGNAL))
    digitalWrite(POWER_MAIN, LOW);

  process_keys();

  /* Read temperature and battery voltage every few seconds */
  static uint16_t i = 30000;
  if (i++ >= 3000) {
      read_bat_temp();
      i = 0;
  }

  process_serial();
}
